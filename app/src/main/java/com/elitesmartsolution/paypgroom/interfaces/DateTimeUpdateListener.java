package com.elitesmartsolution.paypgroom.interfaces;

import android.widget.TextView;

public interface DateTimeUpdateListener {
    void onDateUpdate(String year, String monthOfYear, String dayOfMonth, TextView textView);

    void onTimeUpdate(int hourOfDay, int minute, int second, String am_pm, TextView textView);

    void onTimeUpdate(int hourOfDay, int minute, int second, TextView textView);
}
