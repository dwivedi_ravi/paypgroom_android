package com.elitesmartsolution.paypgroom.activities;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.adapters.AmenitiesAdapter;
import com.elitesmartsolution.paypgroom.adapters.MyCustomSpinnerAdapter;
import com.elitesmartsolution.paypgroom.database.DBHandler;
import com.elitesmartsolution.paypgroom.databinding.ActivityAddPgBinding;
import com.elitesmartsolution.paypgroom.interfaces.DateTimeUpdateListener;
import com.elitesmartsolution.paypgroom.models.PropertyListBO;
import com.elitesmartsolution.paypgroom.models.SpinnerItemBO;
import com.elitesmartsolution.paypgroom.models.add_pg.Location;
import com.elitesmartsolution.paypgroom.models.add_pg.PgDetail;
import com.elitesmartsolution.paypgroom.models.add_pg.PgImagesItem;
import com.elitesmartsolution.paypgroom.models.add_pg.PricingDetail;
import com.elitesmartsolution.paypgroom.models.add_pg.RequestAddPG;
import com.elitesmartsolution.paypgroom.models.add_pg.ResponseUploadPropertyImage;
import com.elitesmartsolution.paypgroom.models.add_pg.VisitScheduleDetail;
import com.elitesmartsolution.paypgroom.models.pgmetadata.AmenitiesItem;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.PermissionUtils;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.ViewAnimation;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.elitesmartsolution.paypgroom.widget.DateTimePicker;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class AddPGActivity extends SharedActivity implements View.OnTouchListener, DateTimeUpdateListener {

    private ActivityAddPgBinding binding;

    private ArrayList<AmenitiesItem> amenitiesItemList;
    private ArrayList<AmenitiesItem> propertyImagesListToSend;
    private ArrayList<ResponseUploadPropertyImage> propertyImagesUploadSuccesList;
    private String currentImageName = "";
    private RequestOptions requestOptions;
    private MyReceiver objMyReceiver;
    private PropertyListBO objPropertyListBO;
    private DBHandler dbHandler;
    private DateTimeUpdateListener updateListener;
    private String lattitude, longitude;
    // You can do the assignment inside onAttach or onCreate, i.e, before the activity is displayed
    ActivityResultLauncher<Intent> autoCompletePlacesActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    //  NOTE: THERE ARE NO REQUEST CODES WITH ACTIVITY RESULT LAUNCHER API.
                    //  Hence Defaults.AUTOCOMPLETE_REQUEST_CODE not required here.

                    Intent data = result.getData();

                    if (result.getResultCode() == RESULT_OK) {
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());

                        binding.contentAddPg.editSelectLocationFromMap.setText(place.getAddress());
                        binding.contentAddPg.editCountry.setText(Utility.getRequiredComponent(AddPGActivity.this, place, Defaults.COUNTRY_COMPONENT));
                        binding.contentAddPg.editState.setText(Utility.getRequiredComponent(AddPGActivity.this, place, Defaults.STATE_COMPONENT));
                        binding.contentAddPg.editCity.setText(Utility.getRequiredComponent(AddPGActivity.this, place, Defaults.CITY_COMPONENT));
                        binding.contentAddPg.editPinCode.setText(Utility.getRequiredComponent(AddPGActivity.this, place, Defaults.PINCODE_COMPONENT));
                        lattitude = place.getLatLng().latitude + "";
                        longitude = place.getLatLng().longitude + "";
                    } else if (result.getResultCode() == AutocompleteActivity.RESULT_ERROR) {
                        // TODO: Handle the error.
                        Status status = Autocomplete.getStatusFromIntent(data);
                        Log.i(TAG, status.getStatusMessage());
                    } else if (result.getResultCode() == RESULT_CANCELED) {
                        // The user canceled the operation.
                    }
                }
            });
    private FusedLocationProviderClient mFusedLocationClient;
    private ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri resultUri) {
            // Handle the returned Uri
            try {
                if (resultUri != null) {
                    try {
//                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
//                        Glide.with(AddPGActivity.this).setDefaultRequestOptions(requestOptions).load(bitmap)
//                                .into(binding.contentAddPg.imgToUpload);

                        Glide.with(AddPGActivity.this).setDefaultRequestOptions(requestOptions).load(resultUri)
                                .into(binding.contentAddPg.imgToUpload);

                        binding.contentAddPg.uploadProgress.setVisibility(View.VISIBLE);
                        binding.contentAddPg.imgCloudUpload.setVisibility(View.GONE);
                        currentImageName = Utility.getUniqueTransactionID();
                        ApiCallerUtility.callUploadPropertyImageApi(AddPGActivity.this, resultUri, currentImageName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });
    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            android.location.Location mLastLocation = locationResult.getLastLocation();
            lattitude = mLastLocation.getLatitude() + "";
            longitude = mLastLocation.getLongitude() + "";
            binding.contentAddPg.tvLattitude.setText("lat: " + lattitude);
            binding.contentAddPg.tvLongitude.setText("lat: " + longitude);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityAddPgBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();

        initToolbar();
        registerReciever();
        initComponent();
        setValuesFromDB();
        setListeners();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            objPropertyListBO = (PropertyListBO) bundle.getSerializable("currentPropertyListBO");
        }

        if (objPropertyListBO != null) {

        }
    }

    private void initToolbar() {
        setSupportActionBar(binding.appbarLayout.toolbar);
        Utility.setStatusBarColor(this);

        binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initComponent() {
        dbHandler = DBHandler.getInstance(this);
        updateListener = this;
        propertyImagesListToSend = new ArrayList<>();
        propertyImagesUploadSuccesList = new ArrayList<>();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        binding.contentAddPg.lytExpandAddLocation.setVisibility(View.GONE);
        binding.contentAddPg.lytExpandListBasics.setVisibility(View.GONE);
        binding.contentAddPg.lytExpandSelectAminities.setVisibility(View.GONE);
        binding.contentAddPg.lytExpandPricingTerms.setVisibility(View.GONE);
        binding.contentAddPg.lytExpandScheduleToVisit.setVisibility(View.GONE);
        binding.contentAddPg.lytExpandAddPhotos.setVisibility(View.GONE);

        binding.contentAddPg.editPhotoTitle.setText("Photo Title");
        binding.contentAddPg.editPhotoDescription.setText("Photo Description");
        binding.contentAddPg.editPhotoTitle.setVisibility(View.GONE);
        binding.contentAddPg.editPhotoDescription.setVisibility(View.GONE);
        binding.contentAddPg.btAddPhoto.setVisibility(View.GONE);

        binding.contentAddPg.editHouseCheckInTime.setEnabled(false);
        binding.contentAddPg.editHouseCheckoutTime.setEnabled(false);

        toggleCurrentSection(binding.contentAddPg.btToggleAddLocation, binding.contentAddPg.lytExpandAddLocation);
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder);
    }

    private void setValuesFromDB() {
        MyCustomSpinnerAdapter adapter = new MyCustomSpinnerAdapter(this, dbHandler.getSpinnerItems(this, "Select Property Type", DBHandler.property_types));
        binding.contentAddPg.spnPropertyType.setAdapter(adapter);
        binding.contentAddPg.spnPropertyType.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        SpinnerItemBO clickedItem = (SpinnerItemBO)
                                parent.getItemAtPosition(position);
                        String name = clickedItem.getTitleText();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        adapter = new MyCustomSpinnerAdapter(this, dbHandler.getSpinnerItems(this, "Select Room Type", DBHandler.room_types));
        binding.contentAddPg.spnRoomType.setAdapter(adapter);
        binding.contentAddPg.spnRoomType.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        SpinnerItemBO clickedItem = (SpinnerItemBO)
                                parent.getItemAtPosition(position);
                        String name = clickedItem.getTitleText();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        adapter = new MyCustomSpinnerAdapter(this, dbHandler.getSpinnerItems(this, "Select Luxury Type", DBHandler.luxury_type));
        binding.contentAddPg.spnLuxuryType.setAdapter(adapter);
        binding.contentAddPg.spnLuxuryType.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        SpinnerItemBO clickedItem = (SpinnerItemBO)
                                parent.getItemAtPosition(position);
                        String name = clickedItem.getTitleText();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

        amenitiesItemList = dbHandler.getAmenitiesListFromDB(this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        binding.contentAddPg.rcvAminities.setLayoutManager(gridLayoutManager);
        AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(this, amenitiesItemList, false, true);
        binding.contentAddPg.rcvAminities.setAdapter(amenitiesAdapter);
    }

    private void setListeners() {
        binding.contentAddPg.editSelectLocationFromMap.setOnTouchListener(this);
        binding.contentAddPg.editHouseCheckInTime.setOnTouchListener(this);
        binding.contentAddPg.editHouseCheckoutTime.setOnTouchListener(this);
        binding.contentAddPg.editVisitFromTime.setOnTouchListener(this);
        binding.contentAddPg.editVisitToTime.setOnTouchListener(this);

        binding.contentAddPg.btToggleAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCurrentSection(binding.contentAddPg.btToggleAddLocation, binding.contentAddPg.lytExpandAddLocation);
            }
        });

        binding.contentAddPg.btToggleListBasics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCurrentSection(binding.contentAddPg.btToggleListBasics, binding.contentAddPg.lytExpandListBasics);
            }
        });

        binding.contentAddPg.btToggleSelectAminities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCurrentSection(binding.contentAddPg.btToggleSelectAminities, binding.contentAddPg.lytExpandSelectAminities);
            }
        });

        binding.contentAddPg.btTogglePricingTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCurrentSection(binding.contentAddPg.btTogglePricingTerms, binding.contentAddPg.lytExpandPricingTerms);
            }
        });
        binding.contentAddPg.btToggleScheduleToVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCurrentSection(binding.contentAddPg.btToggleScheduleToVisit, binding.contentAddPg.lytExpandScheduleToVisit);
            }
        });

        binding.contentAddPg.btToggleAddPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleCurrentSection(binding.contentAddPg.btToggleAddPhotos, binding.contentAddPg.lytExpandAddPhotos);
            }
        });


        binding.contentAddPg.btToggleAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNextSection(binding.contentAddPg.btToggleAddLocation, binding.contentAddPg.lytExpandAddLocation, binding.contentAddPg.btToggleListBasics, binding.contentAddPg.lytExpandListBasics);
            }
        });

        binding.contentAddPg.btNextListBasics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNextSection(binding.contentAddPg.btToggleListBasics, binding.contentAddPg.lytExpandListBasics, binding.contentAddPg.btToggleSelectAminities, binding.contentAddPg.lytExpandSelectAminities);
            }
        });

        binding.contentAddPg.btNextSelectAminities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNextSection(binding.contentAddPg.btToggleSelectAminities, binding.contentAddPg.lytExpandSelectAminities, binding.contentAddPg.btTogglePricingTerms, binding.contentAddPg.lytExpandPricingTerms);
            }
        });

        binding.contentAddPg.btNextPricingTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNextSection(binding.contentAddPg.btTogglePricingTerms, binding.contentAddPg.lytExpandPricingTerms, binding.contentAddPg.btToggleScheduleToVisit, binding.contentAddPg.lytExpandScheduleToVisit);
            }
        });

        binding.contentAddPg.btNextScheduleToVisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNextSection(binding.contentAddPg.btToggleScheduleToVisit, binding.contentAddPg.lytExpandScheduleToVisit, binding.contentAddPg.btToggleAddPhotos, binding.contentAddPg.lytExpandAddPhotos);
            }
        });

        binding.contentAddPg.btAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isPropertyImageFormValid()) {
                    if (binding.contentAddPg.rcvPhotos.getAdapter() == null) {
                        GridLayoutManager linearLayoutManager = new GridLayoutManager(AddPGActivity.this, 2);
                        binding.contentAddPg.rcvPhotos.setLayoutManager(linearLayoutManager);
                        propertyImagesListToSend.add(getUploadedImageObject());
                        AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(AddPGActivity.this, propertyImagesListToSend, true, false);
                        binding.contentAddPg.rcvPhotos.setAdapter(amenitiesAdapter);
                    } else {
                        propertyImagesListToSend.add(getUploadedImageObject());
                        binding.contentAddPg.rcvPhotos.getAdapter().notifyItemInserted(propertyImagesListToSend.size() - 1);
                    }
                    binding.contentAddPg.imgToUpload.setImageResource(R.drawable.ic_buildings);
                    binding.contentAddPg.uploadProgress.setVisibility(View.GONE);
                    binding.contentAddPg.imgCloudUpload.setVisibility(View.VISIBLE);
                    binding.contentAddPg.editPhotoTitle.setText("");
                    binding.contentAddPg.editPhotoDescription.setText("");
                    currentImageName = "";
                }
            }
        });

        binding.contentAddPg.btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(binding.contentAddPg.btSave, "Details Saved Successfully", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                if (isFormValid()) {
                    RequestAddPG objRequestAddPG = new RequestAddPG();

                    objRequestAddPG.setOwnerId(Prefs.getUserId());

                    Location objLocation = new Location();
                    objLocation.setLine1(binding.contentAddPg.editUnitBuilding.getText().toString());
                    objLocation.setLine2(binding.contentAddPg.editSelectLocationFromMap.getText().toString());
                    objLocation.setPincode(binding.contentAddPg.editPinCode.getText().toString());
                    objLocation.setCity(binding.contentAddPg.editCity.getText().toString());
                    objLocation.setState(binding.contentAddPg.editState.getText().toString());
                    objLocation.setCountry(binding.contentAddPg.editCountry.getText().toString());
                    objLocation.setLatitude(lattitude);
                    objLocation.setLongitude(longitude);
                    objRequestAddPG.setLocation(objLocation);

                    PgDetail objPgDetail = new PgDetail();
                    objPgDetail.setPgName(binding.contentAddPg.editPropertyName.getText().toString());
                    objPgDetail.setPropertyTypeId(((SpinnerItemBO) binding.contentAddPg.spnPropertyType.getSelectedItem()).getId());
                    objPgDetail.setRoomTypeId(((SpinnerItemBO) binding.contentAddPg.spnRoomType.getSelectedItem()).getId());
                    objPgDetail.setSize(binding.contentAddPg.editPropertySize.getText().toString());
                    objPgDetail.setGuests(binding.contentAddPg.editGuests.getText().toString());
                    objPgDetail.setBathroom(binding.contentAddPg.editBathroom.getText().toString());
                    objPgDetail.setBedroom(binding.contentAddPg.editBedRoom.getText().toString());
                    objPgDetail.setBeds(binding.contentAddPg.editBed.getText().toString());
                    if (binding.contentAddPg.chkBoxFoodingStatus.isChecked())
                        objPgDetail.setIsFoodingAvailable("1");
                    else
                        objPgDetail.setIsFoodingAvailable("0");
                    objPgDetail.setDescription(binding.contentAddPg.editShortDescription.getText().toString());
                    objPgDetail.setOtherDetails(binding.contentAddPg.editLongDescription.getText().toString());
                    objPgDetail.setServiceCityCode(Prefs.getServiceCityCode());
                    objPgDetail.setLuxuryType(((SpinnerItemBO) binding.contentAddPg.spnLuxuryType.getSelectedItem()).getId());
                    objRequestAddPG.setPgDetail(objPgDetail);

                    List<String> amentiesListToSend = new ArrayList<>();
                    for (int i = 0; i < amenitiesItemList.size(); i++) {
                        if (amenitiesItemList.get(i).isChecked())
                            amentiesListToSend.add(amenitiesItemList.get(i).getId());
                    }
                    objRequestAddPG.setAmenities(amentiesListToSend);

                    PricingDetail objPricingDetail = new PricingDetail();
                    objPricingDetail.setPerMonth(binding.contentAddPg.editPerMonthPrice.getText().toString());
                    objPricingDetail.setSecurityDeposit(binding.contentAddPg.editUnitSecurityDeposit.getText().toString());
                    objPricingDetail.setCleaningFee(binding.contentAddPg.editCleaningFees.getText().toString());
                    objPricingDetail.setMinimumStayMonth(binding.contentAddPg.editMinimumStay.getText().toString());

                    if (binding.contentAddPg.rdBtnCheckInAnyTime.isChecked())
                        objPricingDetail.setCheckIn("Anytime");
                    else
                        objPricingDetail.setCheckIn(binding.contentAddPg.editHouseCheckInTime.getText().toString());

                    if (binding.contentAddPg.rdBtnCheckOutAnyTime.isChecked())
                        objPricingDetail.setCheckOut("Anytime");
                    else
                        objPricingDetail.setCheckOut(binding.contentAddPg.editHouseCheckoutTime.getText().toString());

                    if (binding.contentAddPg.rdBtnFlexible.isChecked())
                        objPricingDetail.setCancellationCharge("0");
                    if (binding.contentAddPg.rdBtnStrict.isChecked())
                        objPricingDetail.setCancellationCharge("500");
                    objRequestAddPG.setPricingDetail(objPricingDetail);

                    VisitScheduleDetail objVisitScheduleDetail = new VisitScheduleDetail();
                    objVisitScheduleDetail.setVisitFrom(binding.contentAddPg.editVisitFromTime.getText().toString());
                    objVisitScheduleDetail.setVisitTo(binding.contentAddPg.editVisitToTime.getText().toString());
                    objRequestAddPG.setVisitScheduleDetail(objVisitScheduleDetail);

                    List<PgImagesItem> objPgImagesItemList = new ArrayList<>();
                    for (int i = 0; i < propertyImagesListToSend.size(); i++) {
                        PgImagesItem objPgImagesItem = new PgImagesItem();
                        objPgImagesItem.setTitle("Photo Title");
                        objPgImagesItem.setDescription("Photo Description");
                        objPgImagesItem.setImage(propertyImagesListToSend.get(i).getId());

                        objPgImagesItemList.add(objPgImagesItem);
                    }
                    objRequestAddPG.setPgImages(objPgImagesItemList);

                    ApiCallerUtility.callAddPGApi(AddPGActivity.this, objRequestAddPG);
                }
            }
        });

        binding.contentAddPg.imgToUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askForReadStoragePermission();
            }
        });

        binding.contentAddPg.rdBtnCheckInAnyTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    binding.contentAddPg.editHouseCheckInTime.setEnabled(false);
                else
                    binding.contentAddPg.editHouseCheckInTime.setEnabled(true);
            }
        });

        binding.contentAddPg.rdBtnCheckOutAnyTime.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked)
                    binding.contentAddPg.editHouseCheckoutTime.setEnabled(false);
                else
                    binding.contentAddPg.editHouseCheckoutTime.setEnabled(true);
            }
        });

        binding.contentAddPg.rdBtnUseCurrentLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    binding.contentAddPg.editUnitBuilding.requestFocus();
                    binding.contentAddPg.containerLocationTextviews.setVisibility(View.VISIBLE);
                    binding.contentAddPg.editSelectLocationFromMap.setText("");
                    binding.contentAddPg.editUnitBuilding.setText("");
                    binding.contentAddPg.editCountry.setText("");
                    binding.contentAddPg.editState.setText("");
                    binding.contentAddPg.editCity.setText("");
                    binding.contentAddPg.editPinCode.setText("");
                } else {
                    binding.contentAddPg.containerLocationTextviews.setVisibility(View.GONE);
                }
            }
        });
    }

    private void toggleCurrentSection(View view, View viewToExpandCollapse) {
        boolean show = toggleArrow(view);
        if (show) {
            ViewAnimation.expand(viewToExpandCollapse, new ViewAnimation.AnimListener() {
                @Override
                public void onFinish() {
                }
            });
        } else {
            ViewAnimation.collapse(viewToExpandCollapse);
        }
    }

//    private void toggleSection(View view) {
//        boolean show = toggleArrow(view);
//        if (show) {
//            ViewAnimation.expand(binding.contentAddPg.lytExpandAddLocation, new ViewAnimation.AnimListener() {
//                @Override
//                public void onFinish() {
//                    Tools.nestedScrollTo(binding.contentAddPg.nestedScrollView, binding.contentAddPg.lytExpandAddLocation);
//                }
//            });
//        } else {
//            ViewAnimation.collapse(binding.contentAddPg.lytExpandAddLocation);
//        }
//    }

    private void toggleNextSection(View viewToCollapse, View layoutToCollapse, View viewToExpand, View layoutToExpand) {
        toggleArrow(viewToCollapse, false);
        ViewAnimation.collapse(layoutToCollapse);

        toggleArrow(viewToExpand, true);
        ViewAnimation.expand(layoutToExpand, new ViewAnimation.AnimListener() {
            @Override
            public void onFinish() {
            }
        });
    }

    public boolean toggleArrow(View view) {
        if (view.getRotation() == 0) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    public boolean toggleArrow(View view, boolean expand) {
        if (expand) {
            view.animate().setDuration(200).rotation(180);
            return true;
        } else {
            view.animate().setDuration(200).rotation(0);
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_SUCCESS);
        filter.addAction(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_FAILED);
        Utility.registerReciever(AddPGActivity.this, filter, objMyReceiver);
    }

    private boolean isFormValid() {
        if (binding.contentAddPg.editSelectLocationFromMap.getVisibility() == View.VISIBLE && !binding.contentAddPg.rdBtnUseCurrentLocation.isChecked() && binding.contentAddPg.editSelectLocationFromMap.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editSelectLocationFromMap, "Location field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editUnitBuilding.getVisibility() == View.VISIBLE && binding.contentAddPg.editUnitBuilding.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editUnitBuilding, "Unit, Building can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editCountry.getVisibility() == View.VISIBLE && binding.contentAddPg.editCountry.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editCountry, binding.contentAddPg.editCountry.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editState.getVisibility() == View.VISIBLE && binding.contentAddPg.editState.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editState, binding.contentAddPg.editState.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editCity.getVisibility() == View.VISIBLE && binding.contentAddPg.editCity.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editCity, binding.contentAddPg.editCity.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editPinCode.getVisibility() == View.VISIBLE && binding.contentAddPg.editPinCode.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editPinCode, binding.contentAddPg.editPinCode.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editPropertyName.getVisibility() == View.VISIBLE && binding.contentAddPg.editPropertyName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editPropertyName, binding.contentAddPg.editPropertyName.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.spnPropertyType.getVisibility() == View.VISIBLE && binding.contentAddPg.spnPropertyType.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentAddPg.spnPropertyType, "Please select id property type", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.spnRoomType.getVisibility() == View.VISIBLE && binding.contentAddPg.spnRoomType.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentAddPg.spnRoomType, "Please select id room type", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.spnLuxuryType.getVisibility() == View.VISIBLE && binding.contentAddPg.spnLuxuryType.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentAddPg.spnLuxuryType, "Please select id luxury type", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editPropertySize.getVisibility() == View.VISIBLE && binding.contentAddPg.editPropertySize.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editPropertySize, binding.contentAddPg.editPropertySize.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editGuests.getVisibility() == View.VISIBLE && binding.contentAddPg.editGuests.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editGuests, binding.contentAddPg.editGuests.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editBathroom.getVisibility() == View.VISIBLE && binding.contentAddPg.editBathroom.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editBathroom, binding.contentAddPg.editBathroom.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editBedRoom.getVisibility() == View.VISIBLE && binding.contentAddPg.editBedRoom.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editBedRoom, binding.contentAddPg.editBedRoom.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editBed.getVisibility() == View.VISIBLE && binding.contentAddPg.editBed.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editBed, binding.contentAddPg.editBed.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editShortDescription.getVisibility() == View.VISIBLE && binding.contentAddPg.editShortDescription.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editShortDescription, binding.contentAddPg.editShortDescription.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editLongDescription.getVisibility() == View.VISIBLE && binding.contentAddPg.editLongDescription.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editLongDescription, binding.contentAddPg.editLongDescription.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editPerMonthPrice.getVisibility() == View.VISIBLE && binding.contentAddPg.editPerMonthPrice.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editPerMonthPrice, binding.contentAddPg.editPerMonthPrice.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editUnitSecurityDeposit.getVisibility() == View.VISIBLE && binding.contentAddPg.editUnitSecurityDeposit.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editUnitSecurityDeposit, binding.contentAddPg.editUnitSecurityDeposit.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editCleaningFees.getVisibility() == View.VISIBLE && binding.contentAddPg.editCleaningFees.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editCleaningFees, binding.contentAddPg.editCleaningFees.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editMinimumStay.getVisibility() == View.VISIBLE && binding.contentAddPg.editMinimumStay.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editMinimumStay, binding.contentAddPg.editMinimumStay.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.rdBtnCheckInSpecificTime.isChecked() && binding.contentAddPg.editHouseCheckInTime.getVisibility() == View.VISIBLE && binding.contentAddPg.editHouseCheckInTime.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editHouseCheckInTime, binding.contentAddPg.editHouseCheckInTime.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.rdBtnCheckOutSpecificTime.isChecked() && binding.contentAddPg.editHouseCheckoutTime.getVisibility() == View.VISIBLE && binding.contentAddPg.editHouseCheckoutTime.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editHouseCheckoutTime, binding.contentAddPg.editHouseCheckoutTime.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editVisitFromTime.getVisibility() == View.VISIBLE && binding.contentAddPg.editVisitFromTime.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editVisitFromTime, binding.contentAddPg.editVisitFromTime.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editVisitToTime.getVisibility() == View.VISIBLE && binding.contentAddPg.editVisitToTime.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editVisitToTime, binding.contentAddPg.editVisitToTime.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
//      UPLOAD IMAGE SELECTION CHECK TO BE PERFORMED HERE.
        else if (propertyImagesListToSend.size() < Defaults.MINIMUM_PROPERTY_IMAGES_TO_UPLOAD) {
            Snackbar.make(binding.contentAddPg.editVisitToTime, "Please upload at least " + Defaults.MINIMUM_PROPERTY_IMAGES_TO_UPLOAD + " Property Images", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
//        AMENITIES SELECTION CHECK TO BE PERFORMED HERE.
        else if (!isAmenityChecked()) {
            Snackbar.make(binding.contentAddPg.editVisitToTime, "Please select at least 1 Amenity", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        /*if (requestCode == Defaults.AUTOCOMPLETE_REQUEST_CODE) {
//            if (resultCode == RESULT_OK) {
//                Place place = Autocomplete.getPlaceFromIntent(data);
//                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
//
//                binding.contentAddPg.editSelectLocationFromMap.setText(place.getAddress());
//                binding.contentAddPg.editCountry.setText(Utility.getRequiredComponent(AddPGActivity.this, place, Defaults.COUNTRY_COMPONENT));
//                binding.contentAddPg.editState.setText(Utility.getRequiredComponent(AddPGActivity.this, place, Defaults.STATE_COMPONENT));
//                binding.contentAddPg.editCity.setText(Utility.getRequiredComponent(AddPGActivity.this, place, Defaults.CITY_COMPONENT));
//                binding.contentAddPg.editPinCode.setText(Utility.getRequiredComponent(AddPGActivity.this, place, Defaults.PINCODE_COMPONENT));
//                lattitude = place.getLatLng().latitude + "";
//                longitude = place.getLatLng().longitude + "";
//            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
//                // TODO: Handle the error.
//                Status status = Autocomplete.getStatusFromIntent(data);
//                Log.i(TAG, status.getStatusMessage());
//            } else if (resultCode == RESULT_CANCELED) {
//                // The user canceled the operation.
//            }
//        } else */
//        if (resultCode == RESULT_OK) {
//            switch (requestCode) {
//                case CAMERA_REQUEST:
//                    Bitmap photo = (Bitmap) data.getExtras().get("data");
//
//                    Glide.with(AddPGActivity.this).setDefaultRequestOptions(requestOptions).load(photo)
//                            .into(binding.contentAddPg.imgToUpload);
//                    break;
//                case PICK_REQUEST:
//                    try {
//                        Uri uri = data.getData();
//
////                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
////                        Glide.with(AddPGActivity.this).setDefaultRequestOptions(requestOptions).load(bitmap)
////                                .into(binding.contentAddPg.imgToUpload);
//
//                        Glide.with(AddPGActivity.this).setDefaultRequestOptions(requestOptions).load(uri)
//                                .into(binding.contentAddPg.imgToUpload);
//
//                        binding.contentAddPg.uploadProgress.setVisibility(View.VISIBLE);
//                        binding.contentAddPg.imgCloudUpload.setVisibility(View.GONE);
//                        currentImageName = Utility.getUniqueTransactionID();
//                        ApiCallerUtility.callUploadPropertyImageApi(AddPGActivity.this, uri, currentImageName);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    break;
//            }
//        }
//
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    private boolean isPropertyImageFormValid() {
        if (currentImageName.isEmpty()) {
            Snackbar.make(binding.contentAddPg.editPhotoTitle, "Please select image to upload", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (!isPropertyImageUploaded(currentImageName)) {
            Snackbar.make(binding.contentAddPg.editPhotoTitle, "Image Upload Failed. Please select any other image to upload", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editPhotoTitle.getVisibility() == View.VISIBLE && binding.contentAddPg.editPhotoTitle.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editPhotoTitle, binding.contentAddPg.editPhotoTitle.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentAddPg.editPhotoDescription.getVisibility() == View.VISIBLE && binding.contentAddPg.editPhotoDescription.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentAddPg.editPhotoDescription, binding.contentAddPg.editPhotoDescription.getHint() + " field can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            switch (view.getId()) {
                case R.id.editSelectLocationFromMap:
                    binding.contentAddPg.rdBtnUseCurrentLocation.setChecked(false);
//                    Utility.launchAutocompletePlacesActivity(AddPGActivity.this, null);
                    Utility.launchAutocompletePlacesActivity(AddPGActivity.this, autoCompletePlacesActivityResultLauncher);
                    break;
                case R.id.editHouseCheckInTime:
                case R.id.editHouseCheckoutTime:
                case R.id.editVisitFromTime:
                case R.id.editVisitToTime:
                    getTime(view);
                    break;
            }
        }
        return true;
    }

    public void getTime(View view) {
        DateTimePicker.getInstance().getTime(AddPGActivity.this, updateListener, null, (TextView) view);
    }

    @Override
    public void onDateUpdate(String year, String monthOfYear, String dayOfMonth, TextView textView) {
        String date = year + "-" + (monthOfYear) + "-" + dayOfMonth;
//        String date = year + "-" + (monthOfYear) + "-" + dayOfMonth;
//        String date = dayOfMonth + "-" + (monthOfYear);

//        String date = (monthOfYear) + "-" + dayOfMonth;
        textView.setText(date);
    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, String am_pm, TextView textView) {
        String time = "";

        if (minute < 10)
            time = hourOfDay + ":0" + minute + " " + am_pm.toUpperCase();
        else
            time = hourOfDay + ":" + minute + " " + am_pm.toUpperCase();

        textView.setText(time);
    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, TextView textView) {

    }

    private AmenitiesItem getUploadedImageObject() {
        AmenitiesItem objAmenitiesItem = new AmenitiesItem();
        for (int i = 0; i < propertyImagesUploadSuccesList.size(); i++) {
            if (currentImageName.equalsIgnoreCase(propertyImagesUploadSuccesList.get(i).getDataUploadImageResponse().getUploadedImageName())) {
//                objAmenitiesItem.setName(binding.contentAddPg.editPhotoTitle.getText().toString());
//                objAmenitiesItem.setDescription(binding.contentAddPg.editPhotoDescription.getText().toString());
                objAmenitiesItem.setName("");
                objAmenitiesItem.setDescription("");
                objAmenitiesItem.setIcon(propertyImagesUploadSuccesList.get(i).getDataUploadImageResponse().getUplodedImageUrl());
                objAmenitiesItem.setId(propertyImagesUploadSuccesList.get(i).getDataUploadImageResponse().getUploadedImageName());
                break;
            }
        }
        return objAmenitiesItem;
    }

    private boolean isPropertyImageUploaded(String currentImageName) {
        try {
            for (int i = 0; i < propertyImagesUploadSuccesList.size(); i++) {
                if (currentImageName.equalsIgnoreCase(propertyImagesUploadSuccesList.get(i).getDataUploadImageResponse().getUploadedImageName())) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean isAmenityChecked() {
        for (int i = 0; i < amenitiesItemList.size(); i++) {
            if (amenitiesItemList.get(i).isChecked())
                return true;
        }
        return false;
    }

    @Override
    public void gotReadStoragePermission() {
        mGetContent.launch("image/*");
    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {
        if (isPermissionGranted) {
            getLastLocation();
        } else {
            Toast.makeText(AddPGActivity.this, getResources().getString(R.string.location_permission), Toast.LENGTH_SHORT).show();
        }
    }

    //**************************************************************************************************

    @Override
    protected void onResume() {
        super.onResume();
        askForLocationPermission();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        if (PermissionUtils.checkForPassedPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) || PermissionUtils.checkForPassedPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            if (mFusedLocationClient != null)
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (isLocationEnabled()) {
            mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<android.location.Location>() {
                @Override
                public void onComplete(@NonNull Task<android.location.Location> task) {
                    android.location.Location location = task.getResult();
                    if (location == null) {
                        requestNewLocationData();
                    } else {
                        lattitude = location.getLatitude() + "";
                        longitude = location.getLongitude() + "";
                        binding.contentAddPg.tvLattitude.setText("lat: " + lattitude);
                        binding.contentAddPg.tvLongitude.setText("lat: " + longitude);
                    }
                }
            });
        } else {
            Toast.makeText(this, "Please turn on" + " your location...", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
    }

    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        // Initializing LocationRequest
        // object with appropriate methods
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(500);
        mLocationRequest.setFastestInterval(250);
        mLocationRequest.setMaxWaitTime(2000);
        mLocationRequest.setNumUpdates(2);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_BROADCAST_SELECTED_CONTACT_LIST)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_SUCCESS)) {
                binding.contentAddPg.uploadProgress.setVisibility(View.GONE);
                Toast.makeText(context, "Property Image Uploaded Successfully", Toast.LENGTH_LONG).show();
                if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                    Bundle bundle = intent.getExtras().getBundle("bundle");
                    ResponseUploadPropertyImage responseUploadPropertyImage = (ResponseUploadPropertyImage) bundle.getSerializable("ResponseUploadPropertyImage");
                    if (responseUploadPropertyImage != null) {
                        propertyImagesUploadSuccesList.add(responseUploadPropertyImage);

                        if (isPropertyImageFormValid()) {
                            if (binding.contentAddPg.rcvPhotos.getAdapter() == null) {
                                GridLayoutManager linearLayoutManager = new GridLayoutManager(AddPGActivity.this, 2);
                                binding.contentAddPg.rcvPhotos.setLayoutManager(linearLayoutManager);
                                propertyImagesListToSend.add(getUploadedImageObject());
                                AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(AddPGActivity.this, propertyImagesListToSend, true, false);
                                binding.contentAddPg.rcvPhotos.setAdapter(amenitiesAdapter);
                            } else {
                                propertyImagesListToSend.add(getUploadedImageObject());
                                binding.contentAddPg.rcvPhotos.getAdapter().notifyItemInserted(propertyImagesListToSend.size() - 1);
                            }
                            binding.contentAddPg.imgToUpload.setImageResource(R.drawable.ic_buildings);
                            binding.contentAddPg.uploadProgress.setVisibility(View.GONE);
                            binding.contentAddPg.imgCloudUpload.setVisibility(View.VISIBLE);
//                            binding.contentAddPg.editPhotoTitle.setText("");
//                            binding.contentAddPg.editPhotoDescription.setText("");
                            currentImageName = "";
                        }
                    }
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_FAILED)) {
                binding.contentAddPg.uploadProgress.setVisibility(View.GONE);
                binding.contentAddPg.imgCloudUpload.setVisibility(View.VISIBLE);
                boolean sizeExceeded = false;
                if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                    Bundle bundle = intent.getExtras().getBundle("bundle");
                    sizeExceeded = bundle.getBoolean("sizeExceeded", false);
                }
                if (!sizeExceeded)
                    Toast.makeText(context, "Property Image Upload Failed", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(context, "Upload Failed. Please upload image less than 2MB.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
