package com.elitesmartsolution.paypgroom.activities.homescreen.mypg;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.PropertyListingActivity;
import com.elitesmartsolution.paypgroom.activities.StaticContentActivity;
import com.elitesmartsolution.paypgroom.adapters.PropertyListAdapterActual;
import com.elitesmartsolution.paypgroom.databinding.FragmentMyPgsBinding;
import com.elitesmartsolution.paypgroom.fragments.SelectServiceCityFragment;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetPgListing;
import com.elitesmartsolution.paypgroom.util.SpacesItemDecoration;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Keys;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

public class MyPGsFragment extends Fragment implements View.OnClickListener {

    private FragmentMyPgsBinding binding;
    private MyPGsViewModel myPGsViewModel;

    private SelectServiceCityFragment selectServiceCityFragment;
    private MyReceiver objMyReceiver;
    private Runnable runnableAvailablePgs = null;
    private Handler handlerAvailablePgs = null;
    private Runnable runnableRecommendedPgs = null;
    private Handler handlerRecommendedPgs = null;
    private int availablePgPosition = 0;
    private int recommendedPgPosition = 0;
    private boolean isSearchBarHide = false;
    private boolean isShowingDialog = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        myPGsViewModel =
                new ViewModelProvider(requireActivity()).get(MyPGsViewModel.class);

        binding = FragmentMyPgsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        binding.btnRentedProperty.setVisibility(View.GONE);
        binding.btnBuyProperty.setVisibility(View.GONE);
        binding.btnSellProperty.setVisibility(View.GONE);

        binding.btnChangeLocation.setText(Prefs.getServiceCityName());
        setPropertyAvailabilityTypeButtonsState(Prefs.getStringValue(Keys.PREF_SELECTED_SERVICE_TYPE, Defaults.SERVICE_TYPE_RENT), false);

        setListenersAndObservers();
        objMyReceiver = new MyReceiver();
        registerReciever();

        return root;
    }

    private void setListenersAndObservers() {
        try {
            myPGsViewModel.getProgressBarStatus().observe(getViewLifecycleOwner(), aBoolean -> {
                if (aBoolean)
                    binding.progressBar.setVisibility(View.VISIBLE);
                else
                    binding.progressBar.setVisibility(View.GONE);
            });

            myPGsViewModel.getPropertyList().observe(getViewLifecycleOwner(), mPropertyList -> {
                try {
                    if (binding.mRecyclerViewAvailablePGs != null && binding.mRecyclerViewAvailablePGs.getAdapter() == null) {
                        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        binding.mRecyclerViewAvailablePGs.setLayoutManager(mLayoutManager);

//                        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(getActivity(),
//                                mLayoutManager.getOrientation());
//                        mDividerItemDecoration.setDrawable();
//                        binding.mRecyclerViewAvailablePGs.addItemDecoration(mDividerItemDecoration);

                        SpacesItemDecoration mSpacesItemDecoration = new SpacesItemDecoration((int) getResources().getDimension(R.dimen.dimen_10dp),
                                true);
                        binding.mRecyclerViewAvailablePGs.addItemDecoration(mSpacesItemDecoration);

                        PropertyListAdapterActual mPropertyListAdapter = new PropertyListAdapterActual(getActivity(), mPropertyList, Defaults.SCREEN_TYPE_PAY_PG_HOME_SCREEN, true);
                        binding.mRecyclerViewAvailablePGs.setAdapter(mPropertyListAdapter);

                        binding.mRecyclerViewAvailablePGs.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                stopAutoSliderAvailablePgs();
                                return false;
                            }
                        });

                        startAutoSliderAvailablePgs();
                    } else if (binding.mRecyclerViewAvailablePGs != null) {
                        ((PropertyListAdapterActual) binding.mRecyclerViewAvailablePGs.getAdapter()).refreshList(mPropertyList);
                        binding.mRecyclerViewAvailablePGs.getAdapter().notifyDataSetChanged();
                    }
                } catch (Exception ex) {

                }
            });

            myPGsViewModel.getPropertyList2().observe(getViewLifecycleOwner(), mPropertyList -> {
                try {
                    if (binding.mRecyclerViewRecommendedPGs != null && binding.mRecyclerViewRecommendedPGs.getAdapter() == null) {
                        binding.mRecyclerViewRecommendedPGs.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

                        SpacesItemDecoration mSpacesItemDecoration = new SpacesItemDecoration((int) getResources().getDimension(R.dimen.dimen_10dp),
                                true);
                        binding.mRecyclerViewRecommendedPGs.addItemDecoration(mSpacesItemDecoration);

                        PropertyListAdapterActual mPropertyListAdapter = new PropertyListAdapterActual(getActivity(), mPropertyList, Defaults.SCREEN_TYPE_PAY_PG_HOME_SCREEN, true);
                        binding.mRecyclerViewRecommendedPGs.setAdapter(mPropertyListAdapter);

                        binding.mRecyclerViewRecommendedPGs.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                stopAutoSliderRecommendedPgs();
                                return false;
                            }
                        });

                        startAutoSliderRecommendedPgs();
                    } else if (binding.mRecyclerViewRecommendedPGs != null) {
                        ((PropertyListAdapterActual) binding.mRecyclerViewRecommendedPGs.getAdapter()).refreshList(mPropertyList);
                        binding.mRecyclerViewRecommendedPGs.getAdapter().notifyDataSetChanged();
                    }
                } catch (Exception ex) {

                }
            });

//            mRecyclerView.addOnScrollListener(new RecyclerViewHideShowScrollListener() {
//                @Override
//                public void onHide() {
//                    animateSearchBar(true);
//                }
//
//                @Override
//                public void onShow() {
//                    animateSearchBar(false);
//                }
//            });

            binding.searchBar.btMenu.setOnClickListener(this);
            binding.btnFullyFurnishedRooms.setOnClickListener(this);
            binding.btnRoomPlusAccomodations.setOnClickListener(this);
            binding.btnScheduleToVisit.setOnClickListener(this);
            binding.btnRefundableSecurity.setOnClickListener(this);
            binding.btnChangeLocation.setOnClickListener(this);
            binding.btnRentedProperty.setOnClickListener(this);
            binding.btnBuyProperty.setOnClickListener(this);
            binding.btnSellProperty.setOnClickListener(this);

            binding.searchBar.searchBarCardView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        Intent intent = new Intent(getContext(), PropertyListingActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("toolTitle", getResources().getString(R.string.lbl_search));
                        bundle.putBoolean("isSearchActivity", true);

                        RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
                        objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
                        bundle.putSerializable("RequestGetPgListing", objRequestGetPgListing);

                        intent.putExtra("bundle", bundle);
                        startActivity(intent);
                    }
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void animateSearchBar(final boolean hide) {
        try {
            if (isSearchBarHide && hide || !isSearchBarHide && !hide) return;
            isSearchBarHide = hide;
            int moveY = hide ? -(2 * binding.searchBar.searchBarCardView.getHeight()) : 0;
            binding.searchBar.searchBarCardView.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_menu:
                break;
            case R.id.btnFullyFurnishedRooms: {
                Intent intent = new Intent(getContext(), PropertyListingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("toolTitle", getResources().getString(R.string.title_fully_furnished_room));

                RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();

                objRequestGetPgListing.setLuxryType(Defaults.FULLY_FURNISHED);
                objRequestGetPgListing.setIsFoodingAvailable(Defaults.FOODING_NOT_AVAILABLE);
                objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
                bundle.putSerializable("RequestGetPgListing", objRequestGetPgListing);

                intent.putExtra("bundle", bundle);
                startActivity(intent);
            }
            break;
            case R.id.btnRoomPlusAccomodations: {
                Intent intent = new Intent(getContext(), PropertyListingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("toolTitle", getResources().getString(R.string.title_room_plus_accomodation));

                RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
                objRequestGetPgListing.setIsFoodingAvailable(Defaults.FOODING_AVAILABLE);
                objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
                bundle.putSerializable("RequestGetPgListing", objRequestGetPgListing);

                intent.putExtra("bundle", bundle);
                startActivity(intent);
            }
            break;
            case R.id.btnScheduleToVisit: {
                Intent intent = new Intent(getContext(), StaticContentActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("toolTitle", getResources().getString(R.string.title_schedule_to_visit));
                bundle.putString("toolShortDescription", getResources().getString(R.string.tool_short_description_schedule_to_visit));
                bundle.putString("toolLongDescription", getResources().getString(R.string.tool_long_description_schedule_to_visit));
                intent.putExtra("bundle", bundle);
                startActivity(intent);
            }
            break;
            case R.id.btnRefundableSecurity: {
                Intent intent = new Intent(getContext(), StaticContentActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("toolTitle", getResources().getString(R.string.title_refundable_security));
                bundle.putString("toolShortDescription", getResources().getString(R.string.tool_short_description_refundable_security));
                bundle.putString("toolLongDescription", getResources().getString(R.string.tool_long_description_refundable_security));
                intent.putExtra("bundle", bundle);
                startActivity(intent);
            }
            break;
            case R.id.btnChangeLocation: {
//                showDialogChangeCity();
            }
            break;
            case R.id.btnRentedProperty: {
//                setPropertyAvailabilityTypeButtonsState(Defaults.SERVICE_TYPE_RENT, true);
            }
            break;
            case R.id.btnBuyProperty: {
//                setPropertyAvailabilityTypeButtonsState(Defaults.SERVICE_TYPE_BUY, true);
            }
            break;
            case R.id.btnSellProperty: {
//                showDialogChangeCity();
            }
            break;
            default:
                break;
        }
    }

    private void showDialogChangeCity() {
        isShowingDialog = true;
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        selectServiceCityFragment = new SelectServiceCityFragment(getActivity());
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, selectServiceCityFragment).addToBackStack(null).commit();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_SERVICE_CITY_CHANGED);
        Utility.registerReciever(getActivity(), filter, objMyReceiver);
    }

    @Override
    public void onDestroy() {
        Utility.unRegisterReciever(getActivity(), objMyReceiver);
        super.onDestroy();
    }

    private void setPropertyAvailabilityTypeButtonsState(String serviceTypeSelected, boolean toggle) {

        if (serviceTypeSelected.equals(Defaults.SERVICE_TYPE_RENT)) {
            binding.btnRentedProperty.setBackgroundResource(R.drawable.rounded_corner_button_primary_color);
            binding.btnRentedProperty.setCompoundDrawablesRelativeWithIntrinsicBounds(null, getActivity().getResources().getDrawable(R.drawable.icon_rent_white), null, null);
            binding.btnRentedProperty.setTextColor(getActivity().getResources().getColor(R.color.white));
            binding.txtAvailable.setText(getActivity().getResources().getString(R.string.lbl_avlbl_on_rent));

            if (toggle) {
                binding.btnBuyProperty.setBackgroundResource(R.drawable.rounded_corner_button_white_color);
                binding.btnBuyProperty.setCompoundDrawablesRelativeWithIntrinsicBounds(null, getActivity().getResources().getDrawable(R.drawable.icon_buy), null, null);
                binding.btnBuyProperty.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));

                Prefs.setStringValue(Keys.PREF_SELECTED_SERVICE_TYPE, Defaults.SERVICE_TYPE_RENT);
                Utility.sendUpdateListBroadCast(Defaults.ACTION_SERVICE_CITY_CHANGED, getContext(), null);
            }
        } else if (serviceTypeSelected.equals(Defaults.SERVICE_TYPE_BUY)) {
            binding.btnBuyProperty.setBackgroundResource(R.drawable.rounded_corner_button_primary_color);
            binding.btnBuyProperty.setCompoundDrawablesRelativeWithIntrinsicBounds(null, getActivity().getResources().getDrawable(R.drawable.icon_buy_white), null, null);
            binding.btnBuyProperty.setTextColor(getActivity().getResources().getColor(R.color.white));
            binding.txtAvailable.setText(getActivity().getResources().getString(R.string.lbl_avlbl_for_purchase));

            if (toggle) {
                binding.btnRentedProperty.setBackgroundResource(R.drawable.rounded_corner_button_white_color);
                binding.btnRentedProperty.setCompoundDrawablesRelativeWithIntrinsicBounds(null, getActivity().getResources().getDrawable(R.drawable.icon_rent), null, null);
                binding.btnRentedProperty.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));

                Prefs.setStringValue(Keys.PREF_SELECTED_SERVICE_TYPE, Defaults.SERVICE_TYPE_BUY);
                Utility.sendUpdateListBroadCast(Defaults.ACTION_SERVICE_CITY_CHANGED, getContext(), null);
            }
        }
    }

    private void startAutoSliderAvailablePgs() {
        try {
            if (runnableAvailablePgs != null) {
                runnableAvailablePgs = null;
                handlerAvailablePgs = null;
            }
            if (binding.mRecyclerViewAvailablePGs.getAdapter() != null && binding.mRecyclerViewAvailablePGs.getAdapter().getItemCount() > 0) {
                handlerAvailablePgs = new Handler();
                runnableAvailablePgs = () -> {
                    if (handlerAvailablePgs != null && runnableAvailablePgs != null) {
                        availablePgPosition++;
                        if (availablePgPosition >= binding.mRecyclerViewAvailablePGs.getAdapter().getItemCount()) {
                            availablePgPosition = 0;
                        }
                        binding.mRecyclerViewAvailablePGs.smoothScrollToPosition(availablePgPosition);
                        handlerAvailablePgs.postDelayed(runnableAvailablePgs, 3000);
                    }
                };
                handlerAvailablePgs.postDelayed(runnableAvailablePgs, 3000);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void startAutoSliderRecommendedPgs() {
        try {
            if (runnableRecommendedPgs != null) {
                runnableRecommendedPgs = null;
                handlerRecommendedPgs = null;
            }
            if (binding.mRecyclerViewRecommendedPGs.getAdapter() != null && binding.mRecyclerViewRecommendedPGs.getAdapter().getItemCount() > 0) {
                handlerRecommendedPgs = new Handler();
                runnableRecommendedPgs = () -> {
                    if (handlerRecommendedPgs != null && runnableRecommendedPgs != null) {
                        recommendedPgPosition++;
                        if (recommendedPgPosition >= binding.mRecyclerViewRecommendedPGs.getAdapter().getItemCount()) {
                            recommendedPgPosition = 0;
                        }
                        binding.mRecyclerViewRecommendedPGs.smoothScrollToPosition(recommendedPgPosition);
                        handlerRecommendedPgs.postDelayed(runnableRecommendedPgs, 3000);
                    }
                };
                handlerRecommendedPgs.postDelayed(runnableRecommendedPgs, 3000);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void stopAutoSliderAvailablePgs() {
        try {
            handlerAvailablePgs.removeCallbacks(runnableAvailablePgs);
            runnableAvailablePgs = null;
            handlerAvailablePgs = null;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void stopAutoSliderRecommendedPgs() {
        try {
            handlerRecommendedPgs.removeCallbacks(runnableRecommendedPgs);
            runnableRecommendedPgs = null;
            handlerRecommendedPgs = null;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopAutoSliderAvailablePgs();
        stopAutoSliderRecommendedPgs();
    }

    @Override
    public void onResume() {
        super.onResume();
//        startAutoSliderAvailablePgs();
//        startAutoSliderRecommendedPgs();
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_SERVICE_CITY_CHANGED)) {
                try {
                    if (selectServiceCityFragment != null) {
                        selectServiceCityFragment.dismiss();
                    }
                    binding.btnChangeLocation.setText(Prefs.getServiceCityName());
                    myPGsViewModel.refreshPropertyList();

//                    final Bundle bundle = intent.getExtras().getBundle("bundle");
//                    if (bundle != null) {
//                        ResponseVerifyOTP objResponseVerifyOTP = (ResponseVerifyOTP) bundle.getSerializable("ResponseVerifyOTP");
//                        if (objResponseVerifyOTP != null) {
//                            if (objResponseVerifyOTP.getMessage().equals(Defaults.FAVORITE_STATUS_SELECTED)) {
//                            } else if (objResponseVerifyOTP.getMessage().equals(Defaults.FAVORITE_STATUS_UN_SELECTED)) {
//                            }
//                        }
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
