package com.elitesmartsolution.paypgroom.activities.homescreen.notifications;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.adapters.NotificationItemRecyclerViewAdapter;
import com.elitesmartsolution.paypgroom.databinding.FragmentNotificationsBinding;

public class NotificationsFragment extends Fragment {

    private FragmentNotificationsBinding binding;
    private NotificationsViewModel notificationsViewModel;

//    private RecyclerView binding.mRecyclerView;
//    private ProgressBar binding.progressBar;
//    private TextView txtNoNotifications;

    private NotificationItemRecyclerViewAdapter notificationItemRecyclerViewAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                new ViewModelProvider(requireActivity()).get(NotificationsViewModel.class);

        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        setListenersAndObservers();
        return root;
    }

    private void setListenersAndObservers() {
        try {
//            notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//                @Override
//                public void onChanged(@Nullable String s) {
//                    binding.txtNoNotifications.setText(s);
//                }
//            });

            notificationsViewModel.getProgressBarStatus().observe(getViewLifecycleOwner(), aBoolean -> {
                if (aBoolean)
                    binding.progressBar.setVisibility(View.VISIBLE);
                else
                    binding.progressBar.setVisibility(View.GONE);
            });

            notificationsViewModel.getNotifications().observe(getViewLifecycleOwner(), notificationsItems -> {
                try {
                    if (binding.mRecyclerView != null && binding.mRecyclerView.getAdapter() == null) {
                        binding.mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        NotificationItemRecyclerViewAdapter notificationItemRecyclerViewAdapter = new NotificationItemRecyclerViewAdapter(getActivity(), notificationsItems);
//                        notificationItemRecyclerViewAdapter.sort();
                        binding.mRecyclerView.setAdapter(notificationItemRecyclerViewAdapter);
                    } else if (binding.mRecyclerView != null) {
                        notificationItemRecyclerViewAdapter.sort();
                        binding.mRecyclerView.getAdapter().notifyDataSetChanged();
                    }
                    if (notificationsItems == null || notificationsItems.size() == 0)
                        binding.txtNoNotifications.setVisibility(View.VISIBLE);
                    else
                        binding.txtNoNotifications.setVisibility(View.GONE);
                } catch (Exception ex) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
