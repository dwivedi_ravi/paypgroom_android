package com.elitesmartsolution.paypgroom.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.coupons.CouponsFragment;
import com.elitesmartsolution.paypgroom.databinding.ActivityViewCouponCodesBinding;

public class ViewCouponCodesActivity extends AppCompatActivity {

    private ActivityViewCouponCodesBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityViewCouponCodesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        CouponsFragment couponsFragment = new CouponsFragment();
        transaction.replace(R.id.fragment_container, couponsFragment, getResources().getString(R.string.lbl_menu_coupons));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}