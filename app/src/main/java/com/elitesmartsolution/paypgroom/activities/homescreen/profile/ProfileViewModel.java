package com.elitesmartsolution.paypgroom.activities.homescreen.profile;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.models.profile.UserProfile;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

import java.util.List;

//    Caution: A ViewModel must never reference a view, Lifecycle, or any class that may hold a reference to the activity context.
public class ProfileViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<List<UserProfile>> mUsers;

    public ProfileViewModel(Application context) {
        super(context);
        mText = new MutableLiveData<>();
        if (Prefs.getUserName() == null || Prefs.getUserName().contains("null"))
            mText.setValue(context.getResources().getString(R.string.your_name));
        else
            mText.setValue(Prefs.getUserName());
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<List<UserProfile>> getUsers() {
        if (mUsers == null) {
            mUsers = new MutableLiveData<List<UserProfile>>();
            loadUsers();
        }
        return mUsers;
    }

    private void loadUsers() {
        // Do an asynchronous operation to fetch users.
    }
}