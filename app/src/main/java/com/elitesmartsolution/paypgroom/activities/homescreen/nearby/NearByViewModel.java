package com.elitesmartsolution.paypgroom.activities.homescreen.nearby;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.elitesmartsolution.paypgroom.models.pglisting.PropertyListItem;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetPgListing;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;

import java.util.List;

public class NearByViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<Boolean> mShowProgressBar;
    private MutableLiveData<List<PropertyListItem>> mPropertyList;
    private Context context;

    public NearByViewModel(Application context) {
        super(context);
        this.context = context;
        mText = new MutableLiveData<>();
        mText.setValue("This is NearBy fragment");
        mShowProgressBar = new MutableLiveData<>();
        mShowProgressBar.setValue(false);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<Boolean> getProgressBarStatus() {
        return mShowProgressBar;
    }

    public LiveData<List<PropertyListItem>> getPropertyList() {
        if (mPropertyList == null) {
            mPropertyList = new MutableLiveData<>();
        }
        return mPropertyList;
    }

    public void callListingApi(RequestGetPgListing objRequestGetPgListing) {
        ApiCallerUtility.callGetPgListingApi(context, mPropertyList, mShowProgressBar, objRequestGetPgListing);
    }

//    public LiveData<List<PropertyListBO>> getPropertyList() {
//        if (mPropertyList == null) {
//            mPropertyList = new MutableLiveData<>();
//            loadPropertyList();
//        }
//        return mPropertyList;
//    }
//
//    private void loadPropertyList() {
//        // Do an asynchronous operation to fetch PropertyListBOs.
//        List<PropertyListBO> PropertyListBOList = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//            PropertyListBO objPropertyListBO = new PropertyListBO();
//            if (i % 2 == 0) {
//                objPropertyListBO.setName("Surat Based Pg Located In Gujarat");
//            } else {
//                objPropertyListBO.setName("Delhi Based Pg");
//            }
//            objPropertyListBO.setAbout(context.getResources().getString(R.string.middle_lorem_ipsum));
//
//            List mImageList = new ArrayList<PropertyImagesBO>();
//            for (int j = 0; j < 4; j++) {
//                PropertyImagesBO propertyImagesBO = new PropertyImagesBO();
//                String id = i + "" + j;
//                propertyImagesBO.setId(id);
//                propertyImagesBO.setContent("Image Name " + id);
//                propertyImagesBO.setDetails("Image Details " + id);
//                propertyImagesBO.setPhotoUrl("Image Url " + id);
//                if (i % 2 == 0 && j % 2 == 0)
//                    propertyImagesBO.setImageResourceId(R.drawable.list_image_1);
//                else
//                    propertyImagesBO.setImageResourceId(R.drawable.list_image_2);
//                mImageList.add(propertyImagesBO);
//            }
//            objPropertyListBO.setmPropertyImagesList(mImageList);
//
//            PropertyListBOList.add(objPropertyListBO);
//        }
//        mPropertyList.setValue(PropertyListBOList);
//    }
}