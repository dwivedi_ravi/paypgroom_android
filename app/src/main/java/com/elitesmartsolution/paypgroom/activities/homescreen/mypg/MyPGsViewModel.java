package com.elitesmartsolution.paypgroom.activities.homescreen.mypg;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.elitesmartsolution.paypgroom.models.pglisting.PropertyListItem;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetPgListing;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

import java.util.List;

public class MyPGsViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<Boolean> mShowProgressBar;
    private MutableLiveData<List<PropertyListItem>> mPropertyList;
    private MutableLiveData<List<PropertyListItem>> mPropertyList2;
    private Context context;

    public MyPGsViewModel(Application context) {
        super(context);
        this.context = context;
        mText = new MutableLiveData<>();
        mText.setValue("This is MyPgs fragment");
        mShowProgressBar = new MutableLiveData<>();
        mShowProgressBar.setValue(false);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<Boolean> getProgressBarStatus() {
        return mShowProgressBar;
    }

    public LiveData<List<PropertyListItem>> getPropertyList() {
        if (mPropertyList == null) {
            mPropertyList = new MutableLiveData<>();
            RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
            objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
            ApiCallerUtility.callGetPgListingApi(context, mPropertyList, mShowProgressBar, objRequestGetPgListing);
        }
        return mPropertyList;
    }

    public LiveData<List<PropertyListItem>> getPropertyList2() {
        if (mPropertyList2 == null) {
            mPropertyList2 = new MutableLiveData<>();
            RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
            objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
            objRequestGetPgListing.setIsRecommended("1");
            ApiCallerUtility.callGetPgListingApi(context, mPropertyList2, mShowProgressBar, objRequestGetPgListing);
        }
        return mPropertyList2;
    }

    public void refreshPropertyList() {
        if (mPropertyList != null) {
            RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
            objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
            ApiCallerUtility.callGetPgListingApi(context, mPropertyList, mShowProgressBar, objRequestGetPgListing);
        }
        if (mPropertyList2 != null) {
            RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
            objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
            objRequestGetPgListing.setIsRecommended("1");
            ApiCallerUtility.callGetPgListingApi(context, mPropertyList2, mShowProgressBar, objRequestGetPgListing);
        }
    }
}