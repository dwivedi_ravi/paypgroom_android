package com.elitesmartsolution.paypgroom.activities.coupons;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.adapters.CouponsAdapter;
import com.elitesmartsolution.paypgroom.databinding.FragmentCouponsBinding;

public class CouponsFragment extends Fragment {

    private FragmentCouponsBinding binding;

    private CouponsViewModel couponsViewModel;
    private CouponsAdapter couponsAdapter;

//    ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
//            new ActivityResultCallback<Uri>() {
//                @Override
//                public void onActivityResult(Uri uri) {
//                    // Handle the returned Uri
//                }
//            });
//
//    ActivityResultLauncher<Intent> mStartActivityForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
//            new ActivityResultCallback<ActivityResult>() {
//                @Override
//                public void onActivityResult(ActivityResult result) {
//                    if (result.getResultCode() == RESULT_OK) {
//                        result.getData();
//                    }
//                }
//            });
//
//    private ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
//        @Override
//        public void onActivityResult(Uri resultUri) {
//            // Handle the returned Uri
//            try {
//                if (resultUri != null) {
//                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultUri);
//                    Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load(bitmap)
//                            .into(binding.imgProfile);
//
//                    binding.uploadProgress.setVisibility(View.VISIBLE);
//                    binding.imgProfile.setClickable(false);
//
//                    ApiCallerUtility.callGenericImageUploadApi(getActivity(), resultUri, Defaults.ACTION_CUSTOMER_LOGO_UPLOAD_SUCCESS, Defaults.ACTION_CUSTOMER_LOGO_UPLOAD_FAILED, Defaults.TYPE_CUSTOMER_IMAGE_UPLOAD, recordId);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    });
//    private ActivityResultLauncher mRequestMultiplePermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
//        @Override
//        public void onActivityResult(Map<String, Boolean> result) {
//            for (Map.Entry<String, Boolean> entry : result.entrySet()) {
//                switch (entry.getKey()) {
//                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:
//                        if (entry.getValue()) {
//                            mGetContent.launch("image/*");
//                        } else {
//                            Toast.makeText(getActivity(), getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
//                        }
//                        break;
//                    case Manifest.permission.READ_EXTERNAL_STORAGE:
////                        if (entry.getValue()) {
////                            mGetContent.launch("image/*");
////                        } else {
//////                            Toast.makeText(getActivity(), getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
////                        }
//                        break;
//                }
//            }
//        }
//    });
//    private ActivityResultLauncher<String> mRequestPermission = registerForActivityResult(new ActivityResultContracts.RequestPermission(), new ActivityResultCallback<Boolean>() {
//        @Override
//        public void onActivityResult(Boolean result) {
//            if (result) {
//                mGetContent.launch("image/*");
//            } else {
//                Toast.makeText(getActivity(), getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
//            }
//        }
//    });

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        couponsViewModel =
                new ViewModelProvider(requireActivity()).get(CouponsViewModel.class);

//        View root = inflater.inflate(R.layout.fragment_coupons, container, false);
        binding = FragmentCouponsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

//        txtNoCoupons = root.findViewById(R.id.txtNoCoupons);
//        mRecyclerView = root.findViewById(R.id.mRecyclerView);
//        progressBar = root.findViewById(R.id.progressBar);

        setListenersAndObservers();
        return root;
    }

    private void setListenersAndObservers() {
        try {
//            couponsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//                @Override
//                public void onChanged(@Nullable String s) {
//                    txtNoNotifications.setText(s);
//                }
//            });

            couponsViewModel.getProgressBarStatus().observe(getViewLifecycleOwner(), aBoolean -> {
                if (aBoolean)
                    binding.progressBar.setVisibility(View.VISIBLE);
                else
                    binding.progressBar.setVisibility(View.GONE);
            });

            couponsViewModel.getNotifications().observe(getViewLifecycleOwner(), notificationsItems -> {
                try {
                    if (binding.mRecyclerView != null && binding.mRecyclerView.getAdapter() == null) {
                        binding.mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        couponsAdapter = new CouponsAdapter(getActivity(), notificationsItems);
                        binding.mRecyclerView.setAdapter(couponsAdapter);
                    } else if (binding.mRecyclerView != null) {
                        couponsAdapter.sort();
                        binding.mRecyclerView.getAdapter().notifyDataSetChanged();
                    }
                    if (notificationsItems == null || notificationsItems.size() == 0)
                        binding.txtNoCoupons.setVisibility(View.VISIBLE);
                    else
                        binding.txtNoCoupons.setVisibility(View.GONE);
                } catch (Exception ex) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

//        txtNoCoupons.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mRequestMultiplePermissions.launch(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE});
//            }
//        });
    }
}
