package com.elitesmartsolution.paypgroom.activities.homescreen.favourites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.adapters.PropertyListAdapterActual;
import com.elitesmartsolution.paypgroom.databinding.FragmentFavouritesBinding;
import com.elitesmartsolution.paypgroom.util.SpacesItemDecoration;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;

public class FavouritesFragment extends Fragment {

    private FragmentFavouritesBinding binding;
    private FavouritesViewModel favouritesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        favouritesViewModel =
                new ViewModelProvider(this).get(FavouritesViewModel.class);

        binding = FragmentFavouritesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        setListenersAndObservers();
        return root;
    }

    private void setListenersAndObservers() {
        try {
//        favouritesViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });

            favouritesViewModel.getProgressBarStatus().observe(getViewLifecycleOwner(), aBoolean -> {
                if (aBoolean)
                    binding.progressBar.setVisibility(View.VISIBLE);
                else
                    binding.progressBar.setVisibility(View.GONE);
            });

            favouritesViewModel.getPropertyList().observe(getViewLifecycleOwner(), mPropertyList -> {
                try {
                    if (binding.mRecyclerView != null && binding.mRecyclerView.getAdapter() == null) {
                        binding.mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        SpacesItemDecoration mSpacesItemDecoration = new SpacesItemDecoration((int) getResources().getDimension(R.dimen.dimen_10dp),
                                false);
                        binding.mRecyclerView.addItemDecoration(mSpacesItemDecoration);
                        PropertyListAdapterActual mPropertyListAdapter = new PropertyListAdapterActual(getActivity(), mPropertyList, Defaults.SCREEN_TYPE_FAVOURITES_FRAGMENT);
                        binding.mRecyclerView.setAdapter(mPropertyListAdapter);
                    } else if (binding.mRecyclerView != null) {
                        ((PropertyListAdapterActual) binding.mRecyclerView.getAdapter()).refreshList(mPropertyList);
                        binding.mRecyclerView.getAdapter().notifyDataSetChanged();
                    }
                    if (mPropertyList == null || mPropertyList.size() == 0)
                        binding.txtNoFavourites.setVisibility(View.VISIBLE);
                    else
                        binding.txtNoFavourites.setVisibility(View.GONE);
                } catch (Exception ex) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
