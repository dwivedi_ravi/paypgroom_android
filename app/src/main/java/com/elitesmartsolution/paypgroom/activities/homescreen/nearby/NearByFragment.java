package com.elitesmartsolution.paypgroom.activities.homescreen.nearby;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.adapters.PropertyListAdapterActual;
import com.elitesmartsolution.paypgroom.databinding.FragmentNearbyBinding;
import com.elitesmartsolution.paypgroom.interfaces.RecyclerViewHideShowScrollListener;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetPgListing;
import com.elitesmartsolution.paypgroom.util.PermissionUtils;
import com.elitesmartsolution.paypgroom.util.SpacesItemDecoration;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;

public class NearByFragment extends Fragment {

    //    LOCATION RELATED SETTINGS
    public static LocationRequest mLocationRequest;
    private FragmentNearbyBinding binding;
    private NearByViewModel nearByViewModel;
    // You can do the assignment inside onAttach or onCreate, i.e, before the activity is displayed
    ActivityResultLauncher<Intent> autoCompletePlacesActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    //  NOTE: THERE ARE NO REQUEST CODES WITH ACTIVITY RESULT LAUNCHER API.
                    //  Hence Defaults.AUTOCOMPLETE_REQUEST_CODE not required here.

                    Intent data = result.getData();

                    if (result.getResultCode() == RESULT_OK) {
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
//                        CALL LISTING API WITH PLACE LAT LNG
                        RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
                        objRequestGetPgListing.setLat(place.getLatLng().latitude + "");
                        objRequestGetPgListing.setLon(place.getLatLng().longitude + "");
                        objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
                        nearByViewModel.callListingApi(objRequestGetPgListing);

                        binding.searchBar.searchText.setText(place.getAddress());
                    } else if (result.getResultCode() == AutocompleteActivity.RESULT_ERROR) {
                        // TODO: Handle the error.
                        Status status = Autocomplete.getStatusFromIntent(data);
                        Log.i(TAG, status.getStatusMessage());
                    } else if (result.getResultCode() == RESULT_CANCELED) {
                        // The user canceled the operation.
                    }
                }
            });
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mCurrentLocation;
    private LocationCallback mLocationCallback;
    private boolean isShowingGPSDialog = false;
    ActivityResultLauncher<IntentSenderRequest> resolvableActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartIntentSenderForResult(), result -> {
                isShowingGPSDialog = false;
                if (result.getResultCode() == RESULT_OK) {
                    startLocationUpdates();
                } else if (result.getResultCode() == RESULT_CANCELED) {
                }
            });
    private boolean isSearchBarHide = false;

    public static LocationRequest getLocationRequestObject() {
        if (mLocationRequest == null) {
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval(Defaults.UPDATE_INTERVAL_IN_MILLISECONDS); // 30 Seconds
            mLocationRequest.setFastestInterval(Defaults.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS); // 20 Seconds
//            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }
        return mLocationRequest;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        nearByViewModel =
                new ViewModelProvider(this).get(NearByViewModel.class);

        binding = FragmentNearbyBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        setListenersAndObservers();
        initLocationSettings();

        return root;
    }

    private void setListenersAndObservers() {
        try {
//            {
//                progressDialog = new ProgressDialog(getActivity());
//                progressDialog.setCancelable(false);
//                progressDialog.setTitle("Location Capture");
//                progressDialog.setMessage("Fetching location details, Please wait...");
//                progressDialog.show();
//                getAddressForLatLng();
//            }

            nearByViewModel.getProgressBarStatus().observe(getViewLifecycleOwner(), aBoolean -> {
                if (aBoolean)
                    binding.progressBar.setVisibility(View.VISIBLE);
                else
                    binding.progressBar.setVisibility(View.GONE);
            });

            binding.searchBar.searchBarCardView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//                        Utility.launchAutocompletePlacesActivity(getActivity(), NearByFragment.this);
                        Utility.launchAutocompletePlacesActivity(getActivity(), autoCompletePlacesActivityResultLauncher);
                    }
                    return true;
                }
            });

            binding.searchBar.btMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            nearByViewModel.getPropertyList().observe(getViewLifecycleOwner(), mPropertyList -> {
                try {
                    if (binding.mRecyclerView != null && binding.mRecyclerView.getAdapter() == null) {
                        binding.mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        SpacesItemDecoration mSpacesItemDecoration = new SpacesItemDecoration((int) getResources().getDimension(R.dimen.dimen_10dp),
                                false);
                        binding.mRecyclerView.addItemDecoration(mSpacesItemDecoration);
                        PropertyListAdapterActual mPropertyListAdapter = new PropertyListAdapterActual(getActivity(), mPropertyList, Defaults.SCREEN_TYPE_NEAR_BY_FRAGMENT);
                        binding.mRecyclerView.setAdapter(mPropertyListAdapter);
                    } else if (binding.mRecyclerView != null) {
                        ((PropertyListAdapterActual) binding.mRecyclerView.getAdapter()).refreshList(mPropertyList);
                        binding.mRecyclerView.getAdapter().notifyDataSetChanged();
                    }
                    if (mPropertyList == null || mPropertyList.size() == 0)
                        binding.txtNoNearByPgsFound.setVisibility(View.VISIBLE);
                    else
                        binding.txtNoNearByPgsFound.setVisibility(View.GONE);
                } catch (Exception ex) {

                }
            });

            binding.mRecyclerView.addOnScrollListener(new RecyclerViewHideShowScrollListener() {
                @Override
                public void onHide() {
                    animateSearchBar(true);
                }

                @Override
                public void onShow() {
                    animateSearchBar(false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void animateSearchBar(final boolean hide) {
        try {
            if (isSearchBarHide && hide || !isSearchBarHide && !hide) return;
            isSearchBarHide = hide;
            int moveY = hide ? -(2 * binding.searchBar.searchBarCardView.getHeight()) : 0;
            binding.searchBar.searchBarCardView.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    LOCATION RELATED CODE FROM HERE
    private void initLocationSettings() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

//        ON LOCATION CHANGED CALLBACK FIRED EVERYTIME THE LOCATION CHANGES
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                mCurrentLocation = locationResult.getLastLocation();
//                CALL LISTING API WITH PLACE LAT LNG FOR THE FIRST TIME WHEN THE ADAPTER IS NULL
                if (binding.mRecyclerView.getAdapter() == null) {
//                    nearByViewModel.callListingApiWithLatLng(mCurrentLocation.getLatitude() + "", mCurrentLocation.getLongitude() + "");
                    RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
                    objRequestGetPgListing.setLat(mCurrentLocation.getLatitude() + "");
                    objRequestGetPgListing.setLon(mCurrentLocation.getLongitude() + "");
                    objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
                    nearByViewModel.callListingApi(objRequestGetPgListing);
                }
            }
        };
        getLocationRequestObject();

//        FOR GETTING LAST KNOWN LOCATION ON APP START
        if (PermissionUtils.checkForPassedPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            showGPSSettingsTurnOnDialog();
            try {
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    // Logic to handle location object
                                    mCurrentLocation = location;
                                }
                            }
                        });
            } catch (SecurityException ex) {
            }
        }
    }

    private void showGPSSettingsTurnOnDialog() {
        if (!isShowingGPSDialog) {
            isShowingGPSDialog = true;
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(getLocationRequestObject());
            builder.setNeedBle(true);
//            builder.setAlwaysShow(true);

            SettingsClient client = LocationServices.getSettingsClient(getActivity());
            Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
            task.addOnSuccessListener(getActivity(), locationSettingsResponse -> {
//                    All location settings are satisfied. The client can initialize location requests here.
//                    startLocationUpdates();
            });

            task.addOnFailureListener(getActivity(), e -> {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
//                        //  OLD CODE COMMENTED
//                        // Show the dialog by calling startResolutionForResult(),
//                        // and check the result in onActivityResult().
//                            ResolvableApiException resolvable = (ResolvableApiException) e;
//                            resolvable.startResolutionForResult(getActivity(),
//                                    Defaults.REQUEST_CODE_GPS_STATUS_CHECK_SETTINGS);

//                            NEW CODE COMMENTED ADDED
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(getActivity(),
                                Defaults.REQUEST_CODE_GPS_STATUS_CHECK_SETTINGS);

                        resolvableActivityResultLauncher.
                                launch(new IntentSenderRequest.Builder(resolvable.getResolution()).build());

                    } catch (IntentSender.SendIntentException sendEx) {
                    }
                }
            });
        }
    }

    private void startLocationUpdates() {
        if (PermissionUtils.checkForPassedPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) || PermissionUtils.checkForPassedPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
            if (mFusedLocationClient != null)
                mFusedLocationClient.requestLocationUpdates(getLocationRequestObject(),
                        mLocationCallback,
                        Looper.myLooper() /* Looper */);
        }
    }

    private void stopLocationUpdates() {
        if (PermissionUtils.checkForPassedPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) || PermissionUtils.checkForPassedPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
            if (mFusedLocationClient != null)
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        checkForGooglePlayServices();
        startLocationUpdates();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        if (requestCode == Defaults.REQUEST_CODE_GPS_STATUS_CHECK_SETTINGS) {
//            isShowingGPSDialog = false;
//            if (resultCode == RESULT_OK) {
//                startLocationUpdates();
//            } else if (resultCode == RESULT_CANCELED) {
//            }
//        }
//
//        if (requestCode == Defaults.AUTOCOMPLETE_REQUEST_CODE) {
//            if (resultCode == RESULT_OK) {
//                Place place = Autocomplete.getPlaceFromIntent(data);
//                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
////                CALL LISTING API WITH PLACE LAT LNG
//                RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
//                objRequestGetPgListing.setLat(place.getLatLng().latitude + "");
//                objRequestGetPgListing.setLon(place.getLatLng().longitude + "");
//                objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
//                nearByViewModel.callListingApi(objRequestGetPgListing);
//
//                binding.searchBar.searchText.setText(place.getAddress());
//            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
//                // TODO: Handle the error.
//                Status status = Autocomplete.getStatusFromIntent(data);
//                Log.i(TAG, status.getStatusMessage());
//            } else if (resultCode == RESULT_CANCELED) {
//                // The user canceled the operation.
//            }
//            return;
//        }
//
//        super.onActivityResult(requestCode, resultCode, data);
//    }

    //    LOCATION RELATED CODE TILL HERE

//    private void checkForGooglePlayServices() {
//        int googlePlayServicesAvailabilityStatus = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity());
//
//        DialogInterface.OnCancelListener onCancelListener = new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                Toast.makeText(getActivity(), "App won't work properly without Google Play Services. Please update Google Play Services", Toast.LENGTH_LONG).show();
//            }
//        };
//
//        if (googlePlayServicesAvailabilityStatus != SUCCESS) {
//            GoogleApiAvailability.getInstance().getErrorDialog(getActivity(), googlePlayServicesAvailabilityStatus, 0, onCancelListener).show();
//        }
//    }
//    class AddressResultReceiver extends ResultReceiver {
//        public AddressResultReceiver(Handler handler) {
//            super(handler);
//        }
//
//        @Override
//        protected void onReceiveResult(int resultCode, Bundle resultData) {
//
//            if (resultData == null) {
//                return;
//            }
//
//            // Display the address string
//            // or an error message sent from the intent service.
//            String mAddressOutput = resultData.getString(Defaults.RESULT_DATA_KEY);
//            Address address = resultData.getParcelable(
//                    Defaults.ADDRESS_DATA_EXTRA);
//
//            if (mAddressOutput == null) {
//                mAddressOutput = "";
//            }
//            Log.e("ADDRESS: ", mAddressOutput);
//            dismissDialog = true;
//
//            if (resultCode == Defaults.RESULT_CODE_SUCCESS) {
//                savePhoto(address, mAddressOutput);
//            } else {
//                Toast.makeText(getActivity(), "Unable to fetch location.", Toast.LENGTH_LONG).show();
//                File file = new File(imageStoragePath);
//                if (file.exists())
//                    file.delete();
//            }
//            if (progressDialog != null && progressDialog.isShowing() && dismissDialog) {
//                progressDialog.dismiss();
//            }
//        }
//    }
//    private void getAddressForLatLng() {
//        startIntentService(mCurrentLocation);
//    }
//    protected void startIntentService(Location mLastLocation) {
//        Intent intent = new Intent(this, FetchAddressIntentService.class);
//        intent.putExtra(Defaults.RECEIVER, mResultReceiver);
//        intent.putExtra(Defaults.LOCATION_DATA_EXTRA, mLastLocation);
//        getActivity().startService(intent);
//    }
}
