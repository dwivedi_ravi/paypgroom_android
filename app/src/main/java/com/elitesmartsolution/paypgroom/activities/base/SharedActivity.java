package com.elitesmartsolution.paypgroom.activities.base;

import android.Manifest;
import android.os.Build;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.elitesmartsolution.paypgroom.R;

import java.util.Map;

/**
 * Created by Ravi on 03/08/16.
 */

public abstract class SharedActivity extends BaseActivity {

    private ActivityResultLauncher mRequestMultiplePermissions = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), new ActivityResultCallback<Map<String, Boolean>>() {
        @Override
        public void onActivityResult(Map<String, Boolean> result) {
            for (Map.Entry<String, Boolean> entry : result.entrySet()) {
                switch (entry.getKey()) {
                    case Manifest.permission.WRITE_EXTERNAL_STORAGE:

//                        if (entry.getValue()) {
//                            gotReadStoragePermission();
//                        } else {
//                            Toast.makeText(SharedActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
//                        }
                        break;
                    case Manifest.permission.READ_EXTERNAL_STORAGE:
                    case Manifest.permission.READ_MEDIA_IMAGES:
//                        if (entry.getValue()) {
//                            mGetContent.launch("image/*");
//                        } else {
//                            //Toast.makeText(EditProfileActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
//                        }
                        if (entry.getValue()) {
                            gotReadStoragePermission();
                        } else {
                            Toast.makeText(SharedActivity.this, getString(R.string.storage_permission), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case Manifest.permission.ACCESS_FINE_LOCATION:
                        gotLocationPermission(entry.getValue());
                        break;
                    case Manifest.permission.ACCESS_COARSE_LOCATION:
                        break;
                }
            }
        }
    });

    /**
     * Method to call from this activity's various events like, when user clicks on the Image to upload his Pic.
     */
    public void askForReadStoragePermission() {
//        mRequestMultiplePermissions.launch(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE});
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            mRequestMultiplePermissions.launch(new String[]{Manifest.permission.READ_MEDIA_IMAGES});
        else
            mRequestMultiplePermissions.launch(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE});
    }

    /**
     * Method to call from this activity's various events like, when user lends on the Activity which requires Location Permission.
     */
    public void askForLocationPermission() {
        mRequestMultiplePermissions.launch(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION});
    }

    /**
     * This is the Callback Method, that will be called when user gives write permission to the app.
     * The implementing activity can handle this method within its body and perform desired operation.
     */
    abstract public void gotReadStoragePermission();

    /**
     * This is the Callback Method, that will be called when user gives location permission to the app.
     * The implementing activity can handle this method within its body and perform desired operation.
     */
    abstract public void gotLocationPermission(boolean isPermissionGranted);
}
