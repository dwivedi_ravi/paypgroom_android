package com.elitesmartsolution.paypgroom.activities;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.widget.Toolbar;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.databinding.ActivityAboutAppBinding;
import com.elitesmartsolution.paypgroom.util.Utility;

import java.io.IOException;

public class AboutApp extends SharedActivity {

    private ActivityAboutAppBinding binding;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAboutAppBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initToolbar();
    }

    @Override
    public void gotReadStoragePermission() {
    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {

    }

    private void initToolbar() {
        binding.toolbar.setTitle("About App");
        setSupportActionBar(binding.toolbar);
        Utility.setStatusBarColor(AboutApp.this);
        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_with_settings, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.action_settings) {
////            finish();
//        } else {
//            Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
//        }
        return super.onOptionsItemSelected(item);
    }
}
