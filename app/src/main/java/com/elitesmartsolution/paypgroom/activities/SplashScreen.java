package com.elitesmartsolution.paypgroom.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.elitesmartsolution.paypgroom.activities.homescreen.activity.HomeScreenBottomNavigationActivity;
import com.elitesmartsolution.paypgroom.activities.homescreenowner.activity.OwnerHomeScreenActivity;
import com.elitesmartsolution.paypgroom.databinding.ActivitySplashBinding;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Keys;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreen extends AppCompatActivity {

    private ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

//        AppSignatureHelper obj = new AppSignatureHelper(this);
//        ArrayList<String> resign = obj.getAppSignatures();
//        for (int i = 0; i < resign.size(); i++) {
//            Log.e("RAVI", resign.get(i));
//        }

//        ApiCallerUtility.callGetAllAmenitiesApi(this);
//        ApiCallerUtility.callGetPgMetaDataApi(this);
//        DBHandler.getInstance(this).insertLuxuryTypes(null);
//        Prefs.setServiceCityCode(Defaults.TEMP_SERVICE_CITY_CODE);

        if (!Prefs.getAuthKey().trim().equals("")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    takeUserIntoTheApp();
                }
            }, 100);
//            takeUserIntoTheApp();
        } else {
//            Prefs.clearAllPreferenceValues();
            ApiCallerUtility.callRegisterDeviceApi(this);
        }
    }

//    public void takeUserIntoTheApp() {
//        Intent inte;
//        if (Prefs.isPhoneNumberVerified()) {
////            if (Prefs.isProfileCompleted()) {
//////                inte = new Intent(SplashScreen.this, HomeScreenBottomNavigationActivity.class);
////                if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
////                    inte = new Intent(SplashScreen.this, OwnerHomeScreenActivity.class);
////                else
////                    inte = new Intent(SplashScreen.this, HomeScreenBottomNavigationActivity.class);
////            } else {
////                inte = new Intent(SplashScreen.this, EditProfileActivity.class);
////                inte.putExtra("navigate", true);
////            }
//
//            if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
//                inte = new Intent(SplashScreen.this, OwnerHomeScreenActivity.class);
//            else
//                inte = new Intent(SplashScreen.this, HomeScreenBottomNavigationActivity.class);
//
//        } else {
//            inte = new Intent(SplashScreen.this, SignInActivity.class);
//        }
//        startActivity(inte);
//        SplashScreen.this.finish();
//    }

    public void takeUserIntoTheApp() {
        Intent inte;
        if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
            inte = new Intent(SplashScreen.this, OwnerHomeScreenActivity.class);
        else
            inte = new Intent(SplashScreen.this, HomeScreenBottomNavigationActivity.class);
        startActivity(inte);
        SplashScreen.this.finish();
    }
}
