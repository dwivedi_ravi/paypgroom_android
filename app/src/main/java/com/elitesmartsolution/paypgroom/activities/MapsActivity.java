package com.elitesmartsolution.paypgroom.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.databinding.ActivityMapsBinding;
import com.elitesmartsolution.paypgroom.models.pglisting.PropertyListItem;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private ActivityMapsBinding binding;

    private GoogleMap mMap;
    private PropertyListItem objPropertyListItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            objPropertyListItem = (PropertyListItem) bundle.getSerializable("currentPropertyListBO");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMapToolbarEnabled(true);

            binding.mapLoadingProgressbar.setVisibility(View.GONE);

            if (objPropertyListItem != null) {
                addMarkerToPosition(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * DIFFERENT ZOOM LEVELS
     * 1: World
     * 5: Landmass/continent
     * 10: City
     * 15: Streets
     * 20: Buildings
     *
     * @param isClearMap
     */
    private void addMarkerToPosition(boolean isClearMap) {
        if (mMap != null) {
            if (isClearMap)
                mMap.clear();

            LatLng pgLocation = new LatLng(Double.parseDouble(objPropertyListItem.getLocation().getLatitude()), Double.parseDouble(objPropertyListItem.getLocation().getLongitude()));
            mMap.addMarker(new MarkerOptions().position(pgLocation).snippet(objPropertyListItem.getLocation().getLine1() + "\n" + objPropertyListItem.getLocation().getLine2()).title(objPropertyListItem.getPgName())).showInfoWindow();
            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    Context context = getApplicationContext(); //or getActivity(), YourActivity.this, etc.

                    LinearLayout info = new LinearLayout(context);
                    info.setOrientation(LinearLayout.VERTICAL);

                    TextView title = new TextView(context);
                    title.setTextColor(Color.BLACK);
                    title.setGravity(Gravity.CENTER);
                    title.setTypeface(null, Typeface.BOLD);
                    title.setText(marker.getTitle());

                    TextView snippet = new TextView(context);
                    snippet.setTextColor(Color.GRAY);
                    snippet.setText(marker.getSnippet());

                    info.addView(title);
                    info.addView(snippet);

                    return info;
                }
            });
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pgLocation, Defaults.MAP_ZOOM_LEVEL_STREETS));

//            mCircleOptions = new CircleOptions()
//                    .center(currentLatLng)
//                    .radius(Constants.DEFAULT_RADIUS_METERS)
//                    .strokeWidth(5)
//                    .strokeColor(getPrimaryColor());
//            mCircle = mMap.addCircle(mCircleOptions);
        }
    }

}