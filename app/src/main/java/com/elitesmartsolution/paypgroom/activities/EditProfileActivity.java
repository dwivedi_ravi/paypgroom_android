package com.elitesmartsolution.paypgroom.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.databinding.ActivityEditProfileBinding;
import com.elitesmartsolution.paypgroom.interfaces.DateTimeUpdateListener;
import com.elitesmartsolution.paypgroom.models.ResponseProfile;
import com.elitesmartsolution.paypgroom.models.profile.RequestEditProfile;
import com.elitesmartsolution.paypgroom.models.profile.RequestGetProfile;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.LegacyCompatFileProvider;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.elitesmartsolution.paypgroom.widget.DateTimePicker;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class EditProfileActivity extends SharedActivity implements DateTimeUpdateListener, View.OnClickListener, View.OnTouchListener {

    private ActivityEditProfileBinding binding;

    private Animator currentAnimator;
    private int shortAnimationDuration;
    private DateTimeUpdateListener updateListener;
    private MyReceiver objMyReceiver;
    private boolean navigate = false;
    private RequestOptions requestOptions;
    private ResponseProfile objResponseProfile;

    private ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri resultUri) {
            // Handle the returned Uri
            try {
                if (resultUri != null) {
                    try {
//                        case CAMERA_REQUEST:
//                            Bitmap photo = (Bitmap) data.getExtras().get("data");
////                    binding.contentEditProfile.imgProfile.setImageBitmap(photo);
////                    binding.imgFullView.setImageBitmap(photo);
//                            Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(photo)
//                                    .into(binding.contentEditProfile.imgProfile);
//                            Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(photo)
//                                    .into(binding.imgFullView);
//                            break;

//                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//                        Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(bitmap)
//                                .into(binding.contentEditProfile.imgProfile);
//                        Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(bitmap)
//                                .into(binding.imgFullView);

                        Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(resultUri)
                                .into(binding.contentEditProfile.imgProfile);
                        Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(resultUri)
                                .into(binding.imgFullView);

                        crossfade(binding.contentEditProfile.imgProfile, binding.fullScreenContainer);
                        binding.contentEditProfile.uploadProgress.setVisibility(View.VISIBLE);
                        binding.appbarLayout.toolbar.setVisibility(View.VISIBLE);
                        binding.fabNext.setVisibility(View.VISIBLE);
                        Utility.setStatusBarColor(EditProfileActivity.this);

                        ApiCallerUtility.callUploadProfileImageApi(EditProfileActivity.this, resultUri);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    });

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityEditProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();

        setSupportActionBar(binding.appbarLayout.toolbar);
        Utility.setStatusBarColor(EditProfileActivity.this);
        updateListener = this;
        registerReciever();
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder);

        binding.contentEditProfile.editMobile.setText(Prefs.getMobileNumber());
//        binding.contentEditProfile.editEmail.setVisibility(View.GONE);
//        binding.contentEditProfile.editDOB.setVisibility(View.GONE);

        binding.fullScreenContainer.setOnTouchListener(this);
        binding.contentEditProfile.editDOB.setOnTouchListener(this);
//        binding.fabNext.shrink();
        binding.fabNext.setOnClickListener(this);
        binding.contentEditProfile.imgProfile.setOnClickListener(this);
        binding.imgEditProfile.setOnClickListener(this);
        binding.imgShare.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);

        shortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        if (getIntent() != null) {
            navigate = getIntent().getExtras().getBoolean("navigate", false);
        }

        RequestGetProfile objRequestGetProfile = new RequestGetProfile();
        objRequestGetProfile.setUserId(Prefs.getUserId());
        ApiCallerUtility.callGetProfileDataApi(this, objRequestGetProfile);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (binding.fullScreenContainer.getVisibility() == View.VISIBLE) {
//            binding.fullScreenContainer.setVisibility(View.GONE);
            crossfade(binding.contentEditProfile.imgProfile, binding.fullScreenContainer);
            binding.appbarLayout.toolbar.setVisibility(View.VISIBLE);
            binding.fabNext.setVisibility(View.VISIBLE);
            Utility.setStatusBarColor(EditProfileActivity.this);
        } else {
            super.onBackPressed();
        }
    }

    private void crossfade(View view1, View view2) {

        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        view1.setAlpha(0f);
        view1.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        view1.animate()
                .alpha(1f)
                .setDuration(shortAnimationDuration)
                .setListener(null);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        view2.animate()
                .alpha(0f)
                .setDuration(shortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view2.setVisibility(View.GONE);
                    }
                });
    }

    private boolean isFormValid() {
        if (binding.contentEditProfile.editFirstName.getVisibility() == View.VISIBLE && binding.contentEditProfile.editFirstName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editFirstName, "First name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditProfile.editLastName.getVisibility() == View.VISIBLE && binding.contentEditProfile.editLastName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editLastName, "Last name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditProfile.editEmail.getVisibility() == View.VISIBLE && binding.contentEditProfile.editEmail.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editEmail, "Email can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditProfile.editMobile.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editMobile, "Mobile Number can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentEditProfile.editDOB.getVisibility() == View.VISIBLE && binding.contentEditProfile.editDOB.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentEditProfile.editDOB, "Date of birth can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_next:
                if (isFormValid()) {
                    RequestEditProfile objRequestEditProfile = new RequestEditProfile();

                    objRequestEditProfile.setFirstName(binding.contentEditProfile.editFirstName.getText().toString());
                    objRequestEditProfile.setLastName(binding.contentEditProfile.editLastName.getText().toString());
                    objRequestEditProfile.setEmail(binding.contentEditProfile.editEmail.getText().toString());
                    objRequestEditProfile.setUserId(Prefs.getUserId());
                    if (binding.contentEditProfile.rdBtnMale.isChecked())
                        objRequestEditProfile.setGender(binding.contentEditProfile.rdBtnMale.getText().toString());
                    else
                        objRequestEditProfile.setGender(binding.contentEditProfile.rdBtnFemale.getText().toString());
//                    objRequestEditProfile.setDob(binding.contentEditProfile.editDOB.getText().toString());
                    objRequestEditProfile.setDob("1910-01-01");

                    ApiCallerUtility.callEditProfileApi(EditProfileActivity.this, objRequestEditProfile, navigate);
                }
                break;
            case R.id.imgProfile:
//                binding.fullScreenContainer.setVisibility(View.VISIBLE);
                crossfade(binding.fullScreenContainer, binding.contentEditProfile.imgProfile);

                setImageFromServer();

                binding.appbarLayout.toolbar.setVisibility(View.GONE);
                binding.fabNext.setVisibility(View.GONE);
                Utility.setTransperentStatusBarColor(EditProfileActivity.this);
                break;
            case R.id.imgEditProfile:
//                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(cameraIntent, CAMERA_REQUEST);

                askForReadStoragePermission();

                break;
            case R.id.imgShare:
                try {
                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/*");
                    BitmapDrawable drawable = (BitmapDrawable) binding.contentEditProfile.imgProfile.getDrawable();
                    Bitmap bitmap = drawable.getBitmap();

//                    File filepath = Environment.getExternalStorageDirectory();
//                    File filepath = StorageHelper.getSdcardFile(EditProfileActivity.this);
//                    File filepath = getExternalFilesDir("external");
//                    File filepath = getFilesDir();

                    // Create a new folder AndroidBegin in SD Card
                    File internalDirectoryPath = new File(getFilesDir().getAbsolutePath() + "/" + Defaults.FOLDER_PATH_PROFILE);
                    if (!internalDirectoryPath.exists())
                        internalDirectoryPath.mkdirs();

                    // Create a name for the saved image
                    File imageFile = new File(internalDirectoryPath, "profile_pic.png");
                    if (!imageFile.exists())
                        imageFile.createNewFile();

                    try {
                        // Saves the file into SD Card
                        OutputStream output = new FileOutputStream(imageFile);
                        // Compress into png format image from 0% - 100%, using 100% for this tutorial
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
                        output.flush();
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    if (imageFile != null) {
                        Uri uri1 = LegacyCompatFileProvider.getUri(this, imageFile);
                        share.putExtra(Intent.EXTRA_STREAM, uri1);
                        share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(Intent.createChooser(share, getString(R.string.send_to)));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.imgBack:
                if (binding.fullScreenContainer.getVisibility() == View.VISIBLE) {
//                    binding.fullScreenContainer.setVisibility(View.GONE);
                    crossfade(binding.contentEditProfile.imgProfile, binding.fullScreenContainer);
                    binding.appbarLayout.toolbar.setVisibility(View.VISIBLE);
                    binding.fabNext.setVisibility(View.VISIBLE);
                    Utility.setStatusBarColor(EditProfileActivity.this);
                }
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (view.getId()) {
            case R.id.editDOB:
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    getDate(view);
                return true;
            case R.id.fullScreenContainer:
                return true;
        }
        return false;
    }

    public void getDate(View view) {
        DateTimePicker.getInstance().getDate(EditProfileActivity.this, updateListener, null, (TextView) view);
    }

    @Override
    public void onDateUpdate(String year, String monthOfYear, String dayOfMonth, TextView textView) {
        String date = year + "-" + (monthOfYear) + "-" + dayOfMonth;
//        String date = year + "-" + (monthOfYear) + "-" + dayOfMonth;
//        String date = dayOfMonth + "-" + (monthOfYear);

//        String date = (monthOfYear) + "-" + dayOfMonth;
        textView.setText(date);
    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, String am_pm, TextView textView) {

    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, TextView textView) {

    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_FILL_PROFILE_DATA);
        filter.addAction(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS);
        filter.addAction(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED);
        Utility.registerReciever(EditProfileActivity.this, filter, objMyReceiver);
    }

    private void setImageFromServer() {
        try {
            if (objResponseProfile != null) {
//                binding.bigImageProgressBar.setVisibility(View.VISIBLE);
                binding.imgFullView.setImageResource(R.drawable.profile_place_holder);
                Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(objResponseProfile.getProfileData().getUserProfile().getProfile())/*.listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        binding.bigImageProgressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        binding.bigImageProgressBar.setVisibility(View.GONE);
                        return false;
                    }
                })*/.into(binding.imgFullView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotReadStoragePermission() {
        mGetContent.launch("image/*");
    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_FILL_PROFILE_DATA)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        objResponseProfile = (ResponseProfile) bundle.getSerializable("ResponseProfile");

                        binding.contentEditProfile.editFirstName.setText(objResponseProfile.getProfileData().getUserProfile().getFirstName());
                        binding.contentEditProfile.editLastName.setText(objResponseProfile.getProfileData().getUserProfile().getLastName());
                        binding.contentEditProfile.editEmail.setText(objResponseProfile.getProfileData().getUserProfile().getEmail());
                        binding.contentEditProfile.editMobile.setText(objResponseProfile.getProfileData().getUserProfile().getMobile());
                        binding.contentEditProfile.editDOB.setText(objResponseProfile.getProfileData().getUserProfile().getDob());

                        if (objResponseProfile.getProfileData().getUserProfile().getGender().equalsIgnoreCase("male"))
                            binding.contentEditProfile.rdBtnMale.setChecked(true);
                        else
                            binding.contentEditProfile.rdBtnFemale.setChecked(true);
//                        binding.contentEditProfile.editStatusText.setText(objResponseProfile.getProfileData().getUserProfile().getAbout());

                        Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(objResponseProfile.getProfileData().getUserProfile().getProfile())
                                .into(binding.contentEditProfile.imgProfile);
                        Glide.with(EditProfileActivity.this).setDefaultRequestOptions(requestOptions).load(objResponseProfile.getProfileData().getUserProfile().getProfile())
                                .into(binding.imgFullView);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS)) {
                binding.contentEditProfile.uploadProgress.setVisibility(View.GONE);
                Toast.makeText(context, "Profile Pic Uploaded Successfully", Toast.LENGTH_LONG).show();
                if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                    Bundle bundle = intent.getExtras().getBundle("bundle");
                    ResponseProfile objResponseProfileNew = (ResponseProfile) bundle.getSerializable("ResponseProfile");
                    if (objResponseProfileNew != null) {
                        objResponseProfile.getProfileData().getUserProfile().setProfile(objResponseProfileNew.getProfileData().getUserProfile().getProfile());
                    }
                }
            }

            if (intent.getAction().equals(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED)) {
                binding.contentEditProfile.uploadProgress.setVisibility(View.GONE);
                boolean sizeExceeded = false;
                if (intent != null && intent.getExtras() != null && intent.getExtras().getBundle("bundle") != null) {
                    Bundle bundle = intent.getExtras().getBundle("bundle");
                    sizeExceeded = bundle.getBoolean("sizeExceeded", false);
                }
                if (!sizeExceeded)
                    Toast.makeText(context, "Profile Pic Upload Failed", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(context, "Upload Failed. Please upload image less than 2MB.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
