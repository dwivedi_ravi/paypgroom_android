package com.elitesmartsolution.paypgroom.activities.homescreenowner.mypglisting;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.elitesmartsolution.paypgroom.models.pglisting.PropertyListItem;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;

import java.util.List;

public class OwnerPgListingViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<Boolean> mShowProgressBar;
    private MutableLiveData<List<PropertyListItem>> mPropertyList;
    private Context context;

    public OwnerPgListingViewModel(Application context) {
        super(context);
        this.context = context;
        mText = new MutableLiveData<>();
        mText.setValue("This is Favourites fragment");
        mShowProgressBar = new MutableLiveData<>();
        mShowProgressBar.setValue(false);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<Boolean> getProgressBarStatus() {
        return mShowProgressBar;
    }

//    public LiveData<Boolean> getProgressBarStatus() {
//        if (mShowProgressBar == null) {
//            mShowProgressBar = new MutableLiveData<>();
//            mShowProgressBar.setValue(false);
//        }
//        return mShowProgressBar;
//    }

    public LiveData<List<PropertyListItem>> getPropertyList() {
        if (mPropertyList == null) {
            mPropertyList = new MutableLiveData<>();

//            ApiCallerUtility.callGetMyPgListingApi(context, mPropertyList, mShowProgressBar, Defaults.REQUEST_TYPE_OWNER_ID);
        }
        return mPropertyList;
    }

    public void callMyPgListingApi() {
        ApiCallerUtility.callGetMyPgListingApi(context, mPropertyList, mShowProgressBar, Defaults.REQUEST_TYPE_OWNER_ID);
    }
}


//package com.elitesmartsolution.paypgroom.activities.homescreenowner.mypglisting;
//
//import android.app.Application;
//import android.content.Context;
//
//import androidx.lifecycle.AndroidViewModel;
//import androidx.lifecycle.LiveData;
//import androidx.lifecycle.MutableLiveData;
//
//import com.elitesmartsolution.paypgroom.R;
//import com.elitesmartsolution.paypgroom.models.PropertyImagesBO;
//import com.elitesmartsolution.paypgroom.models.PropertyListItems;
//import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class OwnerPgListingViewModel extends AndroidViewModel {
//
//    private MutableLiveData<String> mText;
//    private MutableLiveData<List<PropertyListItems>> mPropertyList;
//    private Context context;
//
//    public OwnerPgListingViewModel(Application context) {
//        super(context);
//        this.context = context;
//        mText = new MutableLiveData<>();
//        mText.setValue("This is Favourites fragment");
//    }
//
//    public LiveData<String> getText() {
//        return mText;
//    }
//
//    public LiveData<List<PropertyListItems>> getPropertyList() {
//        if (mPropertyList == null) {
//            mPropertyList = new MutableLiveData<>();
//            ApiCallerUtility.callGetPgListingOwnerWiseApi(context, mPropertyList);
//        }
//        return mPropertyList;
//    }
//}