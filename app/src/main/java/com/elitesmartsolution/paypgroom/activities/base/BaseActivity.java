package com.elitesmartsolution.paypgroom.activities.base;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.CallSuper;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    private static final String TRANSITION_TYPE = "Transition Type";
    private static final String ANDROID_TRANSITION = "switchAndroid";
    private static final String BLUE_TRANSITION = "switchBlue";

    /**
     * Initializes the activity.
     *
     * @param savedInstanceState Contains information about the state of the app.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get the Transition type from the Intent and set it
        if (getIntent().hasExtra(TRANSITION_TYPE) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            switch (getIntent().getStringExtra(TRANSITION_TYPE)) {
                case "Explode":
                    getWindow().setEnterTransition(new Explode());
                    break;
                case "Slide":
                    getWindow().setEnterTransition(new Slide());
                    break;
                case "Fade":
                    getWindow().setEnterTransition(new Fade());
                    break;
                default:
                    break;
            }
        }
    }

    public void explodeTransition(final Context context) {
        //Relaunches the activity with the transition information
        Intent intent = new Intent(context, context.getClass());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.putExtra(TRANSITION_TYPE, "Explode");
            getWindow().setExitTransition(new Explode());
            startActivity(intent, ActivityOptions.
                    makeSceneTransitionAnimation((Activity) context).toBundle());
        } else {
            intent.putExtra(TRANSITION_TYPE, "Explode");
            startActivity(intent);
        }
    }

    /**
     * Method for relaunching the activity with an Fade animation
     *
     * @param context   The application context
     * @param imageView The imageView that was clicked
     */
    public void fadeTransition(final Context context, ImageView imageView) {
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Relaunches the activity with the transition information
                Intent intent = new Intent(context, context.getClass());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    intent.putExtra(TRANSITION_TYPE, "Fade");
                    getWindow().setExitTransition(new Fade());
                    startActivity(intent, ActivityOptions.
                            makeSceneTransitionAnimation((Activity) context).toBundle());
                } else {
                    intent.putExtra(TRANSITION_TYPE, "Fade");
                    startActivity(intent);
                }
            }
        });
    }

    /**
     * Method for animating an ImageView 360 degrees and back
     *
     * @param imageView The ImageView that was clicked
     */
    public void rotateView(ImageView imageView) {
        //Create the object animator with desired options
        final ObjectAnimator animator = ObjectAnimator.ofFloat(imageView, View.ROTATION, 0f, 360f);
        animator.setDuration(1000);
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.setRepeatCount(1);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Start it when the view is clicked
                animator.start();
            }
        });
    }

    public void switchAnimation(final ImageView androidImage, final ImageView otherImage,
                                final Intent intent, final Context context) {
        //Set the transition details and start the second activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation
                    ((Activity) context, Pair.create(androidImage, ANDROID_TRANSITION),
                            Pair.create(otherImage, BLUE_TRANSITION));
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @CallSuper
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
