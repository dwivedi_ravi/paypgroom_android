package com.elitesmartsolution.paypgroom.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.activities.homescreen.nearby.NearByViewModel;
import com.elitesmartsolution.paypgroom.adapters.PropertyListAdapterActual;
import com.elitesmartsolution.paypgroom.databinding.ActivityPropertyListingBinding;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetPgListing;
import com.elitesmartsolution.paypgroom.util.SpacesItemDecoration;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.google.android.material.snackbar.Snackbar;

public class PropertyListingActivity extends SharedActivity {

    private ActivityPropertyListingBinding binding;

    private MyReceiver objMyReceiver;
    private String requestType = Defaults.REQUEST_TYPE_FULLY_FURNISHED;
    private RequestGetPgListing objRequestGetPgListing;

    private NearByViewModel nearByViewModel;
    private boolean isSearchActivity;
    private SearchView searchView;
    private boolean searchPerformed = false;
    private String toolTitle = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPropertyListingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initComponents();
    }

    private void initComponents() {
        try {
            objMyReceiver = new MyReceiver();
            registerReciever();
            nearByViewModel = new ViewModelProvider(this).get(NearByViewModel.class);

            binding.appbarLayout.toolbar.setTitle("");
            setSupportActionBar(binding.appbarLayout.toolbar);
            Utility.setStatusBarColor(PropertyListingActivity.this);
            binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            binding.fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
            initializeData();
            setListenersAndObservers();
            if (!isSearchActivity)
                nearByViewModel.callListingApi(objRequestGetPgListing);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBundle("bundle") != null) {
            final Bundle bundle = getIntent().getExtras().getBundle("bundle");
            toolTitle = bundle.getString("toolTitle", "");
            objRequestGetPgListing = (RequestGetPgListing) bundle.getSerializable("RequestGetPgListing");
            binding.appbarLayout.toolbar.setTitle(toolTitle);
            isSearchActivity = bundle.getBoolean("isSearchActivity", false);
        }
    }

    private void setListenersAndObservers() {
        try {
            nearByViewModel.getProgressBarStatus().observe(this, aBoolean -> {
                if (aBoolean)
                    binding.progressBar.setVisibility(View.VISIBLE);
                else
                    binding.progressBar.setVisibility(View.GONE);
            });

            nearByViewModel.getPropertyList().observe(this, mPropertyList -> {
                try {
                    if (binding.contentPropertyListing.recyclerView != null && binding.contentPropertyListing.recyclerView.getAdapter() == null) {
                        binding.contentPropertyListing.recyclerView.setLayoutManager(new LinearLayoutManager(PropertyListingActivity.this));
                        SpacesItemDecoration mSpacesItemDecoration = new SpacesItemDecoration((int) getResources().getDimension(R.dimen.dimen_10dp),
                                false);
                        binding.contentPropertyListing.recyclerView.addItemDecoration(mSpacesItemDecoration);
                        PropertyListAdapterActual mPropertyListAdapter = new PropertyListAdapterActual(PropertyListingActivity.this, mPropertyList, Defaults.SCREEN_TYPE_MY_PGS);
                        binding.contentPropertyListing.recyclerView.setAdapter(mPropertyListAdapter);
                    } else if (binding.contentPropertyListing.recyclerView != null) {
                        ((PropertyListAdapterActual) binding.contentPropertyListing.recyclerView.getAdapter()).refreshList(mPropertyList);
                        binding.contentPropertyListing.recyclerView.getAdapter().notifyDataSetChanged();
                    }

                    if (mPropertyList == null || mPropertyList.size() == 0)
                        binding.txtNoPgsFound.setVisibility(View.VISIBLE);
                    else
                        binding.txtNoPgsFound.setVisibility(View.GONE);
                } catch (Exception ex) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_GROUP_LIST);
        Utility.registerReciever(PropertyListingActivity.this, filter, objMyReceiver);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (isSearchActivity) {
            getMenuInflater().inflate(R.menu.menu_search, menu);

//            MenuItem searchItem = menu.findItem(R.id.search_action).setIcon(Utility.getToolbarIcon(PropertyListingActivity.this, (GoogleMaterial.Icon.gmd_search)));
            MenuItem searchItem = menu.findItem(R.id.search_action).setIcon(R.drawable.quantum_ic_search_grey600_24);

            searchView = (SearchView) searchItem.getActionView();
            searchView.setIconifiedByDefault(true);
            searchView.setFocusable(true);
            searchView.setIconified(false);
            searchView.setMaxWidth(Integer.MAX_VALUE);

            if (searchView != null) {
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        searchPerformed = true;
                        objRequestGetPgListing.setSearchKey(query);
                        nearByViewModel.callListingApi(objRequestGetPgListing);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        if (searchPerformed && newText.trim().equals("")) {
                            searchPerformed = false;
                        }
                        return false;
                    }
                });
            }
        } else {
            getMenuInflater().inflate(R.menu.menu_with_settings, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.search_action: {
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void gotReadStoragePermission() {

    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {

    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_GROUP_LIST)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
//                        ResponseGroupList objResponseGroupList = (ResponseGroupList) bundle.getSerializable("ResponseGroupList");
//                        if (objResponseGroupList != null && objResponseGroupList.getData() != null && objResponseGroupList.getData().getUserGroups() != null) {
//
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
