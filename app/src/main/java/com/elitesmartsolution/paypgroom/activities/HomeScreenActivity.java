package com.elitesmartsolution.paypgroom.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.databinding.ActivityBottomNavigationBasicBinding;
import com.elitesmartsolution.paypgroom.util.Tools;
import com.google.android.material.navigation.NavigationBarView;

public class HomeScreenActivity extends AppCompatActivity {

    boolean isNavigationHide = false;
    boolean isSearchBarHide = false;

    private ActivityBottomNavigationBasicBinding binding;

//    private TextView binding.searchBar.searchText;
//    private BottomNavigationView binding.navigation;
//    private View search_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBottomNavigationBasicBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initComponent();
    }

    private void initComponent() {

        binding.navigation.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_notifications:
                        binding.searchBar.searchText.setText(item.getTitle());
                        return true;
                    case R.id.navigation_favorites:
                        binding.searchBar.searchText.setText(item.getTitle());
                        return true;
                    case R.id.navigation_nearby:
                        binding.searchBar.searchText.setText(item.getTitle());
                        return true;
                    case R.id.navigation_my_pgs:
                        binding.searchBar.searchText.setText(item.getTitle());
                        return true;
                    case R.id.navigation_profile:
                        binding.searchBar.searchText.setText(item.getTitle());
                        Intent inte = new Intent(HomeScreenActivity.this, EditProfileActivity.class);
                        inte.putExtra("navigate", false);
                        startActivity(inte);
                        return true;
                }
                return false;
            }
        });

        NestedScrollView nested_content = findViewById(R.id.nested_scroll_view);
        nested_content.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY < oldScrollY) { // up
                    animateNavigation(false);
                    animateSearchBar(false);
                }
                if (scrollY > oldScrollY) { // down
                    animateNavigation(true);
                    animateSearchBar(true);
                }
            }
        });

        // display image
        Tools.displayImageOriginal(this, findViewById(R.id.image_1), R.drawable.image_8);
        Tools.displayImageOriginal(this, findViewById(R.id.image_2), R.drawable.image_8);
        Tools.displayImageOriginal(this, findViewById(R.id.image_3), R.drawable.image_8);
        Tools.displayImageOriginal(this, findViewById(R.id.image_4), R.drawable.image_8);
        Tools.displayImageOriginal(this, findViewById(R.id.image_5), R.drawable.image_8);
        Tools.displayImageOriginal(this, findViewById(R.id.image_6), R.drawable.image_8);
        Tools.displayImageOriginal(this, findViewById(R.id.image_7), R.drawable.image_8);

        (findViewById(R.id.bt_menu)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    private void animateNavigation(final boolean hide) {
        if (isNavigationHide && hide || !isNavigationHide && !hide) return;
        isNavigationHide = hide;
        int moveY = hide ? (2 * binding.navigation.getHeight()) : 0;
        binding.navigation.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
    }

    private void animateSearchBar(final boolean hide) {
        if (isSearchBarHide && hide || !isSearchBarHide && !hide) return;
        isSearchBarHide = hide;
        int moveY = hide ? -(2 * binding.searchBar.searchBarCardView.getHeight()) : 0;
        binding.searchBar.searchBarCardView.animate().translationY(moveY).setStartDelay(100).setDuration(300).start();
    }
}
