package com.elitesmartsolution.paypgroom.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import androidx.annotation.Nullable;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.databinding.ActivityVerifyOtpBinding;
import com.elitesmartsolution.paypgroom.models.RequestOTP;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;

import java.util.concurrent.TimeUnit;

/**
 * The Main Activity used to display Albums / Media.
 */
public class VerifyOTPScreen extends SharedActivity {

    private ActivityVerifyOtpBinding binding;

    private MyReceiver objMyReceiver;
    private String mobileNumber = "";
    private RequestOTP objRequestOTP;
    private CountDownTimer objCountDownTimer;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityVerifyOtpBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();
        registerReciever();

        initUi();
    }

    private void initUi() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            objRequestOTP = (RequestOTP) getIntent().getExtras().getSerializable("objRequestOTP");

            mobileNumber = objRequestOTP.getMobile();
            binding.contentMainVerifyOtpActivity.description.setText(binding.contentMainVerifyOtpActivity.description.getText().toString() + " " + mobileNumber + ".");
        }

        binding.appbarLayout.toolbar.setTitle(getResources().getString(R.string.lbl_verify) + " " + mobileNumber);
        setSupportActionBar(binding.appbarLayout.toolbar);
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) binding.appbarLayout.toolbar.getLayoutParams();
        params.setScrollFlags(0);

        Utility.setStatusBarColor(VerifyOTPScreen.this);

        binding.fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValid()) {
                    ApiCallerUtility.callVerifyOTPApi(VerifyOTPScreen.this, objRequestOTP, binding.contentMainVerifyOtpActivity.editOTP.getText().toString().trim());
                }
            }
        });

        objCountDownTimer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                try {
                    if (binding.contentMainVerifyOtpActivity.lblTimer != null)
                        binding.contentMainVerifyOtpActivity.lblTimer.setText("" + String.format("%d:%d",
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onFinish() {
                try {
                    if (binding.contentMainVerifyOtpActivity.lblTimer != null) {
                        binding.contentMainVerifyOtpActivity.lblTimer.setText("0:00");
                        binding.contentMainVerifyOtpActivity.lblResendSMS.setEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

        binding.contentMainVerifyOtpActivity.lblResendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (objCountDownTimer != null) {
                    binding.contentMainVerifyOtpActivity.lblResendSMS.setEnabled(false);
                    objCountDownTimer.start();
                    ApiCallerUtility.callRequestOTPApi(VerifyOTPScreen.this, objRequestOTP, false);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        objCountDownTimer.cancel();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_OTP_RECEIVED);
        Utility.registerReciever(VerifyOTPScreen.this, filter, objMyReceiver);
    }

    private boolean isFormValid() {
        if (binding.contentMainVerifyOtpActivity.editOTP.getText().toString().trim().length() < 6) {
            Snackbar.make(binding.contentMainVerifyOtpActivity.editOTP, "Wrong OTP", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    public void gotReadStoragePermission() {

    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {

    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_OTP_RECEIVED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        String otp = bundle.getString("OTP", "");
                        binding.contentMainVerifyOtpActivity.editOTP.getHandler().post(new Runnable() {
                            @Override
                            public void run() {
                                binding.contentMainVerifyOtpActivity.editOTP.setText(otp);
                                if (isFormValid()) {
                                    ApiCallerUtility.callVerifyOTPApi(VerifyOTPScreen.this, objRequestOTP, binding.contentMainVerifyOtpActivity.editOTP.getText().toString());
                                }
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
