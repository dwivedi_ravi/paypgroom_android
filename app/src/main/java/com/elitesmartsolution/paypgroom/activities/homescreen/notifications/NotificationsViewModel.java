package com.elitesmartsolution.paypgroom.activities.homescreen.notifications;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.elitesmartsolution.paypgroom.models.getallnotifications.RequestGetAllNotification;
import com.elitesmartsolution.paypgroom.models.getallnotifications.UserNotificationsItem;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

import java.util.List;

public class NotificationsViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<List<UserNotificationsItem>> mNotifications;
    private MutableLiveData<Boolean> mShowProgressBar;
    private Context context;

    public NotificationsViewModel(Application context) {
        super(context);
        this.context = context;
        mText = new MutableLiveData<>();
        mText.setValue("This is Inbox fragment");
        mShowProgressBar = new MutableLiveData<>();
        mShowProgressBar.setValue(false);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<Boolean> getProgressBarStatus() {
        return mShowProgressBar;
    }

    public LiveData<List<UserNotificationsItem>> getNotifications() {
        if (mNotifications == null) {
            mNotifications = new MutableLiveData<>();

            RequestGetAllNotification objRequestGetAllNotification = new RequestGetAllNotification();
            if (Prefs.getUserId().equals(""))
                objRequestGetAllNotification.setToUserId("-1");
            else
                objRequestGetAllNotification.setToUserId(Prefs.getUserId());
            objRequestGetAllNotification.setFromUserId("");
            ApiCallerUtility.callGetAllNotificationsApi(context, mNotifications, mShowProgressBar, objRequestGetAllNotification);
        }
        return mNotifications;
    }

//    public LiveData<List<UserProfile>> getUsers() {
//        if (mUsers == null) {
//            mUsers = new MutableLiveData<>();
//            loadUsers();
//        }
//        return mUsers;
//    }
//
//    private void loadUsers() {
//        // Do an asynchronous operation to fetch users.
//        List<UserProfile> userList = new ArrayList<>();
//
//        for (int i = 0; i < 20; i++) {
//            UserProfile objUser = new UserProfile();
//            if (i % 2 == 0) {
//                objUser.setFirstName("Roberts");
//            } else {
//                objUser.setFirstName("Sarah Scott");
//            }
//            objUser.setLastName(context.getResources().getString(R.string.middle_lorem_ipsum));
//            userList.add(objUser);
//        }
//
//        mUsers.setValue(userList);
//    }
}