package com.elitesmartsolution.paypgroom.activities.coupons;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.elitesmartsolution.paypgroom.models.coupons.CoupansItem;
import com.elitesmartsolution.paypgroom.models.my_bookings.RequestMyBookings;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

import java.util.List;

public class CouponsViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<List<CoupansItem>> mCouponsList;
    private MutableLiveData<Boolean> mShowProgressBar;
    private Context context;

    public CouponsViewModel(Application context) {
        super(context);
        this.context = context;
        mText = new MutableLiveData<>();
        mText.setValue("This is coupon fragment");
        mShowProgressBar = new MutableLiveData<>();
        mShowProgressBar.setValue(false);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<Boolean> getProgressBarStatus() {
        return mShowProgressBar;
    }

    public LiveData<List<CoupansItem>> getNotifications() {
        if (mCouponsList == null) {
            mCouponsList = new MutableLiveData<>();

            RequestMyBookings objRequestMyBookings = new RequestMyBookings();
            objRequestMyBookings.setTenantId(Prefs.getUserId());
            ApiCallerUtility.callGetAllCouponsApi(context, mCouponsList, mShowProgressBar, objRequestMyBookings);
        }
        return mCouponsList;
    }
}