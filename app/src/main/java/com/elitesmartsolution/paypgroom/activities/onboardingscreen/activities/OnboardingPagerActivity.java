package com.elitesmartsolution.paypgroom.activities.onboardingscreen.activities;

import android.animation.ArgbEvaluator;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.onboardingscreen.adapter.SectionsPagerAdapter;
import com.elitesmartsolution.paypgroom.databinding.ActivityPagerBinding;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

public class OnboardingPagerActivity extends AppCompatActivity {

    static final String TAG = "OnboardingPagerActivity";
    private ActivityPagerBinding binding;

    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private AppCompatImageView[] indicators;
    private int page = 0;   //  to track page position

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_trans80));
        }

        binding = ActivityPagerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Create the adapter that will return a fragment for each of the binding.introIndicator3
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
            binding.introBtnNext.setImageDrawable(
                    Utility.tintMyDrawable(ContextCompat.getDrawable(this, R.drawable.ic_chevron_right_24dp), Color.WHITE)
            );

        binding.introIndicator3.setVisibility(View.GONE);

        indicators = new AppCompatImageView[]{binding.introIndicator0, binding.introIndicator1, binding.introIndicator2};

        // Set up the ViewPager with the sections adapter.
        binding.container.setAdapter(mSectionsPagerAdapter);

        binding.container.setCurrentItem(page);
        updateIndicators(page);

//        final int color1 = ContextCompat.getColor(this, R.color.green);
//        final int color2 = ContextCompat.getColor(this, R.color.orange);
//        final int color3 = ContextCompat.getColor(this, R.color.blue);
//        final int color4 = ContextCompat.getColor(this, R.color.cyan);

        final int color1 = ContextCompat.getColor(this, R.color.colorPrimary);
        final int color2 = ContextCompat.getColor(this, R.color.green_700);
        final int color3 = ContextCompat.getColor(this, R.color.blue);

        final int[] colorList = new int[]{color1, color2, color3};

        final ArgbEvaluator evaluator = new ArgbEvaluator();

        binding.container.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                /*
                color update
                 */
//                int colorUpdate = (Integer) evaluator.evaluate(positionOffset, colorList[position], colorList[position == 3 ? position : position + 1]);
                int colorUpdate = (Integer) evaluator.evaluate(positionOffset, colorList[position], colorList[position == 2 ? position : position + 1]);
                binding.container.setBackgroundColor(colorUpdate);
            }

            @Override
            public void onPageSelected(int position) {

                page = position;

                updateIndicators(page);

                switch (position) {
                    case 0:
                        binding.container.setBackgroundColor(color1);
                        break;
                    case 1:
                        binding.container.setBackgroundColor(color2);
                        break;
                    case 2:
                        binding.container.setBackgroundColor(color3);
                        break;
                }

                binding.introBtnNext.setVisibility(position == 2 ? View.GONE : View.VISIBLE);
                binding.introBtnFinish.setVisibility(position == 2 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.introBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page += 1;
                binding.container.setCurrentItem(page, true);
            }
        });

        binding.introBtnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Prefs.setIsUserFirstTimeInSharedPreferences(false);
                finish();
            }
        });

        binding.introBtnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //  update 1st time pref
                Prefs.setIsUserFirstTimeInSharedPreferences(false);
            }
        });
    }

    void updateIndicators(int position) {
        for (int i = 0; i < indicators.length; i++) {
            indicators[i].setBackgroundResource(
                    i == position ? R.drawable.indicator_selected : R.drawable.indicator_unselected
            );
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}