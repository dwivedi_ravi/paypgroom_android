package com.elitesmartsolution.paypgroom.activities.homescreenowner.mypglisting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.AddPGActivity;
import com.elitesmartsolution.paypgroom.adapters.PropertyListAdapterActual;
import com.elitesmartsolution.paypgroom.databinding.FragmentOwnerPgListingBinding;
import com.elitesmartsolution.paypgroom.util.SpacesItemDecoration;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;

public class OwnerPgListingFragment extends Fragment {

    private FragmentOwnerPgListingBinding binding;
    private OwnerPgListingViewModel ownerPgListingViewModel;
    private MyReceiver objMyReceiver;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ownerPgListingViewModel =
                new ViewModelProvider(this).get(OwnerPgListingViewModel.class);

        binding = FragmentOwnerPgListingBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        setListenersAndObservers();
        objMyReceiver = new MyReceiver();
        registerReciever();
        return root;
    }

    private void setListenersAndObservers() {
        try {
//        ownerPgListingViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });

            ownerPgListingViewModel.getProgressBarStatus().observe(getViewLifecycleOwner(), aBoolean -> {
                if (aBoolean)
                    binding.progressBar.setVisibility(View.VISIBLE);
                else
                    binding.progressBar.setVisibility(View.GONE);
            });

            ownerPgListingViewModel.getPropertyList().observe(getViewLifecycleOwner(), mPropertyList -> {
                try {
                    if (binding.mRecyclerView != null && binding.mRecyclerView.getAdapter() == null) {
                        binding.mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                        SpacesItemDecoration mSpacesItemDecoration = new SpacesItemDecoration((int) getResources().getDimension(R.dimen.dimen_10dp),
                                false);
                        binding.mRecyclerView.addItemDecoration(mSpacesItemDecoration);
                        PropertyListAdapterActual mPropertyListAdapter = new PropertyListAdapterActual(getActivity(), mPropertyList, Defaults.SCREEN_TYPE_FAVOURITES_FRAGMENT);
                        binding.mRecyclerView.setAdapter(mPropertyListAdapter);
                    } else if (binding.mRecyclerView != null) {
                        ((PropertyListAdapterActual) binding.mRecyclerView.getAdapter()).refreshList(mPropertyList);
                        binding.mRecyclerView.getAdapter().notifyDataSetChanged();
                    }
                    if (mPropertyList == null || mPropertyList.size() == 0)
                        binding.txtNoPropertiesFound.setVisibility(View.VISIBLE);
                    else
                        binding.txtNoPropertiesFound.setVisibility(View.GONE);
                } catch (Exception ex) {

                }
            });

//            CALL LISTING API FOR THE FIRST TIME AND THEN ON REFRESH BROADCAST AS WELL
            ownerPgListingViewModel.callMyPgListingApi();

            binding.btnAddPG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), AddPGActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("toolTitle", getResources().getString(R.string.title_refundable_security));
                    bundle.putString("toolShortDescription", getResources().getString(R.string.tool_short_description_refundable_security));
                    bundle.putString("toolLongDescription", getResources().getString(R.string.tool_long_description_refundable_security));
                    intent.putExtra("bundle", bundle);
                    startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_OWNERS_PG_LISTING);
        Utility.registerReciever(getActivity(), filter, objMyReceiver);
    }

    @Override
    public void onDestroy() {
        Utility.unRegisterReciever(getActivity(), objMyReceiver);
        super.onDestroy();
    }


    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_OWNERS_PG_LISTING)) {
                try {
                    ownerPgListingViewModel.callMyPgListingApi();

//                    final Bundle bundle = intent.getExtras().getBundle("bundle");
//                    if (bundle != null) {
//                        ResponseVerifyOTP objResponseVerifyOTP = (ResponseVerifyOTP) bundle.getSerializable("ResponseVerifyOTP");
//                        if (objResponseVerifyOTP != null) {
//                            if (objResponseVerifyOTP.getMessage().equals(Defaults.FAVORITE_STATUS_SELECTED)) {
//                            } else if (objResponseVerifyOTP.getMessage().equals(Defaults.FAVORITE_STATUS_UN_SELECTED)) {
//                            }
//                        }
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
