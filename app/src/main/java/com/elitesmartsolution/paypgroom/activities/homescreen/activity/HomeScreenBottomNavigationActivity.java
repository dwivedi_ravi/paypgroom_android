package com.elitesmartsolution.paypgroom.activities.homescreen.activity;

import android.Manifest;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.activities.coupons.CouponsFragment;
import com.elitesmartsolution.paypgroom.databinding.ActivityHomeScreenBottomNavigationBinding;
import com.elitesmartsolution.paypgroom.firebasenotifications.firebaseutils.NotificationUtils;
import com.elitesmartsolution.paypgroom.util.PermissionUtils;
import com.elitesmartsolution.paypgroom.util.Utility;

public class HomeScreenBottomNavigationActivity extends SharedActivity {

    private ActivityHomeScreenBottomNavigationBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeScreenBottomNavigationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

//        COMMENTED BY RAVI- FOR DISABLING ACTION BAR FOR THE ACTIVITY.
//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.navigation_nearby, R.id.navigation_favorites, R.id.navigation_my_pgs, R.id.navigation_notifications, R.id.navigation_profile)
//                .build();
//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
//        getSupportActionBar().setBackgroundDrawable(null);

        NavigationUI.setupWithNavController(binding.navView, navController);

//        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId()) {
//                    case R.id.navigation_notifications:
////                        mTextMessage.setText(item.getTitle());
//                        return true;
//                    case R.id.navigation_favorites:
////                        mTextMessage.setText(item.getTitle());
//                        return true;
//                    case R.id.navigation_nearby:
////                        mTextMessage.setText(item.getTitle());
//                        return true;
//                    case R.id.navigation_my_pgs:
////                        mTextMessage.setText(item.getTitle());
//                        return true;
//                    case R.id.navigation_profile:
////                        mTextMessage.setText(item.getTitle());
////                        Intent inte = new Intent(HomeScreenBottomNavigationActivity.this, EditProfileActivity.class);
////                        inte.putExtra("navigate", false);
////                        startActivity(inte);
//                        return true;
//                }
//                return false;
//            }
//        });

        NotificationUtils.registerPushTokenIfMissing(getApplicationContext());
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                removeCouponsFragment();
            }
        });

        Utility.checkAndShowIntroScreen(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!PermissionUtils.checkForPassedPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) || !PermissionUtils.checkForPassedPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            askForLocationPermission();
        }
        Utility.showAppExitAleartOnLoginChange(HomeScreenBottomNavigationActivity.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        removeCouponsFragment();
    }

    private void removeCouponsFragment() {
        try {
            CouponsFragment fragment = (CouponsFragment) getSupportFragmentManager().findFragmentByTag(getResources().getString(R.string.lbl_menu_coupons));
            if (fragment != null) { // and then you define a method allowBackPressed with the logic to allow back pressed or not
                super.onBackPressed();
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void gotReadStoragePermission() {

    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {
        if (isPermissionGranted) {
//            initLocationSettings();
        } else {
//            Toast.makeText(HomeScreenBottomNavigationActivity.this, getResources().getString(R.string.location_permission), Toast.LENGTH_SHORT).show();
        }
    }
}
