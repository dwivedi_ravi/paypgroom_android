package com.elitesmartsolution.paypgroom.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.App;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.BaseActivity;
import com.elitesmartsolution.paypgroom.adapters.GuestListAdapter;
import com.elitesmartsolution.paypgroom.database.DBHandler;
import com.elitesmartsolution.paypgroom.databinding.ActivityPgCheckoutBinding;
import com.elitesmartsolution.paypgroom.fragments.DialogPaymentSuccessFragment;
import com.elitesmartsolution.paypgroom.models.bookpg.RequestBookPg;
import com.elitesmartsolution.paypgroom.models.bookpg.ResponseBookPg;
import com.elitesmartsolution.paypgroom.models.payuresponse.PayuTransactionResponse;
import com.elitesmartsolution.paypgroom.models.profile.UserProfile;
import com.elitesmartsolution.paypgroom.models.verifychecksum.RequestVerifyChecksum;
import com.elitesmartsolution.paypgroom.models.verifychecksum.ResponseVerifyChecksum;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.payumoney.AppEnvironment;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.payu.base.models.ErrorResponse;
import com.payu.base.models.PayUPaymentParams;
import com.payu.base.models.PaymentMode;
import com.payu.checkoutpro.PayUCheckoutPro;
import com.payu.checkoutpro.models.PayUCheckoutProConfig;
import com.payu.checkoutpro.utils.PayUCheckoutProConstants;
import com.payu.ui.model.listeners.PayUCheckoutProListener;
import com.payu.ui.model.listeners.PayUHashGenerationListener;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;

public class PgCheckoutActivity extends BaseActivity implements View.OnClickListener {

    public static final String TAG = "PgCheckoutActivity : ";

    private ActivityPgCheckoutBinding binding;

    private UserProfile objUserProfile;
    private RequestBookPg objRequestBookPg;
    private boolean isDisableExitConfirmation = false;
    private boolean isOverrideResultScreen = true;
    private MyReceiver objMyReceiver;
    private boolean isShowingDialog = false, isProduction;
    private String salt = "", hashName = "", hashData = "", checkSum = "";
    private PayUHashGenerationListener myHashGenerationListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityPgCheckoutBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();
        registerReciever();

        setSupportActionBar(binding.customToolbar);
        binding.customToolbar.setTitleTextColor(Color.WHITE);
        binding.customToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.customToolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.customToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        initListeners();
        setUpUserDetails();
    }

    private void initListeners() {
        binding.logoutButton.setOnClickListener(this);
        binding.payNowButton.setOnClickListener(this);
        binding.payAtPgButton.setOnClickListener(this);
    }

    private void setUpUserDetails() {
        objUserProfile = DBHandler.getInstance(this).getProfileInfoFromDB();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            objRequestBookPg = (RequestBookPg) bundle.getSerializable("objRequestBookPg");
        }

        if (objRequestBookPg != null) {
            binding.txtMonths.setText(objRequestBookPg.getNumberOfMonths());
            binding.txtCheckIn.setText(objRequestBookPg.getFromDate());
            binding.txtCheckOut.setText(objRequestBookPg.getToDate());
            binding.txtRoomOccupancyType.setText(objRequestBookPg.getRoomOccupancyType());
            binding.txtNumberOfQuests.setText(objRequestBookPg.getNumberOfPersons());

            binding.tvPricepermonth.setText("Rs. " + objRequestBookPg.getBookingPrice() + "/-");
            binding.tvCleaningfee.setText("Rs. " + objRequestBookPg.getCleaningFee() + "/-");
            binding.tvSecuritydeposit.setText("Rs. " + objRequestBookPg.getSecurityDeposit() + "/-");

            binding.txtTotalAmount.setText(getResources().getString(R.string.lbl_total_amount) + " " + objRequestBookPg.getTotalAmount());
            binding.txtDiscount.setText(getResources().getString(R.string.lbl_discount) + " " + objRequestBookPg.getDiscount());
            binding.txtPriceAfterDiscount.setText(getResources().getString(R.string.lbl_amount_to_pay));
            binding.txtAmountToPay.setText(objRequestBookPg.getTotalAmountAfterDiscount() + "/-");

            if (!objRequestBookPg.getDiscount().isEmpty()) {
                binding.txtDiscount.setVisibility(View.VISIBLE);
                binding.txtAmountToPay.setText(objRequestBookPg.getTotalAmountAfterDiscount() + "/-");
            } else {
                binding.txtDiscount.setVisibility(View.GONE);
                binding.txtAmountToPay.setText(objRequestBookPg.getTotalAmount() + "/-");
            }

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            binding.rcvQuests.setLayoutManager(linearLayoutManager);
            GuestListAdapter mGuestListAdapter = new GuestListAdapter(this, objRequestBookPg.getQuests(), false);
            binding.rcvQuests.setAdapter(mGuestListAdapter);
        }
        binding.logoutButton.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.payNowButton.setEnabled(true);
        binding.payAtPgButton.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_now_button:
                binding.payNowButton.setEnabled(false);
                objRequestBookPg.setPaymentMode(Defaults.PAYMENT_MODE_ONLINE);
                objRequestBookPg.setTransactionStatus(Defaults.TRANSACTION_STATUS_PENDING);
                launchPayUMoneyFlow(preparePayUBizParams());
                break;
            case R.id.pay_at_pg_button:
                binding.payAtPgButton.setEnabled(false);
                objRequestBookPg.setPaymentMode(Defaults.PAYMENT_MODE_OFFLINE);
                objRequestBookPg.setTransactionStatus(Defaults.TRANSACTION_STATUS_SUCCESSFUL);
                launchPayAtPgFlow();
                break;
            case R.id.logout_button:
                break;
        }
    }

    private PayUCheckoutProConfig getCheckoutProConfig() {
        PayUCheckoutProConfig checkoutProConfig = new PayUCheckoutProConfig();

        checkoutProConfig.setPaymentModesOrder(getCheckoutOrderList());
//        checkoutProConfig.setOfferDetails(getOfferDetailsList());
        // uncomment below code for performing enforcement
//        checkoutProConfig.setEnforcePaymentList(getEnforcePaymentList());
        checkoutProConfig.setShowCbToolbar(true);
        checkoutProConfig.setAutoSelectOtp(true);
        checkoutProConfig.setAutoApprove(true);
        checkoutProConfig.setSurePayCount(0);
        checkoutProConfig.setShowExitConfirmationOnPaymentScreen(true);
        checkoutProConfig.setShowExitConfirmationOnCheckoutScreen(true);
        checkoutProConfig.setMerchantName(getResources().getString(R.string.payu_merchant_name));
        checkoutProConfig.setMerchantLogo(R.mipmap.ic_launcher);
        checkoutProConfig.setWaitingTime(45000);  //  Waiting for OTP Timeout
        checkoutProConfig.setMerchantResponseTimeout(30000);  //  The time period that PayU will wait for you to load surl/furl before passing the transaction response back to the app.
//        checkoutProConfig.setCustomNoteDetails(getCustomeNoteList());
//        checkoutProConfig.setCartDetails(reviewOrderAdapter.getOrderDetailsList());
        return checkoutProConfig;
    }

    private PayUPaymentParams preparePayUBizParams() {
        HashMap<String, Object> additionalParams = new HashMap<>();
        additionalParams.put(PayUCheckoutProConstants.CP_UDF1, "udf1");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF2, "udf2");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF3, "udf3");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF4, "udf4");
        additionalParams.put(PayUCheckoutProConstants.CP_UDF5, "udf5");

        //Below params should be passed only when integrating Multi-currency support
        //TODO Please pass your own Merchant Access Key below as provided by your Key Account Manager at PayU.
//        additionalParams.put(PayUCheckoutProConstants.CP_MERCHANT_ACCESS_KEY,CP_MERCHANT_ACCESS_KEY testMerchantAccessKey);

//        Below params should be passed only when sodexo payment option is enabled and want to show saved sodexo card
        //TODO Please pass sodexosrcid for sodexo card which will be recieved in new sodexo card txn response
//        additionalParams.put(PayUCheckoutProConstants.SODEXO_SOURCE_ID, sodexosrcid);

        double amount = 0;
        try {
            if (objRequestBookPg.getTotalAmountAfterDiscount() != null && !objRequestBookPg.getTotalAmountAfterDiscount().isEmpty())
                amount = Double.parseDouble(objRequestBookPg.getTotalAmountAfterDiscount());
            else
                amount = Double.parseDouble(objRequestBookPg.getTotalAmount());

//            DISCOUNT COMMENTED BY RAVI
//            amount = Double.parseDouble(objRequestBookPg.getTotalAmount());
//            DISCOUNT COMMENTED BY RAVI TILL HERE
        } catch (Exception e) {
            e.printStackTrace();
        }

//        amount = 1.0;

        String txnId = objRequestBookPg.getTransactionId();
        String productName = objRequestBookPg.getPgName() != null ? objRequestBookPg.getPgName() : "";
        String firstName = (objUserProfile.getFirstName() != null ? objUserProfile.getFirstName().trim() : "") + " " + (objUserProfile.getLastName() != null ? objUserProfile.getLastName().trim() : "");
        String phone = objUserProfile.getMobile() != null ? objUserProfile.getMobile().trim() : "";
        String email = objUserProfile.getEmail() != null ? objUserProfile.getEmail().trim() : "";

        AppEnvironment appEnvironment = ((App) getApplication()).getAppEnvironment();
        objRequestBookPg.setMerchantId(appEnvironment.merchant_ID());
        objRequestBookPg.setMerchantKey(appEnvironment.merchant_Key());
        salt = appEnvironment.salt();
        isProduction = appEnvironment.production();
        isProduction = true;

        PayUPaymentParams.Builder builder = new PayUPaymentParams.Builder();
        builder.setAmount(String.valueOf(amount))
                .setIsProduction(isProduction)
                .setProductInfo(productName)
                .setKey(appEnvironment.merchant_Key())
                .setPhone(phone)
                .setTransactionId(txnId)
                .setFirstName(firstName)
                .setEmail(email)
                .setSurl(appEnvironment.surl())
                .setFurl(appEnvironment.furl())
                .setUserCredential(appEnvironment.merchant_Key() + ":" + email)
                .setAdditionalParams(additionalParams)
                .setPayUSIParams(null)
                .setSplitPaymentDetails(null);
        PayUPaymentParams payUPaymentParams = builder.build();

        return payUPaymentParams;
    }

    /**
     * This function prepares the data for payment and launches payumoney plug n play sdk
     */
    private void launchPayUMoneyFlow(PayUPaymentParams payUPaymentParams) {
        PayUCheckoutPro.open(
                this,
                payUPaymentParams,
                getCheckoutProConfig(),
                new PayUCheckoutProListener() {
                    @Override
                    public void onPaymentSuccess(Object response) {
                        parsePayUResponse(response);
                    }

                    @Override
                    public void onPaymentFailure(Object response) {
                        parsePayUResponse(response);
                    }

                    @Override
                    public void onPaymentCancel(boolean isTxnInitiated) {
                        showSnackBar(getResources().getString(R.string.transaction_cancelled_by_user));
                    }

                    @Override
                    public void onError(ErrorResponse errorResponse) {
                        String errorMessage = errorResponse.getErrorMessage();
                        if (TextUtils.isEmpty(errorMessage))
                            errorMessage = getResources().getString(R.string.some_error_occurred);
                        showSnackBar(errorMessage);
                    }

                    @Override
                    public void setWebViewProperties(@Nullable WebView webView, @Nullable Object o) {
                        //For setting webview properties, if any. Check Customized Integration section for more details on this
                    }

                    @Override
                    public void generateHash(HashMap<String, String> valueMap, PayUHashGenerationListener hashGenerationListener) {
                        hashName = valueMap.get(PayUCheckoutProConstants.CP_HASH_NAME);
                        hashData = valueMap.get(PayUCheckoutProConstants.CP_HASH_STRING);
                        if (!TextUtils.isEmpty(hashName) && !TextUtils.isEmpty(hashData)) {
                            if (isProduction) {
                                /*
                                 * Generate Hash from your backend here
                                 * Hash should always be generated from your server side.
                                 * */
                                myHashGenerationListener = hashGenerationListener;
                                generateHashFromServer(hashName, hashData);
                            } else {
//                            Locally Calculate SHA-512 Hash here for testing purpose
                                String hash = null;
                                hash = calculateHash(hashData + salt);
                                HashMap<String, String> dataMap = new HashMap<>();
                                dataMap.put(hashName, hash);
                                hashGenerationListener.onHashGenerated(dataMap);
                            }
                        }
                    }
                }
        );
    }

    private void launchPayAtPgFlow() {
        try {
            objRequestBookPg.setMerchantId("");
            objRequestBookPg.setMerchantKey("");
            objRequestBookPg.setHashSequence("");

            ApiCallerUtility.callBookPGApi(PgCheckoutActivity.this, objRequestBookPg);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            binding.payAtPgButton.setEnabled(true);
        }
    }

    private ArrayList<PaymentMode> getCheckoutOrderList() {
        ArrayList<PaymentMode> checkoutOrderList = new ArrayList();
//        checkoutOrderList.add(new PaymentMode(PaymentType.UPI, PayUCheckoutProConstants.CP_GOOGLE_PAY));
//        checkoutOrderList.add(new PaymentMode(PaymentType.WALLET, PayUCheckoutProConstants.CP_PHONEPE));
//        checkoutOrderList.add(new PaymentMode(PaymentType.WALLET, PayUCheckoutProConstants.CP_PAYTM));
        return checkoutOrderList;
    }

    /**
     * Hash Should be generated from your sever side only.
     * <p>
     * Do not use this, you may use this only for testing.
     * This should be done from server side..
     * Do not keep salt anywhere in app.
     */
    private String calculateHash(String hashString) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
            messageDigest.update(hashString.getBytes());
            byte[] mdbytes = messageDigest.digest();
            return getHexString(mdbytes);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String getHexString(byte[] array) {
        StringBuilder hash = new StringBuilder();
        for (byte hashByte : array) {
            hash.append(Integer.toString((hashByte & 0xff) + 0x100, 16).substring(1));
        }
        return hash.toString();
    }

    public void generateHashFromServer(String hashName, String hashSequence) {
//        stringBuilder.append(appEnvironment.salt()); // TO BE DONE ON SERVER
//        String hash = hashCal(stringBuilder.toString());
        objRequestBookPg.setHashSequence(hashSequence);
        // OVERWRITING MERCHANT ID WITH HASH NAME HERE AS WE NEED TO MAP GENERATED(FROM SERVER) HASH WITH SEPARATE HASH NAMES.
        objRequestBookPg.setMerchantId(hashName);

        ApiCallerUtility.callBookPGApi(PgCheckoutActivity.this, objRequestBookPg);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        try {
//            super.onActivityResult(requestCode, resultCode, data);
//            // Result Code is -1 send from Payumoney activity
//            Log.d("MainActivity", "request code " + requestCode + " resultcode " + resultCode);
//            if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
//                    null) {
//                TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
//                        .INTENT_EXTRA_TRANSACTION_RESPONSE);
//                ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);
//
//                RequestVerifyChecksum objRequestVerifyChecksum = new RequestVerifyChecksum();
////            objRequestVerifyChecksum.setTransectionId(objRequestBookPg.getTransactionId());
//                objRequestVerifyChecksum.setTenantId(Prefs.getUserId());
//                objRequestVerifyChecksum.setOwnerId(objRequestBookPg.getOwnerId());
//                objRequestVerifyChecksum.setPgId(objRequestBookPg.getPgId());
//
//                // Check which object is non-null
//                if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
//                    Gson gson = new Gson();
//                    PayuTransactionResponse objPayuTransactionResponse = gson.fromJson(transactionResponse.getPayuResponse(), PayuTransactionResponse.class);
//                    objRequestBookPg.setPayuTransactionResponse(objPayuTransactionResponse);
//                    objRequestBookPg.setTransactionStatus(transactionResponse.getTransactionStatus() + "");
//
////                AppEnvironment appEnvironment = ((App) getApplication()).getAppEnvironment();
////                String responseHashSequence = appEnvironment.salt() + "|" + objPayuTransactionResponse.getResult().getStatus()
////                        + "|||||||||||" + objPayuTransactionResponse.getResult().getEmail()
////                        + "|" + objPayuTransactionResponse.getResult().getFirstname()
////                        + "|" + objPayuTransactionResponse.getResult().getProductinfo()
////                        + "|" + objPayuTransactionResponse.getResult().getAmount()
////                        + "|" + objPayuTransactionResponse.getResult().getTxnid()
////                        + "|" + objPayuTransactionResponse.getResult().getKey();
////                String hash = hashCal(responseHashSequence);
//
////                    // Response from Payumoney
////                    String payuResponse = transactionResponse.getPayuResponse();
////                    // Response from SURl and FURL
////                    String merchantResponse = transactionResponse.getTransactionDetails();
////                    new AlertDialog.Builder(this)
////                            .setCancelable(false)
////                            .setMessage("Payu's Data : " + payuResponse + "\n\n\n Merchant's Data: " + merchantResponse)
////                            .setPositiveButton(android.R.string.ok, (dialog, whichButton) -> dialog.dismiss()).show();
//
//                    String responseHashSequence = "|" + objPayuTransactionResponse.getResult().getStatus()
//                            + "|||||||||||" + objPayuTransactionResponse.getResult().getEmail()
//                            + "|" + objPayuTransactionResponse.getResult().getFirstname()
//                            + "|" + objPayuTransactionResponse.getResult().getProductinfo()
//                            + "|" + objPayuTransactionResponse.getResult().getAmount()
//                            + "|" + objPayuTransactionResponse.getResult().getTxnid()
//                            + "|" + objPayuTransactionResponse.getResult().getKey();
//
//                    objRequestVerifyChecksum.setTransectionId(objPayuTransactionResponse.getResult().getTxnid());
//                    objRequestVerifyChecksum.setHashSequence(responseHashSequence);
//                    objRequestVerifyChecksum.setPayuCheckSum(objPayuTransactionResponse.getResult().getHash());
//                    objRequestVerifyChecksum.setPayuResponseJson(transactionResponse.getPayuResponse());
////                    objRequestVerifyChecksum.setPayuTransactionStatus(objPayuTransactionResponse.getResult().getStatus());
//                    objRequestVerifyChecksum.setPayuTransactionStatus(transactionResponse.getTransactionStatus() + "");
//                    objRequestVerifyChecksum.setCouponId(objRequestBookPg.getAppliedCoupanId());
//
//                    if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
//                        //Success Transaction
//                    } else {
//                        //Failure Transaction
//                    }
//                } else if (resultModel != null && resultModel.getError() != null) {
//                    Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
//
//                    objRequestVerifyChecksum.setHashSequence("ERROR_RESPONSE_FROM_PAYU");
//                    objRequestVerifyChecksum.setPayuCheckSum("ERROR_RESPONSE_FROM_PAYU");
//                    objRequestVerifyChecksum.setPayuResponseJson(resultModel.getError().getTransactionResponse().getJsonResponse());
//                    objRequestVerifyChecksum.setPayuTransactionStatus(resultModel.getError().getTransactionResponse().getTransactionStatus() + "");
//
//                    objRequestBookPg.setTransactionStatus(resultModel.getError().getTransactionResponse().getTransactionStatus() + "");
//                } else {
//                    Log.d(TAG, "Both objects are null!");
//
//                    objRequestVerifyChecksum.setHashSequence("NULL");
//                    objRequestVerifyChecksum.setPayuCheckSum("NULL");
//                    objRequestVerifyChecksum.setPayuResponseJson("NULL");
//                    objRequestVerifyChecksum.setPayuTransactionStatus("NULL");
//
//                    objRequestBookPg.setTransactionStatus("FAILED");
//                }
//
////            TEMPORARILY COMMENTED DUE TO PAYMENT METHOD RESTRICTIONS TO DEBIT AND CREDIT CARDS.
////            ApiCallerUtility.callVerifyChecksumAndFinalizeBookingApi(PgCheckoutActivity.this, objRequestVerifyChecksum);
//
////          TEMPORARILY ADDED BY ME TO SUPPORT ONLY DEBIT CARD, CREDIT CARD AND UPI TRANSACTIONS FROM PAYU.
//                if (objRequestBookPg != null && objRequestBookPg.getPayuTransactionResponse() != null && objRequestBookPg.getPayuTransactionResponse().getResult() != null && objRequestBookPg.getPayuTransactionResponse().getResult().getMode() != null) {
//                    if (objRequestBookPg.getPayuTransactionResponse().getResult().getMode().equalsIgnoreCase("DC") || objRequestBookPg.getPayuTransactionResponse().getResult().getMode().equalsIgnoreCase("CC") || objRequestBookPg.getPayuTransactionResponse().getResult().getMode().equalsIgnoreCase("UPI")) {
//                        ApiCallerUtility.callVerifyChecksumAndFinalizeBookingApi(PgCheckoutActivity.this, objRequestVerifyChecksum);
//                    } else {
//                        AlertDialog textDialog = Utility.getTextDialog_Material(PgCheckoutActivity.this, PgCheckoutActivity.this.getResources().getString(R.string.dialog_title_error), PgCheckoutActivity.this.getResources().getString(R.string.dialog_message_booking_failed));
//                        textDialog.setButton(DialogInterface.BUTTON_POSITIVE, PgCheckoutActivity.this.getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                textDialog.dismiss();
//                            }
//                        });
//                        textDialog.show();
//                    }
//                } else {
//                    ApiCallerUtility.callVerifyChecksumAndFinalizeBookingApi(PgCheckoutActivity.this, objRequestVerifyChecksum);
//                }
////          TEMPORARILY ADDED BY ME TO SUPPORT ONLY DEBIT CARD AND CREDIT CARD TRANSACTIONS FROM PAYU TILL HERE.
//            }
//        } catch (Exception ex) {
//
//        }
//    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_HASH_RECEIVED);
        filter.addAction(Defaults.ACTION_ONLINE_TRANSACTION_COMPLETE_FROM_SERVER_END);
        filter.addAction(Defaults.ACTION_OFFLINE_TRANSACTION_COMPLETE_FROM_SERVER_END);
        Utility.registerReciever(PgCheckoutActivity.this, filter, objMyReceiver);
    }

    private void showDialogPaymentSuccess(RequestBookPg objRequestBookPg) {
        isShowingDialog = true;
        FragmentManager fragmentManager = getSupportFragmentManager();
        DialogPaymentSuccessFragment newFragment = new DialogPaymentSuccessFragment(objRequestBookPg);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isShowingDialog) {
            Utility.sendUpdateListBroadCast(Defaults.ACTION_FINISH_BOOKING_SCREEN, PgCheckoutActivity.this, null);
            PgCheckoutActivity.this.finish();
        }
    }

    private void showSnackBar(String message) {
        Snackbar.make(binding.payNowButton, message, Snackbar.LENGTH_LONG).show();
    }

    private void showAlertDialog(Object response) {
        HashMap<String, Object> result = (HashMap<String, Object>) response;
        new AlertDialog.Builder(this)
                .setCancelable(false)
                .setMessage(
                        "Payu's Data : " + result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE) + "\n\n\n Merchant's Data: " + result.get(
                                PayUCheckoutProConstants.CP_MERCHANT_RESPONSE
                        )
                )
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    private void parsePayUResponse(Object response) {
        try {
            HashMap<String, Object> result = (HashMap<String, Object>) response;
            String payUData = result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE) + "";
            String merchantData = "Merchant's Data: " + result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);

            RequestVerifyChecksum objRequestVerifyChecksum = new RequestVerifyChecksum();
            objRequestVerifyChecksum.setTenantId(Prefs.getUserId());
            objRequestVerifyChecksum.setOwnerId(objRequestBookPg.getOwnerId());
            objRequestVerifyChecksum.setPgId(objRequestBookPg.getPgId());

            // Check which object is non-null
            if (payUData != null && !payUData.trim().equals("")) {
                Gson gson = new Gson();

                if (payUData.contains(Defaults.PAYMENT_GATEWAY_DEBIT_CARD) || payUData.contains(Defaults.PAYMENT_GATEWAY_CREDIT_CARD) || payUData.contains(Defaults.PAYMENT_GATEWAY_NET_BANKING) || payUData.contains(Defaults.PAYMENT_GATEWAY_UPI_MODE) /*|| payUData.contains("ibibo_code")*/) {
                    PayuTransactionResponse objPayuTransactionResponse = gson.fromJson(payUData, PayuTransactionResponse.class);

                    objRequestBookPg.setPayuTransactionResponse(objPayuTransactionResponse);
                    objRequestBookPg.setTransactionStatus(objPayuTransactionResponse.getStatus() + "");

                    String responseHashSequence = "|" + objPayuTransactionResponse.getStatus()
                            + "|||||||||||" + objPayuTransactionResponse.getEmail()
                            + "|" + objPayuTransactionResponse.getFirstname()
                            + "|" + objPayuTransactionResponse.getProductinfo()
                            + "|" + objPayuTransactionResponse.getAmount()
                            + "|" + objPayuTransactionResponse.getTxnid()
                            + "|" + objPayuTransactionResponse.getKey();

                    objRequestVerifyChecksum.setTransectionId(objPayuTransactionResponse.getTxnid());

                    objRequestVerifyChecksum.setHashSequence(responseHashSequence);
                    objRequestVerifyChecksum.setPayuCheckSum(objPayuTransactionResponse.getHash());

                    objRequestVerifyChecksum.setPayuResponseJson(payUData);
                    objRequestVerifyChecksum.setPayuTransactionStatus(objPayuTransactionResponse.getStatus() + "");
                }
                objRequestVerifyChecksum.setCouponId(objRequestBookPg.getAppliedCoupanId());
            }

//          TEMPORARILY COMMENTED DUE TO PAYMENT METHOD RESTRICTIONS TO DEBIT AND CREDIT CARDS.
            ApiCallerUtility.callVerifyChecksumAndFinalizeBookingApi(PgCheckoutActivity.this, objRequestVerifyChecksum);

//          TEMPORARILY ADDED BY ME TO SUPPORT ONLY DEBIT CARD, CREDIT CARD AND UPI TRANSACTIONS FROM PAYU.
//            if (objRequestBookPg != null && objRequestBookPg.getPayuTransactionResponse() != null && objRequestBookPg.getPayuTransactionResponse().getResult() != null && objRequestBookPg.getPayuTransactionResponse().getMode() != null) {
//                if (objRequestBookPg.getPayuTransactionResponse().getMode().equalsIgnoreCase("DC") || objRequestBookPg.getPayuTransactionResponse().getMode().equalsIgnoreCase("CC") || objRequestBookPg.getPayuTransactionResponse().getMode().equalsIgnoreCase("UPI")) {
//                    ApiCallerUtility.callVerifyChecksumAndFinalizeBookingApi(PgCheckoutActivity.this, objRequestVerifyChecksum);
//                } else {
//                    AlertDialog textDialog = Utility.getTextDialog_Material(PgCheckoutActivity.this, PgCheckoutActivity.this.getResources().getString(R.string.dialog_title_error), PgCheckoutActivity.this.getResources().getString(R.string.dialog_message_booking_failed));
//                    textDialog.setButton(DialogInterface.BUTTON_POSITIVE, PgCheckoutActivity.this.getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            textDialog.dismiss();
//                        }
//                    });
//                    textDialog.show();
//                }
//            } else {
//                ApiCallerUtility.callVerifyChecksumAndFinalizeBookingApi(PgCheckoutActivity.this, objRequestVerifyChecksum);
//            }
//          TEMPORARILY ADDED BY ME TO SUPPORT ONLY DEBIT CARD AND CREDIT CARD TRANSACTIONS FROM PAYU TILL HERE.
        } catch (Exception ex) {
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_HASH_RECEIVED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseBookPg objResponseBookPg = (ResponseBookPg) bundle.getSerializable("ResponseBookPg");
                        if (objResponseBookPg != null) {
                            binding.payNowButton.setEnabled(true);

                            if (objResponseBookPg.getData().getCheckSum().isEmpty()) {
                                Toast.makeText(PgCheckoutActivity.this, "Could not generate hash", Toast.LENGTH_SHORT).show();
                            } else {
                                HashMap<String, String> dataMap = new HashMap<>();
                                checkSum = objResponseBookPg.getData().getCheckSum();
//                                dataMap.put(hashName, checkSum);
                                dataMap.put(objResponseBookPg.getData().getHashName(), checkSum);
                                myHashGenerationListener.onHashGenerated(dataMap);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (intent.getAction().equals(Defaults.ACTION_ONLINE_TRANSACTION_COMPLETE_FROM_SERVER_END)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseVerifyChecksum objResponseVerifyChecksum = (ResponseVerifyChecksum) bundle.getSerializable("ResponseVerifyChecksum");
                        if (objResponseVerifyChecksum != null && objResponseVerifyChecksum.isStatus()) {
                            if (objRequestBookPg != null) {
                                showDialogPaymentSuccess(objRequestBookPg);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (intent.getAction().equals(Defaults.ACTION_OFFLINE_TRANSACTION_COMPLETE_FROM_SERVER_END)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseBookPg objResponseBookPg = (ResponseBookPg) bundle.getSerializable("ResponseBookPg");
                        if (objResponseBookPg != null && objResponseBookPg.isStatus()) {
                            if (objRequestBookPg != null) {
                                binding.payAtPgButton.setEnabled(true);
                                showDialogPaymentSuccess(objRequestBookPg);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}