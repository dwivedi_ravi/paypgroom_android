package com.elitesmartsolution.paypgroom.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.databinding.ActivityChangePasswordBinding;
import com.elitesmartsolution.paypgroom.models.RequestChangePassword;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.android.material.snackbar.Snackbar;

/**
 * The Main Activity used to display Albums / Media.
 */
public class ChangePasswordScreen extends SharedActivity {

    private ActivityChangePasswordBinding binding;
    private MyReceiver objMyReceiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityChangePasswordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();
        registerReciever();

        initUi();
    }

    private void initUi() {
        if (getIntent() != null && getIntent().getExtras() != null) {
//            objRequestOTP = (RequestOTP) getIntent().getExtras().getSerializable("objRequestOTP");
        }

//        binding.appbarLayout.toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        setSupportActionBar(binding.appbarLayout.toolbar);
        Utility.setStatusBarColor(this);
        binding.appbarLayout.toolbar.setTitle(getResources().getString(R.string.change_password));
        binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValid()) {
                    RequestChangePassword objRequestChangePassword = new RequestChangePassword();
                    objRequestChangePassword.setMobile(Prefs.getMobileNumber());
                    objRequestChangePassword.setNewPassword(binding.editPassword.getText().toString());
                    objRequestChangePassword.setConformPassword(binding.editConfirmPassword.getText().toString());
                    ApiCallerUtility.callChangePasswordApi(ChangePasswordScreen.this, objRequestChangePassword);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_MEDIA_AFTER_PHOTO_EDITING);
        Utility.registerReciever(ChangePasswordScreen.this, filter, objMyReceiver);
    }

    private boolean isFormValid() {
        if (binding.editPassword.getText().toString().trim().length() < 4) {
            Snackbar.make(binding.editPassword, "Please enter valid password", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editConfirmPassword.getText().toString().trim().length() < 4) {
            Snackbar.make(binding.editConfirmPassword, "Please enter valid confirm password", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (!binding.editPassword.getText().toString().trim().equals(binding.editConfirmPassword.getText().toString().trim())) {
            Snackbar.make(binding.editConfirmPassword, "Passwords do not match", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    @Override
    public void gotReadStoragePermission() {

    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {

    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_REFRESH_MEDIA_AFTER_PHOTO_EDITING)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
