package com.elitesmartsolution.paypgroom.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleObserver;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.adapters.GuestListAdapter;
import com.elitesmartsolution.paypgroom.databinding.ActivityBookPgBinding;
import com.elitesmartsolution.paypgroom.interfaces.DateTimeUpdateListener;
import com.elitesmartsolution.paypgroom.models.bookpg.GuestItem;
import com.elitesmartsolution.paypgroom.models.bookpg.RequestBookPg;
import com.elitesmartsolution.paypgroom.models.coupons.RequestVerifyCoupon;
import com.elitesmartsolution.paypgroom.models.coupons.ResponseVerifyCoupon;
import com.elitesmartsolution.paypgroom.models.pglisting.PropertyListItem;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.elitesmartsolution.paypgroom.widget.DateTimePicker;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BookPGActivity extends AppCompatActivity implements View.OnTouchListener, DateTimeUpdateListener, LifecycleObserver {

    private ActivityBookPgBinding binding;

    private int basePrice = 0;
    private int totalPrice = 0;
    private int discount = 0;
    private int priceAfterDiscount = 0;
    private MyReceiver objMyReceiver;
    private PropertyListItem objPropertyListItem;
    private List<GuestItem> guestList;
    private DateTimeUpdateListener updateListener;
    private int inc = 0;
    private ResponseVerifyCoupon objResponseVerifyCoupon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityBookPgBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();
        updateListener = this;

        setSupportActionBar(binding.appbarLayout.toolbar);
        Utility.setStatusBarColor(this);
        registerReciever();

        binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.contentBookPg.btnAddQuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValidForAddGuest()) {
                    addItem();
                }
            }
        });

        binding.contentBookPg.btnApplyCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!binding.contentBookPg.etCouponCode.getText().toString().isEmpty()) {
                    RequestVerifyCoupon objRequestVerifyCoupon = new RequestVerifyCoupon();
                    objRequestVerifyCoupon.setTenantId(Prefs.getUserId());
                    objRequestVerifyCoupon.setCouponCode(binding.contentBookPg.etCouponCode.getText().toString().trim());
                    ApiCallerUtility.callApplyCouponApi(BookPGActivity.this, objRequestVerifyCoupon);
                } else {
                    Toast.makeText(BookPGActivity.this, "Please enter coupon code", Toast.LENGTH_LONG).show();
                }
            }
        });

        binding.contentBookPg.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDiscountValues();

                binding.contentBookPg.lblCouponCode.setText("");
                binding.contentBookPg.lblCouponCodeMessage.setText("");
                binding.contentBookPg.cardCouponApplied.setVisibility(View.GONE);
            }
        });

        binding.contentBookPg.editCheckIn.setOnTouchListener(this);
        binding.contentBookPg.editCheckOut.setOnTouchListener(this);

        binding.contentBookPg.editMonths.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().trim().isEmpty()) {
                    try {
                        int months = Math.abs(Integer.parseInt(s.toString().trim()));
                        setTotalPriceAsPerGuests(months);
                    } catch (NumberFormatException e) {
                        binding.contentBookPg.editMonths.setText("1");
                    }
                } else {
                    setTotalPriceAsPerGuests(1);
                }
            }
        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            objPropertyListItem = (PropertyListItem) bundle.getSerializable("currentPropertyListBO");
        }

        if (objPropertyListItem != null) {
            binding.contentBookPg.tvHousetitle.setText(objPropertyListItem.getPgName());
            binding.contentBookPg.tvHostName.setText("Owner: " + objPropertyListItem.getPgOwnerDetail().getName());
            binding.contentBookPg.tvOwnerPhone.setText("Mobile: " + objPropertyListItem.getPgOwnerDetail().getMobile());
            binding.contentBookPg.tvPgLocation.setText("Location: " + objPropertyListItem.getLocation().getLine1() + " " + objPropertyListItem.getLocation().getLine2());
            binding.contentBookPg.lblRoomOccupancyType.setText(getResources().getString(R.string.lbl_room_occupancy_type) + ": " + objPropertyListItem.getPgRoomType());

            try {
                totalPrice = Integer.parseInt(objPropertyListItem.getPgPricingDetail().getPerMonth()) + Integer.parseInt(objPropertyListItem.getPgPricingDetail().getCleaningFee()) + Integer.parseInt(objPropertyListItem.getPgPricingDetail().getSecurityDeposit());
                if (objPropertyListItem.getPgPricingDetail().getDiscount() == null || objPropertyListItem.getPgPricingDetail().getDiscount().isEmpty()) {
                    objPropertyListItem.getPgPricingDetail().setDiscount("0");
                } else if (objPropertyListItem.getPgPricingDetail().getDiscount().contains("%")) {
                    int amountForPercentage = Integer.parseInt(objPropertyListItem.getPgPricingDetail().getDiscount().substring(0, objPropertyListItem.getPgPricingDetail().getDiscount().indexOf("%")));
                    amountForPercentage = (totalPrice * amountForPercentage) / 100;
                    objPropertyListItem.getPgPricingDetail().setDiscount(amountForPercentage + "");
                }
                discount = Integer.parseInt(objPropertyListItem.getPgPricingDetail().getDiscount());
                priceAfterDiscount = totalPrice - discount;
            } catch (NumberFormatException e) {
                e.printStackTrace();
                priceAfterDiscount = totalPrice;
            }

//            DISCOUNT COMMENTED BY RAVI
            discount = 0;
            priceAfterDiscount = totalPrice;
            binding.contentBookPg.lblDiscount.setVisibility(View.GONE);
            binding.contentBookPg.lblPriceAfterDiscount.setVisibility(View.GONE);
//            DISCOUNT COMMENTED BY RAVI TILL HERE

            binding.contentBookPg.tvPricepermonth.setText("Rs. " + objPropertyListItem.getPgPricingDetail().getPerMonth() + "/-");
            binding.contentBookPg.tvCleaningfee.setText("Rs. " + objPropertyListItem.getPgPricingDetail().getCleaningFee() + "/-");
            binding.contentBookPg.tvSecuritydeposit.setText("Rs. " + objPropertyListItem.getPgPricingDetail().getSecurityDeposit() + "/-");

            basePrice = totalPrice;
            binding.contentBookPg.lblTotalAmount.setText(getResources().getString(R.string.lbl_total_amount) + " " + totalPrice + "/-");
            binding.contentBookPg.lblDiscount.setText(getResources().getString(R.string.lbl_discount) + " " + discount + "/-");
            binding.contentBookPg.lblPriceAfterDiscount.setText(getResources().getString(R.string.lbl_price_after_discount) + " " + priceAfterDiscount + "/-");
        }

        guestList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.contentBookPg.rcvQuests.setLayoutManager(linearLayoutManager);
        GuestListAdapter mGuestListAdapter = new GuestListAdapter(this, guestList, true);
        binding.contentBookPg.rcvQuests.setAdapter(mGuestListAdapter);

        binding.contentBookPg.propertyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (objPropertyListItem != null) {
                    Intent detailIntent = new Intent(BookPGActivity.this, MapsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("currentPropertyListBO", objPropertyListItem);
                    detailIntent.putExtras(bundle);
                    startActivity(detailIntent);
                }
            }
        });

        binding.btnReviewBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValidForReview()) {
                    RequestBookPg objRequestBookPg = new RequestBookPg();

                    objRequestBookPg.setUserId(Prefs.getUserId());
                    objRequestBookPg.setPgId(objPropertyListItem.getId());
                    objRequestBookPg.setTransactionId(Utility.getUniqueTransactionID());
                    objRequestBookPg.setPaymentId(objRequestBookPg.getTransactionId());
                    objRequestBookPg.setOwnerId(objPropertyListItem.getPgOwnerDetail().getOwnerId());
                    objRequestBookPg.setOwnerName(objPropertyListItem.getPgOwnerDetail().getName());

                    objRequestBookPg.setPgName(objPropertyListItem.getPgName());
                    objRequestBookPg.setNumberOfMonths(binding.contentBookPg.editMonths.getText().toString());
                    objRequestBookPg.setFromDate(binding.contentBookPg.editCheckIn.getText().toString());
                    objRequestBookPg.setToDate(binding.contentBookPg.editCheckOut.getText().toString());
//                    objRequestBookPg.setRoomOccupancyType(binding.contentBookPg.spnRoomOccupancyType.getSelectedItemPosition() + "");
//                    objRequestBookPg.setRoomOccupancyType(binding.contentBookPg.spnRoomOccupancyType.getSelectedItem() + "");
                    objRequestBookPg.setRoomOccupancyType(objPropertyListItem.getPgRoomType());
                    objRequestBookPg.setNumberOfPersons(binding.contentBookPg.editNumberOfQuests.getText().toString());

                    objRequestBookPg.setQuests(guestList);

                    objRequestBookPg.setBookingPrice(objPropertyListItem.getPgPricingDetail().getPerMonth());
                    objRequestBookPg.setSecurityDeposit(objPropertyListItem.getPgPricingDetail().getSecurityDeposit());
                    objRequestBookPg.setCleaningFee(objPropertyListItem.getPgPricingDetail().getCleaningFee());

                    objRequestBookPg.setTotalAmount(totalPrice + "");
                    objRequestBookPg.setDiscount(discount + "");
                    objRequestBookPg.setTotalAmountAfterDiscount(priceAfterDiscount + "");
//                    objRequestBookPg.setMerchantId();
//                    objRequestBookPg.setMerchantKey();

                    if (discount > 0 && objResponseVerifyCoupon != null) {
                        objRequestBookPg.setAppliedCoupanId(objResponseVerifyCoupon.getData().getId());
                        objRequestBookPg.setAppliedCoupanCode(objResponseVerifyCoupon.getData().getCoupanCode());
                        objRequestBookPg.setDiscount(discount + "");
                    } else {
                        objRequestBookPg.setAppliedCoupanId("");
                        objRequestBookPg.setAppliedCoupanCode("");
                        objRequestBookPg.setDiscount("");
                    }

                    Intent detailIntent = new Intent(BookPGActivity.this, PgCheckoutActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("objRequestBookPg", objRequestBookPg);
                    detailIntent.putExtras(bundle);
                    startActivity(detailIntent);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_BROADCAST_SELECTED_CONTACT_LIST);
        filter.addAction(Defaults.ACTION_FINISH_BOOKING_SCREEN);
        filter.addAction(Defaults.ACTION_COUPON_CODE_APPLIED);
        filter.addAction(Defaults.ACTION_GUEST_REMOVED);
        Utility.registerReciever(BookPGActivity.this, filter, objMyReceiver);
    }

    public void getDate(View view) {
        DateTimePicker.getInstance().getDate(BookPGActivity.this, updateListener, null, (TextView) view);
    }

    @Override
    public void onDateUpdate(String year, String monthOfYear, String dayOfMonth, TextView textView) {
        if (textView.getId() == binding.contentBookPg.editCheckIn.getId() && !binding.contentBookPg.editMonths.getText().toString().isEmpty()) {
            String date = year + "-" + (monthOfYear) + "-" + dayOfMonth;
            textView.setText(date);
            try {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Integer.parseInt(year), Integer.parseInt(monthOfYear), Integer.parseInt(dayOfMonth));
                calendar.add(Calendar.MONTH, Math.abs(Integer.parseInt(binding.contentBookPg.editMonths.getText().toString().trim())));
                String month, day;
                if (calendar.get(Calendar.MONTH) < 10)
                    month = "0" + calendar.get(Calendar.MONTH);
                else
                    month = "" + calendar.get(Calendar.MONTH);

                if (calendar.get(Calendar.DAY_OF_MONTH) < 10)
                    day = "0" + calendar.get(Calendar.DAY_OF_MONTH);
                else
                    day = "" + calendar.get(Calendar.DAY_OF_MONTH);
                date = calendar.get(Calendar.YEAR) + "-" + month + "-" + day;
                binding.contentBookPg.editCheckOut.setText(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String date = year + "-" + (monthOfYear) + "-" + dayOfMonth;
            textView.setText(date);
        }
    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, String am_pm, TextView textView) {
        String time = "";

        if (minute < 10)
            time = hourOfDay + ":0" + minute + " " + am_pm.toUpperCase();
        else
            time = hourOfDay + ":" + minute + " " + am_pm.toUpperCase();

        textView.setText(time);
    }

    @Override
    public void onTimeUpdate(int hourOfDay, int minute, int second, TextView textView) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            switch (view.getId()) {
                case R.id.editCheckIn:
                case R.id.editCheckOut:
                    getDate(view);
                    break;
            }
        }
        return true;
    }

    private boolean isFormValidForAddGuest() {
        if (binding.contentBookPg.editQuestsName.getVisibility() == View.VISIBLE && binding.contentBookPg.editQuestsName.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBookPg.editQuestsName, "Guest Name can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBookPg.editQuestsMobile.getVisibility() == View.VISIBLE && binding.contentBookPg.editQuestsMobile.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBookPg.editQuestsMobile, "Mobile can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBookPg.spnIdCardType.getVisibility() == View.VISIBLE && binding.contentBookPg.spnIdCardType.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentBookPg.spnIdCardType, "Please select id card type", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBookPg.editIdNumber.getVisibility() == View.VISIBLE && binding.contentBookPg.editIdNumber.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBookPg.editIdNumber, "Id Number can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    private boolean isFormValidForReview() {
        if (binding.contentBookPg.editMonths.getVisibility() == View.VISIBLE && binding.contentBookPg.editMonths.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBookPg.editMonths, "Number of Months can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBookPg.editCheckIn.getVisibility() == View.VISIBLE && binding.contentBookPg.editCheckIn.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBookPg.editCheckIn, "Check-In Date can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBookPg.editCheckOut.getVisibility() == View.VISIBLE && binding.contentBookPg.editCheckOut.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBookPg.editCheckOut, "Check-In Date can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentBookPg.editNumberOfQuests.getVisibility() == View.VISIBLE && binding.contentBookPg.editNumberOfQuests.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.contentBookPg.editNumberOfQuests, "Number of Guests can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } /*else if (binding.contentBookPg.spnRoomOccupancyType.getVisibility() == View.VISIBLE && binding.contentBookPg.spnRoomOccupancyType.getSelectedItemPosition() == 0) {
            Snackbar.make(binding.contentBookPg.spnRoomOccupancyType, "Please Select Room Occupancy Type", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }*/ else if (guestList.size() < Integer.parseInt(binding.contentBookPg.editNumberOfQuests.getText().toString())) {
//            Snackbar.make(binding.contentBookPg.btnAddQuest, "Please add " +
//                    Integer.parseInt(binding.contentBookPg.editNumberOfQuests.getText().toString()) +
//                    " guest details", Snackbar.LENGTH_LONG)
//                    .setAction("Action", null).show();
            if (isFormValidForAddGuest()) {
                addItem();
                return true;
            } else {
                return false;
            }
        } else if (guestList.size() > Integer.parseInt(binding.contentBookPg.editNumberOfQuests.getText().toString())) {
            Snackbar.make(binding.contentBookPg.btnAddQuest, "Please change number of guests above to " +
                    guestList.size(), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (guestList.size() <= 0) {
            Snackbar.make(binding.contentBookPg.btnAddQuest, "Please add at least one guest details", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (guestList.size() == Integer.parseInt(binding.contentBookPg.editNumberOfQuests.getText().toString())) {
            if (binding.contentBookPg.editQuestsName.getText().toString().trim().length() != 0 || binding.contentBookPg.editQuestsMobile.getText().toString().trim().length() != 0 || binding.contentBookPg.editIdNumber.getText().toString().trim().length() != 0 || binding.contentBookPg.spnIdCardType.getSelectedItemPosition() != 0) {
                Snackbar.make(binding.contentBookPg.btnAddQuest, "Please change number of guests above to " +
                        guestList.size(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                return false;
            }
        }

        return true;
    }

    private void addItem() {
        GuestItem objGuestItem = new GuestItem();
        objGuestItem.setGuestId((inc++) + "");
        objGuestItem.setGuestName(binding.contentBookPg.editQuestsName.getText().toString());
        objGuestItem.setGuestMobile(binding.contentBookPg.editQuestsMobile.getText().toString());
        objGuestItem.setGuestDocumentType(binding.contentBookPg.spnIdCardType.getSelectedItem().toString());
        objGuestItem.setGuestDocumentNumber(binding.contentBookPg.editIdNumber.getText().toString());
        guestList.add(objGuestItem);

        ((GuestListAdapter) binding.contentBookPg.rcvQuests.getAdapter()).refreshList(guestList);
        binding.contentBookPg.rcvQuests.getAdapter().notifyItemInserted(0);
        binding.contentBookPg.rcvQuests.getAdapter().notifyDataSetChanged();

        binding.contentBookPg.editQuestsName.setText("");
        binding.contentBookPg.editQuestsMobile.setText("");
        binding.contentBookPg.editIdNumber.setText("");
        binding.contentBookPg.spnIdCardType.setSelection(0);
//        lblNoItems.setVisibility(View.GONE);

        setTotalPriceAsPerGuests(-1);
    }

    private void resetDiscountValues() {
        discount = 0;
        priceAfterDiscount = totalPrice;
        binding.contentBookPg.lblDiscount.setVisibility(View.GONE);
        binding.contentBookPg.lblPriceAfterDiscount.setVisibility(View.GONE);
        binding.contentBookPg.etCouponCode.setText("");
        objResponseVerifyCoupon = null;
    }

    public void haveCouponCodeClick(View view) {
        Intent intent = new Intent(BookPGActivity.this, ViewCouponCodesActivity.class);
        startActivity(intent);
    }

    private void setTotalPriceAsPerGuests(int months) {
        if (months >= 0) {
            if (months > 1) {
                totalPrice = ((Integer.parseInt(objPropertyListItem.getPgPricingDetail().getPerMonth()) + Integer.parseInt(objPropertyListItem.getPgPricingDetail().getCleaningFee())) * months) + Integer.parseInt(objPropertyListItem.getPgPricingDetail().getSecurityDeposit());
            } else {
                totalPrice = Integer.parseInt(objPropertyListItem.getPgPricingDetail().getPerMonth()) + Integer.parseInt(objPropertyListItem.getPgPricingDetail().getCleaningFee()) + Integer.parseInt(objPropertyListItem.getPgPricingDetail().getSecurityDeposit());
            }
            basePrice = totalPrice;
        }
        if (guestList.size() > 0) {
//            totalPrice = basePrice * guestList.size();
//            priceAfterDiscount = totalPrice - discount;
//            binding.contentBookPg.lblTotalAmount.setText(getResources().getString(R.string.lbl_total_amount) + " " + totalPrice + "/-");
//            binding.contentBookPg.lblPriceAfterDiscount.setText(getResources().getString(R.string.lbl_price_after_discount) + " " + priceAfterDiscount + "/-");
            totalPrice = basePrice * guestList.size();
        }
        priceAfterDiscount = totalPrice - discount;
        binding.contentBookPg.lblTotalAmount.setText(getResources().getString(R.string.lbl_total_amount) + " " + totalPrice + "/-");
        binding.contentBookPg.lblPriceAfterDiscount.setText(getResources().getString(R.string.lbl_price_after_discount) + " " + priceAfterDiscount + "/-");
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_BROADCAST_SELECTED_CONTACT_LIST)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (intent.getAction().equals(Defaults.ACTION_FINISH_BOOKING_SCREEN)) {
                BookPGActivity.this.finish();
            } else if (intent.getAction().equals(Defaults.ACTION_COUPON_CODE_APPLIED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        objResponseVerifyCoupon = (ResponseVerifyCoupon) bundle.getSerializable("ResponseVerifyCoupon");
                        if (objResponseVerifyCoupon != null) {
                            binding.contentBookPg.cardCouponApplied.setVisibility(View.VISIBLE);

                            if (objResponseVerifyCoupon.isStatus()) {
                                int discountPercent = Integer.parseInt(objResponseVerifyCoupon.getData().getDiscountPercentage());
                                int minimumTransactionAmount = Integer.parseInt(objResponseVerifyCoupon.getData().getMinimumTransactionAmount());
                                int maximumDiscountAmount = Integer.parseInt(objResponseVerifyCoupon.getData().getMaximumDiscountAmount());

                                binding.contentBookPg.lblCouponCode.setText(objResponseVerifyCoupon.getData().getCoupanCode());

                                if (totalPrice < minimumTransactionAmount) {
                                    binding.contentBookPg.lblCouponCodeMessage.setTextColor(getResources().getColor(R.color.red_600));
                                    binding.contentBookPg.lblCouponCode.setTextColor(getResources().getColor(R.color.red_600));
                                    binding.contentBookPg.lblCouponCodeMessage.setText("Minimum Transaction Amount Is: " + minimumTransactionAmount);
                                    resetDiscountValues();
                                } else {
                                    int discountAmount = (totalPrice * discountPercent) / 100;
                                    if (discountAmount > maximumDiscountAmount)
                                        discount = maximumDiscountAmount;
                                    else
                                        discount = discountAmount;
                                    priceAfterDiscount = totalPrice - discount;

                                    binding.contentBookPg.lblCouponCodeMessage.setTextColor(getResources().getColor(R.color.green_700));
                                    binding.contentBookPg.lblCouponCode.setTextColor(getResources().getColor(R.color.green_700));
                                    binding.contentBookPg.lblCouponCodeMessage.setText(objResponseVerifyCoupon.getMessage() + ". Discount of Rs. " + discount + " is availed to you instantly.");

                                    binding.contentBookPg.lblDiscount.setVisibility(View.VISIBLE);
                                    binding.contentBookPg.lblPriceAfterDiscount.setVisibility(View.VISIBLE);
                                    binding.contentBookPg.lblDiscount.setText(getResources().getString(R.string.lbl_discount) + " " + discount + "/-");
                                    binding.contentBookPg.lblPriceAfterDiscount.setText(getResources().getString(R.string.lbl_price_after_discount) + " " + priceAfterDiscount + "/-");
                                    binding.contentBookPg.etCouponCode.setText("");
                                }
                            } else {
                                binding.contentBookPg.lblCouponCodeMessage.setTextColor(getResources().getColor(R.color.red_600));
                                binding.contentBookPg.lblCouponCode.setTextColor(getResources().getColor(R.color.red_600));
                                binding.contentBookPg.lblCouponCodeMessage.setText(objResponseVerifyCoupon.getMessage());
                                binding.contentBookPg.lblCouponCode.setText(binding.contentBookPg.etCouponCode.getText().toString());
                                resetDiscountValues();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (intent.getAction().equals(Defaults.ACTION_GUEST_REMOVED)) {
                setTotalPriceAsPerGuests(-1);
            }
        }
    }
}
