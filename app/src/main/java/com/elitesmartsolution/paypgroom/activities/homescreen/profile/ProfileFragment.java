package com.elitesmartsolution.paypgroom.activities.homescreen.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.AboutApp;
import com.elitesmartsolution.paypgroom.activities.EditProfileActivity;
import com.elitesmartsolution.paypgroom.activities.SignInActivity;
import com.elitesmartsolution.paypgroom.activities.SignUpActivity;
import com.elitesmartsolution.paypgroom.activities.SplashScreen;
import com.elitesmartsolution.paypgroom.activities.coupons.CouponsFragment;
import com.elitesmartsolution.paypgroom.activities.homescreen.activity.HomeScreenBottomNavigationActivity;
import com.elitesmartsolution.paypgroom.activities.homescreenowner.activity.OwnerHomeScreenActivity;
import com.elitesmartsolution.paypgroom.activities.my_bookings.MyBookingsActivity;
import com.elitesmartsolution.paypgroom.database.DBHandler;
import com.elitesmartsolution.paypgroom.databinding.FragmentProfileBinding;
import com.elitesmartsolution.paypgroom.models.my_bookings.RequestMyBookings;
import com.elitesmartsolution.paypgroom.util.ApplicationUtils;
import com.elitesmartsolution.paypgroom.util.DynamicLinkUtils;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Keys;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;
    private ProfileViewModel profileViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//        BELOW IS THE OLD DEPRICATED CODE THAT IS WHY IT HAS BEEN COMMENTED.
//        profileViewModel =
//                ViewModelProviders.of(this).get(ProfileViewModel.class);

//        IMPORTANT NOTE: ViewModel objects are scoped to the Lifecycle passed to the ViewModelProvider when getting the ViewModel. The ViewModel remains in memory until the Lifecycle it's scoped to goes away permanently: in the case of an activity, when it finishes, while in the case of a fragment, when it's detached.
//        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        profileViewModel = new ViewModelProvider(requireActivity()).get(ProfileViewModel.class);

        binding = FragmentProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        binding.textMobile.setText(Prefs.getMobileNumber());
        Glide.with(getContext()).setDefaultRequestOptions(Utility.getCustomizedRequestOptions(R.drawable.profile_place_holder, R.drawable.profile_place_holder)).load(DBHandler.getInstance(getContext()).getProfileInfoFromDB().getProfile())
                .into(binding.imgProfile);

        setListenersAndObservers();

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Prefs.getUserId().isEmpty()) {
            binding.btnLoginLogout.setText(getResources().getString(R.string.dialog_title_login));
            binding.textProfile.setText(getResources().getString(R.string.guest));
        } else {
            binding.btnLoginLogout.setText(getResources().getString(R.string.payumoney_logout));
//            binding.textProfile.setText(Prefs.getUserName());
            if (Prefs.getUserName() == null || Prefs.getUserName().contains("null"))
                binding.textProfile.setText(getResources().getString(R.string.your_name));
            else
                binding.textProfile.setText(Prefs.getUserName());
        }
        binding.textMobile.setText(Prefs.getMobileNumber());
        Glide.with(getContext()).setDefaultRequestOptions(Utility.getCustomizedRequestOptions(R.drawable.profile_place_holder, R.drawable.profile_place_holder)).load(DBHandler.getInstance(getContext()).getProfileInfoFromDB().getProfile())
                .into(binding.imgProfile);

        if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER)) {
            binding.lblSwitchUser.setText(getResources().getString(R.string.switch_to_tenant));
        } else {
            binding.lblSwitchUser.setText(getResources().getString(R.string.switch_to_owner));
        }
    }

    private void setListenersAndObservers() {
        try {
            profileViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
                @Override
                public void onChanged(@Nullable String s) {
                    if (s != null && !s.isEmpty())
                        binding.textProfile.setText(s);
                }
            });
            profileViewModel.getUsers().observe(getViewLifecycleOwner(), users -> {
                // update UI
            });

            binding.profileContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent inte = new Intent(getContext(), EditProfileActivity.class);
//                    inte.putExtra("navigate", false);
//                    getContext().startActivity(inte);

                    Intent detailIntent;
                    if (Prefs.isPhoneNumberVerified()) {
                        detailIntent = new Intent(getActivity(), EditProfileActivity.class);
//                        BELOW LINE ADDED ON 06-04-2022 TO REMOVE TWO TIME PROFILE ACTIVITY OPENING PROBLEM.
                        detailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        detailIntent.putExtra("navigate", false);
                        getContext().startActivity(detailIntent);
                    } else {
                        detailIntent = new Intent(getActivity(), SignInActivity.class);
                        detailIntent.putExtra("navigateToHomeScreen", true);
                    }
                    startActivity(detailIntent);
                }
            });

            binding.switchUserContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent inte;
                    if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER)) {
                        Prefs.setStringValue(Keys.PREF_USER_TYPE, Defaults.USER_TYPE_TENANT);
                        inte = new Intent(getActivity(), OwnerHomeScreenActivity.class);
                    } else {
                        Prefs.setStringValue(Keys.PREF_USER_TYPE, Defaults.USER_TYPE_OWNER);
                        inte = new Intent(getActivity(), HomeScreenBottomNavigationActivity.class);
                    }
                    inte.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    inte.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    inte.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    inte.putExtra("EXIT", true);
                    getActivity().startActivity(inte);
                    getActivity().finish();
                }
            });

//            NOT USED NOW. USE BTNLOGINLOGOUT FOR BOTH LOGIN AND LOGOUT ACTIONS.
//            binding.logoutContainer.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
////                    Utility.logoutFromDeviceAndCloseAllPreviousActivities(getActivity(), SignInActivity.class);
//                    Utility.logoutFromDeviceAndCloseAllPreviousActivities(getActivity(), SplashScreen.class);
//                }
//            });

            binding.btnLoginLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (binding.btnLoginLogout.getText().equals(getResources().getString(R.string.payumoney_logout))) {
                        Utility.logoutFromDeviceAndCloseAllPreviousActivities(getActivity(), SplashScreen.class);
                    } else {
                        Intent detailIntent = new Intent(getActivity(), SignInActivity.class);
                        detailIntent.putExtra("navigateToHomeScreen", true);
                        startActivity(detailIntent);
                    }
                }
            });

            binding.changePasswordContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent inte = new Intent(getContext(), SignUpActivity.class);
                    inte.putExtra("screenType", Defaults.SCREEN_TYPE_FORGOT_PASSWORD);
                    getContext().startActivity(inte);
                }
            });

            binding.rateUsContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utility.rateThisAppOnPlaystore(getActivity(), ApplicationUtils.getPackageName());
                }
            });

            binding.helpAndSupportContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Utility.getSupportDialog(getActivity()).show();
                }
            });

            binding.myBookingsContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Utility.isUserLoggedIn(getActivity())) {
//                    Intent intent = new Intent(getActivity(), MainActivityPayuMoney.class);
//                    getActivity().startActivity(intent);
                        Intent intent = new Intent(getActivity(), MyBookingsActivity.class);

                        Bundle bundle = new Bundle();
                        bundle.putString("toolTitle", getResources().getString(R.string.lbl_my_bookings));

                        RequestMyBookings objRequestGetPgListing = new RequestMyBookings();
                        if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER)) {
                            objRequestGetPgListing.setOwnerId(Prefs.getUserId());
                            objRequestGetPgListing.setTenantId("");
                        } else {
                            objRequestGetPgListing.setTenantId(Prefs.getUserId());
                            objRequestGetPgListing.setOwnerId("");
                        }

                        bundle.putSerializable("RequestMyBookings", objRequestGetPgListing);
                        intent.putExtra("bundle", bundle);
                        getActivity().startActivity(intent);
                    }
                }
            });

            binding.availableCouponsContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utility.isUserLoggedIn(getActivity())) {
                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        CouponsFragment couponsFragment = new CouponsFragment();
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
//                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//                    transaction.replace(android.R.id.content, couponsFragment,getResources().getString(R.string.lbl_menu_coupons)).addToBackStack(null).commit();
                        transaction.replace(((ViewGroup) getView().getParent()).getId(), couponsFragment, getResources().getString(R.string.lbl_menu_coupons)).addToBackStack(null).commit();
                    }
                }
            });

            binding.referAndEarnContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DynamicLinkUtils.shareLink(getActivity(), DynamicLinkUtils.createDynamicLink_Advanced(getActivity(), "RAVI-123456"));
                }
            });

            binding.aboutAppContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), AboutApp.class);
                    getContext().startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
