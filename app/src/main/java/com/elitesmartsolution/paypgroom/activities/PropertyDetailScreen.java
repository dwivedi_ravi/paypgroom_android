package com.elitesmartsolution.paypgroom.activities;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.adapters.AmenitiesAdapter;
import com.elitesmartsolution.paypgroom.databinding.ActivityPropertyDetailsBinding;
import com.elitesmartsolution.paypgroom.databinding.ListItemViewPagerPropertyImageBinding;
import com.elitesmartsolution.paypgroom.models.RequestAddToFavorites;
import com.elitesmartsolution.paypgroom.models.RequestChangeAvailabilityStatus;
import com.elitesmartsolution.paypgroom.models.ResponseChangeAvailabilityStatus;
import com.elitesmartsolution.paypgroom.models.ResponseVerifyOTP;
import com.elitesmartsolution.paypgroom.models.pglisting.PropertyListItem;
import com.elitesmartsolution.paypgroom.models.pgmetadata.AmenitiesItem;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.Measure;
import com.elitesmartsolution.paypgroom.util.Tools;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class PropertyDetailScreen extends SharedActivity {

    private ActivityPropertyDetailsBinding binding;

//    private FloatingActionButton binding.fab;
//    private Toolbar binding.toolbar;
//    private AppCompatButton binding.btnInstantbook, binding.btnScheduleToVisit, binding.contentPropertyDetail.btnChangeAvailablityStatus;
//    private TextView binding.contentPropertyDetail.tvPropertyType, binding.contentPropertyDetail.tvHousetitle, binding.contentPropertyDetail.tvHousedesc, binding.contentPropertyDetail.tvHostName, binding.contentPropertyDetail.tvGuestcount, binding.contentPropertyDetail.tvBedroomscount, binding.contentPropertyDetail.tvBedscount, binding.contentPropertyDetail.tvBathscount, binding.contentPropertyDetail.tvFromTime, binding.contentPropertyDetail.tvToTime, binding.contentPropertyDetail.tvCheckinHour, binding.contentPropertyDetail.tvCheckoutHour, binding.contentPropertyDetail.tvMinimumStay, binding.contentPropertyDetail.tvCancellationPolicy, binding.contentPropertyDetail.tvCheckAvailability, binding.contentPropertyDetail.tvPricepermonth, binding.contentPropertyDetail.tvPericepermonthfor6months, binding.contentPropertyDetail.tvCleaningfee, binding.contentPropertyDetail.tvSecuritydeposit, binding.contentPropertyDetail.tvMinnoofmonths, binding.tvPrice, binding.contentPropertyDetail.tvAvailablityStatus, binding.contentPropertyDetail.tvViewOnMap, binding.contentPropertyDetail.tvAddressText;
//    private ExpandableTextView binding.contentPropertyDetail.expandTextView;
//    private AppCompatImageView binding.contentPropertyDetail.verfiedImg;
//    private CircleImageView binding.contentPropertyDetail.ivHostProfile;
//    private RecyclerView binding.contentPropertyDetail.rvAmenitiesDesc;
//    private LinearLayout binding.contentPropertyDetail.containerAvailableStatus;
//    private NestedScrollView binding.contentPropertyDetail.nestedScrollView;
//    private ViewPager binding.viewpager;
//    private CirclePageIndicator binding.indicator;

    private MyReceiver objMyReceiver;
    private PropertyListItem objPropertyListBO;
    private Runnable runnablePgImages = null;
    private Handler handlerPgImages = null;
    private int pgImagePosition = 0;
    private boolean takeUserToNextScreen;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            binding = ActivityPropertyDetailsBinding.inflate(getLayoutInflater());
            setContentView(binding.getRoot());

            takeUserToNextScreen = false;
            setSupportActionBar(binding.toolbar);
            binding.toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white_24dp));
            Tools.setSystemBarTransparent(this);
            binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });

            objMyReceiver = new MyReceiver();
            registerReciever();

            binding.fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Utility.isUserLoggedIn(PropertyDetailScreen.this)) {
                        if (objPropertyListBO != null) {
                            RequestAddToFavorites objRequestAddToFavorites = new RequestAddToFavorites();
                            objRequestAddToFavorites.setUserId(Prefs.getUserId());
                            objRequestAddToFavorites.setPgId(objPropertyListBO.getId());

                            if (binding.fab.getTag() != null && (int) binding.fab.getTag() == 0) {
                                objRequestAddToFavorites.setFavoriteStatus(Defaults.FAVORITE_STATUS_SELECTED);
                            } else {
                                objRequestAddToFavorites.setFavoriteStatus(Defaults.FAVORITE_STATUS_UN_SELECTED);
                            }

                            ApiCallerUtility.callAddToFavoriteApi(PropertyDetailScreen.this, objRequestAddToFavorites);
                        }
//                Intent detailIntent = new Intent(PropertyDetailScreen.this, ReadPropertyListBOActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("currentPropertyListBO", objPropertyListBO);
//                detailIntent.putExtras(bundle);
//                startActivity(detailIntent);
                    }
                }
            });

            binding.btnInstantbook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (objPropertyListBO.getIsAvailable().equals("1")) {
                        takeUserToBookingScreen();
                    } else {
                        Snackbar.make(binding.btnInstantbook, "Booking Not Allowed", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }
            });

            binding.btnScheduleToVisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(PropertyDetailScreen.this, StaticContentActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("toolTitle", getResources().getString(R.string.title_schedule_to_visit));
                    bundle.putString("toolShortDescription", getResources().getString(R.string.tool_short_description_schedule_to_visit));
                    bundle.putString("toolLongDescription", getResources().getString(R.string.tool_long_description_schedule_to_visit));
                    intent.putExtra("bundle", bundle);
                    startActivity(intent);
                }
            });

            Typeface robotoBold = Typeface.createFromAsset(getAssets(),
                    "roboto_bold.ttf");
            Typeface robotoMedium = Typeface.createFromAsset(getAssets(),
                    "roboto_medium.ttf");

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                objPropertyListBO = (PropertyListItem) bundle.getSerializable("currentPropertyListBO");
            }

            if (objPropertyListBO != null) {
                binding.toolbarLayout.setTitle(objPropertyListBO.getPgName());
                binding.toolbarLayout.setCollapsedTitleTextAppearance(R.style.Toolbar_TitleText);
                binding.toolbarLayout.setCollapsedTitleTypeface(robotoMedium);

                if (objPropertyListBO.getIsFavorite().equals("1")) {
                    binding.fab.setImageResource(R.drawable.ic_favselected);
                    binding.fab.setTag(1);
                } else {
                    binding.fab.setImageResource(R.drawable.ic_favunselected);
                    binding.fab.setTag(0);
                }

                if (objPropertyListBO.getPgOwnerDetail().getOwnerId().trim().equals(Prefs.getUserId().trim())) {
                    binding.contentPropertyDetail.containerAvailableStatus.setVisibility(View.VISIBLE);
                    if (objPropertyListBO.getIsAvailable().equals(Defaults.IS_AVAILABLE)) {
                        binding.contentPropertyDetail.tvAvailablityStatus.setTextColor(getResources().getColor(R.color.md_green_700));
                        binding.contentPropertyDetail.tvAvailablityStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_circle, 0, 0, 0);
                        binding.contentPropertyDetail.tvAvailablityStatus.setText(getResources().getString(R.string.available_for_booking));
                    } else {
                        binding.contentPropertyDetail.tvAvailablityStatus.setTextColor(getResources().getColor(R.color.red_600));
                        binding.contentPropertyDetail.tvAvailablityStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_not_available_round_close, 0, 0, 0);
                        binding.contentPropertyDetail.tvAvailablityStatus.setText(getResources().getString(R.string.not_available_for_booking));
                    }

                    binding.contentPropertyDetail.btnChangeAvailablityStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (objPropertyListBO != null) {
                                RequestChangeAvailabilityStatus objRequestChangeAvailabilityStatus = new RequestChangeAvailabilityStatus();
                                objRequestChangeAvailabilityStatus.setUserId(Prefs.getUserId());
                                objRequestChangeAvailabilityStatus.setPgId(objPropertyListBO.getId());

                                if (objPropertyListBO.getIsAvailable().trim().equals(Defaults.IS_AVAILABLE)) {
                                    objRequestChangeAvailabilityStatus.setAvailablityStatus(Defaults.NOT_IS_AVAILABLE);
                                } else {
                                    objRequestChangeAvailabilityStatus.setAvailablityStatus(Defaults.IS_AVAILABLE);
                                }
                                ApiCallerUtility.callChangeAvailableStatusApi(PropertyDetailScreen.this, objRequestChangeAvailabilityStatus);
                            }
                        }
                    });
                }

                String luxuryType = "";
                if (objPropertyListBO.getLuxryType().equals(Defaults.FULLY_FURNISHED))
                    luxuryType = getResources().getString(R.string.furnished);
                else if (objPropertyListBO.getLuxryType().equals(Defaults.NON_FURNISHED))
                    luxuryType = getResources().getString(R.string.non_furnished);
                else if (objPropertyListBO.getLuxryType().equals(Defaults.SEMI_FURNISHED))
                    luxuryType = getResources().getString(R.string.semi_furnished);

                binding.contentPropertyDetail.tvPropertyType.setText(objPropertyListBO.getPropertyType().concat(" ").concat("|").concat(" ").concat(objPropertyListBO.getPgRoomType()).concat(" ").concat("|").concat(" ").concat(luxuryType));
                binding.contentPropertyDetail.tvHousetitle.setText(objPropertyListBO.getPgName());
                binding.contentPropertyDetail.tvHousedesc.setText(objPropertyListBO.getDescription());
                binding.contentPropertyDetail.tvHostName.setText(objPropertyListBO.getPgOwnerDetail().getName());
                binding.contentPropertyDetail.expandTextView.setText(objPropertyListBO.getOtherDetails());

//                binding.contentPropertyDetail.tvAddressText.setText(objPropertyListBO.getDescription());
                if (objPropertyListBO.getLocation().getLine2() != null && !objPropertyListBO.getLocation().getLine2().trim().isEmpty()) {
                    binding.contentPropertyDetail.tvAddressText.setText(objPropertyListBO.getLocation().getLine1() + " " + objPropertyListBO.getLocation().getLine2());
                } else {
                    binding.contentPropertyDetail.tvAddressText.setText(objPropertyListBO.getLocation().getLine1() + " " + objPropertyListBO.getLocation().getCity() + " " + objPropertyListBO.getLocation().getState() + " " + objPropertyListBO.getLocation().getCountry() + " " + objPropertyListBO.getLocation().getPincode());
                }

                binding.contentPropertyDetail.tvGuestcount.setText(objPropertyListBO.getGuests());
                binding.contentPropertyDetail.tvBedroomscount.setText(objPropertyListBO.getBedroom());
                binding.contentPropertyDetail.tvBedscount.setText(objPropertyListBO.getBeds());
                binding.contentPropertyDetail.tvBathscount.setText(objPropertyListBO.getBathroom());

                binding.contentPropertyDetail.tvGuestcount.setVisibility(View.GONE);
                binding.contentPropertyDetail.tvBedroomscount.setVisibility(View.GONE);
                binding.contentPropertyDetail.tvBedscount.setVisibility(View.GONE);
                binding.contentPropertyDetail.tvBathscount.setVisibility(View.GONE);

                binding.contentPropertyDetail.tvFromTime.setText(objPropertyListBO.getVisitScheduleDetail().getVisitFrom());
                binding.contentPropertyDetail.tvToTime.setText(objPropertyListBO.getVisitScheduleDetail().getVisitTo());
                binding.contentPropertyDetail.tvCheckinHour.setText(objPropertyListBO.getPgPricingDetail().getCheckIn());
                binding.contentPropertyDetail.tvCheckoutHour.setText(objPropertyListBO.getPgPricingDetail().getCheckOut());
                binding.contentPropertyDetail.tvMinimumStay.setText(objPropertyListBO.getPgPricingDetail().getMinimumStayMonth() + " Months");
                binding.contentPropertyDetail.tvCancellationPolicy.setText("Charge: " + objPropertyListBO.getPgPricingDetail().getCancellationCharge());
                if (objPropertyListBO.getIsAvailable().equals("1"))
                    binding.contentPropertyDetail.tvCheckAvailability.setText("Available");
                else
                    binding.contentPropertyDetail.tvCheckAvailability.setText("Not Available");
//            binding.contentPropertyDetail.tvPricepermonth.setText("Rs. " + objPropertyListBO.getPgPricingDetail().getPerMonth() + "/-");
                binding.contentPropertyDetail.tvPricepermonth.setText("Rs. " + objPropertyListBO.getPgPricingDetail().getPerMonth() + "/-");
                try {
                    binding.tvPrice.setText("Rs. " + (Integer.parseInt(objPropertyListBO.getPgPricingDetail().getPerMonth()) + Integer.parseInt(objPropertyListBO.getPgPricingDetail().getCleaningFee()) + Integer.parseInt(objPropertyListBO.getPgPricingDetail().getSecurityDeposit())) + "/-");
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                try {
                    binding.contentPropertyDetail.tvPericepermonthfor6months.setText("Rs. " + (Integer.parseInt(objPropertyListBO.getPgPricingDetail().getPerMonth()) * 5) + "/-");
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                binding.contentPropertyDetail.tvCleaningfee.setText("Rs. " + objPropertyListBO.getPgPricingDetail().getCleaningFee() + "/-");
                binding.contentPropertyDetail.tvSecuritydeposit.setText("Rs. " + objPropertyListBO.getPgPricingDetail().getSecurityDeposit() + "/-");
                binding.contentPropertyDetail.tvMinnoofmonths.setText(objPropertyListBO.getPgPricingDetail().getMinimumStayMonth());

                if (objPropertyListBO.getIsActive().equals("1"))
                    binding.contentPropertyDetail.verfiedImg.setVisibility(View.VISIBLE);
                else
                    binding.contentPropertyDetail.verfiedImg.setVisibility(View.GONE);

                Glide.with(this).load(objPropertyListBO.getPgOwnerDetail().getProfile())
                        .apply(Utility.getCustomizedRequestOptions(R.drawable.profile_place_holder, R.drawable.profile_place_holder)).into(binding.contentPropertyDetail.ivHostProfile);

                ArrayList<AmenitiesItem> amenitiesItemList = new ArrayList<>();
                for (int i = 0; i < objPropertyListBO.getPgAmenities().size(); i++) {
                    AmenitiesItem objAmenitiesItem = new AmenitiesItem();
                    objAmenitiesItem.setId(i + "");
                    objAmenitiesItem.setName(objPropertyListBO.getPgAmenities().get(i).getName());
                    objAmenitiesItem.setDescription(objPropertyListBO.getPgAmenities().get(i).getDescription());
                    objAmenitiesItem.setIcon(objPropertyListBO.getPgAmenities().get(i).getIcon());
                    amenitiesItemList.add(objAmenitiesItem);
                }
//            GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 2);
//            binding.contentPropertyDetail.rvAmenitiesDesc.setLayoutManager(linearLayoutManager);
                binding.contentPropertyDetail.rvAmenitiesDesc.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(this, amenitiesItemList, false, false);
                binding.contentPropertyDetail.rvAmenitiesDesc.setAdapter(amenitiesAdapter);

                ViewPagerAdapter mViewPagerAdapter = new ViewPagerAdapter(PropertyDetailScreen.this, objPropertyListBO, objPropertyListBO.getId());
                binding.viewpager.setAdapter(mViewPagerAdapter);
                binding.indicator.setViewPager(binding.viewpager);
                mViewPagerAdapter.notifyDataSetChanged();

                binding.viewpager.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        stopAutoSliderPgImages();
                        return false;
                    }
                });

                startAutoSliderPgImages();

//            binding.contentPropertyDetail.nestedScrollView.fullScroll(binding.contentPropertyDetail.tvPropertyType.FOCUS_UP);

                binding.contentPropertyDetail.tvViewOnMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent detailIntent = new Intent(PropertyDetailScreen.this, MapsActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("currentPropertyListBO", objPropertyListBO);
                        detailIntent.putExtras(bundle);
                        startActivity(detailIntent);
                    }
                });
            }
        } catch (Exception ex) {
        }
    }

    private void takeUserToBookingScreen() {
        takeUserToNextScreen = false;
        if (objPropertyListBO.getIsAvailable().trim().equals(Defaults.IS_AVAILABLE)) {
            Intent detailIntent;
            if (Prefs.isPhoneNumberVerified()) {
                if (!Prefs.getUserName().trim().equals("")) {
                    detailIntent = new Intent(PropertyDetailScreen.this, BookPGActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("currentPropertyListBO", objPropertyListBO);
                    detailIntent.putExtras(bundle);
                } else {
                    detailIntent = new Intent(PropertyDetailScreen.this, EditProfileActivity.class);
                    detailIntent.putExtra("navigate", false);
                    takeUserToNextScreen = true;
                }
            } else {
                detailIntent = new Intent(PropertyDetailScreen.this, SignInActivity.class);
                detailIntent.putExtra("navigateToHomeScreen", false);
                takeUserToNextScreen = true;
            }
            startActivity(detailIntent);
        } else {
            Toast.makeText(PropertyDetailScreen.this, "Booking Closed", Toast.LENGTH_LONG).show();
        }
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
        binding.fab.setVisibility(View.GONE);
        super.onBackPressed();
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_FAVORITE_ADDED);
        filter.addAction(Defaults.ACTION_AVAILABILITY_STATUS_CHANGED);
        Utility.registerReciever(this, filter, objMyReceiver);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_property_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {

            String amenitiesList = "\n\n*Amenities:*\n";
            for (int i = 0; i < objPropertyListBO.getPgAmenities().size(); i++) {
                amenitiesList = amenitiesList + objPropertyListBO.getPgAmenities().get(i).getName() + "\n";
            }

            Utility.getSharableText(PropertyDetailScreen.this, objPropertyListBO, amenitiesList);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void gotReadStoragePermission() {

    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {

    }

    private void startAutoSliderPgImages() {
        try {
            if (runnablePgImages != null) {
                runnablePgImages = null;
                handlerPgImages = null;
            }

            if (binding.viewpager.getAdapter() != null && binding.viewpager.getAdapter().getCount() > 0) {
                handlerPgImages = new Handler();
                runnablePgImages = () -> {
                    if (handlerPgImages != null && runnablePgImages != null) {
                        pgImagePosition++;
                        if (pgImagePosition >= binding.viewpager.getAdapter().getCount()) {
                            pgImagePosition = 0;
                        }
                        binding.viewpager.setCurrentItem(pgImagePosition);
                        handlerPgImages.postDelayed(runnablePgImages, 3000);
                    }
                };
                handlerPgImages.postDelayed(runnablePgImages, 3000);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private void stopAutoSliderPgImages() {
        try {
            handlerPgImages.removeCallbacks(runnablePgImages);
            runnablePgImages = null;
            handlerPgImages = null;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopAutoSliderPgImages();
    }

    @Override
    public void onResume() {
        super.onResume();
//        startAutoSliderPgImages();
        if (takeUserToNextScreen && Prefs.isPhoneNumberVerified() && !Prefs.getUserName().trim().equals("")) {
            takeUserToNextScreen = false;
            Intent detailIntent = new Intent(PropertyDetailScreen.this, BookPGActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("currentPropertyListBO", objPropertyListBO);
            detailIntent.putExtras(bundle);
            startActivity(detailIntent);
        }
    }

    private class ViewPagerAdapter extends PagerAdapter {
        Context context;
        LayoutInflater layoutInflater;
        String mId;
        PropertyListItem objPropertyListItem;
        private RequestOptions requestOptions;
        private ListItemViewPagerPropertyImageBinding binding;

        public ViewPagerAdapter(Context mContext, PropertyListItem objPropertyListItem, String id) {
            this.context = mContext;
            this.objPropertyListItem = objPropertyListItem;
            this.mId = id;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.color.mdtp_line_background);
        }

        @Override
        public int getCount() {
            return objPropertyListItem.getPgImages().size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
//            View itemView = layoutInflater.inflate(R.layout.list_item_view_pager_property_image, container, false);
            binding = ListItemViewPagerPropertyImageBinding.inflate(layoutInflater, container, false);
            View itemView = binding.getRoot();

//            if (verified.equals("0")) {
//                binding.bannerImg.setVisibility(View.GONE);
//            } else {
//                binding.bannerImg.setVisibility(View.VISIBLE);
//            }

            if (objPropertyListItem.getIsActive().equals("1"))
                binding.bannerImg.setVisibility(View.VISIBLE);
            else
                binding.bannerImg.setVisibility(View.GONE);

            if (objPropertyListItem.getIsAvailable().equals("1"))
                binding.imgBooked.setVisibility(View.GONE);
            else {
                binding.imgBooked.setPadding(0, (int) Measure.dpToPx(30, context), 0, 0);
                binding.imgBooked.getLayoutParams().width = (int) Measure.dpToPx(200, context);
                binding.imgBooked.getLayoutParams().height = (int) Measure.dpToPx(200, context);
                binding.imgBooked.setVisibility(View.VISIBLE);
            }

//            objPropertyListItem.getPgImages().get(position).setImage(objPropertyListItem.getPgImages().get(position).getImage().trim().replace("http:","https:"));
            binding.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            Glide.with(context).load(objPropertyListItem.getPgImages().get(position).getImage().trim()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).apply(requestOptions).into(binding.imageView);

////            https://paypgroom.pgbooking.co.in/public/image/1649234127_10_06042022020527472.jpg
//            String imgUrl = objPropertyListItem.getPgImages().get(position).getImage().replace("http://", "https://");
//            Glide.with(context).load(imgUrl).listener(new RequestListener<Drawable>() {
//                @Override
//                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                    return false;
//                }
//
//                @Override
//                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                    return false;
//                }
//            }).apply(requestOptions).into(binding.imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(context, PropertyDetailScreen.class);
//                    context.startActivity(intent);
                }
            });

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((RelativeLayout) object);
        }
    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_FAVORITE_ADDED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseVerifyOTP objResponseVerifyOTP = (ResponseVerifyOTP) bundle.getSerializable("ResponseVerifyOTP");
                        if (objResponseVerifyOTP != null) {
                            if (objResponseVerifyOTP.getMessage().equals(Defaults.FAVORITE_STATUS_SELECTED)) {
                                objPropertyListBO.setIsFavorite(Defaults.FAVORITE_STATUS_SELECTED);
                                binding.fab.setImageResource(R.drawable.ic_favselected);
                                binding.fab.setTag(1);
                            } else if (objResponseVerifyOTP.getMessage().equals(Defaults.FAVORITE_STATUS_UN_SELECTED)) {
                                objPropertyListBO.setIsFavorite(Defaults.FAVORITE_STATUS_UN_SELECTED);
                                binding.fab.setImageResource(R.drawable.ic_favunselected);
                                binding.fab.setTag(0);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (intent.getAction().equals(Defaults.ACTION_AVAILABILITY_STATUS_CHANGED)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                        ResponseChangeAvailabilityStatus objResponseChangeAvailabilityStatus = (ResponseChangeAvailabilityStatus) bundle.getSerializable("ResponseChangeAvailabilityStatus");
                        if (objResponseChangeAvailabilityStatus != null) {
                            if (objResponseChangeAvailabilityStatus.getData().getPgData().getIsAvailable().equals(Defaults.IS_AVAILABLE)) {
                                objPropertyListBO.setIsAvailable(Defaults.IS_AVAILABLE);
                                binding.contentPropertyDetail.tvAvailablityStatus.setTextColor(getResources().getColor(R.color.md_green_700));
                                binding.contentPropertyDetail.tvAvailablityStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_circle, 0, 0, 0);
                                binding.contentPropertyDetail.tvAvailablityStatus.setText(getResources().getString(R.string.available_for_booking));
                                binding.contentPropertyDetail.tvCheckAvailability.setText("Available");
                            } else if (objResponseChangeAvailabilityStatus.getData().getPgData().getIsAvailable().equals(Defaults.NOT_IS_AVAILABLE)) {
                                objPropertyListBO.setIsAvailable(Defaults.NOT_IS_AVAILABLE);
                                binding.contentPropertyDetail.tvAvailablityStatus.setTextColor(getResources().getColor(R.color.red_600));
                                binding.contentPropertyDetail.tvAvailablityStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_not_available_round_close, 0, 0, 0);
                                binding.contentPropertyDetail.tvAvailablityStatus.setText(getResources().getString(R.string.not_available_for_booking));
                                binding.contentPropertyDetail.tvCheckAvailability.setText("Not Available");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
