package com.elitesmartsolution.paypgroom.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.elitesmartsolution.paypgroom.databinding.ActivitySigninBinding;
import com.elitesmartsolution.paypgroom.models.RequestLogin;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.Tools;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

public class SignInActivity extends AppCompatActivity {

    private ActivitySigninBinding binding;

    private View parent_view;
//    private TextInputEditText binding.editPhoneNumber;
//    private TextInputEditText binding.editPassword;

    private boolean navigateToHomeScreen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySigninBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        parent_view = findViewById(android.R.id.content);

        Tools.setSystemBarColor(this, android.R.color.white);
        Tools.setSystemBarLight(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            navigateToHomeScreen = getIntent().getExtras().getBoolean("navigateToHomeScreen", false);
        }

        binding.signUpForAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(parent_view, "Sign up for an account", Snackbar.LENGTH_SHORT).show();
                Intent inte = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(inte);
            }
        });

        binding.btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFormValid()) {
                    RequestLogin objRequestLogin = new RequestLogin();
                    objRequestLogin.setMobile(binding.editPhoneNumber.getText().toString().trim());
                    objRequestLogin.setPassword(binding.editPassword.getText().toString());
                    objRequestLogin.setNavigateToHomeScreen(navigateToHomeScreen);
                    ApiCallerUtility.callLoginApi(SignInActivity.this, objRequestLogin);
                }
            }
        });

        binding.btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inte = new Intent(SignInActivity.this, SignUpActivity.class);
                inte.putExtra("screenType", Defaults.SCREEN_TYPE_FORGOT_PASSWORD);
                SignInActivity.this.startActivity(inte);
            }
        });

//        Utility.checkAndShowIntroScreen(this);
//        handleDynamicLink();
    }

    private boolean isFormValid() {
        if (binding.editPhoneNumber.getVisibility() == View.VISIBLE && binding.editPhoneNumber.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editPhoneNumber, "Mobile number can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.editPassword.getVisibility() == View.VISIBLE && binding.editPassword.getText().toString().trim().length() == 0) {
            Snackbar.make(binding.editPassword, "Password can not be empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } /*else if (Prefs.isPhoneNumberVerified() && !Prefs.getMobileNumber().equals("")) {
            if (Prefs.getMobileNumber().equals(binding.editPhoneNumber.getText().toString())) {
                Intent inte = new Intent(SignInActivity.this, EditProfileActivity.class);
                inte.putExtra("navigate", true);
                SignInActivity.this.startActivity(inte);
                SignInActivity.this.finish();
            } else {
                Snackbar.make(binding.editPhoneNumber, "Mobile Number Not Registered", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        } else {
            Snackbar.make(binding.editPhoneNumber, "Please Sign Up First", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }*/
        return true;
    }

    private void handleDynamicLink() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                        }

                        try {
                            Prefs.setStringValue("CouponCode", deepLink.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Handle the deep link. For example, open the linked
                        // content, or apply promotional credit to the user's
                        // account.
                        // ...

                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(SignInActivity.this.getLocalClassName(), "getDynamicLink:onFailure", e);
                    }
                });
    }
}
