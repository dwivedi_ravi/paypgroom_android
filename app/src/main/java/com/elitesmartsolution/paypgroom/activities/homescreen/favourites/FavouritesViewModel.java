package com.elitesmartsolution.paypgroom.activities.homescreen.favourites;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.elitesmartsolution.paypgroom.models.pglisting.PropertyListItem;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetPgListing;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

import java.util.List;

public class FavouritesViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<Boolean> mShowProgressBar;
    private MutableLiveData<List<PropertyListItem>> mPropertyList;
    private Context context;

    public FavouritesViewModel(Application context) {
        super(context);
        this.context = context;
        mText = new MutableLiveData<>();
        mText.setValue("This is Favourites fragment");
        mShowProgressBar = new MutableLiveData<>();
        mShowProgressBar.setValue(false);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<Boolean> getProgressBarStatus() {
        return mShowProgressBar;
    }

    public LiveData<List<PropertyListItem>> getPropertyList() {
        if (mPropertyList == null) {
            mPropertyList = new MutableLiveData<>();

            RequestGetPgListing objRequestGetPgListing = new RequestGetPgListing();
            if (Prefs.getUserId().equals(""))
                objRequestGetPgListing.setUserId("-1");
            else
                objRequestGetPgListing.setUserId(Prefs.getUserId());
            objRequestGetPgListing.setIsFavorite("1");
//            objRequestGetPgListing.setServiceCityCode(Prefs.getServiceCityCode());
            ApiCallerUtility.callGetPgListingApi(context, mPropertyList, mShowProgressBar, objRequestGetPgListing);
        }
        return mPropertyList;
    }
}