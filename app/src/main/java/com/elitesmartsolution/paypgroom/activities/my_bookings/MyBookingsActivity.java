package com.elitesmartsolution.paypgroom.activities.my_bookings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.adapters.BookingListAdapter;
import com.elitesmartsolution.paypgroom.databinding.ActivityMyBookingsBinding;
import com.elitesmartsolution.paypgroom.models.my_bookings.RequestMyBookings;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class MyBookingsActivity extends SharedActivity {

    private ActivityMyBookingsBinding binding;

    private MyReceiver objMyReceiver;
    private RequestMyBookings objRequestMyBookings;

    private MyBookingsViewModel myBookingsViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMyBookingsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initComponents();
    }

    private void initComponents() {
        try {
            objMyReceiver = new MyReceiver();
            registerReciever();
            myBookingsViewModel = new ViewModelProvider(this).get(MyBookingsViewModel.class);

            binding.appbarLayout.toolbar.setTitle("");
            setSupportActionBar(binding.appbarLayout.toolbar);
            Utility.setStatusBarColor(MyBookingsActivity.this);
            binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            binding.fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
            initializeData();
            setListenersAndObservers();

            myBookingsViewModel.callListingApi(objRequestMyBookings);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        String toolTitle;
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBundle("bundle") != null) {
            final Bundle bundle = getIntent().getExtras().getBundle("bundle");
            toolTitle = bundle.getString("toolTitle", "");
            objRequestMyBookings = (RequestMyBookings) bundle.getSerializable("RequestMyBookings");
            binding.appbarLayout.toolbar.setTitle(toolTitle);
        }
    }

    private void setListenersAndObservers() {
        try {
            myBookingsViewModel.getProgressBarStatus().observe(this, aBoolean -> {
                if (aBoolean)
                    binding.progressBar.setVisibility(View.VISIBLE);
                else
                    binding.progressBar.setVisibility(View.GONE);
            });

            myBookingsViewModel.getPropertyList().observe(this, mPropertyList -> {
                try {
                    if (binding.contentMyBookings.recyclerView != null && binding.contentMyBookings.recyclerView.getAdapter() == null) {
                        binding.contentMyBookings.recyclerView.setLayoutManager(new LinearLayoutManager(MyBookingsActivity.this));
                        BookingListAdapter mBookingListAdapter = new BookingListAdapter(MyBookingsActivity.this, mPropertyList, Defaults.SCREEN_TYPE_MY_PGS);
                        binding.contentMyBookings.recyclerView.setAdapter(mBookingListAdapter);
                    } else if (binding.contentMyBookings.recyclerView != null) {
                        ((BookingListAdapter) binding.contentMyBookings.recyclerView.getAdapter()).refreshList(mPropertyList);
                        binding.contentMyBookings.recyclerView.getAdapter().notifyDataSetChanged();
                    }

                    if (mPropertyList == null || mPropertyList.size() == 0)
                        binding.txtNoPgsFound.setVisibility(View.VISIBLE);
                    else
                        binding.txtNoPgsFound.setVisibility(View.GONE);
                } catch (Exception ex) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_GROUP_LIST);
        Utility.registerReciever(MyBookingsActivity.this, filter, objMyReceiver);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_with_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.search_action: {
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void gotReadStoragePermission() {

    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {

    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_GROUP_LIST)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
//                        ResponseGroupList objResponseGroupList = (ResponseGroupList) bundle.getSerializable("ResponseGroupList");
//                        if (objResponseGroupList != null && objResponseGroupList.getData() != null && objResponseGroupList.getData().getUserGroups() != null) {
//
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
