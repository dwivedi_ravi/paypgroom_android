package com.elitesmartsolution.paypgroom.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.database.DBHandler;
import com.elitesmartsolution.paypgroom.databinding.ActivityStaticContentBinding;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.google.android.material.snackbar.Snackbar;

public class StaticContentActivity extends SharedActivity {

    private ActivityStaticContentBinding binding;

    private DBHandler dbHandler;
    private AlertDialog progressDialog;
    private MyReceiver objMyReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityStaticContentBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initComponents();
    }

    private void initComponents() {
        try {
            objMyReceiver = new MyReceiver();
            registerReciever();

            binding.appbarLayout.toolbar.setTitle("");
            setSupportActionBar(binding.appbarLayout.toolbar);
            Utility.setStatusBarColor(StaticContentActivity.this);
            binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
            binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

            binding.fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });

            initializeData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        String toolTitle, toolShortDescription, toolLongDescription;
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBundle("bundle") != null) {
            final Bundle bundle = getIntent().getExtras().getBundle("bundle");
            toolTitle = bundle.getString("toolTitle", "");
            toolShortDescription = bundle.getString("toolShortDescription", "");
            toolLongDescription = bundle.getString("toolLongDescription", "");

            binding.appbarLayout.toolbar.setTitle(toolTitle);
            binding.contentStaticContentActivity.toolTitle.setText(toolTitle);
            binding.contentStaticContentActivity.toolShortDescription.setText(toolShortDescription);
            binding.contentStaticContentActivity.toolLongDescription.setText(toolLongDescription);
        }
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_GROUP_LIST);
        Utility.registerReciever(StaticContentActivity.this, filter, objMyReceiver);
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    @Override
    public void gotReadStoragePermission() {

    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {

    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Defaults.ACTION_REFRESH_GROUP_LIST)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
//                        ResponseGroupList objResponseGroupList = (ResponseGroupList) bundle.getSerializable("ResponseGroupList");
//                        if (objResponseGroupList != null && objResponseGroupList.getData() != null && objResponseGroupList.getData().getUserGroups() != null) {
//
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
