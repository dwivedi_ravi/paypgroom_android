package com.elitesmartsolution.paypgroom.activities.my_bookings;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.elitesmartsolution.paypgroom.models.my_bookings.DataItem;
import com.elitesmartsolution.paypgroom.models.my_bookings.RequestMyBookings;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;

import java.util.List;

public class MyBookingsViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<Boolean> mShowProgressBar;
    private MutableLiveData<List<DataItem>> mBookingDataItems;
    private Context context;

    public MyBookingsViewModel(Application context) {
        super(context);
        this.context = context;
        mText = new MutableLiveData<>();
        mText.setValue("This is my bookings fragment");
        mShowProgressBar = new MutableLiveData<>();
        mShowProgressBar.setValue(false);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<Boolean> getProgressBarStatus() {
        return mShowProgressBar;
    }

    public LiveData<List<DataItem>> getPropertyList() {
        if (mBookingDataItems == null) {
            mBookingDataItems = new MutableLiveData<>();
        }
        return mBookingDataItems;
    }

    public void callListingApi(RequestMyBookings objRequestMyBookings) {
        ApiCallerUtility.callGetMyBookingsApi(context, mBookingDataItems, mShowProgressBar, objRequestMyBookings);
    }
}