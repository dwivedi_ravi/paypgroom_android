package com.elitesmartsolution.paypgroom.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.base.SharedActivity;
import com.elitesmartsolution.paypgroom.databinding.ActivitySignUpBinding;
import com.elitesmartsolution.paypgroom.models.RequestOTP;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.identity.SignInClient;
import com.google.android.material.snackbar.Snackbar;

/**
 * The Main Activity used to display Albums / Media.
 */
public class SignUpActivity extends SharedActivity {

    private ActivitySignUpBinding binding;

    private MyReceiver objMyReceiver;
    private boolean isShowingTraining = false;
    private boolean isDialogShown = false;
    private int screenType = Defaults.SCREEN_TYPE_SIGN_UP;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivitySignUpBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        objMyReceiver = new MyReceiver();
        registerReciever();

        initUi();
    }

    private void initUi() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            screenType = bundle.getInt("screenType", Defaults.SCREEN_TYPE_SIGN_UP);
        }

        if (screenType == Defaults.SCREEN_TYPE_FORGOT_PASSWORD) {
            binding.appbarLayout.toolbar.setTitle(getResources().getString(R.string.verify_its_you));
            binding.contentMainSignUpActivity.title.setText(getResources().getString(R.string.verify_your_phone_number));
            if (Prefs.getMobileNumber().isEmpty()) {
                binding.contentMainSignUpActivity.editPhoneNumber.setEnabled(true);
            } else {
                binding.contentMainSignUpActivity.editPhoneNumber.setText(Prefs.getMobileNumber());
                binding.contentMainSignUpActivity.editPhoneNumber.setEnabled(false);
            }
            binding.contentMainSignUpActivity.containerUserType.setVisibility(View.GONE);
            binding.contentMainSignUpActivity.txtInputLayoutPassword.setVisibility(View.GONE);
            binding.contentMainSignUpActivity.txtInputLayoutConfirmPassword.setVisibility(View.GONE);
        } else {
            binding.appbarLayout.toolbar.setTitle(getResources().getString(R.string.action_sign_up_short));
        }

        setSupportActionBar(binding.appbarLayout.toolbar);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.appbarLayout.toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        binding.appbarLayout.toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        binding.appbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Utility.setStatusBarColor(SignUpActivity.this);

        binding.fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFormValid()) {
                    AlertDialog textDialog = Utility.getTextDialog_Material(SignUpActivity.this, "", getResources().getString(R.string.verify_phone_dialog_detail_1) + binding.contentMainSignUpActivity.editCountryCode.getText().toString() + " " + binding.contentMainSignUpActivity.editPhoneNumber.getText().toString() + getResources().getString(R.string.verify_phone_dialog_detail_2));
                    textDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

//                            Utility.startSmsRetriever(SignUpActivity.this);

                            RequestOTP objRequestOTP = new RequestOTP();
                            if (binding.contentMainSignUpActivity.reBtnTenant.isChecked())
                                objRequestOTP.setUserType(Defaults.USER_TYPE_TENANT);
                            else
                                objRequestOTP.setUserType(Defaults.USER_TYPE_OWNER);
                            objRequestOTP.setCountryCode(binding.contentMainSignUpActivity.editCountryCode.getText().toString());
                            objRequestOTP.setMobile(binding.contentMainSignUpActivity.editPhoneNumber.getText().toString());
                            objRequestOTP.setPassword(binding.contentMainSignUpActivity.editPassword.getText().toString());
                            if (screenType == Defaults.SCREEN_TYPE_FORGOT_PASSWORD)
                                objRequestOTP.setRequestType(Defaults.REQUEST_TYPE_FORGOT_PASSWORD);
                            else
                                objRequestOTP.setRequestType(Defaults.REQUEST_TYPE_REGISTRATION);

                            ApiCallerUtility.callRequestOTPApi(SignUpActivity.this, objRequestOTP, true);
                        }
                    });
                    textDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.edit_action).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            textDialog.dismiss();
                        }
                    });
                    textDialog.show();

                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.app_text_color));
                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(getResources().getDrawable(R.drawable.ripple));
                }
            }
        });

        binding.contentMainSignUpActivity.spnCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
//                        binding.contentMainSignUpActivity.editCountryCode.setText("+00");
                        binding.contentMainSignUpActivity.editCountryCode.setText("+91");
                        break;
                    case 1:
                        binding.contentMainSignUpActivity.editCountryCode.setText("+91");
                        break;
                    case 2:
                        binding.contentMainSignUpActivity.editCountryCode.setText("+267");
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        binding.contentMainSignUpActivity.editCountryCode.setText("+91");
    }

    @Override
    protected void onDestroy() {
        Utility.unRegisterReciever(this, objMyReceiver);
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

//        UNCOMMENT FOR AUTO MOBILE NUMBER SELECTION MENU.
//        if (!Prefs.getIsUserFirstTimeFromSharedPreferences() && !isDialogShown) {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Utility.requestHint(SignUpActivity.this);
//                    isDialogShown = true;
//                }
//            }, 500);
//        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void registerReciever() {
        IntentFilter filter = new IntentFilter();
        filter.setPriority(1);
        filter.addAction(Defaults.ACTION_REFRESH_MEDIA_AFTER_PHOTO_EDITING);
        Utility.registerReciever(SignUpActivity.this, filter, objMyReceiver);
    }

    private boolean isFormValid() {
        if (binding.contentMainSignUpActivity.editPhoneNumber.getText().toString().trim().length() < 8) {
            Snackbar.make(binding.contentMainSignUpActivity.editPhoneNumber, "Please enter valid phone number", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentMainSignUpActivity.txtInputLayoutPassword.getVisibility() == View.VISIBLE && binding.contentMainSignUpActivity.editPassword.getText().toString().trim().length() < 4) {
            Snackbar.make(binding.contentMainSignUpActivity.editPassword, "Please enter valid password", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentMainSignUpActivity.txtInputLayoutConfirmPassword.getVisibility() == View.VISIBLE && binding.contentMainSignUpActivity.editConfirmPassword.getText().toString().trim().length() < 4) {
            Snackbar.make(binding.contentMainSignUpActivity.editConfirmPassword, "Please enter valid confirm password", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (binding.contentMainSignUpActivity.txtInputLayoutPassword.getVisibility() == View.VISIBLE && binding.contentMainSignUpActivity.txtInputLayoutConfirmPassword.getVisibility() == View.VISIBLE && !binding.contentMainSignUpActivity.editPassword.getText().toString().trim().equals(binding.contentMainSignUpActivity.editConfirmPassword.getText().toString().trim())) {
            Snackbar.make(binding.contentMainSignUpActivity.editConfirmPassword, "Passwords do not match", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }
        return true;
    }

    // Obtain the phone number from the result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Defaults.RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                // credential.getId();  <-- will need to process phone number string
                binding.contentMainSignUpActivity.editPhoneNumber.setText(credential.getId());
            }
        }
    }

    @Override
    public void gotReadStoragePermission() {

    }

    @Override
    public void gotLocationPermission(boolean isPermissionGranted) {

    }

//    private void createNotificationChannelForOreoAndAboveDevice() {
//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                // Create channel to show notifications.
//                String channelId = getString(R.string.default_notification_channel_id);
//                String channelName = getString(R.string.default_notification_channel_name);
//                NotificationManager notificationManager =
//                        getSystemService(NotificationManager.class);
//                notificationManager.createNotificationChannel(new NotificationChannel(channelId,
//                        channelName, NotificationManager.IMPORTANCE_LOW));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Defaults.ACTION_REFRESH_MEDIA_AFTER_PHOTO_EDITING)) {
                try {
                    final Bundle bundle = intent.getExtras().getBundle("bundle");
                    if (bundle != null) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
