package com.elitesmartsolution.paypgroom.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.PropertyDetailScreen;
import com.elitesmartsolution.paypgroom.databinding.ListItemViewPagerPropertyImageBinding;
import com.elitesmartsolution.paypgroom.interfaces.ItemClickListener;
import com.elitesmartsolution.paypgroom.models.pglisting.PropertyListItem;
import com.elitesmartsolution.paypgroom.util.Tools;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;


public class PropertyListAdapterActual extends RecyclerView.Adapter<PropertyListAdapterActual.MyViewHolder> {
    private Context mContext;
    private List<PropertyListItem> mPropertyList;
    private String id;
    private int screenType;
    private boolean clipChildren;

    public PropertyListAdapterActual(Context mContext, List<PropertyListItem> list, int screenType) {
        this.mContext = mContext;
        this.mPropertyList = list;
        this.screenType = screenType;
        this.clipChildren = false;
    }

    public PropertyListAdapterActual(Context mContext, List<PropertyListItem> list, int screenType, boolean clipChildren) {
        this.mContext = mContext;
        this.mPropertyList = list;
        this.screenType = screenType;
        this.clipChildren = clipChildren;
    }

    @NonNull
    @Override
    public PropertyListAdapterActual.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_property, parent, false);
        return new PropertyListAdapterActual.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final PropertyListAdapterActual.MyViewHolder holder, final int position) {
        int pos = holder.getBindingAdapterPosition();
        id = mPropertyList.get(pos).getId();
//        if (mListt.get(pos).getIs_favourite().equalsIgnoreCase("0")) {
//            holder.mFav_Property_iv.setImageResource(R.drawable.ic_favunselected);
//        } else {
//            holder.mFav_Property_iv.setImageResource(R.drawable.ic_favselected);
//        }

        ViewPagerAdapter mViewPagerAdapter = new ViewPagerAdapter(mContext, mPropertyList.get(pos), id, screenType);
        holder.mViewPager.setAdapter(mViewPagerAdapter);
        holder.indicator.setViewPager(holder.mViewPager);
        mViewPagerAdapter.notifyDataSetChanged();

        holder.mPropertyname_tv.setText(mPropertyList.get(pos).getPgName());
        if (mPropertyList.get(pos).getPgPricingDetail() != null) {
            holder.mPrice_tv.setText("Rs. " + mPropertyList.get(pos).getPgPricingDetail().getPerMonth());
        } else {
            holder.mPrice_tv.setText("Rs. - ");
        }
//        holder.mProperty_type_tv.setText(mPropertyList.get(pos).getPropertyType().concat(" ").concat("|").concat(" ").concat(mPropertyList.get(pos).getPgRoomType()));
        if (mPropertyList.get(pos).getPropertyType() != null && mPropertyList.get(pos).getPgRoomType() != null) {
            holder.mProperty_type_tv.setText(mPropertyList.get(pos).getPropertyType().concat(" ").concat("|").concat(" ").concat(mPropertyList.get(pos).getPgRoomType()));
        }

        if (mPropertyList.get(pos).getIsFavorite() != null) {
            if (mPropertyList.get(pos).getIsFavorite().equals("1"))
                holder.mFav_Property_iv.setImageResource(R.drawable.ic_favselected);
            else
                holder.mFav_Property_iv.setImageResource(R.drawable.ic_favunselected);
        }

        holder.setmItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int pos, boolean isLongClick) {
                openPropertyDetailsScreen(mContext, mPropertyList.get(pos));
            }
        });

        holder.mFav_Property_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (screenType == Defaults.SCREEN_TYPE_NEAR_BY_FRAGMENT && pos == 0) {
            holder.topMarginView.setVisibility(View.VISIBLE);
        } else {
            holder.topMarginView.setVisibility(View.GONE);
        }

        holder.imgDeleteProperty.setVisibility(View.GONE);

//      TEMPORARILY COMMENTED TO HIDE THIS FUNCTIONALITY FOR PAYPGROOM
//        if (mPropertyList.get(pos).getPgOwnerDetail().getOwnerId().equals(Prefs.getUserId()))
//            holder.imgDeleteProperty.setVisibility(View.VISIBLE);
//        else
//            holder.imgDeleteProperty.setVisibility(View.GONE);
//        holder.imgDeleteProperty.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AlertDialog textDialog = Utility.getTextDialog_Material(((AppCompatActivity) mContext), mContext.getString(R.string.title_delete_property), mContext.getString(R.string.msg_delete_property));
//                textDialog.setButton(DialogInterface.BUTTON_POSITIVE, mContext.getString(R.string.yes_action).toUpperCase(), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        RequestDeletePG objRequestDeletePG = new RequestDeletePG();
//                        objRequestDeletePG.setOwnerId(Prefs.getUserId());
//                        objRequestDeletePG.setPgId(mPropertyList.get(pos).getId());
//                        ApiCallerUtility.callDeletePGApi(mContext, objRequestDeletePG);
//                        textDialog.dismiss();
//                    }
//                });
//                textDialog.setButton(DialogInterface.BUTTON_NEGATIVE, mContext.getString(R.string.no_action).toUpperCase(), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        textDialog.dismiss();
//                    }
//                });
//                textDialog.show();
//
//                textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(mContext.getResources().getColor(R.color.app_text_color));
//                textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(mContext.getResources().getColor(R.color.app_text_color));
//                textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(mContext.getResources().getDrawable(R.drawable.ripple));
//                textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(mContext.getResources().getDrawable(R.drawable.ripple));
//            }
//        });
    }

    public void refreshList(List<PropertyListItem> list) {
        mPropertyList = list;
    }

    @Override
    public int getItemCount() {
        return mPropertyList.size();
    }

    private void openPropertyDetailsScreen(Context context, PropertyListItem currentPropertyListItem) {
        Intent intent = new Intent(context, PropertyDetailScreen.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("currentPropertyListBO", currentPropertyListItem);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ArrayList<String> images = new ArrayList<>();
        AppCompatImageView mFav_Property_iv, imgDeleteProperty;
        ViewPager mViewPager;
        CirclePageIndicator indicator;
        View topMarginView;
        RelativeLayout container;
        private TextView mProperty_type_tv, mPropertyname_tv, mPrice_tv, mPerMonth_tv;
        private ItemClickListener mItemClickListener;

        @SuppressLint("NewApi")
        public MyViewHolder(View itemView) {
            super(itemView);
            mProperty_type_tv = itemView.findViewById(R.id.tv_property_type);
            mPropertyname_tv = itemView.findViewById(R.id.tv_propertyname);
            mPrice_tv = itemView.findViewById(R.id.tv_price);
            mPerMonth_tv = itemView.findViewById(R.id.tv_per_night);
            mViewPager = itemView.findViewById(R.id.viewpager);
            indicator = itemView.findViewById(R.id.indicator);
            mFav_Property_iv = itemView.findViewById(R.id.iv_propertyfav);
            imgDeleteProperty = itemView.findViewById(R.id.iv_propertyDelete);
            topMarginView = itemView.findViewById(R.id.topMarginView);
            container = itemView.findViewById(R.id.container);
            if (clipChildren)
                container.getLayoutParams().width = Tools.getScreenWidth() - 100;

            mViewPager.setClipToOutline(true);
            itemView.setOnClickListener(this);
        }

        public void setmItemClickListener(ItemClickListener mItemClickListener) {
            this.mItemClickListener = mItemClickListener;
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onClick(v, getPosition(), false);
        }
    }

    private class ViewPagerAdapter extends PagerAdapter {
        Context context;
        LayoutInflater layoutInflater;
        String mId;
        PropertyListItem objPropertyListItem;
        private RequestOptions requestOptions;
        private int screenType;

        private ListItemViewPagerPropertyImageBinding binding;

        public ViewPagerAdapter(Context mContext, PropertyListItem objPropertyListItem, String id, int screenType) {
            this.context = mContext;
            this.objPropertyListItem = objPropertyListItem;
            this.mId = id;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.color.mdtp_line_background);
            this.screenType = screenType;
        }

        @Override
        public int getCount() {
            return objPropertyListItem.getPgImages().size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
//            View itemView = layoutInflater.inflate(R.layout.list_item_view_pager_property_image, container, false);
            binding = ListItemViewPagerPropertyImageBinding.inflate(layoutInflater, container, false);
            View itemView = binding.getRoot();

//            if (verified.equals("0")) {
//                binding.bannerImg.setVisibility(View.GONE);
//            } else {
//                binding.bannerImg.setVisibility(View.VISIBLE);
//            }

            if (objPropertyListItem.getIsActive().equals("1"))
                binding.bannerImg.setVisibility(View.VISIBLE);
            else
                binding.bannerImg.setVisibility(View.GONE);

            if (objPropertyListItem.getIsAvailable().equals("1"))
                binding.imgBooked.setVisibility(View.GONE);
            else
                binding.imgBooked.setVisibility(View.VISIBLE);

//            Glide.with(context).load(objPropertyListItem.getPgImages().get(position).getImage().trim())
//                    .apply(requestOptions).into(binding.imageView);

            if (screenType == Defaults.SCREEN_TYPE_PAY_PG_HOME_SCREEN) {
                if (position == 0) {
                    Glide.with(context).load(objPropertyListItem.getPgImages().get(position).getImage().trim())
                            .apply(requestOptions).into(binding.imageView);
                } else {
                }
            } else {
                Glide.with(context).load(objPropertyListItem.getPgImages().get(position).getImage().trim())
                        .apply(requestOptions).into(binding.imageView);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openPropertyDetailsScreen(context, objPropertyListItem);
                }
            });

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((RelativeLayout) object);
        }
    }
}
