package com.elitesmartsolution.paypgroom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.models.SpinnerItemBO;

import java.util.ArrayList;

public class MyCustomSpinnerAdapter extends ArrayAdapter<SpinnerItemBO> {

    public MyCustomSpinnerAdapter(Context context,
                                  ArrayList<SpinnerItemBO> spinnerItemList) {
        super(context, 0, spinnerItemList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView,
                          ViewGroup parent) {
        // It is used to set our custom view. 
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
        }

        TextView textViewName = convertView.findViewById(R.id.text_view);
        SpinnerItemBO currentItem = getItem(position);

        // It is used the name to the TextView when the 
        // current item is not null. 
        if (currentItem != null) {
            textViewName.setText(currentItem.getTitleText());
        }
        return convertView;
    }
} 

