package com.elitesmartsolution.paypgroom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.models.getallnotifications.UserNotificationsItem;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationItemRecyclerViewAdapter extends RecyclerView.Adapter<NotificationItemRecyclerViewAdapter.ViewHolder> {

    private List<UserNotificationsItem> mValues;
    private Context context;
    private RequestOptions requestOptions;

    public NotificationItemRecyclerViewAdapter(Context context, List<UserNotificationsItem> items) {
        this.context = context;
        mValues = items;
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder);
        sort();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_notifications, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        int pos = holder.getBindingAdapterPosition();
        holder.mItem = mValues.get(pos);

        holder.contact_name.setText(mValues.get(pos).getPgName());
        holder.description.setText(mValues.get(pos).getMessagePayload());
        holder.lblNotificationTime.setText(mValues.get(pos).getSentDatetime());

        Glide.with(context).setDefaultRequestOptions(requestOptions).load(mValues.get(pos).getFromUserProfileUrl())
                .into(holder.imgProfile);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent detailIntent = new Intent(context, GroupDetailsActivity.class);
//                Bundle bundle = new Bundle();
//
//                UserGroupsItem currentGroup = new UserGroupsItem();
//                currentGroup.setUserId(Prefs.getUserId());
//                currentGroup.setId(holder.mItem.getGroupId());
//                currentGroup.setName(" ");
//
//                bundle.putSerializable("currentGroup", currentGroup);
//                detailIntent.putExtras(bundle);
//                context.startActivity(detailIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void sort() {
        SimpleDateFormat sdf = new SimpleDateFormat(Defaults.DEVICE_DATE_FORMAT);

        Collections.sort(mValues, (obj1, obj2) -> {
            Date d1 = null;
            Date d2 = null;
            try {
                if (obj1.getSentDatetime() == null || obj2.getSentDatetime() == null) {
                    return 0;
                }
                d1 = sdf.parse(obj1.getSentDatetime());
                d2 = sdf.parse(obj2.getSentDatetime());

                return (d1.getTime() > d2.getTime() ? -1 : 1);     //descending
//                    return (d1.getTime() > d2.getTime() ? 1 : -1);     //ascending
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        });
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView contact_name;
        public final TextView lblNotificationTime;
        public final CircleImageView imgProfile;
        public final ImageView imgNotificationType;
        public View lyt_parent;
        public TextView description;
        public UserNotificationsItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            contact_name = view.findViewById(R.id.contact_name);
            lblNotificationTime = view.findViewById(R.id.lblNotificationTime);
            imgProfile = view.findViewById(R.id.imgProfile);
            imgNotificationType = view.findViewById(R.id.imgNotificationType);
            lyt_parent = view.findViewById(R.id.lyt_parent);
            description = view.findViewById(R.id.description);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
