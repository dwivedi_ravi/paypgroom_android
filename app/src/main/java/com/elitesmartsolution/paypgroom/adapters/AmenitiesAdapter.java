package com.elitesmartsolution.paypgroom.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.models.pgmetadata.AmenitiesItem;
import com.elitesmartsolution.paypgroom.util.Measure;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

import java.util.ArrayList;
import java.util.List;

public class AmenitiesAdapter extends RecyclerView.Adapter<AmenitiesAdapter.ViewHolder> {

    private List<AmenitiesItem> mValues;
    private Context context;
    private boolean mShowCancelButton, mShowCheckbox, isServiceCityDialog;
    private Typeface robotoBold, robotoRegular, robotoMedium;
    private RequestOptions requestOptions;

    public AmenitiesAdapter(Context context, ArrayList<AmenitiesItem> amenitiesItemList, boolean showCancelButton, boolean showCheckbox) {
        this.context = context;
        mValues = amenitiesItemList;
        mShowCancelButton = showCancelButton;
        mShowCheckbox = showCheckbox;
        this.isServiceCityDialog = false;
        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder);
    }

    public AmenitiesAdapter(Context context, ArrayList<AmenitiesItem> amenitiesItemList, boolean showCancelButton, boolean showCheckbox, boolean isServiceCityDialog) {
        this.context = context;
        mValues = amenitiesItemList;
        mShowCancelButton = showCancelButton;
        mShowCheckbox = showCheckbox;
        this.isServiceCityDialog = isServiceCityDialog;
        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_amenities, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        int pos = holder.getBindingAdapterPosition();
        holder.mItem = mValues.get(pos);

        if (mValues.get(pos).getName().isEmpty()) {
            holder.name.setVisibility(View.GONE);
        } else {
            holder.name.setVisibility(View.VISIBLE);
            holder.name.setText(mValues.get(pos).getName());
        }
        if (mValues.get(pos).getDescription().isEmpty()) {
            holder.description.setVisibility(View.GONE);
        } else {
            holder.description.setVisibility(View.VISIBLE);
            holder.description.setText(mValues.get(pos).getDescription());
        }

        if (holder.mItem.getIcon().equals(Defaults.REQUEST_TYPE_SERVICE_CITY_CODE))
            Glide.with(context).setDefaultRequestOptions(requestOptions).load(R.drawable.ic_buildings).centerInside().into(holder.imgIcon);
        else
            Glide.with(context).setDefaultRequestOptions(requestOptions).load(holder.mItem.getIcon()).centerInside().into(holder.imgIcon);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isServiceCityDialog) {
                    Prefs.setServiceCityCode(holder.mItem.getId());
                    Prefs.setServiceCityName(holder.mItem.getName());
                    Utility.sendUpdateListBroadCast(Defaults.ACTION_SERVICE_CITY_CHANGED, context, null);
                    Toast.makeText(context, "City Changed", Toast.LENGTH_LONG).show();
                }
            }
        });

        if (mShowCancelButton) {
            holder.imgCancel.setVisibility(View.VISIBLE);
            holder.imgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mValues.remove(pos);
                    notifyItemRemoved(pos);
                    notifyDataSetChanged();
                }
            });
        } else holder.imgCancel.setVisibility(View.GONE);

        if (mShowCheckbox) {
            holder.chkBoxAvailableStatus.setVisibility(View.VISIBLE);
            holder.chkBoxAvailableStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                    mValues.remove(pos);
//                    notifyItemRemoved(pos);
//                    notifyDataSetChanged();
                    mValues.get(pos).setChecked(b);
                }
            });
        } else holder.chkBoxAvailableStatus.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void refreshList(List<AmenitiesItem> amenitiesItemList) {
        this.mValues = amenitiesItemList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView name;
        public final TextView description;
        public final ImageView imgCancel, imgIcon;
        public final CheckBox chkBoxAvailableStatus;
        public AmenitiesItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            name = view.findViewById(R.id.name);
            description = view.findViewById(R.id.description);
            imgCancel = view.findViewById(R.id.imgCancel);
            imgIcon = view.findViewById(R.id.imgIcon);
            chkBoxAvailableStatus = view.findViewById(R.id.chkBoxAvailableStatus);

//            if (!mShowCheckbox) {
//                imgIcon.getLayoutParams().width = (int) Measure.dpToPx(100, context);
//                imgIcon.getLayoutParams().height = (int) Measure.dpToPx(100, context);
//            }

            if (mShowCancelButton) {
                imgIcon.getLayoutParams().width = (int) Measure.dpToPx(100, context);
                imgIcon.getLayoutParams().height = (int) Measure.dpToPx(100, context);
            }

            name.setTypeface(robotoMedium);
            description.setTypeface(robotoRegular);
        }
    }
}