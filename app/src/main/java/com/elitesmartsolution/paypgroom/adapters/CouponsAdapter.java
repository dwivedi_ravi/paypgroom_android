package com.elitesmartsolution.paypgroom.adapters;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.models.coupons.CoupansItem;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class CouponsAdapter extends RecyclerView.Adapter<CouponsAdapter.ViewHolder> {

    private List<CoupansItem> mValues;
    private Context context;
    private RequestOptions requestOptions;
    private ClipboardManager clipboard;

    public CouponsAdapter(Context context, List<CoupansItem> items) {
        this.context = context;
        mValues = items;
        requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.drawable.profile_place_holder);
        clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        sort();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_coupons, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        int pos = holder.getBindingAdapterPosition();
        holder.mItem = mValues.get(pos);

        holder.contact_name.setText(mValues.get(pos).getCoupanCode());
        holder.description.setText(mValues.get(pos).getDescription());
        if (mValues.get(pos).getEndDate() != null)
            holder.lblNotificationTime.setText("Expires on: " + mValues.get(pos).getEndDate());
        else
            holder.lblNotificationTime.setText("Expires on: NA");

        if (mValues.get(pos).isUsed())
            holder.lblCouponAlreadyUsed.setVisibility(View.VISIBLE);
        else
            holder.lblCouponAlreadyUsed.setVisibility(View.GONE);


//        Glide.with(context).setDefaultRequestOptions(requestOptions).load(mValues.get(pos).getCoupanCode())
//                .into(holder.imgProfile);
        holder.txtPercent.setBackgroundResource(mValues.get(pos).getDrawableResource());
        holder.txtPercent.setText(mValues.get(pos).getDiscountPercentage() + "%");

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent detailIntent = new Intent(context, GroupDetailsActivity.class);
//                Bundle bundle = new Bundle();
//
//                UserGroupsItem currentGroup = new UserGroupsItem();
//                currentGroup.setUserId(Prefs.getUserId());
//                currentGroup.setId(holder.mItem.getGroupId());
//                currentGroup.setName(" ");
//
//                bundle.putSerializable("currentGroup", currentGroup);
//                detailIntent.putExtras(bundle);
//                context.startActivity(detailIntent);

                // Creates a new text clip to put on the clipboard
                ClipData clip = ClipData.newPlainText("COUPON CODE", mValues.get(pos).getCoupanCode());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(context, "Coupon Code Copied", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void sort() {
        SimpleDateFormat sdf = new SimpleDateFormat(Defaults.DEVICE_DATE_FORMAT);

        Collections.sort(mValues, (obj1, obj2) -> {
            Date d1 = null;
            Date d2 = null;
            try {
                if (obj1.getUpdatedAt() == null || obj2.getUpdatedAt() == null) {
                    return 0;
                }
                d1 = sdf.parse(obj1.getUpdatedAt());
                d2 = sdf.parse(obj2.getUpdatedAt());

                return (d1.getTime() > d2.getTime() ? -1 : 1);     //descending
//                    return (d1.getTime() > d2.getTime() ? 1 : -1);     //ascending
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        });
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView contact_name;
        public final TextView lblNotificationTime, lblCouponAlreadyUsed;
        public final TextView txtPercent;
        public final ImageView imgNotificationType;
        public View lyt_parent;
        public TextView description;
        public CoupansItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            contact_name = view.findViewById(R.id.contact_name);
            lblNotificationTime = view.findViewById(R.id.lblNotificationTime);
            lblCouponAlreadyUsed = view.findViewById(R.id.lblCouponAlreadyUsed);
            txtPercent = view.findViewById(R.id.txtPercent);
            imgNotificationType = view.findViewById(R.id.imgNotificationType);
            lyt_parent = view.findViewById(R.id.lyt_parent);
            description = view.findViewById(R.id.description);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
