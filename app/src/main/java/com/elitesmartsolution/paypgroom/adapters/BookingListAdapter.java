package com.elitesmartsolution.paypgroom.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.interfaces.ItemClickListener;
import com.elitesmartsolution.paypgroom.models.my_bookings.DataItem;
import com.elitesmartsolution.paypgroom.util.DateTimeUtils;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class BookingListAdapter extends RecyclerView.Adapter<BookingListAdapter.MyViewHolder> {
    private Context mContext;
    private List<DataItem> mPropertyList;
    private SimpleDateFormat serverDateFormat, deviceDateFormat;
    private Calendar calendar;

    public BookingListAdapter(Context mContext, List<DataItem> list, int screenType) {
        this.mContext = mContext;
        this.mPropertyList = list;
        this.serverDateFormat = new SimpleDateFormat(DateTimeUtils.SERVER_DATE_FORMAT);
        this.deviceDateFormat = new SimpleDateFormat(DateTimeUtils.DEVICE_DATE_FORMAT);
        this.calendar = Calendar.getInstance();
        sort();
    }

    @NonNull
    @Override
    public BookingListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_bookings, parent, false);
        return new BookingListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final BookingListAdapter.MyViewHolder holder, final int position) {
        int pos = holder.getBindingAdapterPosition();
        mPropertyList.get(pos).getId();

        holder.tv_PgName.setText(mPropertyList.get(pos).getPgName());
        holder.tv_Host.setText("Hosted By: " + mPropertyList.get(pos).getOwnerName());

        holder.tv_TenantName.setText("Booked By: " + mPropertyList.get(pos).getTenantName());
        holder.tv_TenantMobile.setText("Mo: " + mPropertyList.get(pos).getTenantMobile());
        holder.tv_BookingDate.setText("Booked On: " + DateTimeUtils.getDisplayDateFromDesiredFormat(mPropertyList.get(position).getCreatedAt(), serverDateFormat, calendar));

        if (mPropertyList.get(pos).getPaymentMode().equals("1")) {
            holder.tv_PaymentMode.setText("Mode: Online");
            holder.lbl_AmountPaid.setText("Amount Paid");
        } else if (mPropertyList.get(pos).getPaymentMode().equals("2")) {
            holder.tv_PaymentMode.setText("Mode: Pay at pg");
            holder.lbl_AmountPaid.setText("To be paid at pg");
        }

//        holder.tv_FromDate.setText("From Date: " + DateTimeUtils.getDisplayDateFromDesiredFormat(mPropertyList.get(position).getFromDate(), DateTimeUtils.DEVICE_DATE_FORMAT));
//        holder.tv_ToDate.setText("To Date: " + DateTimeUtils.getDisplayDateFromDesiredFormat(mPropertyList.get(position).getToDate(), DateTimeUtils.DEVICE_DATE_FORMAT));
        holder.tv_FromDate.setText("From Date: " + DateTimeUtils.getDisplayDateFromDesiredFormat(mPropertyList.get(position).getFromDate(), deviceDateFormat, calendar));
        holder.tv_ToDate.setText("To Date: " + DateTimeUtils.getDisplayDateFromDesiredFormat(mPropertyList.get(position).getToDate(), deviceDateFormat, calendar));

        holder.tv_PerMonth.setText("Monthly Rent: " + mPropertyList.get(pos).getBookingPrice());
        holder.tv_SecurityDeposit.setText("Security Deposit: " + mPropertyList.get(pos).getSecurityDeposit());
        holder.tv_CleaningFees.setText("Cleaning Fees: " + mPropertyList.get(pos).getCleaningFee());

        //            DISCOUNT COMMENTED BY RAVI
        if (mPropertyList.get(pos).getDiscount() != null && !mPropertyList.get(pos).getDiscount().isEmpty()) {
            holder.tv_Discount.setText("Discount: " + mPropertyList.get(pos).getDiscount());
            if (mPropertyList.get(pos).getCoupanCodeApplied() != null && !mPropertyList.get(pos).getCoupanCodeApplied().isEmpty()) {
                holder.tv_CouponCode.setVisibility(View.VISIBLE);
                holder.tv_CouponCode.setText("Coupon: " + mPropertyList.get(pos).getCoupanCodeApplied());
            } else {
                holder.tv_CouponCode.setVisibility(View.GONE);
            }
        } else {
            holder.tv_Discount.setText("Discount: -");
            holder.tv_Discount.setVisibility(View.GONE);
            holder.tv_CouponCode.setVisibility(View.GONE);
        }
        //            DISCOUNT COMMENTED BY RAVI TILL HERE

        holder.tv_TotalAmount.setText("Total Amount: " + mPropertyList.get(pos).getTotalAmount());
        holder.tv_AmountPaid.setText(mPropertyList.get(pos).getTotalAmountAfterDiscount());
        holder.tv_TransactionStatus.setText(mPropertyList.get(pos).getTransactionStatus() != null ? mPropertyList.get(pos).getTransactionStatus().toUpperCase() : "FAILED");

        if (holder.tv_TransactionStatus.getText().toString().toLowerCase().contains("fail"))
            holder.tv_TransactionStatus.setTextColor(Color.RED);

        if (mPropertyList.get(pos).getPayuTransactionResponse() != null && mPropertyList.get(pos).getPayuTransactionResponse().getErrorMessage() != null && !mPropertyList.get(pos).getPayuTransactionResponse().getErrorMessage().toLowerCase().contains("no error")) {
            holder.tv_FailedReason.setText("Reason: " + mPropertyList.get(pos).getPayuTransactionResponse().getErrorMessage());
            holder.tv_FailedReason.setVisibility(View.VISIBLE);
        } else {
            holder.tv_FailedReason.setVisibility(View.GONE);
        }

        holder.setmItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int pos, boolean isLongClick) {
                openPropertyDetailsScreen(mContext, mPropertyList.get(pos));
            }
        });
    }

    public void refreshList(List<DataItem> list) {
        mPropertyList = list;
        sort();
    }

    @Override
    public int getItemCount() {
        return mPropertyList.size();
    }

    private void openPropertyDetailsScreen(Context context, DataItem currentPropertyListItem) {
//        Intent intent = new Intent(context, PropertyDetailScreen.class);
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("currentPropertyListBO", currentPropertyListItem);
//        intent.putExtras(bundle);
//        context.startActivity(intent);
    }

    public void sort() {
        SimpleDateFormat sdf = new SimpleDateFormat(Defaults.SERVER_DATE_FORMAT);

        Collections.sort(mPropertyList, (obj1, obj2) -> {
            Date d1 = null;
            Date d2 = null;
            try {
                if (obj1.getUpdatedAt() == null || obj2.getUpdatedAt() == null) {
                    return 0;
                }
                d1 = sdf.parse(obj1.getUpdatedAt());
                d2 = sdf.parse(obj2.getUpdatedAt());

                return (d1.getTime() > d2.getTime() ? -1 : 1);     //descending
//                    return (d1.getTime() > d2.getTime() ? 1 : -1);     //ascending
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        });
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_PgName, tv_Host, tv_PaymentMode, tv_FromDate, tv_ToDate, tv_PerMonth, tv_SecurityDeposit, tv_CleaningFees, tv_Discount, tv_TotalAmount, tv_AmountPaid, lbl_AmountPaid, tv_TransactionStatus, tv_TenantName, tv_TenantMobile, tv_BookingDate, tv_CouponCode, tv_FailedReason;
        private ItemClickListener mItemClickListener;

        @SuppressLint("NewApi")
        public MyViewHolder(View itemView) {
            super(itemView);

            tv_PgName = itemView.findViewById(R.id.tv_PgName);
            tv_Host = itemView.findViewById(R.id.tv_Host);
            tv_PaymentMode = itemView.findViewById(R.id.tv_PaymentMode);
            tv_FromDate = itemView.findViewById(R.id.tv_FromDate);
            tv_ToDate = itemView.findViewById(R.id.tv_ToDate);
            tv_PerMonth = itemView.findViewById(R.id.tv_PerMonth);
            tv_SecurityDeposit = itemView.findViewById(R.id.tv_SecurityDeposit);
            tv_CleaningFees = itemView.findViewById(R.id.tv_CleaningFees);
            tv_Discount = itemView.findViewById(R.id.tv_Discount);
            tv_TotalAmount = itemView.findViewById(R.id.tv_TotalAmount);
            tv_AmountPaid = itemView.findViewById(R.id.tv_AmountPaid);
            lbl_AmountPaid = itemView.findViewById(R.id.lbl_AmountPaid);
            tv_TransactionStatus = itemView.findViewById(R.id.tv_TransactionStatus);
            tv_TenantName = itemView.findViewById(R.id.tv_TenantName);
            tv_TenantMobile = itemView.findViewById(R.id.tv_TenantMobile);
            tv_BookingDate = itemView.findViewById(R.id.tv_BookingDate);
            tv_CouponCode = itemView.findViewById(R.id.tv_CouponCode);
            tv_FailedReason = itemView.findViewById(R.id.tv_FailedReason);

            itemView.setOnClickListener(this);
        }

        public void setmItemClickListener(ItemClickListener mItemClickListener) {
            this.mItemClickListener = mItemClickListener;
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onClick(v, getPosition(), false);
        }
    }
}
