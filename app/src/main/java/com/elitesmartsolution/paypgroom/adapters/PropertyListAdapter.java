package com.elitesmartsolution.paypgroom.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.PropertyDetailScreen;
import com.elitesmartsolution.paypgroom.databinding.ListItemViewPagerPropertyImageBinding;
import com.elitesmartsolution.paypgroom.interfaces.ItemClickListener;
import com.elitesmartsolution.paypgroom.models.PropertyListBO;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;


public class PropertyListAdapter extends RecyclerView.Adapter<PropertyListAdapter.MyViewHolder> {
    private Context mContext;
    private List<PropertyListBO> mPropertyList;
    private String id;
    private int screenType;

    public PropertyListAdapter(Context mContext, List<PropertyListBO> list, int screenType) {
        this.mContext = mContext;
        this.mPropertyList = list;
        this.screenType = screenType;
    }

    @NonNull
    @Override
    public PropertyListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_property, parent, false);
        return new PropertyListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final PropertyListAdapter.MyViewHolder holder, final int position) {
        int pos = holder.getBindingAdapterPosition();
        id = mPropertyList.get(pos).getId();

//        ViewPagerAdapter mViewPagerAdapter = new ViewPagerAdapter(mContext, mListt.get(pos).getProperty_images(), id, mListt.get(pos).getLat(), mListt.get(pos).getLng(), mListt.get(pos).getLocation(), mListt.get(pos).getVerified(), "1");
//        holder.mViewPager.setAdapter(mViewPagerAdapter);
//        holder.indicator.setViewPager(holder.mViewPager);
//        mViewPagerAdapter.notifyDataSetChanged();

//        holder.mProperty_type_tv.setText(mListt.get(pos).getProperty_category().concat(" ").concat("|").concat(" ").concat(mListt.get(pos).getRoom_type()));
//        holder.mPropertyname_tv.setText(mListt.get(pos).getTitle());

//        if (!mListt.get(pos).getPrice_per_night().equalsIgnoreCase("0")) {
//            holder.mPrice_tv.setText(mCurrencyCode.concat(mListt.get(pos).getPrice_per_night()));
//        } else {
//            holder.mPrice_tv.setVisibility(View.INVISIBLE);
//            holder.mPerMonth_tv.setVisibility(View.INVISIBLE);
//        }

//        if (mListt.get(pos).getIs_favourite().equalsIgnoreCase("0")) {
//            holder.mFav_Property_iv.setImageResource(R.drawable.ic_favunselected);
//        } else {
//            holder.mFav_Property_iv.setImageResource(R.drawable.ic_favselected);
//        }
        ViewPagerAdapter mViewPagerAdapter = new ViewPagerAdapter(mContext, mPropertyList.get(pos), id);
        holder.mViewPager.setAdapter(mViewPagerAdapter);
        holder.indicator.setViewPager(holder.mViewPager);
        mViewPagerAdapter.notifyDataSetChanged();

//        holder.mProperty_type_tv.setText(mPropertyList.get(pos).getFirstname());
        holder.mPropertyname_tv.setText(mPropertyList.get(pos).getName());
        holder.mPrice_tv.setText("Rs. 10,000");
        holder.mFav_Property_iv.setImageResource(R.drawable.ic_favselected);

        holder.setmItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int pos, boolean isLongClick) {
                openPropertyDetailsScreen(mContext, mPropertyList.get(pos));
            }
        });

        holder.mFav_Property_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (screenType == Defaults.SCREEN_TYPE_NEAR_BY_FRAGMENT && pos == 0) {
            holder.topMarginView.setVisibility(View.VISIBLE);
        } else {
            holder.topMarginView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mPropertyList.size();
    }

    private void openPropertyDetailsScreen(Context context, PropertyListBO currentPropertyListBO) {
        Intent intent = new Intent(context, PropertyDetailScreen.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("currentPropertyListBO", currentPropertyListBO);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ArrayList<String> images = new ArrayList<>();
        AppCompatImageView mFav_Property_iv;
        ViewPager mViewPager;
        CirclePageIndicator indicator;
        View topMarginView;
        private TextView mProperty_type_tv, mPropertyname_tv, mPrice_tv, mPerMonth_tv;
        private ItemClickListener mItemClickListener;

        @SuppressLint("NewApi")
        public MyViewHolder(View itemView) {
            super(itemView);
            mProperty_type_tv = itemView.findViewById(R.id.tv_property_type);
            mPropertyname_tv = itemView.findViewById(R.id.tv_propertyname);
            mPrice_tv = itemView.findViewById(R.id.tv_price);
            mPerMonth_tv = itemView.findViewById(R.id.tv_per_night);
            mViewPager = itemView.findViewById(R.id.viewpager);
            indicator = itemView.findViewById(R.id.indicator);
            mFav_Property_iv = itemView.findViewById(R.id.iv_propertyfav);
            topMarginView = itemView.findViewById(R.id.topMarginView);

            mViewPager.setClipToOutline(true);
            itemView.setOnClickListener(this);
        }

        public void setmItemClickListener(ItemClickListener mItemClickListener) {
            this.mItemClickListener = mItemClickListener;
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onClick(v, getPosition(), false);
        }
    }

    private class ViewPagerAdapter extends PagerAdapter {
        Context context;
        LayoutInflater layoutInflater;
        String mId;
        PropertyListBO objPropertyListBO;
        private RequestOptions requestOptions;
        private ListItemViewPagerPropertyImageBinding binding;

        public ViewPagerAdapter(Context mContext, PropertyListBO objPropertyListBO, String id) {
            this.context = mContext;
            this.objPropertyListBO = objPropertyListBO;
            this.mId = id;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            requestOptions = Utility.getCustomizedRequestOptions(R.color.mdtp_line_background, R.color.mdtp_line_background);
        }

        @Override
        public int getCount() {
            return objPropertyListBO.getmPropertyImagesList().size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int pos) {
//            View itemView = layoutInflater.inflate(R.layout.list_item_view_pager_property_image, container, false);
            binding = ListItemViewPagerPropertyImageBinding.inflate(layoutInflater, container, false);
            View itemView = binding.getRoot();

//            if (verified.equals("0")) {
//                binding.bannerImg.setVisibility(View.GONE);
//            } else {
//                binding.bannerImg.setVisibility(View.VISIBLE);
//            }

            if (objPropertyListBO.getmPropertyImagesList().get(pos).getId().contains("1"))
                binding.bannerImg.setVisibility(View.VISIBLE);
            if (objPropertyListBO.getmPropertyImagesList().get(pos).getId().contains("2"))
                binding.bannerImg.setVisibility(View.GONE);
            else
                binding.bannerImg.setVisibility(View.VISIBLE);

            Glide.with(context).load(objPropertyListBO.getmPropertyImagesList().get(pos).getImageResourceId())
                    .apply(requestOptions).into(binding.imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openPropertyDetailsScreen(context, objPropertyListBO);
                }
            });

            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int pos, @NonNull Object object) {
            container.removeView((RelativeLayout) object);
        }
    }
}
