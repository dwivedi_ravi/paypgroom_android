package com.elitesmartsolution.paypgroom.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.models.bookpg.GuestItem;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;

import java.util.List;

public class GuestListAdapter extends RecyclerView.Adapter<GuestListAdapter.ViewHolder> {

    private List<GuestItem> mValues;
    private Context context;
    private boolean mShowCancelButton;
    private Typeface robotoBold, robotoRegular, robotoMedium;

    public GuestListAdapter(Context context, List<GuestItem> questList, boolean showCancelButton) {
        this.context = context;
        mValues = questList;
        mShowCancelButton = showCancelButton;
        this.robotoBold = Typeface.createFromAsset(context.getAssets(),
                "roboto_bold.ttf");
        this.robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "roboto_regular.ttf");
        this.robotoMedium = Typeface.createFromAsset(context.getAssets(),
                "roboto_medium.ttf");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_quest, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        int pos = holder.getBindingAdapterPosition();
        holder.mItem = mValues.get(pos);

        holder.contact_name.setText(mValues.get(pos).getGuestName());
        holder.txtAddress.setText("Mo: " + mValues.get(pos).getGuestMobile());
        holder.txtMobile.setText(mValues.get(pos).getGuestDocumentNumber());
        holder.txtEmail.setText(mValues.get(pos).getGuestDocumentType());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        if (mShowCancelButton) {
            holder.imgCancel.setVisibility(View.VISIBLE);
            holder.imgCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mValues.remove(pos);
                    notifyItemRemoved(pos);
                    notifyDataSetChanged();
                    Utility.sendUpdateListBroadCast(Defaults.ACTION_GUEST_REMOVED, context, null);
                }
            });
        } else
            holder.imgCancel.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void refreshList(List<GuestItem> guestItemList) {
        this.mValues = guestItemList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView contact_name;
        public final TextView txtAddress, txtMobile, txtEmail;
        public final ImageView imgCancel;
        public GuestItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            contact_name = view.findViewById(R.id.contact_name);
            txtAddress = view.findViewById(R.id.txtAddress);
            txtMobile = view.findViewById(R.id.txtMobile);
            txtEmail = view.findViewById(R.id.txtEmail);
            imgCancel = view.findViewById(R.id.imgCancel);

            contact_name.setTypeface(robotoMedium);
            txtAddress.setTypeface(robotoRegular);
            txtEmail.setTypeface(robotoRegular);
            txtMobile.setTypeface(robotoRegular);
        }
    }
}