package com.elitesmartsolution.paypgroom.payumoney;

public enum AppEnvironment {

    SANDBOX {
        @Override
        public String merchant_Name() {
            return "PayPg";
        }

//        @Override
//        public String merchant_Key() {
//            return "gtKFFx";
//        } // PAUY TEST CREDENTIALS

        @Override
        public String merchant_Key() {
            return "GXvSDH8O";
        }

        @Override
        public String merchant_ID() {
            return "7230978";
        }

        @Override
        public String furl() {
            return "https://payuresponse.firebaseapp.com/failure";
        }

        @Override
        public String surl() {
            return "https://payuresponse.firebaseapp.com/success";
        }

        @Override
        public String salt() {
            return "";
        } // PAUY TEST CREDENTIALS

//        @Override
//        public String salt() {
//            return "wia56q6O";
//        } // PAUY TEST CREDENTIALS

        @Override
        public boolean production() {
            return false;
        }
    },

    PRODUCTION {
        @Override
        public String merchant_Name() {
            return "PayPg";
        }

        @Override
        public String merchant_Key() {
            return "GXvSDH8O";
        }

        @Override
        public String merchant_ID() {
            return "7230978";
        }

        @Override
        public String furl() {
            return "https://payuresponse.firebaseapp.com/failure";
        }

        @Override
        public String surl() {
            return "https://payuresponse.firebaseapp.com/success";
        }

        @Override
        public String salt() {
            return "";
        }

        @Override
        public boolean production() {
            return true;
        }
    };

    public abstract String merchant_Name();

    public abstract String merchant_Key();

    public abstract String merchant_ID();

    public abstract String furl();

    public abstract String surl();

    public abstract String salt();

    public abstract boolean production();
}