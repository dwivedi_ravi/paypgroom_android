package com.elitesmartsolution.paypgroom.widget;

import android.graphics.Color;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.elitesmartsolution.paypgroom.interfaces.DateTimeUpdateListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Date;

/**
 * This class is to manage the utility like fetching the current date
 * and display widget i.e calender(date picker) and time picker.
 * <p/>
 * Created by Ravi D. on 29-04-2016.
 */


public class DateTimePicker implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    static DateTimePicker instance;
    DateTimeUpdateListener updateListener;
    TextView textView;

    public static DateTimePicker getInstance() {
        if (instance == null)
            instance = new DateTimePicker();
        return instance;
    }

    /**
     * Method to show the time picker dialog
     *
     * @param activity       : instance of an activity from which request to open dialog has called
     * @param updateListener : listener object to send the result back to activity
     * @param date           : if date is already selected then selected date should be set on datepicker
     * @param textView       : view to set the result string
     */
    public void getDate(AppCompatActivity activity, DateTimeUpdateListener updateListener, Date date, TextView textView) {
        this.updateListener = updateListener;
        this.textView = textView;
        Calendar now = Calendar.getInstance();
        if (date != null) {
            now.setTime(date);
        }
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                getInstance(),
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        if (dpd != null) {
            dpd.setOnDateSetListener(this);
//            dpd.setAccentColor(Color.parseColor("#21bfd0"));
//            dpd.setAccentColor(activity.getResources().getColor(R.color.colorAccent));
            dpd.setAccentColor(Color.parseColor("#d61339"));
            dpd.setOkColor(Color.parseColor("#000000"));
            dpd.setCancelColor(Color.parseColor("#000000"));
        }

        dpd.show(activity.getSupportFragmentManager(), "Datepickerdialog");
    }


    /**
     * Method to show the time picker dialog
     *
     * @param activity       : instance of an activity from which request to open dialog has called
     * @param updateListener : listener object to send the result back to activity
     * @param date           : if time is already selected then selected time should be set on timepicker
     * @param textView       : view to set the result string
     */
    public void getTime(AppCompatActivity activity, DateTimeUpdateListener updateListener, Date date, TextView textView) {
        getTime(activity, updateListener, date, textView, false);
    }

    /**
     * Method to show the time picker dialog
     *
     * @param activity        : instance of an activity from which request to open dialog has called
     * @param updateListener  : listener object to send the result back to activity
     * @param date            : if time is already selected then selected time should be set on timepicker
     * @param textView        : view to set the result string
     * @param isIn24HrsFormat : whether time is required in 24 hours format or not
     */
    public void getTime(AppCompatActivity activity, DateTimeUpdateListener updateListener, Date date, TextView textView, boolean isIn24HrsFormat) {
        this.updateListener = updateListener;
        this.textView = textView;
        Calendar now = Calendar.getInstance();
        if (date != null) {
            now.setTime(date);
        }
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                getInstance(),
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                isIn24HrsFormat
        );
        if (tpd != null) {
            tpd.setOnTimeSetListener(this);
//            tpd.setAccentColor(Color.parseColor("#21bfd0"));
            tpd.setAccentColor(Color.parseColor("#d61339"));
            tpd.setOkColor(Color.parseColor("#000000"));
            tpd.setCancelColor(Color.parseColor("#000000"));
        }
        tpd.show(activity.getSupportFragmentManager(), "Timepickerdialog");
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: " + dayOfMonth + "/" + (monthOfYear) + "/" + year;
        monthOfYear++;
        String dayString = dayOfMonth < 10 ? "0" + dayOfMonth : "" + dayOfMonth;
        String monthString = monthOfYear < 10 ? "0" + monthOfYear : "" + monthOfYear;
//        resultTV.setText(date);
        if (updateListener != null) {
            updateListener.onDateUpdate(year + "", monthString, dayString, textView);
        }
    }

//    @Override
//    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
//        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
//        String minuteString = minute < 10 ? "0" + minute : "" + minute;
//        String secondString = second < 10 ? "0" + second : "" + second;
////        String time = "You picked the following time: " + hourString + "h" + minuteString + "m" + secondString + "s";
////        resultTV.setText(time);
//
//        int hours24 = hourOfDay;
//
//        String timeSet = "";
//        if (hourOfDay > 12) {
//            hourOfDay -= 12;
//            timeSet = "PM";
//        } else if (hourOfDay == 0) {
//            hourOfDay += 12;
//            timeSet = "AM";
//        } else if (hourOfDay == 12) {
//            timeSet = "PM";
//        } else {
//            timeSet = "AM";
//        }
//
//
//        if (updateListener != null) {
//            updateListener.onTimeUpdate(hourOfDay, minute, second, timeSet, textView);
//            updateListener.onTimeUpdate(hours24, minute, second, textView);
//        }
//    }

    public void setTime(AppCompatActivity activity, DateTimeUpdateListener updateListener, Date date, TextView textView, boolean isIn24HrsFormat, String inputTime) {
        this.updateListener = updateListener;
        this.textView = textView;
        if (inputTime != null && inputTime.contains(":")) {
            String[] parts = inputTime.split(":", 2);
            TimePickerDialog tpd = TimePickerDialog.newInstance(
                    getInstance(),
                    Integer.parseInt(parts[0]),
                    Integer.parseInt(parts[1]),
                    isIn24HrsFormat
            );

            if (tpd != null) {
                tpd.setOnTimeSetListener(this);
//            tpd.setAccentColor(Color.parseColor("#21bfd0"));
                tpd.setAccentColor(Color.parseColor("#d61339"));
                tpd.setOkColor(Color.parseColor("#000000"));
                tpd.setCancelColor(Color.parseColor("#000000"));
            }
            tpd.show(activity.getSupportFragmentManager(), "Timepickerdialog");
        }
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
//        String time = "You picked the following time: " + hourString + "h" + minuteString + "m" + secondString + "s";
//        resultTV.setText(time);

        int hours24 = hourOfDay;

        String timeSet = "";
        if (hourOfDay > 12) {
            hourOfDay -= 12;
            timeSet = "PM";
        } else if (hourOfDay == 0) {
            hourOfDay += 12;
            timeSet = "AM";
        } else if (hourOfDay == 12) {
            timeSet = "PM";
        } else {
            timeSet = "AM";
        }


        if (updateListener != null) {
            updateListener.onTimeUpdate(hourOfDay, minute, second, timeSet, textView);
            updateListener.onTimeUpdate(hours24, minute, second, textView);
        }
    }
}
