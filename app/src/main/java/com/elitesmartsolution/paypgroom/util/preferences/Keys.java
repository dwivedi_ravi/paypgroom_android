package com.elitesmartsolution.paypgroom.util.preferences;

/**
 * Class for Preference keys.
 */
public final class Keys {

    public static final String PREF_USER_FIRST_TIME = "user_first_time";
    public static final String PREF_UNIQUE_INTEGER_VALUE = "unique_integer_value";
    public static final String PREF_VERSION_CODE_INFO = "version_code_info";
    public static final String PREF_SHOW_TRAINING_MAIN_SCREEN = "show_training_main_screen";
    public static final String PREF_SHOW_TRAINING_DETAIL_SCREEN = "show_training_detail_screen";
    public static final String PREF_ANIMATIONS_DISABLED = "disable_animations";
    public static final String PREF_AUTH_KEY = "auth_key";
    public static final String PREF_PHONE_NUMBER_VERIFIED = "phone_number_verified";
    public static final String PREF_PROFILE_STATUS = "profile_status";
    public static final String PREF_MOBILE_NUMBER = "mobile_number";
    public static final String PREF_USER_ID = "user_id";
    public static final String PREF_USER_NAME = "user_name";
    public static final String PREF_LOGOUT_MOBILE_NUMBER = "logout_mobile_number";
    //    FIREBASE
    public static final String PREF_FIREBASE_INSTANCE_ID = "firebase_instance_id";
    public static final String PREF_FIREBASE_GLOBAL_BROADCAST_TOPIC_SUBSCRIPTION = "firebase_paypgroom_global_topic_subscription";
    public static final String PREF_FIREBASE_INDIAN_BROADCAST_TOPIC_SUBSCRIPTION = "firebase_paypgroom_indian_topic_subscription";
    public static final String PREF_REGISTER_PUSH_TOKEN_STATUS = "register_push_token_status";

    public static final String PREF_SERVICE_CITY_CODE = "service_city_code";
    public static final String PREF_USER_TYPE = "user_type";
    public static final String PREF_IS_PRODUCTION_ENVIRONMENT = "is_prod_env";
    public static final String PREF_SERVICE_CITY_NAME = "service_city_name";
    public static final String PREF_SELECTED_SERVICE_TYPE = "selected_service_type";
    public static final String PREF_IS_RESTART_APP = "is_restart_app";

    // Prevent class instantiation
    private Keys() {
    }
}
