package com.elitesmartsolution.paypgroom.util.preferences;

import android.content.Context;

import androidx.annotation.NonNull;

/**
 * Class for storing & retrieving application data.
 * <p>
 * Abstracts clients from from DB / SharedPreferences / Hawk.
 */
public class Prefs {

    private static SharedPrefs sharedPrefs;

    /**
     * Initialise the Prefs object for future static usage.
     * Make sure to initialise this in Application class.
     *
     * @param context The context to initialise with.
     */
    public static void init(@NonNull Context context) {
        if (sharedPrefs != null) {
            throw new RuntimeException("Prefs has already been instantiated");
        }
        sharedPrefs = new SharedPrefs(context);
    }

    @NonNull
    private static SharedPrefs getPrefs() {
        if (sharedPrefs == null) {
            throw new RuntimeException("Prefs has not been instantiated. Call init() with context");
        }
        return sharedPrefs;
    }

    @Deprecated
    public static void setToggleValue(@NonNull String key, boolean value) {
        getPrefs().put(key, value);
    }

    @Deprecated
    public static boolean getToggleValue(@NonNull String key, boolean defaultValue) {
        return getPrefs().get(key, defaultValue);
    }

    public static void setIsUserFirstTimeInSharedPreferences(boolean value) {
        getPrefs().put(Keys.PREF_USER_FIRST_TIME, value);
    }

    public static boolean getIsUserFirstTimeFromSharedPreferences() {
        return getPrefs().get(Keys.PREF_USER_FIRST_TIME, true);
    }

    /**
     * Get Incremental Integer Number.
     */
    public static int getUniqueIntegerValueFromSharedPrefs() {
        int val = getPrefs().get(Keys.PREF_UNIQUE_INTEGER_VALUE, 0) + 1;
        setUniqueIntegerValueInSharedPrefs(val);
        return val;
    }

    /**
     * Set Unique Incremental Integer In Shared Prefs
     */
    public static void setUniqueIntegerValueInSharedPrefs(int value) {
        getPrefs().put(Keys.PREF_UNIQUE_INTEGER_VALUE, value);
    }

    /**
     * Get Current Application Version Code.
     */
    public static int getCurrentVersionCodeFromSharedPrefs() {
        return getPrefs().get(Keys.PREF_VERSION_CODE_INFO, 1);
    }

    /**
     * Set Current Application Version Code.
     */
    public static void setCurrentVersionCodeInSharedPrefs(int value) {
        getPrefs().put(Keys.PREF_VERSION_CODE_INFO, value);
    }

    public static boolean getIsShowTrainingOnMainScreen() {
        return getPrefs().get(Keys.PREF_SHOW_TRAINING_MAIN_SCREEN, true);
    }

    public static void setIsShowTrainingOnMainScreen(boolean value) {
        getPrefs().put(Keys.PREF_SHOW_TRAINING_MAIN_SCREEN, value);
    }

    public static boolean getIsShowTrainingOnDetailScreen() {
        return getPrefs().get(Keys.PREF_SHOW_TRAINING_DETAIL_SCREEN, true);
    }

    public static void setIsShowTrainingOnDetailScreen(boolean value) {
        getPrefs().put(Keys.PREF_SHOW_TRAINING_DETAIL_SCREEN, value);
    }

    public static String getAuthKey() {
        return getPrefs().get(Keys.PREF_AUTH_KEY, "");
    }

    public static void setAuthKey(String value) {
        getPrefs().put(Keys.PREF_AUTH_KEY, value);
    }

    public static boolean isPhoneNumberVerified() {
        return getPrefs().get(Keys.PREF_PHONE_NUMBER_VERIFIED, false);
    }

    public static void setPhoneNumberVerified(boolean value) {
        getPrefs().put(Keys.PREF_PHONE_NUMBER_VERIFIED, value);
    }

    public static boolean isProfileCompleted() {
        return getPrefs().get(Keys.PREF_PROFILE_STATUS, false);
    }

    public static void setProfileCompleted(boolean value) {
        getPrefs().put(Keys.PREF_PROFILE_STATUS, value);
    }

    public static void clearAllPreferenceValues() {
        getPrefs().clearAllPreferenceValues();
    }

    public static String getMobileNumber() {
        return getPrefs().get(Keys.PREF_MOBILE_NUMBER, "");
    }

    public static void setMobileNumber(String value) {
        getPrefs().put(Keys.PREF_MOBILE_NUMBER, value);
    }

    public static String getLogoutMobileNumber() {
        return getPrefs().get(Keys.PREF_LOGOUT_MOBILE_NUMBER, "");
    }

    public static void setLogoutMobileNumber(String value) {
        getPrefs().put(Keys.PREF_LOGOUT_MOBILE_NUMBER, value);
    }

    public static String getUserId() {
        return getPrefs().get(Keys.PREF_USER_ID, "");
    }

    public static void setUserId(String value) {
        getPrefs().put(Keys.PREF_USER_ID, value);
    }

    public static String getUserName() {
        return getPrefs().get(Keys.PREF_USER_NAME, "");
    }

    public static void setUserName(String value) {
        getPrefs().put(Keys.PREF_USER_NAME, value);
    }

    /**
     * Should use animations
     */
    public static boolean animationsEnabled() {
        return !getPrefs().get(Keys.PREF_ANIMATIONS_DISABLED, Defaults.ANIMATIONS_DISABLED);
    }

    //    FIREBASE
    public static void setFirebaseInstanceIdInSharedPreferences(String value) {
        getPrefs().put(Keys.PREF_FIREBASE_INSTANCE_ID, value);
    }

    public static String getFirebaseInstanceIdFromSharedPreferences() {
        return getPrefs().get(Keys.PREF_FIREBASE_INSTANCE_ID, "");
    }

    public static void setIsGlobalBroadcastTopicSubscribedInSharedPreferences(boolean value) {
        getPrefs().put(Keys.PREF_FIREBASE_GLOBAL_BROADCAST_TOPIC_SUBSCRIPTION, value);
    }

    public static boolean getIsGlobalBroadcastTopicSubscribedFromSharedPreferences() {
        return getPrefs().get(Keys.PREF_FIREBASE_GLOBAL_BROADCAST_TOPIC_SUBSCRIPTION, false);
    }

    public static void setIsIndianBroadcastTopicSubscribedInSharedPreferences(boolean value) {
        getPrefs().put(Keys.PREF_FIREBASE_INDIAN_BROADCAST_TOPIC_SUBSCRIPTION, value);
    }

    public static boolean getIsIndianBroadcastTopicSubscribedFromSharedPreferences() {
        return getPrefs().get(Keys.PREF_FIREBASE_INDIAN_BROADCAST_TOPIC_SUBSCRIPTION, false);
    }

    public static boolean isPushTokenRegistered() {
        return getPrefs().get(Keys.PREF_REGISTER_PUSH_TOKEN_STATUS, false);
    }

    public static void setPushTokenRegistered(boolean value) {
        getPrefs().put(Keys.PREF_REGISTER_PUSH_TOKEN_STATUS, value);
    }

    public static void setStringValue(String key, String value) {
        getPrefs().put(key, value);
    }

    public static String getStringValue(String key) {
        return getPrefs().get(key, "");
    }

    public static String getStringValue(String key, String defaultValue) {
        return getPrefs().get(key, defaultValue);
    }

    public static void setBooleanValue(String key, boolean value) {
        getPrefs().put(key, value);
    }

    public static boolean getBooleanValue(String key) {
        return getPrefs().get(key, false);
    }

    public static String getServiceCityCode() {
        return getPrefs().get(Keys.PREF_SERVICE_CITY_CODE, Defaults.TEMP_SERVICE_CITY_CODE);
    }

    public static void setServiceCityCode(String value) {
        getPrefs().put(Keys.PREF_SERVICE_CITY_CODE, value);
    }

    public static String getServiceCityName() {
        return getPrefs().get(Keys.PREF_SERVICE_CITY_NAME, Defaults.TEMP_SERVICE_CITY_NAME);
    }

    public static void setServiceCityName(String value) {
        getPrefs().put(Keys.PREF_SERVICE_CITY_NAME, value);
    }

}
