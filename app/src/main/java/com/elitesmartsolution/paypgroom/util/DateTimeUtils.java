package com.elitesmartsolution.paypgroom.util;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Sachin.M on 10-05-2016.
 */
public class DateTimeUtils {
    //    public static final String WHOLE_DATE_WITHOUT_SEPARATOR = "ddMMyyyyhhmmss";
    public static final String WHOLE_DATE_WITHOUT_SEPARATOR = "ddMMyyyyhhmmssSSS";
    public static final String WHOLE_DATE_WITH_SEPARATOR = "dd/MM/yyyy hh:mm:ss";
    public static final String DATE_DD_MM_YYYY = "dd/MM/yyyy";
    public static final String TXN_DATETIME = "dd/MM/yyyy hh:mm:ss";
    public static final String DATE_DD_MM_YYYY_HYPHEN = "dd-MM-yyyy";
    public static final String TIME_12_HRS = "hh:mm:ss a";
    public static final String TIME_24_HRS = "hh:mm:ss";
    //    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'.000000Z'";
    public static final String DEVICE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String getDate(String dateFormat) {
        try {
            String timeStamp = new SimpleDateFormat(dateFormat,
                    Locale.ENGLISH).format(Calendar.getInstance().getTime());
            return timeStamp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTime(String timeFormat) {
        try {
            String timeStamp = new SimpleDateFormat(timeFormat,
                    Locale.ENGLISH).format(Calendar.getInstance().getTime());
            return timeStamp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static boolean isDateBefore(String time, String endtime) {
        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.before(date2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isDateAfter(String time, String endtime) {
        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.after(date2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isDateEquals(String time, String endtime) {
        String pattern = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            Date date1 = sdf.parse(time);
            Date date2 = sdf.parse(endtime);

            return date1.getTime() == date2.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //   Conversion From String to Date
    public static String getDisplayDateFromDesiredFormat(String dateString, String dateFormat) {
//        dateStringInServerFormat = "2010-10-15T09:27:37Z";
//        dateStringInServerFormat = "2020-10-15T12:26:07.000000Z";
//        dateStringInServerFormat = "2020-10-15 00:00:00";

        String dateToDisplay = "";
        try {
            SimpleDateFormat format = new SimpleDateFormat(dateFormat);
            Date date = format.parse(dateString);
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(date.getTime());

            dateToDisplay = cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateToDisplay;
    }

    //   Conversion From String to Date
    public static String getDisplayDateFromDesiredFormat(String dateString, SimpleDateFormat format, Calendar cal) {
        try {
            Date date = format.parse(dateString);
            cal.setTimeInMillis(date.getTime());

            return cal.get(Calendar.DAY_OF_MONTH) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //   Conversion From Date to String
    public static String getStringFromDate(Context context) {
        String dateTime = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Date date = new Date();
            dateTime = dateFormat.format(date);
            System.out.println("Current Date Time : " + dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateTime;
    }
}
