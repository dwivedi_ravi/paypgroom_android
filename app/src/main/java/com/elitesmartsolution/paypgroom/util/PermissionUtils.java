package com.elitesmartsolution.paypgroom.util;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;

/**
 * Created by Ravi on 01/04/16.
 */
public final class PermissionUtils {

    public static boolean checkPermissions(Context context, String... permissions) {
        for (String permission : permissions) {
            if (!checkPermission(context, permission)) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isStoragePermissionsGranted(Context context) {
        return checkPermissions(context, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    public static boolean isLocationPermissionsGranted(Context context) {
        return checkPermissions(context, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public static boolean isContactPermissionsGranted(Context context) {
        return checkPermissions(context, Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CONTACTS);
    }

//    public static void requestPermissions(Object o, int permissionId, String... permissions) {
//        if (o instanceof Activity) {
////            ActivityCompat.requestPermissions((AppCompatActivity) o, permissions, permissionId);
//            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) o, permissions[0])) {
//                String message = ((Activity) o).getResources().getString(R.string.this_permission);
//                switch (permissionId) {
//                    case Defaults.PERMISSION_CODE_WRITE_STORAGE:
//                        message = ((Activity) o).getResources().getString(R.string.storage_permission);
//                        break;
//                    case Defaults.PERMISSION_CODE_READ_CONTACTS:
//                        message = ((Activity) o).getResources().getString(R.string.read_contacts_permission);
//                        break;
//                    case Defaults.PERMISSION_CODE_REQUEST_ACCESS_FINE_LOCATION:
//                        message = ((Activity) o).getResources().getString(R.string.location_permission);
//                        break;
//                }
//                showPermissionsAlert((Activity) o, permissions[0], permissionId, message);
//            } else {
//                ActivityCompat.requestPermissions((AppCompatActivity) o, permissions, permissionId);
//            }
//        }
//    }

    public static void requestPermissions(Object o, int permissionId, String... permissions) {
        if (o instanceof Activity) {
//            ActivityCompat.requestPermissions((AppCompatActivity) o, permissions, permissionId);
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) o, permissions[0])) {
                String message = ((Activity) o).getResources().getString(R.string.this_permission);
                switch (permissionId) {
                    case Defaults.PERMISSION_CODE_WRITE_STORAGE:
                        message = ((Activity) o).getResources().getString(R.string.storage_permission);
                        break;
                    case Defaults.PERMISSION_CODE_READ_CONTACTS:
                        message = ((Activity) o).getResources().getString(R.string.read_contacts_permission);
                        break;
                    case Defaults.PERMISSION_CODE_REQUEST_ACCESS_FINE_LOCATION:
                        message = ((Activity) o).getResources().getString(R.string.location_permission);
                        break;
                }
                showPermissionsAlert((Activity) o, permissions[0], permissionId, message);
            } else {
                ActivityCompat.requestPermissions((AppCompatActivity) o, permissions, permissionId);
            }
        }
    }

    public static boolean checkForReadPermission(Context context) {
        if (Utility.checkIfVersionGreaterOrEqual(Build.VERSION_CODES.M)) {
            int permissionState = ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
            return permissionState == PackageManager.PERMISSION_GRANTED;
        } else
            return true;
    }

    public static boolean checkForPassedPermission(Context context, String permission) {
        if (Utility.checkIfVersionGreaterOrEqual(Build.VERSION_CODES.M)) {
            return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        } else
            return true;
    }

    /**
     * Alert dialog to navigate to app settings
     * to enable necessary permissions
     */
    @TargetApi(Build.VERSION_CODES.M)
    public static void showPermissionsAlert(final Activity activity, final String permission, final int permissionCode, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setTitle("Permission required!")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                            activity.requestPermissions(new String[]{permission, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    permissionCode);
                        } else {
                            activity.requestPermissions(new String[]{permission},
                                    permissionCode);
                        }
                    }
                }).show();
    }
}