package com.elitesmartsolution.paypgroom.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

public class DynamicLinkUtils {

//    CALLED handleDynamicLink() ON SignInActivity.java TO HANDLE DYNAMIC LINK FOR USERS.

    public static Uri createDynamicLink_Basic(String referralCode) {
        // [START create_link_basic]
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://www.paypgroom.com/referals/" + referralCode))
                .setDomainUriPrefix("https://paypgroom.page.link")
                // Open links with this app on Android
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                // Open links with com.paypgroom.ios on iOS
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.paypgroom.ios").build())
                .buildDynamicLink();

        // [END create_link_basic]

        Uri dynamicLinkUri = dynamicLink.getUri();
        return dynamicLinkUri;
    }

    public static Uri createDynamicLink_Advanced(Context context, String referralCode) {
        // [START create_link_advanced]
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://www.paypgroom.com/referals/" + Tools.getDeviceID(context) + "-" + referralCode))
                .setDomainUriPrefix("https://paypgroom.page.link")
                .setAndroidParameters(
//                        new DynamicLink.AndroidParameters.Builder("com.paypgroom.android")
                        new DynamicLink.AndroidParameters.Builder("com.elitesmartsolution.paypgroom")
//                                .setMinimumVersion(125)
                                .build())
//                .setIosParameters(
//                        new DynamicLink.IosParameters.Builder("com.paypgroom.ios")
//                                .setAppStoreId("123456789")
//                                .setMinimumVersion("1.0.1")
//                                .build())
//                .setGoogleAnalyticsParameters(
//                        new DynamicLink.GoogleAnalyticsParameters.Builder()
//                                .setSource("orkut")
//                                .setMedium("social")
//                                .setCampaign("paypgroom-promo")
//                                .build())
//                .setItunesConnectAnalyticsParameters(
//                        new DynamicLink.ItunesConnectAnalyticsParameters.Builder()
//                                .setProviderToken("123456")
//                                .setCampaignToken("paypgroom-promo")
//                                .build())
//                .setSocialMetaTagParameters(
//                        new DynamicLink.SocialMetaTagParameters.Builder()
//                                .setTitle("Example of a Dynamic Link")
//                                .setDescription("This link works whether the app is installed or not!")
//                                .build())
                .buildDynamicLink();  // Or buildShortDynamicLink()
        // [END create_link_advanced]

        Uri dynamicLinkUri = dynamicLink.getUri();
        return dynamicLinkUri;
    }

    public static void createShortLink(Activity activity, String referralCode) {
        // [START create_short_link]
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://www.paypgroom.com/referals/" + Tools.getDeviceID(activity) + "-" + referralCode))
                .setDomainUriPrefix("https://paypgroom.page.link")
                // Set parameters
                // ...
                .buildShortDynamicLink()
                .addOnCompleteListener(activity, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();
                        } else {
                            // Error
                            // ...
                        }
                    }
                });
        // [END create_short_link]
    }

    public static void shortenLongLink(Activity activity, String referralCode) {
        // [START shorten_long_link]
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse("https://paypgroom.page.link/?link=https://www.paypgroom.com/referals/" + Tools.getDeviceID(activity) + "-" + referralCode + "&apn=com.elitesmartsolution.paypgroom&ibn=com.paypgroom.ios"))
                .buildShortDynamicLink()
                .addOnCompleteListener(activity, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();
                        } else {
                            // Error
                            // ...
                        }
                    }
                });
        // [END shorten_long_link]
    }

    public static void buildShortSuffix() {
        // [START ddl_short_suffix]
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                // ...
                .buildShortDynamicLink(ShortDynamicLink.Suffix.SHORT);
        // ...
        // [END ddl_short_suffix]
    }

    public static void shareLink(Context context, Uri myDynamicLink) {
        // [START ddl_share_link]
        Intent sendIntent = new Intent();
        String msg = "Hey, check this out: " + myDynamicLink;
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
        // [END ddl_share_link]
    }

    public static void getInvitation(Activity activity) {
        // [START ddl_get_invitation]
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(activity.getIntent())
                .addOnCompleteListener(new OnCompleteListener<PendingDynamicLinkData>() {
                    @Override
                    public void onComplete(@NonNull Task<PendingDynamicLinkData> task) {
                        if (!task.isSuccessful()) {
                            // Handle error
                            // ...
                        }

//                        FirebaseAppInvite invite = FirebaseAppInvite.getInvitation(task.getResult());
//                        if (invite != null) {
//                            // Handle invite
//                            // ...
//                        }
                    }
                });
        // [END ddl_get_invitation]
    }

    public static void onboardingShare(Context context, ShortDynamicLink dl) {
        // [START ddl_onboarding_share]
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Try this amazing app: " + dl.getShortLink());
        context.startActivity(Intent.createChooser(intent, "Share using"));
        // [END ddl_onboarding_share]
    }
}
