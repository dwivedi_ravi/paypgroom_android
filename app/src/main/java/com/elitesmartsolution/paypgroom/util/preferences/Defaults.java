package com.elitesmartsolution.paypgroom.util.preferences;

import com.elitesmartsolution.paypgroom.R;

/**
 * Class for storing Preference default values.
 */
public final class Defaults {

    public static final String FOLDER_PATH = "PayPgRoom/";
    public static final String FOLDER_PATH_PROFILE = FOLDER_PATH + "Profile/";
    public static final String FOLDER_PATH_PDF = FOLDER_PATH + "PDF/";
    public static final String TEMP_SERVICE_CITY_CODE = "102";
    public static final String TEMP_SERVICE_CITY_NAME = "Dehradun";
    public static final int MINIMUM_PROPERTY_IMAGES_TO_UPLOAD = 2;
    public static final Double MAXIMUM_IMAGES_SIZE_TO_UPLOAD = 2.0;
    public static final String SUPPORT_EMAIL = "support@paypgroom.com";
    public static final String SUPPORT_PHONE = "+918899606551";
    public static final String DEVICE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'.000000Z'";
    public static final String PACKAGE_NAME =
            "com.google.android.gms.location.sample.locationaddress";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";
    public static final String ADDRESS_DATA_EXTRA = PACKAGE_NAME +
            ".ADDRESS_DATA_EXTRA";
    public static final boolean ANIMATIONS_DISABLED = false;
    public static final String ACTION_REFRESH_MEDIA_AFTER_PHOTO_EDITING = "refresh_media_after_photo_editing";
    public static final String ACTION_REFRESH_GROUP_LIST = "action_refresh_group_list";
    public static final String ACTION_FETCH_GROUP_LIST_FROM_SERVER = "action_fetch_group_list_from_server";
    public static final String ACTION_BROADCAST_SELECTED_CONTACT_LIST = "broadcast_selected_contact_list";
    public static final String ACTION_FILL_PROFILE_DATA = "action_fill_profile_data";
    public static final String ACTION_FILL_GROUP_DETAILS_DATA = "action_fill_group_details_data";
    public static final String ACTION_UPDATE_CONNECTIONS_LIST = "action_update_connections_list";
    public static final String ACTION_CALL_GET_MY_CONNECTIONS = "action_call_get_my_connections";
    public static final String ACTION_PROFILE_PIC_UPLOAD_SUCCESS = "action_profile_pic_upload_success";
    public static final String ACTION_PROFILE_PIC_UPLOAD_FAILED = "action_profile_pic_upload_failed";
    public static final String ACTION_PROPERTY_IMAGE_UPLOAD_SUCCESS = "action_property_image_upload_success";
    public static final String ACTION_PROPERTY_IMAGE_UPLOAD_FAILED = "action_property_image_upload_failed";
    public static final String ACTION_FAVORITE_ADDED = "action_favorite_added";
    public static final String ACTION_AVAILABILITY_STATUS_CHANGED = "action_availability_status_changed";
    public static final String ACTION_OTP_RECEIVED = "otp_received";
    public static final String ACTION_HASH_RECEIVED = "hash_checksum_received";
    public static final String ACTION_ONLINE_TRANSACTION_COMPLETE_FROM_SERVER_END = "transaction_online_complete_from_server_end";
    public static final String ACTION_OFFLINE_TRANSACTION_COMPLETE_FROM_SERVER_END = "transaction_offline_complete_from_server_end";
    public static final String ACTION_REFRESH_OWNERS_PG_LISTING = "action_refresh_owners_pg_list";
    public static final String ACTION_REFRESH_BOOKING_LISTING = "action_refresh_booking_list";
    public static final String ACTION_COUPON_CODE_APPLIED = "action_coupon_code_applied";
    public static final String ACTION_REFRESH_INVOICE_LIST = "action_refresh_invoice_list";
    public static final String ACTION_FETCH_INVOICE_LIST_FROM_SERVER = "action_fetch_invoice_list_from_server";
    public static final String ACTION_TOOL_ENABLED_REFRESH_INVOICESCREEN = "action_tool_enabled_refresh_invoicescreen";
    public static final String ACTION_FINISH_BOOKING_SCREEN = "action_finish_booking_screen";
    public static final String ACTION_REFRESH_NOTIFICATION_LIST = "action_refresh_notification_list";
    public static final String ACTION_SERVICE_CITY_CHANGED = "action_service_city_changed";
    public static final String ACTION_GUEST_REMOVED = "action_guest_removed";
    public static final int PERMISSION_CODE_MY_CAMERA = 100;
    public static final int PERMISSION_CODE_WRITE_STORAGE = 101;
    public static final int PERMISSION_CODE_READ_CONTACTS = 102;
    public static final int PERMISSION_CODE_REQUEST_ACCESS_FINE_LOCATION = 103;
    public static final int RESOLVE_HINT = 104;
    public static final String DUMMY_YEAR_PREFIX = "1950-";
    public static final int REQUEST_CODE_GPS_STATUS_CHECK_SETTINGS = 201;
    public static final int REQUEST_CODE_AUTOCOMPLETE_PLACES = 202;
    public static final int AUTOCOMPLETE_REQUEST_CODE = 203;
    public static final int RESULT_CODE_SUCCESS = 301;
    public static final int RESULT_CODE_FAILURE = 302;
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 30 * 1000;//30 Seconds
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 20 * 1000;//20 Seconds
    //    FIREBASE
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String FIREBASE_TOPIC_GLOBAL_BROADCAST = "PayPgRoomNotifications";
    public static final String FIREBASE_TOPIC_INDIAN_BROADCAST = "PayPgRoomNotificationsIndia";
    public static final String FIREBASE_TOPIC_TEST_BROADCAST = "TestNotificationsPayPgRoom";
    public static final String CONTRY_CODE_INDIA = "in";
    public static final String GROUP_KEY_SECURE_GALLERY_NOTIFICATIONS = "group_key_PayPgRoom_notifications";
    public static final int RESPONSE_CODE_OK = 200;
    public static final int RESPONSE_CODE_NOT_FOUND = 404;
    public static final int RESPONSE_CODE_UN_AUTHORIZED = 401;
    public static final int RESPONSE_CODE_INTERNAL_SERVER_ERROR = 500;
    public static final String TAB_TITLE_DEPOSITS = "DEPOSIT";
    public static final String TAB_TITLE_EXPENSES = "EXPENSE";
    public static final String TAB_TITLE_TRANSFERS = "TRANSFER";
    public static final String TAB_TITLE_WITHDRAWAL = "WITHDRAWAL";
    public static final String TAB_TITLE_INTEREST = "INTEREST";
    public static final int SCREEN_TYPE_DEPOSITS = 0;
    public static final int SCREEN_TYPE_EXPENSES = 1;
    public static final int SCREEN_TYPE_WITHDRAWAL = 1;
    public static final int SCREEN_TYPE_TRANSFERS = 2;
    public static final int SCREEN_TYPE_INTEREST = 2;
    public static final String TAB_TITLE_ENTERPRISE = "ENTERPRISE";
    public static final String TAB_TITLE_GROUPS = "GROUPS";
    public static final String TAB_TITLE_PERSONAL = "PERSONAL";
    public static final int SCREEN_TYPE_ENTERPRISE = 0;
    public static final int SCREEN_TYPE_GROUPS = 1;
    public static final int SCREEN_TYPE_PERSONAL = 2;
    public static final String TAB_TITLE_INVOICES = "INVOICES";
    public static final String TAB_TITLE_ESTIMATES = "ESTIMATES";
    public static final String TAB_TITLE_CLIENTS = "CLIENTS";
    public static final int SCREEN_TYPE_INVOICES = 0;
    public static final int SCREEN_TYPE_ESTIMATES = 1;
    public static final int SCREEN_TYPE_CLIENTS = 2;
    public static final int TOOL_ID_ENTERPRICE = 101;
    public static final int TOOL_ID_GROUPS = 102;
    public static final int TOOL_ID_PERSONAL = 103;
    public static final int SUB_TOOL_ID_ROTATING_SAVINGS = 1; // 0-Select Group*, 1- Birthday, 2- Stockvel
    public static final int SUB_TOOL_ID_BIRTHDAY = 1; //
    public static final int SUB_TOOL_ID_STOCKVEL = 2;
    public static final int SUB_TOOL_ID_WEDDINGS = 3; //
    public static final int SUB_TOOL_ID_FUNERALS = 4;
    public static final int SUB_TOOL_ID_BABY_SHOWER = 5;
    public static final int SUB_TOOL_ID_HOLIDAY_ROAD_TRIP = 6;
    public static final int SUB_TOOL_ID_PEER_TO_PEER = 7;
    public static final String ACTION_TOOL_ENABLED_REFRESH_HOMESCREEN = "action_tool_enabled_refresh_homescreen";
    public static final String ACTION_TOOL_ENABLED_REFRESH_MARKETPLACE = "action_tool_enabled_refresh_marketplace";
    public static final int SCREEN_TYPE_NEAR_BY_FRAGMENT = 1;
    public static final int SCREEN_TYPE_FAVOURITES_FRAGMENT = 2;
    public static final int SCREEN_TYPE_PAY_PG_HOME_SCREEN = 3;
    public static final int SCREEN_TYPE_MY_PGS = 4;
    public static final int SCREEN_TYPE_SIGN_UP = 5;
    public static final int SCREEN_TYPE_FORGOT_PASSWORD = 6;
    public static final String REQUEST_TYPE_REGISTRATION = "Registration";
    public static final String REQUEST_TYPE_FORGOT_PASSWORD = "ForgotPassword";
    public static final String USER_TYPE_ADMIN = "1";
    public static final String USER_TYPE_OWNER = "2";
    public static final String USER_TYPE_TENANT = "3";
    public static final String COUNTRY_COMPONENT = "country";
    public static final String STATE_COMPONENT = "administrative_area_level_1";
    public static final String CITY_COMPONENT = "administrative_area_level_2";
    public static final String PINCODE_COMPONENT = "postal_code";
    public static final String REQUEST_TYPE_OWNER_ID = "owner_id";
    public static final String REQUEST_TYPE_SERVICE_CITY_CODE = "service_city_code";
    public static final String REQUEST_TYPE_IS_RECOMMENDED = "service_city_code_is_recommended";
    public static final String REQUEST_TYPE_LAT_LNG = "lat_lon";
    public static final String REQUEST_TYPE_FULLY_FURNISHED = "luxry_type";
    public static final String REQUEST_TYPE_ROOM_ACCOMODATION = "is_fooding_available";
    public static final float MAP_ZOOM_LEVEL_STREETS = 15;
    public static final float MAP_ZOOM_LEVEL_BETWEEN_STREETS_CITY = 13;
    public static final float MAP_ZOOM_LEVEL_CITY = 10;
    public static final String IS_ACTIVE_STATUS_ACTIVE = "1";
    public static final String IS_ACTIVE_STATUS_DEACTIVE = "0";
    public static final String IS_ACTIVE_STATUS_ALL = "2";
    public static final String FAVORITE_STATUS_SELECTED = "1";
    public static final String FAVORITE_STATUS_UN_SELECTED = "0";
    public static final String PAYMENT_MODE_ONLINE = "1";
    public static final String PAYMENT_MODE_OFFLINE = "2";
    public static final String IS_AVAILABLE = "1";
    public static final String NOT_IS_AVAILABLE = "0";
    public static final String PHONE_PATTERN = "^[987]\\d{9}$";
    public static final long MENU_DELAY = 300;
    public static final String TRANSACTION_STATUS_PENDING = "PENDING";
    public static final String TRANSACTION_STATUS_SUCCESSFUL = "SUCCESSFUL";
    public static final String TRANSACTION_STATUS_FAILED = "FAILED";
    public static final String TRANSACTION_STATUS_CANCELLED = "CANCELLED";
    public static final String PAYMENT_GATEWAY_DEBIT_CARD = "DC-PG";
    public static final String PAYMENT_GATEWAY_CREDIT_CARD = "CC-PG";
    public static final String PAYMENT_GATEWAY_NET_BANKING = "NB-PG";
    public static final String PAYMENT_GATEWAY_UPI_MODE = "UPI-PG";

    public static final String SERVICE_TYPE_RENT = "RENT";
    public static final String SERVICE_TYPE_BUY = "BUY";
    public static final String FULLY_FURNISHED = "1";
    public static final String NON_FURNISHED = "2";
    public static final String SEMI_FURNISHED = "3";
    public static final String FOODING_AVAILABLE = "1";
    public static final String FOODING_NOT_AVAILABLE = "0";
    public static int MY_PAYU_MONEY_THEME = R.style.AppTheme_pink;
    //    public static int DEFAULT_PAYU_MONEY_THEME = R.style.AppTheme_default;
    //    public static int selectedTheme = -1;
    private boolean isDisableWallet, isDisableSavedCards, isDisableNetBanking, isDisableThirdPartyWallets, isDisableExitConfirmation;

    // Prevent class instantiation
    private Defaults() {
    }
}
