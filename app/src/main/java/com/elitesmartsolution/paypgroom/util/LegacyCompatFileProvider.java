package com.elitesmartsolution.paypgroom.util;

import android.content.Context;
import android.net.Uri;

import androidx.core.content.FileProvider;

import java.io.File;

/**
 * Created by Ravi on 16/10/17.
 */

public class LegacyCompatFileProvider extends FileProvider {

    public static Uri getUri(Context context, File file) {
        return getUriForFile(context, ApplicationUtils.getPackageName() + ".provider", file);
    }
}
