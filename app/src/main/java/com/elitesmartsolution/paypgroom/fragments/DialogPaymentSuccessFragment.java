package com.elitesmartsolution.paypgroom.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.databinding.DialogPaymentSuccessBinding;
import com.elitesmartsolution.paypgroom.models.bookpg.RequestBookPg;
import com.elitesmartsolution.paypgroom.util.DateTimeUtils;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DialogPaymentSuccessFragment extends DialogFragment {

    private DialogPaymentSuccessBinding binding;

    private RequestBookPg objRequestBookPg;
    private View root_view;

    public DialogPaymentSuccessFragment(RequestBookPg objRequestBookPg) {
        this.objRequestBookPg = objRequestBookPg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        root_view = inflater.inflate(R.layout.dialog_payment_success, container, false);

        binding = DialogPaymentSuccessBinding.inflate(inflater, container, false);
        root_view = binding.getRoot();

        binding.fab.setOnClickListener(v -> {
            dismiss();
            Utility.sendUpdateListBroadCast(Defaults.ACTION_FINISH_BOOKING_SCREEN, getActivity(), null);
            getActivity().finish();
        });

        binding.tvBookingStatus.setText("BOOKING " + objRequestBookPg.getTransactionStatus().toUpperCase() + "!");
        binding.tvBookingStatusDetails.setText(getResources().getString(R.string.lbl_your_booking_was) + " " + objRequestBookPg.getTransactionStatus().toLowerCase());
//        binding.tvFromDate.setText(objRequestBookPg.getFromDate());
//        binding.tvToDate.setText(objRequestBookPg.getToDate());
        binding.tvFromDate.setText(DateTimeUtils.getDisplayDateFromDesiredFormat(objRequestBookPg.getFromDate(), new SimpleDateFormat("yyyy-MM-dd"), Calendar.getInstance()));
        binding.tvToDate.setText(DateTimeUtils.getDisplayDateFromDesiredFormat(objRequestBookPg.getToDate(), new SimpleDateFormat("yyyy-MM-dd"), Calendar.getInstance()));

        binding.tvPgName.setText(objRequestBookPg.getPgName());
        binding.tvPgHost.setText(getResources().getString(R.string.lbl_hosted_by) + " " + objRequestBookPg.getOwnerName());
        binding.tvAmount.setText(objRequestBookPg.getTotalAmountAfterDiscount());

        if (objRequestBookPg != null) {
            if (objRequestBookPg.getPayuTransactionResponse() != null) {
                //    ONLINE TRANSACTION
                if (objRequestBookPg.getPayuTransactionResponse().getPGTYPE() != null && !objRequestBookPg.getPayuTransactionResponse().getPGTYPE().trim().isEmpty()) {
                    binding.tvPaymentMode.setText(getString(R.string.payment_mode) + " " +
                            (objRequestBookPg.getPayuTransactionResponse().getPGTYPE().equalsIgnoreCase(Defaults.PAYMENT_GATEWAY_UPI_MODE)
                                    ? getString(R.string.payment_mode_upi)
                                    : objRequestBookPg.getPayuTransactionResponse().getPGTYPE().equalsIgnoreCase(Defaults.PAYMENT_GATEWAY_NET_BANKING)
                                    ? getString(R.string.payment_mode_net_banking)
                                    : objRequestBookPg.getPayuTransactionResponse().getPGTYPE().equalsIgnoreCase(Defaults.PAYMENT_GATEWAY_DEBIT_CARD)
                                    ? getString(R.string.payment_mode_debit_card)
                                    : getString(R.string.payment_mode_credit_card)));

                    if (objRequestBookPg.getPayuTransactionResponse().getPGTYPE().equalsIgnoreCase(Defaults.PAYMENT_GATEWAY_UPI_MODE)) {
                        if (objRequestBookPg.getPayuTransactionResponse().getField1() != null && !objRequestBookPg.getPayuTransactionResponse().getField1().trim().isEmpty())
                            binding.tvPaymentModeDescription.setText(getString(R.string.upi_id) + " " + objRequestBookPg.getPayuTransactionResponse().getField1());
                    } else {
                        if (objRequestBookPg.getPayuTransactionResponse().getBankRefNo() != null && !objRequestBookPg.getPayuTransactionResponse().getBankRefNo().trim().isEmpty())
                            binding.tvPaymentModeDescription.setText(getString(R.string.card_num) + " " + objRequestBookPg.getPayuTransactionResponse().getBankRefNo());
                    }
                }
            } else {
                //    OFFLINE TRANSACTION
                binding.tvPaymentMode.setText(getString(R.string.payment_mode) + " " + getResources().getString(R.string.lbl_pay_at_pg));
                binding.tvPaymentModeDescription.setText(getResources().getString(R.string.lbl_pay_at_pg_description));
            }
        }

        return root_view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}