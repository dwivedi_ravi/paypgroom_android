package com.elitesmartsolution.paypgroom.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;

import com.elitesmartsolution.paypgroom.adapters.AmenitiesAdapter;
import com.elitesmartsolution.paypgroom.database.DBHandler;
import com.elitesmartsolution.paypgroom.databinding.DialogSelectServiceCityBinding;
import com.elitesmartsolution.paypgroom.models.pgmetadata.AmenitiesItem;

import java.util.ArrayList;

public class SelectServiceCityFragment extends DialogFragment {

    ArrayList<AmenitiesItem> amenitiesItemList;
    private View root_view;
    private DBHandler dbHandler;
    private Context context;

    private DialogSelectServiceCityBinding binding;

    public SelectServiceCityFragment(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        root_view = inflater.inflate(R.layout.dialog_select_service_city, container, false);
        binding = DialogSelectServiceCityBinding.inflate(inflater, container, false);
        root_view = binding.getRoot();

        dbHandler = DBHandler.getInstance(context);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        amenitiesItemList = dbHandler.getServiceCityListFromDB(getActivity());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        binding.rcvServiceCities.setLayoutManager(gridLayoutManager);
        AmenitiesAdapter amenitiesAdapter = new AmenitiesAdapter(getActivity(), amenitiesItemList, false, false, true);
        binding.rcvServiceCities.setAdapter(amenitiesAdapter);

        return root_view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}