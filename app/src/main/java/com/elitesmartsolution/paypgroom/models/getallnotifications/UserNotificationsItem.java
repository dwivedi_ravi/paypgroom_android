package com.elitesmartsolution.paypgroom.models.getallnotifications;

import com.google.gson.annotations.SerializedName;

public class UserNotificationsItem {

    @SerializedName("from_user_name")
    private String fromUserName;

    @SerializedName("message_payload")
    private String messagePayload;

    @SerializedName("to_user_profile_url")
    private String toUserProfileUrl;

    @SerializedName("to_user_id")
    private String toUserId;

    @SerializedName("to_user_name")
    private String toUserName;

    @SerializedName("pg_name")
    private String pgName;

    @SerializedName("pg_id")
    private String pgId;

    @SerializedName("id")
    private String id;

    @SerializedName("type")
    private String type;

    @SerializedName("from_user_profile_url")
    private String fromUserProfileUrl;

    @SerializedName("from_user_id")
    private String fromUserId;

    @SerializedName("sent_datetime")
    private String sentDatetime;

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName;
    }

    public String getMessagePayload() {
        return messagePayload;
    }

    public void setMessagePayload(String messagePayload) {
        this.messagePayload = messagePayload;
    }

    public String getToUserProfileUrl() {
        return toUserProfileUrl;
    }

    public void setToUserProfileUrl(String toUserProfileUrl) {
        this.toUserProfileUrl = toUserProfileUrl;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName;
    }

    public String getPgName() {
        return pgName;
    }

    public void setPgName(String pgName) {
        this.pgName = pgName;
    }

    public String getPgId() {
        return pgId;
    }

    public void setPgId(String pgId) {
        this.pgId = pgId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFromUserProfileUrl() {
        return fromUserProfileUrl;
    }

    public void setFromUserProfileUrl(String fromUserProfileUrl) {
        this.fromUserProfileUrl = fromUserProfileUrl;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getSentDatetime() {
        return sentDatetime;
    }

    public void setSentDatetime(String sentDatetime) {
        this.sentDatetime = sentDatetime;
    }
}