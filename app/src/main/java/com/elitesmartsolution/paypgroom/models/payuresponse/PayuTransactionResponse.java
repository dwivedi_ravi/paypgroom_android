package com.elitesmartsolution.paypgroom.models.payuresponse;

import com.google.gson.annotations.SerializedName;

//{
//		"id": 17058100922,
//		"status": "success",
//		"unmappedstatus": "captured",
//		"key": "GXvSDH8O",
//		"txnid": "26032023062807840",
//		"transaction_fee": "2.00",
//		"amount": "2.00",
//		"cardCategory": "domestic",
//		"discount": "0.00",
//		"addedon": "2023-03-26 18:29:36",
//		"productinfo": "Test Property Kanpur",
//		"firstname": "Ravi Dwivedi Tenant",
//		"email": "dwivedi.ravi.1807@gmail.com",
//		"phone": "8770971404",
//		"udf1": "udf1",
//		"udf2": "udf2",
//		"udf3": "udf3",
//		"udf4": "udf4",
//		"udf5": "udf5",
//		"hash": "96355dbca8601e39462fb42420aa34b120e320ed411196fe2cee62f80566d4540c48608f15410a30a2e3a694698e3045de53aaa80f9b562d068a6b8db78ca95d",
//		"field2": "031080",
//		"field7": "EVPOSITIVE",
//		"field9": "success",
//		"payment_source": "payuS2S",
//		"PG_TYPE": "DC-PG",
//		"bank_ref_no": "202308582193011",
//		"error_code": "E000",
//		"Error_Message": "No Error",
//		"is_seamless": 1,
//		"surl": "https://payuresponse.firebaseapp.com/success",
//		"furl": "https://payuresponse.firebaseapp.com/failure"
//		}

public class PayuTransactionResponse {

	@SerializedName("firstname")
	private String firstname;

	@SerializedName("cardCategory")
	private String cardCategory;

	@SerializedName("discount")
	private String discount;

	@SerializedName("bank_ref_no")
	private String bankRefNo;

	@SerializedName("addedon")
	private String addedon;

	@SerializedName("Error_Message")
	private String errorMessage;

	@SerializedName("id")
	private long id;

	@SerializedName("payment_source")
	private String paymentSource;

	@SerializedName("key")
	private String key;

	@SerializedName("email")
	private String email;

	@SerializedName("txnid")
	private String txnid;

	@SerializedName("amount")
	private String amount;

	@SerializedName("unmappedstatus")
	private String unmappedstatus;

	@SerializedName("udf5")
	private String udf5;

	@SerializedName("transaction_fee")
	private String transactionFee;

	@SerializedName("is_seamless")
	private int isSeamless;

	@SerializedName("udf3")
	private String udf3;

	@SerializedName("surl")
	private String surl;

	@SerializedName("udf4")
	private String udf4;

	@SerializedName("udf1")
	private String udf1;

	@SerializedName("udf2")
	private String udf2;

	@SerializedName("field1")
	private String field1;

	@SerializedName("phone")
	private String phone;

	@SerializedName("field6")
	private String field6;

	@SerializedName("field7")
	private String field7;

	@SerializedName("furl")
	private String furl;

	@SerializedName("field9")
	private String field9;

	@SerializedName("error_code")
	private String errorCode;

	@SerializedName("productinfo")
	private String productinfo;

	@SerializedName("field2")
	private String field2;

	@SerializedName("field3")
	private String field3;

	@SerializedName("hash")
	private String hash;

	@SerializedName("field5")
	private String field5;

	@SerializedName("PG_TYPE")
	private String pGTYPE;

	@SerializedName("status")
	private String status;

	@SerializedName("field4")
	private String field4;

	public void setFirstname(String firstname){
		this.firstname = firstname;
	}

	public String getFirstname(){
		return firstname;
	}

	public void setCardCategory(String cardCategory){
		this.cardCategory = cardCategory;
	}

	public String getCardCategory(){
		return cardCategory;
	}

	public void setDiscount(String discount){
		this.discount = discount;
	}

	public String getDiscount(){
		return discount;
	}

	public void setBankRefNo(String bankRefNo){
		this.bankRefNo = bankRefNo;
	}

	public String getBankRefNo(){
		return bankRefNo;
	}

	public void setAddedon(String addedon){
		this.addedon = addedon;
	}

	public String getAddedon(){
		return addedon;
	}

	public void setErrorMessage(String errorMessage){
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage(){
		return errorMessage;
	}

	public void setId(long id){
		this.id = id;
	}

	public long getId(){
		return id;
	}

	public void setPaymentSource(String paymentSource){
		this.paymentSource = paymentSource;
	}

	public String getPaymentSource(){
		return paymentSource;
	}

	public void setKey(String key){
		this.key = key;
	}

	public String getKey(){
		return key;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setTxnid(String txnid){
		this.txnid = txnid;
	}

	public String getTxnid(){
		return txnid;
	}

	public void setAmount(String amount){
		this.amount = amount;
	}

	public String getAmount(){
		return amount;
	}

	public void setUnmappedstatus(String unmappedstatus){
		this.unmappedstatus = unmappedstatus;
	}

	public String getUnmappedstatus(){
		return unmappedstatus;
	}

	public void setUdf5(String udf5){
		this.udf5 = udf5;
	}

	public String getUdf5(){
		return udf5;
	}

	public void setTransactionFee(String transactionFee){
		this.transactionFee = transactionFee;
	}

	public String getTransactionFee(){
		return transactionFee;
	}

	public void setIsSeamless(int isSeamless){
		this.isSeamless = isSeamless;
	}

	public int getIsSeamless(){
		return isSeamless;
	}

	public void setUdf3(String udf3){
		this.udf3 = udf3;
	}

	public String getUdf3(){
		return udf3;
	}

	public void setSurl(String surl){
		this.surl = surl;
	}

	public String getSurl(){
		return surl;
	}

	public void setUdf4(String udf4){
		this.udf4 = udf4;
	}

	public String getUdf4(){
		return udf4;
	}

	public void setUdf1(String udf1){
		this.udf1 = udf1;
	}

	public String getUdf1(){
		return udf1;
	}

	public void setUdf2(String udf2){
		this.udf2 = udf2;
	}

	public String getUdf2(){
		return udf2;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setField7(String field7){
		this.field7 = field7;
	}

	public String getField7(){
		return field7;
	}

	public void setFurl(String furl){
		this.furl = furl;
	}

	public String getFurl(){
		return furl;
	}

	public void setField9(String field9){
		this.field9 = field9;
	}

	public String getField9(){
		return field9;
	}

	public void setErrorCode(String errorCode){
		this.errorCode = errorCode;
	}

	public String getErrorCode(){
		return errorCode;
	}

	public void setProductinfo(String productinfo){
		this.productinfo = productinfo;
	}

	public String getProductinfo(){
		return productinfo;
	}

	public void setField2(String field2){
		this.field2 = field2;
	}

	public String getField2(){
		return field2;
	}

	public void setHash(String hash){
		this.hash = hash;
	}

	public String getHash(){
		return hash;
	}

	public void setPGTYPE(String pGTYPE){
		this.pGTYPE = pGTYPE;
	}

	public String getPGTYPE(){
		return pGTYPE;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}


	public void setField1(String field1){
		this.field1 = field1;
	}

	public String getField1(){
		return field1;
	}


	public void setField6(String field6){
		this.field6 = field6;
	}

	public String getField6(){
		return field6;
	}


	public void setField3(String field3){
		this.field3 = field3;
	}

	public String getField3(){
		return field3;
	}

	public void setField5(String field5){
		this.field5 = field5;
	}

	public String getField5(){
		return field5;
	}

	public void setField4(String field4){
		this.field4 = field4;
	}

	public String getField4(){
		return field4;
	}
}