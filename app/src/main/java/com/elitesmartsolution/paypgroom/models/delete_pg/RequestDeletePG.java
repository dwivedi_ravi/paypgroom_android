package com.elitesmartsolution.paypgroom.models.delete_pg;

import com.google.gson.annotations.SerializedName;

public class RequestDeletePG {

    @SerializedName("owner_id")
    private String ownerId;

    @SerializedName("pg_id")
    private String pgId;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getPgId() {
        return pgId;
    }

    public void setPgId(String pgId) {
        this.pgId = pgId;
    }
}