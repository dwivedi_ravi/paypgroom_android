package com.elitesmartsolution.paypgroom.models.pglisting;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ResponsePgListing implements Serializable {

    @SerializedName("data")
    private List<PropertyListItem> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<PropertyListItem> getData() {
        return data;
    }

    public void setData(List<PropertyListItem> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}