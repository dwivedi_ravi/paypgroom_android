package com.elitesmartsolution.paypgroom.models.coupons;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("coupans")
    private List<CoupansItem> coupans;

    public List<CoupansItem> getCoupans() {
        return coupans;
    }

    public void setCoupans(List<CoupansItem> coupans) {
        this.coupans = coupans;
    }
}