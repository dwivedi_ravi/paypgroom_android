package com.elitesmartsolution.paypgroom.models.verifychecksum;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestVerifyChecksum implements Serializable {

    @SerializedName("tenant_id")
    private String tenantId;

    @SerializedName("owner_id")
    private String ownerId;

    @SerializedName("pg_id")
    private String pgId;

    @SerializedName("transection_id")
    private String transectionId;

    @SerializedName("hash_sequence")
    private String hashSequence;

    @SerializedName("compare_sequence")
    private String payuCheckSum;

    @SerializedName("payu_json")
    private String payuResponseJson;

    @SerializedName("status")
    private String payuTransactionStatus;

    @SerializedName("coupan_id")
    private String couponId;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getPgId() {
        return pgId;
    }

    public void setPgId(String pgId) {
        this.pgId = pgId;
    }

    public String getTransectionId() {
        return transectionId;
    }

    public void setTransectionId(String transectionId) {
        this.transectionId = transectionId;
    }

    public String getHashSequence() {
        return hashSequence;
    }

    public void setHashSequence(String hashSequence) {
        this.hashSequence = hashSequence;
    }

    public String getPayuCheckSum() {
        return payuCheckSum;
    }

    public void setPayuCheckSum(String payuCheckSum) {
        this.payuCheckSum = payuCheckSum;
    }

    public String getPayuTransactionStatus() {
        return payuTransactionStatus;
    }

    public void setPayuTransactionStatus(String payuTransactionStatus) {
        this.payuTransactionStatus = payuTransactionStatus;
    }

    public String getPayuResponseJson() {
        return payuResponseJson;
    }

    public void setPayuResponseJson(String payuResponseJson) {
        this.payuResponseJson = payuResponseJson;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }
}