package com.elitesmartsolution.paypgroom.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PgData implements Serializable {

    @SerializedName("other_details")
    private String otherDetails;

    @SerializedName("visit_from")
    private String visitFrom;

    @SerializedName("luxry_type")
    private String luxryType;

    @SerializedName("owner_id")
    private String ownerId;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("description")
    private String description;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("property_type_id")
    private String propertyTypeId;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("id")
    private String id;

    @SerializedName("visit_to")
    private String visitTo;

    @SerializedName("bathroom")
    private String bathroom;

    @SerializedName("longitude")
    private String longitude;

    @SerializedName("is_fooding_available")
    private String isFoodingAvailable;

    @SerializedName("is_active")
    private String isActive;

    @SerializedName("address_id")
    private String addressId;

    @SerializedName("room_type_id")
    private String roomTypeId;

    @SerializedName("deleted_at")
    private String deletedAt;

    @SerializedName("bedroom")
    private String bedroom;

    @SerializedName("is_available")
    private String isAvailable;

    @SerializedName("is_recommended")
    private String isRecommended;

    @SerializedName("size")
    private String size;

    @SerializedName("service_city_code")
    private String serviceCityCode;

    @SerializedName("pg_name")
    private String pgName;

    @SerializedName("guests")
    private String guests;

    @SerializedName("beds")
    private String beds;

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public String getVisitFrom() {
        return visitFrom;
    }

    public void setVisitFrom(String visitFrom) {
        this.visitFrom = visitFrom;
    }

    public String getLuxryType() {
        return luxryType;
    }

    public void setLuxryType(String luxryType) {
        this.luxryType = luxryType;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPropertyTypeId() {
        return propertyTypeId;
    }

    public void setPropertyTypeId(String propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVisitTo() {
        return visitTo;
    }

    public void setVisitTo(String visitTo) {
        this.visitTo = visitTo;
    }

    public String getBathroom() {
        return bathroom;
    }

    public void setBathroom(String bathroom) {
        this.bathroom = bathroom;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getIsFoodingAvailable() {
        return isFoodingAvailable;
    }

    public void setIsFoodingAvailable(String isFoodingAvailable) {
        this.isFoodingAvailable = isFoodingAvailable;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(String roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getBedroom() {
        return bedroom;
    }

    public void setBedroom(String bedroom) {
        this.bedroom = bedroom;
    }

    public String getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(String isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getIsRecommended() {
        return isRecommended;
    }

    public void setIsRecommended(String isRecommended) {
        this.isRecommended = isRecommended;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getServiceCityCode() {
        return serviceCityCode;
    }

    public void setServiceCityCode(String serviceCityCode) {
        this.serviceCityCode = serviceCityCode;
    }

    public String getPgName() {
        return pgName;
    }

    public void setPgName(String pgName) {
        this.pgName = pgName;
    }

    public String getGuests() {
        return guests;
    }

    public void setGuests(String guests) {
        this.guests = guests;
    }

    public String getBeds() {
        return beds;
    }

    public void setBeds(String beds) {
        this.beds = beds;
    }
}