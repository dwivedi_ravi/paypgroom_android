package com.elitesmartsolution.paypgroom.models.pglisting;

import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestGetPgListing implements Serializable {

    @SerializedName("is_recommended")
    private String isRecommended = "";

    @SerializedName("luxry_type")
    private String luxryType = "";

    @SerializedName("is_favorite")
    private String isFavorite = "";

    @SerializedName("search_key")
    private String searchKey = "";

    @SerializedName("user_id")
    private String userId = Prefs.getUserId();

    @SerializedName("owner_id")
    private String ownerId = "";

    @SerializedName("service_city_code")
    private String serviceCityCode = "";

    @SerializedName("property_type")
    private String propertyType = "";

    @SerializedName("lon")
    private String lon = "";

    @SerializedName("lat")
    private String lat = "";

    @SerializedName("room_type")
    private String roomType = "";

    @SerializedName("is_fooding_available")
    private String isFoodingAvailable = "";

    @SerializedName("is_active")
    private String isActive = Defaults.IS_ACTIVE_STATUS_ACTIVE;

    @SerializedName("min_price")
    private String minPrice = "";

    @SerializedName("max_price")
    private String maxPrice = "";

    public String getIsRecommended() {
        return isRecommended;
    }

    public void setIsRecommended(String isRecommended) {
        this.isRecommended = isRecommended;
    }

    public String getLuxryType() {
        return luxryType;
    }

    public void setLuxryType(String luxryType) {
        this.luxryType = luxryType;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getServiceCityCode() {
        return serviceCityCode;
    }

    public void setServiceCityCode(String serviceCityCode) {
        this.serviceCityCode = serviceCityCode;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getIsFoodingAvailable() {
        return isFoodingAvailable;
    }

    public void setIsFoodingAvailable(String isFoodingAvailable) {
        this.isFoodingAvailable = isFoodingAvailable;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(String minPrice) {
        this.minPrice = minPrice;
    }

    public String getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(String maxPrice) {
        this.maxPrice = maxPrice;
    }
}