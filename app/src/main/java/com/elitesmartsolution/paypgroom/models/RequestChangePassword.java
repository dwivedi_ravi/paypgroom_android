package com.elitesmartsolution.paypgroom.models;

import com.google.gson.annotations.SerializedName;

public class RequestChangePassword {

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("new_password")
    private String newPassword;

    @SerializedName("conform_password")
    private String conformPassword;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConformPassword() {
        return conformPassword;
    }

    public void setConformPassword(String conformPassword) {
        this.conformPassword = conformPassword;
    }
}