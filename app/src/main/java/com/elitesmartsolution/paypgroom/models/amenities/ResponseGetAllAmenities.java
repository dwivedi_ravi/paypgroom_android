package com.elitesmartsolution.paypgroom.models.amenities;

import com.elitesmartsolution.paypgroom.models.pgmetadata.AmenitiesItem;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetAllAmenities {

    @SerializedName("data")
    private List<AmenitiesItem> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<AmenitiesItem> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public boolean isStatus() {
        return status;
    }
}