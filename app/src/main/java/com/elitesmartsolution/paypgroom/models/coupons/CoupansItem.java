package com.elitesmartsolution.paypgroom.models.coupons;

import com.elitesmartsolution.paypgroom.R;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CoupansItem implements Serializable {

    @SerializedName("end_date")
    private String endDate;

    //	ACTIVE-1 & DEACTIVE-2
    @SerializedName("is_active")
    private String isActive;

    @SerializedName("description")
    private String description;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("discount_percentage")
    private String discountPercentage;

    @SerializedName("maximum_discount_amount")
    private String maximumDiscountAmount;

    @SerializedName("deleted_at")
    private String deletedAt;

    @SerializedName("is_used")
    private boolean isUsed;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("coupan_code")
    private String coupanCode;

    @SerializedName("id")
    private String id;

    @SerializedName("minimum_transaction_amount")
    private String minimumTransactionAmount;

    @SerializedName("start_date")
    private String startDate;

    private int drawableResource = R.drawable.oval_red;

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getMaximumDiscountAmount() {
        return maximumDiscountAmount;
    }

    public void setMaximumDiscountAmount(String maximumDiscountAmount) {
        this.maximumDiscountAmount = maximumDiscountAmount;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCoupanCode() {
        return coupanCode;
    }

    public void setCoupanCode(String coupanCode) {
        this.coupanCode = coupanCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMinimumTransactionAmount() {
        return minimumTransactionAmount;
    }

    public void setMinimumTransactionAmount(String minimumTransactionAmount) {
        this.minimumTransactionAmount = minimumTransactionAmount;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public int getDrawableResource() {
        return drawableResource;
    }

    public void setDrawableResource(int drawableResource) {
        this.drawableResource = drawableResource;
    }
}