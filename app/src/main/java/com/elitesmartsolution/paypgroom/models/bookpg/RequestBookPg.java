package com.elitesmartsolution.paypgroom.models.bookpg;

import com.elitesmartsolution.paypgroom.models.payuresponse.PayuTransactionResponse;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RequestBookPg implements Serializable {

    @SerializedName("tenant_id")
    private String userId;

    @SerializedName("owner_id")
    private String ownerId;

    @SerializedName("pg_id")
    private String pgId;

    @SerializedName("payment_id")
    private String paymentId;

    @SerializedName("transection_id")
    private String transactionId;

    @SerializedName("from_date")
    private String fromDate;

    @SerializedName("to_date")
    private String toDate;

    @SerializedName("booking_price")
    private String bookingPrice;

    @SerializedName("security_deposit")
    private String securityDeposit;

    @SerializedName("cleaning_fee")
    private String cleaningFee;

    @SerializedName("discount")
    private String discount;

    @SerializedName("total_amount")
    private String totalAmount;

    @SerializedName("total_amount_after_discount")
    private String totalAmountAfterDiscount;

    @SerializedName("number_of_months")
    private String numberOfMonths;

    @SerializedName("room_type_id")
    private String roomOccupancyType;

    @SerializedName("number_of_persons")
    private String numberOfPersons;

    @SerializedName("quests")
    private List<GuestItem> quests;

    @SerializedName("merchant_id")
    private String merchantId;

    @SerializedName("merchant_key")
    private String merchantKey;

    @SerializedName("hash_sequence")
    private String hashSequence;

    @SerializedName("payment_mode")
    private String paymentMode;

    @SerializedName("status")
    private String transactionStatus;

    @SerializedName("coupan_id")
    private String appliedCoupanId = "";

    @SerializedName("coupan_code")
    private String appliedCoupanCode;

    private String pgName;
    private PayuTransactionResponse payuTransactionResponse;
    private String ownerName;

    public String getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(String securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public List<GuestItem> getQuests() {
        return quests;
    }

    public void setQuests(List<GuestItem> quests) {
        this.quests = quests;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantKey() {
        return merchantKey;
    }

    public void setMerchantKey(String merchantKey) {
        this.merchantKey = merchantKey;
    }

    public String getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(String numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public String getCleaningFee() {
        return cleaningFee;
    }

    public void setCleaningFee(String cleaningFee) {
        this.cleaningFee = cleaningFee;
    }

    public String getTotalAmountAfterDiscount() {
        return totalAmountAfterDiscount;
    }

    public void setTotalAmountAfterDiscount(String totalAmountAfterDiscount) {
        this.totalAmountAfterDiscount = totalAmountAfterDiscount;
    }

    public String getRoomOccupancyType() {
        return roomOccupancyType;
    }

    public void setRoomOccupancyType(String roomOccupancyType) {
        this.roomOccupancyType = roomOccupancyType;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getPgId() {
        return pgId;
    }

    public void setPgId(String pgId) {
        this.pgId = pgId;
    }

    public String getNumberOfMonths() {
        return numberOfMonths;
    }

    public void setNumberOfMonths(String numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }

    public String getPgName() {
        return pgName;
    }

    public void setPgName(String pgName) {
        this.pgName = pgName;
    }

    public String getHashSequence() {
        return hashSequence;
    }

    public void setHashSequence(String hashSequence) {
        this.hashSequence = hashSequence;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getAppliedCoupanId() {
        return appliedCoupanId;
    }

    public void setAppliedCoupanId(String appliedCoupanId) {
        this.appliedCoupanId = appliedCoupanId;
    }

    public String getAppliedCoupanCode() {
        return appliedCoupanCode;
    }

    public void setAppliedCoupanCode(String appliedCoupanCode) {
        this.appliedCoupanCode = appliedCoupanCode;
    }

    public PayuTransactionResponse getPayuTransactionResponse() {
        return payuTransactionResponse;
    }

    public void setPayuTransactionResponse(PayuTransactionResponse payuTransactionResponse) {
        this.payuTransactionResponse = payuTransactionResponse;
    }
}