package com.elitesmartsolution.paypgroom.models.error;

import com.google.gson.annotations.SerializedName;

public class ErrorResponse {

    @SerializedName("message")
    private String message;

    @SerializedName("errors")
    private Errors errors;

    @SerializedName("status")
    private boolean status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Errors getErrors() {
        return errors;
    }

    public void setErrors(Errors errors) {
        this.errors = errors;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return
                "ErrorResponse{" +
                        "message = '" + message + '\'' +
                        ",errors = '" + errors + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}