package com.elitesmartsolution.paypgroom.models.pgmetadata;

import com.google.gson.annotations.SerializedName;

public class CancellationPolicy {

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("day30")
    private String day30;

    @SerializedName("created_at")
    private Object createdAt;

    @SerializedName("id")
    private int id;

    @SerializedName("day1")
    private String day1;

    @SerializedName("title")
    private String title;

    @SerializedName("day15")
    private String day15;

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDay30() {
        return day30;
    }

    public void setDay30(String day30) {
        this.day30 = day30;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDay1() {
        return day1;
    }

    public void setDay1(String day1) {
        this.day1 = day1;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDay15() {
        return day15;
    }

    public void setDay15(String day15) {
        this.day15 = day15;
    }
}