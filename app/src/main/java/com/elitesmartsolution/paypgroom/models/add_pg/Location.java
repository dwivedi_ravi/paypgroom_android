package com.elitesmartsolution.paypgroom.models.add_pg;

import com.google.gson.annotations.SerializedName;

public class Location {

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("country")
    private String country;

    @SerializedName("city")
    private String city;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("state")
    private String state;

    @SerializedName("line2")
    private String line2;

    @SerializedName("line1")
    private String line1;

    @SerializedName("longitude")
    private String longitude;

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}