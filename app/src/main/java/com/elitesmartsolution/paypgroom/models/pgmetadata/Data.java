package com.elitesmartsolution.paypgroom.models.pgmetadata;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("amenities")
    private List<AmenitiesItem> amenities;

    @SerializedName("property_types")
    private List<PropertyTypesItem> propertyTypes;

    @SerializedName("room_types")
    private List<RoomTypesItem> roomTypes;

    @SerializedName("cityMaster")
    private List<CityMasterItem> cityMasterItemList;

    @SerializedName("cancellation_policy")
    private List<CancellationPolicy> cancellationPolicyList;

//    @SerializedName("luxryTypes")
//    private List<LuxuryType> luxryTypesList;

    public List<AmenitiesItem> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<AmenitiesItem> amenities) {
        this.amenities = amenities;
    }

    public List<PropertyTypesItem> getPropertyTypes() {
        return propertyTypes;
    }

    public void setPropertyTypes(List<PropertyTypesItem> propertyTypes) {
        this.propertyTypes = propertyTypes;
    }

    public List<RoomTypesItem> getRoomTypes() {
        return roomTypes;
    }

    public void setRoomTypes(List<RoomTypesItem> roomTypes) {
        this.roomTypes = roomTypes;
    }

    public List<CityMasterItem> getCityMasterItemList() {
        return cityMasterItemList;
    }

    public void setCityMasterItemList(List<CityMasterItem> cityMasterItemList) {
        this.cityMasterItemList = cityMasterItemList;
    }

    public List<CancellationPolicy> getCancellationPolicyList() {
        return cancellationPolicyList;
    }

    public void setCancellationPolicyList(List<CancellationPolicy> cancellationPolicyList) {
        this.cancellationPolicyList = cancellationPolicyList;
    }
}