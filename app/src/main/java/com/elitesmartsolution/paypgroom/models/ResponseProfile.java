package com.elitesmartsolution.paypgroom.models;

import com.elitesmartsolution.paypgroom.models.profile.ProfileData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseProfile implements Serializable {

    @SerializedName("data")
    private ProfileData profileData;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public ProfileData getProfileData() {
        return profileData;
    }

    public void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}