package com.elitesmartsolution.paypgroom.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Data implements Serializable {

    @SerializedName("pg_data")
    private PgData pgData;

    public PgData getPgData() {
        return pgData;
    }

    public void setPgData(PgData pgData) {
        this.pgData = pgData;
    }
}