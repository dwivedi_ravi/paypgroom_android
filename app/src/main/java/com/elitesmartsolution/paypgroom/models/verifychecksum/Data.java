package com.elitesmartsolution.paypgroom.models.verifychecksum;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("transection_id")
    private String transectionId;

    public String getTransectionId() {
        return transectionId;
    }

    public void setTransectionId(String transectionId) {
        this.transectionId = transectionId;
    }
}