package com.elitesmartsolution.paypgroom.models;

public class RequestRegenerateAccessToken {
    private String deviceUUID;

    public String getDeviceUUID() {
        return deviceUUID;
    }

    public void setDeviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
    }

    @Override
    public String toString() {
        return
                "RequestRegenerateAccessToken{" +
                        "deviceUUID = '" + deviceUUID + '\'' +
                        "}";
    }
}
