package com.elitesmartsolution.paypgroom.models.getallnotifications;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetAllNotifications {

    @SerializedName("data")
    private List<UserNotificationsItem> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<UserNotificationsItem> getData() {
        return data;
    }

    public void setData(List<UserNotificationsItem> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}