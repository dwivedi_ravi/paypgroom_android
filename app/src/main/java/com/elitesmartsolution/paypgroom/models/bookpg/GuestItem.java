package com.elitesmartsolution.paypgroom.models.bookpg;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GuestItem implements Serializable {

    @SerializedName("quest_mobile")
    private String guestMobile;

    @SerializedName("quest_document_number")
    private String guestDocumentNumber;

    @SerializedName("quest_document_type")
    private String guestDocumentType;

    @SerializedName("quest_id")
    private String guestId;

    @SerializedName("quest_name")
    private String guestName;

    public String getGuestMobile() {
        return guestMobile;
    }

    public void setGuestMobile(String guestMobile) {
        this.guestMobile = guestMobile;
    }

    public String getGuestDocumentNumber() {
        return guestDocumentNumber;
    }

    public void setGuestDocumentNumber(String guestDocumentNumber) {
        this.guestDocumentNumber = guestDocumentNumber;
    }

    public String getGuestDocumentType() {
        return guestDocumentType;
    }

    public void setGuestDocumentType(String guestDocumentType) {
        this.guestDocumentType = guestDocumentType;
    }

    public String getGuestId() {
        return guestId;
    }

    public void setGuestId(String guestId) {
        this.guestId = guestId;
    }

    public String getGuestName() {
        return guestName;
    }

    public void setGuestName(String guestName) {
        this.guestName = guestName;
    }
}