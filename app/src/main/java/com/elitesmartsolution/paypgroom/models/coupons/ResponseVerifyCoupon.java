package com.elitesmartsolution.paypgroom.models.coupons;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseVerifyCoupon implements Serializable {

    @SerializedName("data")
    private CoupansItem data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public CoupansItem getData() {
        return data;
    }

    public void setData(CoupansItem data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}