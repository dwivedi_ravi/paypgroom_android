package com.elitesmartsolution.paypgroom.models.pglisting;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PropertyListItem implements Serializable {

    @SerializedName("other_details")
    private String otherDetails;

    @SerializedName("is_fooding_available")
    private String isFoodingAvailable;

    @SerializedName("is_favorite")
    private String isFavorite = "";

    @SerializedName("is_active")
    private String isActive;

    @SerializedName("pg_pricing_detail")
    private PgPricingDetail pgPricingDetail;

    @SerializedName("pg_room_type")
    private String pgRoomType;

    @SerializedName("description")
    private String description;

    @SerializedName("bedroom")
    private String bedroom;

    @SerializedName("is_available")
    private String isAvailable;

    @SerializedName("size")
    private String size;

    @SerializedName("pg_images")
    private List<PgImagesItem> pgImages;

    @SerializedName("pg_name")
    private String pgName;

    @SerializedName("guests")
    private String guests;

    @SerializedName("property_type")
    private String propertyType;

    @SerializedName("location")
    private Location location;

    @SerializedName("id")
    private String id;

    @SerializedName("pg_owner_detail")
    private PgOwnerDetail pgOwnerDetail;

    @SerializedName("beds")
    private String beds;

    @SerializedName("pg_amenities")
    private List<PgAmenitiesItem> pgAmenities;

    @SerializedName("visit_schedule_detail")
    private VisitScheduleDetail visitScheduleDetail;

    @SerializedName("bathroom")
    private String bathroom;

    @SerializedName("luxry_type")
    private String luxryType;

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public String getIsFoodingAvailable() {
        return isFoodingAvailable;
    }

    public void setIsFoodingAvailable(String isFoodingAvailable) {
        this.isFoodingAvailable = isFoodingAvailable;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public PgPricingDetail getPgPricingDetail() {
        return pgPricingDetail;
    }

    public void setPgPricingDetail(PgPricingDetail pgPricingDetail) {
        this.pgPricingDetail = pgPricingDetail;
    }

    public String getPgRoomType() {
        return pgRoomType;
    }

    public void setPgRoomType(String pgRoomType) {
        this.pgRoomType = pgRoomType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBedroom() {
        return bedroom;
    }

    public void setBedroom(String bedroom) {
        this.bedroom = bedroom;
    }

    public String getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(String isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public List<PgImagesItem> getPgImages() {
        return pgImages;
    }

    public void setPgImages(List<PgImagesItem> pgImages) {
        this.pgImages = pgImages;
    }

    public String getPgName() {
        return pgName;
    }

    public void setPgName(String pgName) {
        this.pgName = pgName;
    }

    public String getGuests() {
        return guests;
    }

    public void setGuests(String guests) {
        this.guests = guests;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PgOwnerDetail getPgOwnerDetail() {
        return pgOwnerDetail;
    }

    public void setPgOwnerDetail(PgOwnerDetail pgOwnerDetail) {
        this.pgOwnerDetail = pgOwnerDetail;
    }

    public String getBeds() {
        return beds;
    }

    public void setBeds(String beds) {
        this.beds = beds;
    }

    public List<PgAmenitiesItem> getPgAmenities() {
        return pgAmenities;
    }

    public void setPgAmenities(List<PgAmenitiesItem> pgAmenities) {
        this.pgAmenities = pgAmenities;
    }

    public VisitScheduleDetail getVisitScheduleDetail() {
        return visitScheduleDetail;
    }

    public void setVisitScheduleDetail(VisitScheduleDetail visitScheduleDetail) {
        this.visitScheduleDetail = visitScheduleDetail;
    }

    public String getBathroom() {
        return bathroom;
    }

    public void setBathroom(String bathroom) {
        this.bathroom = bathroom;
    }

    public String getLuxryType() {
        return luxryType;
    }

    public void setLuxryType(String luxryType) {
        this.luxryType = luxryType;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }
}