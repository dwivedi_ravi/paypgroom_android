package com.elitesmartsolution.paypgroom.models.my_bookings;

import com.elitesmartsolution.paypgroom.models.payuresponse.PayuTransactionResponse;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataItem implements Serializable {

    @SerializedName("security_deposit")
    private String securityDeposit;

    @SerializedName("payment_mode")
    private String paymentMode;

    @SerializedName("owner_name")
    private String ownerName;

    @SerializedName("from_date")
    private String fromDate;

    @SerializedName("owner_id")
    private String ownerId;

    @SerializedName("discount")
    private String discount;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("number_of_persons")
    private String numberOfPersons;

    @SerializedName("cleaning_fee")
    private String cleaningFee;

    @SerializedName("total_amount_after_discount")
    private String totalAmountAfterDiscount;

    @SerializedName("to_date")
    private String toDate;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("total_amount")
    private String totalAmount;

    @SerializedName("pg_name")
    private String pgName;

    @SerializedName("booking_price")
    private String bookingPrice;

    @SerializedName("number_of_months")
    private String numberOfMonths;

    @SerializedName("location")
    private Location location;

    @SerializedName("id")
    private String id;

    @SerializedName("payu_json")
    private String payuJson;

    //    OBJECTS INITIALIZED AT APP SIDE FROM PAYU_JSON VALUE
    private PayuTransactionResponse payuTransactionResponse;

    @SerializedName("status")
    private String transactionStatus;

    @SerializedName("tenant_name")
    private String tenantName;

    @SerializedName("tenant_mobile")
    private String tenantMobile;

    @SerializedName("coupan_code")
    private String coupanCodeApplied;

    public String getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(String securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(String numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public String getCleaningFee() {
        return cleaningFee;
    }

    public void setCleaningFee(String cleaningFee) {
        this.cleaningFee = cleaningFee;
    }

    public String getPayuJson() {
        return payuJson;
    }

    public void setPayuJson(String payuJson) {
        this.payuJson = payuJson;
    }

    public String getTotalAmountAfterDiscount() {
        return totalAmountAfterDiscount;
    }

    public void setTotalAmountAfterDiscount(String totalAmountAfterDiscount) {
        this.totalAmountAfterDiscount = totalAmountAfterDiscount;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPgName() {
        return pgName;
    }

    public void setPgName(String pgName) {
        this.pgName = pgName;
    }

    public String getBookingPrice() {
        return bookingPrice;
    }

    public void setBookingPrice(String bookingPrice) {
        this.bookingPrice = bookingPrice;
    }

    public String getNumberOfMonths() {
        return numberOfMonths;
    }

    public void setNumberOfMonths(String numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantMobile() {
        return tenantMobile;
    }

    public void setTenantMobile(String tenantMobile) {
        this.tenantMobile = tenantMobile;
    }

    public String getCoupanCodeApplied() {
        return coupanCodeApplied;
    }

    public void setCoupanCodeApplied(String coupanCodeApplied) {
        this.coupanCodeApplied = coupanCodeApplied;
    }

    public PayuTransactionResponse getPayuTransactionResponse() {
        return payuTransactionResponse;
    }

    public void setPayuTransactionResponse(PayuTransactionResponse payuTransactionResponse) {
        this.payuTransactionResponse = payuTransactionResponse;
    }
}