package com.elitesmartsolution.paypgroom.models.registerdevice;

public class Data {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return
                "Data{" +
                        "token = '" + token + '\'' +
                        "}";
    }
}
