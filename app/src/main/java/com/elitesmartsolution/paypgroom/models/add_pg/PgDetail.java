package com.elitesmartsolution.paypgroom.models.add_pg;

import com.google.gson.annotations.SerializedName;

public class PgDetail {

    @SerializedName("other_details")
    private String otherDetails;

    @SerializedName("property_type_id")
    private String propertyTypeId;

    @SerializedName("is_fooding_available")
    private String isFoodingAvailable;

    @SerializedName("size")
    private String size;

    @SerializedName("pg_name")
    private String pgName;

    @SerializedName("guests")
    private String guests;

    @SerializedName("description")
    private String description;

    @SerializedName("room_type_id")
    private String roomTypeId;

    @SerializedName("beds")
    private String beds;

    @SerializedName("bathroom")
    private String bathroom;

    @SerializedName("bedroom")
    private String bedroom;

    @SerializedName("service_city_code")
    private String serviceCityCode;

    @SerializedName("luxry_type")
    private String luxuryType;

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public String getPropertyTypeId() {
        return propertyTypeId;
    }

    public void setPropertyTypeId(String propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }

    public String getIsFoodingAvailable() {
        return isFoodingAvailable;
    }

    public void setIsFoodingAvailable(String isFoodingAvailable) {
        this.isFoodingAvailable = isFoodingAvailable;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPgName() {
        return pgName;
    }

    public void setPgName(String pgName) {
        this.pgName = pgName;
    }

    public String getGuests() {
        return guests;
    }

    public void setGuests(String guests) {
        this.guests = guests;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoomTypeId() {
        return roomTypeId;
    }

    public void setRoomTypeId(String roomTypeId) {
        this.roomTypeId = roomTypeId;
    }

    public String getBeds() {
        return beds;
    }

    public void setBeds(String beds) {
        this.beds = beds;
    }

    public String getBathroom() {
        return bathroom;
    }

    public void setBathroom(String bathroom) {
        this.bathroom = bathroom;
    }

    public String getBedroom() {
        return bedroom;
    }

    public void setBedroom(String bedroom) {
        this.bedroom = bedroom;
    }

    public String getServiceCityCode() {
        return serviceCityCode;
    }

    public void setServiceCityCode(String serviceCityCode) {
        this.serviceCityCode = serviceCityCode;
    }

    public String getLuxuryType() {
        return luxuryType;
    }

    public void setLuxuryType(String luxuryType) {
        this.luxuryType = luxuryType;
    }
}