package com.elitesmartsolution.paypgroom.models;

import com.google.gson.annotations.SerializedName;

public class RequestVerifyOTP {

    @SerializedName("request_type")
    private String requestType;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("otp")
    private String otp;

    @SerializedName("referral_code")
    private String referralCode;

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }
}