package com.elitesmartsolution.paypgroom.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent implements Serializable {

    public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String id;
        public final String content;
        public final String details;
        public final String photoUrl;

        public DummyItem(String id, String content, String details, String photoUrl) {
            this.id = id;
            this.content = content;
            this.details = details;
            this.photoUrl = photoUrl;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
