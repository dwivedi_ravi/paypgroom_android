package com.elitesmartsolution.paypgroom.models.my_bookings;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestMyBookings implements Serializable {

    @SerializedName("tenant_id")
    private String tenantId;

    @SerializedName("owner_id")
    private String ownerId;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}