package com.elitesmartsolution.paypgroom.models.categories;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetCategories {

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("meta_data_change_date")
    private String metaDataChangeDate;

    @SerializedName("message")
    private String message;

    @SerializedName("latest_meta_data_change_date")
    private String latestMetaDataChangeDate;

    @SerializedName("status")
    private boolean status;

    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    public String getMetaDataChangeDate() {
        return metaDataChangeDate;
    }

    public void setMetaDataChangeDate(String metaDataChangeDate) {
        this.metaDataChangeDate = metaDataChangeDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLatestMetaDataChangeDate() {
        return latestMetaDataChangeDate;
    }

    public void setLatestMetaDataChangeDate(String latestMetaDataChangeDate) {
        this.latestMetaDataChangeDate = latestMetaDataChangeDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}