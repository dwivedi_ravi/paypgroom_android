package com.elitesmartsolution.paypgroom.models.registerdevice;

public class RequestRegisterDevice {
    private String deviceOSVer;
    private String deviceOS;
    private String deviceName;
    private String deviceUUID;

    public String getDeviceOSVer() {
        return deviceOSVer;
    }

    public void setDeviceOSVer(String deviceOSVer) {
        this.deviceOSVer = deviceOSVer;
    }

    public String getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(String deviceOS) {
        this.deviceOS = deviceOS;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceUUID() {
        return deviceUUID;
    }

    public void setDeviceUUID(String deviceUUID) {
        this.deviceUUID = deviceUUID;
    }

    @Override
    public String toString() {
        return
                "RequestRegisterDevice{" +
                        "deviceOSVer = '" + deviceOSVer + '\'' +
                        ",deviceOS = '" + deviceOS + '\'' +
                        ",deviceName = '" + deviceName + '\'' +
                        ",deviceUUID = '" + deviceUUID + '\'' +
                        "}";
    }
}
