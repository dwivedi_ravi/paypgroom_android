package com.elitesmartsolution.paypgroom.models.getallnotifications;

import com.google.gson.annotations.SerializedName;

public class RequestGetAllNotification {

    @SerializedName("to_userid")
    private String toUserId;

    @SerializedName("from_userid")
    private String fromUserId;

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }
}