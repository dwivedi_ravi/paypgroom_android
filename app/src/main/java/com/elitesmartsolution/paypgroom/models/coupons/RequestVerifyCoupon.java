package com.elitesmartsolution.paypgroom.models.coupons;

import com.google.gson.annotations.SerializedName;

public class RequestVerifyCoupon {

    @SerializedName("tenant_id")
    private String tenantId;

    @SerializedName("coupan_code")
    private String couponCode;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
}