package com.elitesmartsolution.paypgroom.models;

import com.google.gson.annotations.SerializedName;

public class RequestLogin {

    @SerializedName("password")
    private String password;

    @SerializedName("mobile")
    private String mobile;

    private boolean navigateToHomeScreen = false;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public boolean isNavigateToHomeScreen() {
        return navigateToHomeScreen;
    }

    public void setNavigateToHomeScreen(boolean navigateToHomeScreen) {
        this.navigateToHomeScreen = navigateToHomeScreen;
    }
}