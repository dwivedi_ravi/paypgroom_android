package com.elitesmartsolution.paypgroom.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestOTP implements Serializable {

    @SerializedName("country_code")
    private String countryCode;

    @SerializedName("password")
    private String password;

    @SerializedName("user_type")
    private String userType;

    @SerializedName("request_type")
    private String requestType;

    @SerializedName("mobile")
    private String mobile;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}