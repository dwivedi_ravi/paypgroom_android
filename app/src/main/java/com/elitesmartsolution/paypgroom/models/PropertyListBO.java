package com.elitesmartsolution.paypgroom.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PropertyListBO implements Serializable {

    public List<PropertyImagesBO> mPropertyImagesList = new ArrayList<>();
    @SerializedName("firstname")
    private String firstname;
    @SerializedName("about")
    private String about;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("email_verified_at")
    private String emailVerifiedAt;
    @SerializedName("otp")
    private String otp;
    @SerializedName("is_mobile_verified")
    private String isMobileVerified;
    @SerializedName("deleted_at")
    private String deletedAt;
    @SerializedName("lastname")
    private String lastname;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("dob")
    private String dob;
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private String id;
    @SerializedName("mobile_number")
    private String mobileNumber;
    @SerializedName("avtar")
    private String avtar;
    @SerializedName("email")
    private String email;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(String emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getIsMobileVerified() {
        return isMobileVerified;
    }

    public void setIsMobileVerified(String isMobileVerified) {
        this.isMobileVerified = isMobileVerified;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAvtar() {
        return avtar;
    }

    public void setAvtar(String avtar) {
        this.avtar = avtar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return
                "PropertyListBO{" +
                        "firstname = '" + firstname + '\'' +
                        ",about = '" + about + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",email_verified_at = '" + emailVerifiedAt + '\'' +
                        ",otp = '" + otp + '\'' +
                        ",is_mobile_verified = '" + isMobileVerified + '\'' +
                        ",deleted_at = '" + deletedAt + '\'' +
                        ",lastname = '" + lastname + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",dob = '" + dob + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        ",mobile_number = '" + mobileNumber + '\'' +
                        ",avtar = '" + avtar + '\'' +
                        ",email = '" + email + '\'' +
                        "}";
    }

    public List<PropertyImagesBO> getmPropertyImagesList() {
        return mPropertyImagesList;
    }

    public void setmPropertyImagesList(List<PropertyImagesBO> mPropertyImagesList) {
        this.mPropertyImagesList = mPropertyImagesList;
    }
}