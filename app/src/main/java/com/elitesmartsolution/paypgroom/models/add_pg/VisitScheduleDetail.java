package com.elitesmartsolution.paypgroom.models.add_pg;

import com.google.gson.annotations.SerializedName;

public class VisitScheduleDetail {

    @SerializedName("visit_from")
    private String visitFrom;

    @SerializedName("visit_to")
    private String visitTo;

    public String getVisitFrom() {
        return visitFrom;
    }

    public void setVisitFrom(String visitFrom) {
        this.visitFrom = visitFrom;
    }

    public String getVisitTo() {
        return visitTo;
    }

    public void setVisitTo(String visitTo) {
        this.visitTo = visitTo;
    }
}