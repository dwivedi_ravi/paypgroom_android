package com.elitesmartsolution.paypgroom.models.add_pg;

import com.google.gson.annotations.SerializedName;

public class PgImagesItem {

    @SerializedName("image_name")
    private String image;

    @SerializedName("description")
    private String description;

    @SerializedName("title")
    private String title;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}