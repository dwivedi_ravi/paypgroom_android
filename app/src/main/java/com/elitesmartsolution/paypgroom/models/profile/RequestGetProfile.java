package com.elitesmartsolution.paypgroom.models.profile;

import com.google.gson.annotations.SerializedName;

public class RequestGetProfile {

    @SerializedName("user_id")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}