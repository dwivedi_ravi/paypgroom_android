package com.elitesmartsolution.paypgroom.models;

import com.google.gson.annotations.SerializedName;

public class RequestChangeAvailabilityStatus {

    @SerializedName("user_id")
    private String userId;

    @SerializedName("pg_id")
    private String pgId;

    @SerializedName("is_available")
    private String availablityStatus;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPgId() {
        return pgId;
    }

    public void setPgId(String pgId) {
        this.pgId = pgId;
    }

    public String getAvailablityStatus() {
        return availablityStatus;
    }

    public void setAvailablityStatus(String availablityStatus) {
        this.availablityStatus = availablityStatus;
    }
}