package com.elitesmartsolution.paypgroom.models.add_pg;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ResponseUploadPropertyImage implements Serializable {

    @SerializedName("data")
    private DataUploadImageResponse dataUploadImageResponse;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    private boolean imageUploaded;

    public DataUploadImageResponse getDataUploadImageResponse() {
        return dataUploadImageResponse;
    }

    public void setDataUploadImageResponse(DataUploadImageResponse dataUploadImageResponse) {
        this.dataUploadImageResponse = dataUploadImageResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isImageUploaded() {
        return imageUploaded;
    }

    public void setImageUploaded(boolean imageUploaded) {
        this.imageUploaded = imageUploaded;
    }
}