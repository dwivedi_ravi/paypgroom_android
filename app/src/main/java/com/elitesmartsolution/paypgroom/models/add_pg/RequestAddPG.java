package com.elitesmartsolution.paypgroom.models.add_pg;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestAddPG {

    @SerializedName("amenities")
    private List<String> amenities;

    @SerializedName("pricing_detail")
    private PricingDetail pricingDetail;

    @SerializedName("pg_images")
    private List<PgImagesItem> pgImages;

    @SerializedName("owner_id")
    private String ownerId;

    @SerializedName("location")
    private Location location;

    @SerializedName("pg_detail")
    private PgDetail pgDetail;

    @SerializedName("visit_schedule_detail")
    private VisitScheduleDetail visitScheduleDetail;

    public List<String> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<String> amenities) {
        this.amenities = amenities;
    }

    public PricingDetail getPricingDetail() {
        return pricingDetail;
    }

    public void setPricingDetail(PricingDetail pricingDetail) {
        this.pricingDetail = pricingDetail;
    }

    public List<PgImagesItem> getPgImages() {
        return pgImages;
    }

    public void setPgImages(List<PgImagesItem> pgImages) {
        this.pgImages = pgImages;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public PgDetail getPgDetail() {
        return pgDetail;
    }

    public void setPgDetail(PgDetail pgDetail) {
        this.pgDetail = pgDetail;
    }

    public VisitScheduleDetail getVisitScheduleDetail() {
        return visitScheduleDetail;
    }

    public void setVisitScheduleDetail(VisitScheduleDetail visitScheduleDetail) {
        this.visitScheduleDetail = visitScheduleDetail;
    }
}