package com.elitesmartsolution.paypgroom.models.pglisting;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PgPricingDetail implements Serializable {

    @SerializedName("per_month")
    private String perMonth = "0";

    @SerializedName("check_out")
    private String checkOut;

    @SerializedName("security_deposit")
    private String securityDeposit = "0";

    @SerializedName("cleaning_fee")
    private String cleaningFee = "0";

    @SerializedName("minimum_stay_month")
    private String minimumStayMonth;

    @SerializedName("check_in")
    private String checkIn;

    @SerializedName("discount")
    private String discount = "0";

    @SerializedName("cancellation_charge")
    private String cancellationCharge;

    public String getPerMonth() {
        return perMonth;
    }

    public void setPerMonth(String perMonth) {
        this.perMonth = perMonth;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getSecurityDeposit() {
        return securityDeposit;
    }

    public void setSecurityDeposit(String securityDeposit) {
        this.securityDeposit = securityDeposit;
    }

    public String getCleaningFee() {
        return cleaningFee;
    }

    public void setCleaningFee(String cleaningFee) {
        this.cleaningFee = cleaningFee;
    }

    public String getMinimumStayMonth() {
        return minimumStayMonth;
    }

    public void setMinimumStayMonth(String minimumStayMonth) {
        this.minimumStayMonth = minimumStayMonth;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getCancellationCharge() {
        return cancellationCharge;
    }

    public void setCancellationCharge(String cancellationCharge) {
        this.cancellationCharge = cancellationCharge;
    }
}