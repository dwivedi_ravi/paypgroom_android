package com.elitesmartsolution.paypgroom.models.delete_pg;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseDeletePG {

    @SerializedName("data")
    private List<Data> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}