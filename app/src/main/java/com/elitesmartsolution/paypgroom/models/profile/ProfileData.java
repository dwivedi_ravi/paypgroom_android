package com.elitesmartsolution.paypgroom.models.profile;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProfileData implements Serializable {

    @SerializedName("user")
    private UserProfile userProfile;

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}