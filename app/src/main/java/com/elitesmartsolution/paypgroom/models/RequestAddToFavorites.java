package com.elitesmartsolution.paypgroom.models;

import com.google.gson.annotations.SerializedName;

public class RequestAddToFavorites {

    @SerializedName("user_id")
    private String userId;

    @SerializedName("pg_id")
    private String pgId;

    @SerializedName("is_favorite")
    private String favoriteStatus;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPgId() {
        return pgId;
    }

    public void setPgId(String pgId) {
        this.pgId = pgId;
    }

    public String getFavoriteStatus() {
        return favoriteStatus;
    }

    public void setFavoriteStatus(String favoriteStatus) {
        this.favoriteStatus = favoriteStatus;
    }
}