package com.elitesmartsolution.paypgroom.models.pglisting;

import com.google.gson.annotations.SerializedName;

public class RequestGetPgListingSingleValue {

    @SerializedName("is_recommended")
    private String isRecommended = "";

    @SerializedName("request_type")
    private String requestType = "";

    @SerializedName("value")
    private String value = "";

    public String getIsRecommended() {
        return isRecommended;
    }

    public void setIsRecommended(String isRecommended) {
        this.isRecommended = isRecommended;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}