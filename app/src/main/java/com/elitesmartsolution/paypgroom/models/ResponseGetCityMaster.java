package com.elitesmartsolution.paypgroom.models;

import com.elitesmartsolution.paypgroom.models.pgmetadata.CityMasterItem;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseGetCityMaster {

    @SerializedName("data")
    private List<CityMasterItem> data;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private boolean status;

    public List<CityMasterItem> getData() {
        return data;
    }

    public void setData(List<CityMasterItem> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}