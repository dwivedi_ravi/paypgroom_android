package com.elitesmartsolution.paypgroom.models.add_pg;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataUploadImageResponse implements Serializable {

    @SerializedName("uploaded_image_name")
    private String uploadedImageName;

    @SerializedName("uploaded_image_url")
    private String uplodedImageUrl;

    public String getUploadedImageName() {
        return uploadedImageName;
    }

    public void setUploadedImageName(String uploadedImageName) {
        this.uploadedImageName = uploadedImageName;
    }

    public String getUplodedImageUrl() {
        return uplodedImageUrl;
    }

    public void setUplodedImageUrl(String uplodedImageUrl) {
        this.uplodedImageUrl = uplodedImageUrl;
    }
}