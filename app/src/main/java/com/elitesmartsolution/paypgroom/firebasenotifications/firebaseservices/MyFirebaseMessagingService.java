package com.elitesmartsolution.paypgroom.firebasenotifications.firebaseservices;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.elitesmartsolution.paypgroom.activities.homescreen.activity.HomeScreenBottomNavigationActivity;
import com.elitesmartsolution.paypgroom.activities.homescreenowner.activity.OwnerHomeScreenActivity;
import com.elitesmartsolution.paypgroom.firebasenotifications.firebaseutils.MyJobService;
import com.elitesmartsolution.paypgroom.firebasenotifications.firebaseutils.NotificationUtils;
import com.elitesmartsolution.paypgroom.models.fcmregistration.RequestFCMPushToken;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Keys;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by Ravi Dwivedi.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
//            handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getIcon());
//        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                handleNow();
//            }
            try {
//                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(remoteMessage.getData());
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        } else if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getIcon());
        }
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        Prefs.setPushTokenRegistered(false);
        sendRegistrationToServer(token);
        Prefs.setFirebaseInstanceIdInSharedPreferences(token);
        Prefs.setIsGlobalBroadcastTopicSubscribedInSharedPreferences(false);
        Prefs.setIsIndianBroadcastTopicSubscribedInSharedPreferences(false);
        NotificationUtils.subscribeToTopic(getApplicationContext(), true);
    }

    private void handleNotification(String title, String message, String icon) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
//            // play notification sound
//            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//            notificationUtils.playNotificationSound();

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();

//            Intent resultIntent = new Intent(getApplicationContext(), HomeScreenActivity.class);
            Intent resultIntent;
            if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
                resultIntent = new Intent(getApplicationContext(), OwnerHomeScreenActivity.class);
            else
                resultIntent = new Intent(getApplicationContext(), HomeScreenBottomNavigationActivity.class);
            resultIntent.putExtra("title", title);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("icon", icon);

            // check for image attachment
            if (TextUtils.isEmpty(icon)) {
                showNotificationMessage(getApplicationContext(), title, message, Calendar.getInstance().getTimeInMillis(), resultIntent, true);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, Calendar.getInstance().getTimeInMillis(), resultIntent, icon);
            }
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(Map<String, String> resultDataObject) {
        Log.e(TAG, "push json: " + resultDataObject.toString());

        try {
            String title = resultDataObject.get("title");
            String message = resultDataObject.get("message");
            String url = resultDataObject.get("url");
            String imageUrl = resultDataObject.get("imageUrl");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "url: " + url);
            Log.e(TAG, "imageUrl: " + imageUrl);

//            Intent resultIntent = new Intent(getApplicationContext(), HomeScreenBottomNavigationActivity.class);
            Intent resultIntent;
            if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
                resultIntent = new Intent(getApplicationContext(), OwnerHomeScreenActivity.class);
            else
                resultIntent = new Intent(getApplicationContext(), HomeScreenBottomNavigationActivity.class);
            resultIntent.putExtra("title", title);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("url", url);
            resultIntent.putExtra("imageUrl", imageUrl);
            // check for image attachment
            if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, Calendar.getInstance().getTimeInMillis(), resultIntent, true);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, Calendar.getInstance().getTimeInMillis(), resultIntent, imageUrl);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, long timeStamp, Intent intent, boolean isActivityInForeground) {
        notificationUtils = new NotificationUtils(context);
        if (isActivityInForeground) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, long timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        if (!Prefs.isPushTokenRegistered() && !token.isEmpty() && !Prefs.getUserId().isEmpty()) {
            RequestFCMPushToken objRequestFCMPushToken = new RequestFCMPushToken();
            objRequestFCMPushToken.setUserId(Prefs.getUserId());
            objRequestFCMPushToken.setGcmId(token);
            ApiCallerUtility.callRegisterPushTokenApi(getApplicationContext(), objRequestFCMPushToken);
        }
    }
}