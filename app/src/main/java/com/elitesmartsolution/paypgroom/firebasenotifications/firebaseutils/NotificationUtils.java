package com.elitesmartsolution.paypgroom.firebasenotifications.firebaseutils;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.elitesmartsolution.paypgroom.BuildConfig;
import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.models.fcmregistration.RequestFCMPushToken;
import com.elitesmartsolution.paypgroom.network.ApiCallerUtility;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotificationUtils {

    private static String TAG = NotificationUtils.class.getSimpleName();
    private Context mContext;

    public NotificationUtils(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * Method checks if the app is in background or not
     */
    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (String activeProcess : processInfo.pkgList) {
                            if (activeProcess.equals(context.getPackageName())) {
                                isInBackground = false;
                            }
                        }
                    }
                }
            } else {
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                if (componentInfo.getPackageName().equals(context.getPackageName())) {
                    isInBackground = false;
                }
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }

        return isInBackground;
    }

    // Clears notification tray messages
    public static void clearNotifications(Context context) {
        try {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void getInstanceIdForFirebaseNotifications(Context context, boolean forceInstanceIdGet) {
        try {
            if (Prefs.getFirebaseInstanceIdFromSharedPreferences().equals("") || forceInstanceIdGet) {
//                FirebaseInstanceId.getInstance().getInstanceId()
//                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                            @Override
//                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                                try {
//                                    if (task != null && !task.isSuccessful()) {
//                                        Log.w(TAG, "getInstanceId failed", task.getException());
//                                        return;
//                                    }
//                                    // Get new Instance ID token
//                                    String token = task.getResult().getToken();
//                                    Prefs.setFirebaseInstanceIdInSharedPreferences(token);
//
//                                    if (!Prefs.getUserId().equals("")) {
//                                        RequestFCMPushToken objRequestFCMPushToken = new RequestFCMPushToken();
//                                        objRequestFCMPushToken.setUserId(Prefs.getUserId());
//                                        objRequestFCMPushToken.setGcmId(token);
//                                        ApiCallerUtility.callRegisterPushTokenApi(context, objRequestFCMPushToken);
//                                    }
//
//                                    subscribeToTopic(context, true);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
                FirebaseInstallations.getInstance().getToken(true).addOnCompleteListener(task -> {
                    try {
                        if (task != null && !task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().toString();
                        Prefs.setFirebaseInstanceIdInSharedPreferences(token);

                        if (!Prefs.getUserId().equals("")) {
                            RequestFCMPushToken objRequestFCMPushToken = new RequestFCMPushToken();
                            objRequestFCMPushToken.setUserId(Prefs.getUserId());
                            objRequestFCMPushToken.setGcmId(token);
                            ApiCallerUtility.callRegisterPushTokenApi(context, objRequestFCMPushToken);
                        }
                        subscribeToTopic(context, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            } else {
                subscribeToTopic(context, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void subscribeToTopic(Context context, boolean forceSubscription) {
        try {
            if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug")) {
                FirebaseMessaging.getInstance().subscribeToTopic(Defaults.FIREBASE_TOPIC_TEST_BROADCAST)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task != null && task.isSuccessful()) {
                                    Prefs.setIsIndianBroadcastTopicSubscribedInSharedPreferences(true);
                                }
                            }
                        });
            } else {
                if (Utility.checkIfUserFromIndia(context)) {
                    if (!Prefs.getIsIndianBroadcastTopicSubscribedFromSharedPreferences() || forceSubscription) {
                        FirebaseMessaging.getInstance().subscribeToTopic(Defaults.FIREBASE_TOPIC_INDIAN_BROADCAST)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task != null && task.isSuccessful()) {
                                            Prefs.setIsIndianBroadcastTopicSubscribedInSharedPreferences(true);
                                        }
                                    }
                                });
                    }
                } else {
                    if (!Prefs.getIsGlobalBroadcastTopicSubscribedFromSharedPreferences() || forceSubscription) {
                        FirebaseMessaging.getInstance().subscribeToTopic(Defaults.FIREBASE_TOPIC_GLOBAL_BROADCAST)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task != null && task.isSuccessful()) {
                                            Prefs.setIsGlobalBroadcastTopicSubscribedInSharedPreferences(true);
                                        }
                                    }
                                });
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void registerPushTokenIfMissing(Context context) {
        if (!Prefs.isPushTokenRegistered() && !Prefs.getFirebaseInstanceIdFromSharedPreferences().isEmpty() && !Prefs.getUserId().isEmpty()) {
            RequestFCMPushToken objRequestFCMPushToken = new RequestFCMPushToken();
            objRequestFCMPushToken.setUserId(Prefs.getUserId());
            objRequestFCMPushToken.setGcmId(Prefs.getFirebaseInstanceIdFromSharedPreferences());
            ApiCallerUtility.callRegisterPushTokenApi(context, objRequestFCMPushToken);
        }
    }

    public void showNotificationMessage(String title, String message, long timeStamp, Intent intent) {
        showNotificationMessage(title, message, timeStamp, intent, null);
    }

    public void showNotificationMessage(final String title, final String message, final long timeStamp, Intent intent, String imageUrl) {
        try {
            // Check for empty push message
            if (TextUtils.isEmpty(message))
                return;

            // notification icon
            final int icon = R.mipmap.ic_launcher;

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            final PendingIntent resultPendingIntent =
//                    PendingIntent.getActivity(
//                            mContext,
//                            0,
//                            intent,
//                            /*PendingIntent.FLAG_CANCEL_CURRENT*/
//                            FLAG_ONE_SHOT
//                    );
            final PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            mContext,
                            0,
                            intent,
                            /*PendingIntent.FLAG_CANCEL_CURRENT*/
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            String channelId = mContext.getString(R.string.default_notification_channel_id);
            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    mContext, channelId);

            final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + mContext.getPackageName() + "/raw/notification");

            if (!TextUtils.isEmpty(imageUrl)) {
                if (imageUrl != null && imageUrl.length() > 4 && Patterns.WEB_URL.matcher(imageUrl).matches()) {
                    Bitmap bitmap = getBitmapFromURL(imageUrl);
                    if (bitmap != null) {
                        showBigNotification(bitmap, mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound, channelId);
                    } else {
                        showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound, channelId);
                    }
                }
            } else {
                showSmallNotification(mBuilder, icon, title, message, timeStamp, resultPendingIntent, alarmSound, channelId);
                playNotificationSound();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSmallNotification(NotificationCompat.Builder mBuilder, int icon, String title, String message, long timeStamp, PendingIntent resultPendingIntent, Uri alarmSound, String channelId) {
//        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
//        inboxStyle.addLine(message);

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText(message);
        bigTextStyle.setBigContentTitle(title);
//        bigTextStyle.setSummaryText(Html.fromHtml(message).toString());

        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(bigTextStyle)
//                .setWhen(getTimeMilliSec(timeStamp))
                .setWhen(timeStamp)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setGroup(Defaults.GROUP_KEY_SECURE_GALLERY_NOTIFICATIONS)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "PayPg Room Notifications",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
//        notificationManager.notify(Defaults.NOTIFICATION_ID, notification);
        notificationManager.notify(Prefs.getUniqueIntegerValueFromSharedPrefs(), notification);
    }

    private void showBigNotification(Bitmap bitmap, NotificationCompat.Builder mBuilder, int icon, String title, String message, long timeStamp, PendingIntent resultPendingIntent, Uri alarmSound, String channelId) {
        NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
        bigPictureStyle.setBigContentTitle(title);
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString());
        bigPictureStyle.bigPicture(bitmap);
        Notification notification;
        notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentIntent(resultPendingIntent)
                .setSound(alarmSound)
                .setStyle(bigPictureStyle)
//                .setWhen(getTimeMilliSec(timeStamp))
                .setWhen(timeStamp)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setGroup(Defaults.GROUP_KEY_SECURE_GALLERY_NOTIFICATIONS)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), icon))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "PayPg Room Notifications",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(Defaults.NOTIFICATION_ID_BIG_IMAGE, notification);
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Playing notification sound
    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + mContext.getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(mContext, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}