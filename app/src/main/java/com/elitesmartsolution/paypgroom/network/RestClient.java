package com.elitesmartsolution.paypgroom.network;

//import com.squareup.okhttp.Interceptor;
//import com.squareup.okhttp.OkHttpClient;
//import com.squareup.okhttp.DirectionsResponse;

import com.elitesmartsolution.paypgroom.models.RequestAddToFavorites;
import com.elitesmartsolution.paypgroom.models.RequestChangeAvailabilityStatus;
import com.elitesmartsolution.paypgroom.models.RequestChangePassword;
import com.elitesmartsolution.paypgroom.models.RequestLogin;
import com.elitesmartsolution.paypgroom.models.RequestOTP;
import com.elitesmartsolution.paypgroom.models.RequestRegenerateAccessToken;
import com.elitesmartsolution.paypgroom.models.RequestVerifyOTP;
import com.elitesmartsolution.paypgroom.models.ResponseChangeAvailabilityStatus;
import com.elitesmartsolution.paypgroom.models.ResponseGetCityMaster;
import com.elitesmartsolution.paypgroom.models.ResponseProfile;
import com.elitesmartsolution.paypgroom.models.ResponseRequestOTP;
import com.elitesmartsolution.paypgroom.models.ResponseVerifyOTP;
import com.elitesmartsolution.paypgroom.models.add_pg.RequestAddPG;
import com.elitesmartsolution.paypgroom.models.add_pg.ResponseUploadPropertyImage;
import com.elitesmartsolution.paypgroom.models.amenities.ResponseGetAllAmenities;
import com.elitesmartsolution.paypgroom.models.bookpg.RequestBookPg;
import com.elitesmartsolution.paypgroom.models.bookpg.ResponseBookPg;
import com.elitesmartsolution.paypgroom.models.categories.ResponseGetCategories;
import com.elitesmartsolution.paypgroom.models.coupons.RequestVerifyCoupon;
import com.elitesmartsolution.paypgroom.models.coupons.ResponseGetCoupon;
import com.elitesmartsolution.paypgroom.models.coupons.ResponseVerifyCoupon;
import com.elitesmartsolution.paypgroom.models.delete_pg.RequestDeletePG;
import com.elitesmartsolution.paypgroom.models.delete_pg.ResponseDeletePG;
import com.elitesmartsolution.paypgroom.models.fcmregistration.RequestFCMPushToken;
import com.elitesmartsolution.paypgroom.models.fcmregistration.ResponseFCMPushToken;
import com.elitesmartsolution.paypgroom.models.getallnotifications.RequestGetAllNotification;
import com.elitesmartsolution.paypgroom.models.getallnotifications.ResponseGetAllNotifications;
import com.elitesmartsolution.paypgroom.models.my_bookings.RequestMyBookings;
import com.elitesmartsolution.paypgroom.models.my_bookings.ResponseMyBookings;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetMyPgs;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetPgListing;
import com.elitesmartsolution.paypgroom.models.pglisting.ResponsePgListing;
import com.elitesmartsolution.paypgroom.models.pgmetadata.ResponseGetAppMetaData;
import com.elitesmartsolution.paypgroom.models.profile.RequestEditProfile;
import com.elitesmartsolution.paypgroom.models.profile.RequestGetProfile;
import com.elitesmartsolution.paypgroom.models.registerdevice.RequestRegisterDevice;
import com.elitesmartsolution.paypgroom.models.registerdevice.ResponseRegisterDevice;
import com.elitesmartsolution.paypgroom.models.verifychecksum.RequestVerifyChecksum;
import com.elitesmartsolution.paypgroom.models.verifychecksum.ResponseVerifyChecksum;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public class RestClient {

//    LIVE SERVER LINK
//    MUST REMOVE NETWORK SECURITY FILE FROM THE ANDROID MANIFEST AND REMOVE LINES WHICH ARE PERMITTING CLEARTEXT TRANSMISSION.
//    public static String baseUrl = "https://paypgroom.com/api/";

    //  DEVELOPMENT SERVER LINK
//    public static String baseUrl = "http://paypgroom.shyamdeveloper.co.in/api/";
    public static String baseUrl = "http://paypgroom.pgbooking.co.in/api/";
    private static GitApiInterface gitApiInterface;

    public static GitApiInterface getClient() {

        if (gitApiInterface == null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
//                    Request.Builder requestBuilder = original.newBuilder()
//                            .header("Authorization", "auth-value"); // <-- this is the important line

//                    Request.Builder requestBuilder = original.newBuilder()
//                            .header("Authorization", Prefs.getAuthKey()); // <-- this is the important line
//                    requestBuilder.addHeader("Content-Type", "application/json");

                    Request.Builder requestBuilder = original.newBuilder();
                    requestBuilder.addHeader("Authorization", Prefs.getAuthKey());
                    requestBuilder.addHeader("Content-Type", "application/json");

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
            OkHttpClient okClient = httpClient.build();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(okClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }
        return gitApiInterface;
    }

    public interface GitApiInterface {
        //        @POST("new-register-device")
        @POST("new-device")
        Call<ResponseRegisterDevice> callRegisterDevice(@Body RequestRegisterDevice objRequestRegisterDevice);

        @POST("token-revoke")
        Call<ResponseRegisterDevice> callRegenerateAccessToken(@Body RequestRegenerateAccessToken objRequestRegenerateAccessToken);

        @POST("user/request-otp")
        Call<ResponseRequestOTP> callRequestOTP(@Body RequestOTP objRequestOTP);
//        Call<ResponseRequestOTP> callRequestOTP(@HeaderMap Map<String, String> headers, @Body RequestOTP objRequestOTP);

        @POST("user/verify-otp")
        Call<ResponseVerifyOTP> callVerifyOTP(@Body RequestVerifyOTP objRequestVerifyOTP);

        @POST("user/login")
        Call<ResponseProfile> callLogin(@Body RequestLogin objRequestLogin);

        @POST("user/profile")
        Call<ResponseProfile> callGetProfile(@Body RequestGetProfile objRequestGetProfile);

        @POST("user/update-profile")
        Call<ResponseProfile> callEditProfile(@Body RequestEditProfile objRequestEditProfile);

        @POST("user/user-gcm")
        Call<ResponseFCMPushToken> callRegisterPushToken(@Body RequestFCMPushToken objRequestFCMPushToken);

        @Multipart
        @POST("user/update-avtar")
        Call<ResponseProfile> callUploadProfileImage(
                @Part("user_id") RequestBody userId,
                @Part MultipartBody.Part file
        );

//        @Multipart
//        @POST("user/upload-multiple-file")
//        Call<ResponseProfile> callUploadMultipleImages(
//                @Part("property_id") RequestBody propertyId,
//                @Part MultipartBody.Part file1,
//                @Part MultipartBody.Part file2,
//                @Part MultipartBody.Part file3
//        );

        @POST("user/change-password")
        Call<ResponseVerifyOTP> callChangePassword(@Body RequestChangePassword objRequestChangePassword);

        @POST("pg/get-amenities")
        Call<ResponseGetAllAmenities> callGetAllAmenities();

        @POST("pg/pg-meta-data")
        Call<ResponseGetAppMetaData> callGetPgMetaData();

        @Multipart
        @POST("pg/add-pg-image")
        Call<ResponseUploadPropertyImage> callUploadPropertyImage(
                @Part("image_name") RequestBody imageName,
                @Part MultipartBody.Part file
        );

        @POST("pg/add-pg")
        Call<ResponseVerifyOTP> callAddPG(@Body RequestAddPG objRequestAddPG);

        @POST("pg/get-pgs")
        Call<ResponsePgListing> callGetPgListingMasterApi(@Body RequestGetPgListing objRequestGetPgListing);

//        @POST("pg/get-pgs")
//        Call<ResponsePgListing> callGetPgListingSingleKeyValueApi(@Body RequestGetPgListingSingleValue objRequestGetPgListing);

        @POST("pg/my-pgs")
        Call<ResponsePgListing> callGetMyPgListing(@Body RequestGetMyPgs objRequestGetPgListing);

        @POST("pg/add-favaorate")
        Call<ResponseVerifyOTP> callAddToFavorite(@Body RequestAddToFavorites objRequestAddToFavorites);

        @POST("pg/book-pg")
        Call<ResponseBookPg> callBookPG(@Body RequestBookPg objRequestAddPG);

        @POST("pg/verify-hash-checksum")
        Call<ResponseVerifyChecksum> callVerifyChecksumAndFinalizeBooking(@Body RequestVerifyChecksum objRequestVerifyChecksum);

        @POST("pg/update-pg-available-status")
        Call<ResponseChangeAvailabilityStatus> callChangeAvailabilityStatus(@Body RequestChangeAvailabilityStatus objRequestAddToFavorites);

        @POST("pg/my-bookings")
        Call<ResponseMyBookings> callGetMyBookings(@Body RequestMyBookings objRequestMyBookings);

        @POST("get_notiication_list")
        Call<ResponseGetAllNotifications> callGetAllNotifications(@Body RequestGetAllNotification objRequestGetAllNotification);

        @POST("pg/get-city-master")
        Call<ResponseGetCityMaster> callGetAllCities();

        @POST("pg/delete-pg")
        Call<ResponseDeletePG> callDeletePg(@Body RequestDeletePG objRequestDeletePG);

        @POST("get-coupans")
        Call<ResponseGetCoupon> callGetCoupons(@Body RequestMyBookings objRequestMyBookings);

        @POST("verified-coupan")
        Call<ResponseVerifyCoupon> callVerifyCoupon(@Body RequestVerifyCoupon objRequestVerifyCoupon);

        @POST("ads/get-categories")
        Call<ResponseGetCategories> callGetCategories();
    }
}
