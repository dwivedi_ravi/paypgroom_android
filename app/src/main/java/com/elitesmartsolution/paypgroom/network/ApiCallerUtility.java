package com.elitesmartsolution.paypgroom.network;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.activities.ChangePasswordScreen;
import com.elitesmartsolution.paypgroom.activities.EditProfileActivity;
import com.elitesmartsolution.paypgroom.activities.SplashScreen;
import com.elitesmartsolution.paypgroom.activities.VerifyOTPScreen;
import com.elitesmartsolution.paypgroom.activities.homescreen.activity.HomeScreenBottomNavigationActivity;
import com.elitesmartsolution.paypgroom.activities.homescreenowner.activity.OwnerHomeScreenActivity;
import com.elitesmartsolution.paypgroom.database.DBHandler;
import com.elitesmartsolution.paypgroom.models.RequestAddToFavorites;
import com.elitesmartsolution.paypgroom.models.RequestChangeAvailabilityStatus;
import com.elitesmartsolution.paypgroom.models.RequestChangePassword;
import com.elitesmartsolution.paypgroom.models.RequestLogin;
import com.elitesmartsolution.paypgroom.models.RequestOTP;
import com.elitesmartsolution.paypgroom.models.RequestRegenerateAccessToken;
import com.elitesmartsolution.paypgroom.models.RequestVerifyOTP;
import com.elitesmartsolution.paypgroom.models.ResponseChangeAvailabilityStatus;
import com.elitesmartsolution.paypgroom.models.ResponseGetCityMaster;
import com.elitesmartsolution.paypgroom.models.ResponseProfile;
import com.elitesmartsolution.paypgroom.models.ResponseRequestOTP;
import com.elitesmartsolution.paypgroom.models.ResponseVerifyOTP;
import com.elitesmartsolution.paypgroom.models.add_pg.RequestAddPG;
import com.elitesmartsolution.paypgroom.models.add_pg.ResponseUploadPropertyImage;
import com.elitesmartsolution.paypgroom.models.amenities.ResponseGetAllAmenities;
import com.elitesmartsolution.paypgroom.models.bookpg.RequestBookPg;
import com.elitesmartsolution.paypgroom.models.bookpg.ResponseBookPg;
import com.elitesmartsolution.paypgroom.models.coupons.CoupansItem;
import com.elitesmartsolution.paypgroom.models.coupons.RequestVerifyCoupon;
import com.elitesmartsolution.paypgroom.models.coupons.ResponseGetCoupon;
import com.elitesmartsolution.paypgroom.models.coupons.ResponseVerifyCoupon;
import com.elitesmartsolution.paypgroom.models.delete_pg.RequestDeletePG;
import com.elitesmartsolution.paypgroom.models.delete_pg.ResponseDeletePG;
import com.elitesmartsolution.paypgroom.models.error.ErrorResponse;
import com.elitesmartsolution.paypgroom.models.error.Errors;
import com.elitesmartsolution.paypgroom.models.fcmregistration.RequestFCMPushToken;
import com.elitesmartsolution.paypgroom.models.fcmregistration.ResponseFCMPushToken;
import com.elitesmartsolution.paypgroom.models.getallnotifications.RequestGetAllNotification;
import com.elitesmartsolution.paypgroom.models.getallnotifications.ResponseGetAllNotifications;
import com.elitesmartsolution.paypgroom.models.getallnotifications.UserNotificationsItem;
import com.elitesmartsolution.paypgroom.models.my_bookings.DataItem;
import com.elitesmartsolution.paypgroom.models.my_bookings.RequestMyBookings;
import com.elitesmartsolution.paypgroom.models.my_bookings.ResponseMyBookings;
import com.elitesmartsolution.paypgroom.models.payuresponse.PayuTransactionResponse;
import com.elitesmartsolution.paypgroom.models.pglisting.PropertyListItem;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetMyPgs;
import com.elitesmartsolution.paypgroom.models.pglisting.RequestGetPgListing;
import com.elitesmartsolution.paypgroom.models.pglisting.ResponsePgListing;
import com.elitesmartsolution.paypgroom.models.pgmetadata.ResponseGetAppMetaData;
import com.elitesmartsolution.paypgroom.models.profile.RequestEditProfile;
import com.elitesmartsolution.paypgroom.models.profile.RequestGetProfile;
import com.elitesmartsolution.paypgroom.models.registerdevice.RequestRegisterDevice;
import com.elitesmartsolution.paypgroom.models.registerdevice.ResponseRegisterDevice;
import com.elitesmartsolution.paypgroom.models.verifychecksum.RequestVerifyChecksum;
import com.elitesmartsolution.paypgroom.models.verifychecksum.ResponseVerifyChecksum;
import com.elitesmartsolution.paypgroom.util.Connectivity;
import com.elitesmartsolution.paypgroom.util.StorageHelper;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Keys;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.gson.Gson;

import java.io.File;
import java.util.List;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiCallerUtility {

    public static void callRegisterDeviceApi(AppCompatActivity activity) {
        try {
            if (Connectivity.isConnected(activity)) {
                RequestRegisterDevice objRequestRegisterDevice = new RequestRegisterDevice();
                objRequestRegisterDevice.setDeviceName(Build.MODEL);
                objRequestRegisterDevice.setDeviceOS(Build.VERSION.RELEASE);
                objRequestRegisterDevice.setDeviceOSVer(Build.VERSION.SDK_INT + "");
                objRequestRegisterDevice.setDeviceUUID(Settings.Secure.getString(activity.getContentResolver(),
                        Settings.Secure.ANDROID_ID));

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseRegisterDevice> registerDeviceApiCall = service.callRegisterDevice(objRequestRegisterDevice);
                registerDeviceApiCall.enqueue(new Callback<ResponseRegisterDevice>() {
                    @Override
                    public void onResponse(Call<ResponseRegisterDevice> call, Response<ResponseRegisterDevice> response) {

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
//                            if (response.code() == Defaults.RESPONSE_CODE_INTERNAL_SERVER_ERROR) {
//                                callRegenerateAccessTokenAPI(activity);
//                            } else {
//                                Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
//                            }
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Prefs.setAuthKey(response.body().getData().getToken());
                                    ((SplashScreen) activity).takeUserIntoTheApp();
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
//                                    ADDED IN CASE OF DEVICE ALREADY REGISTERED THEN WE'LL NEED TO REGENERATE THE ACCESS TOKEN.
                                    callRegenerateAccessTokenAPI(activity);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseRegisterDevice> call, Throwable t) {
                        Toast.makeText(activity, "Device registration failed. Please try again.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Toast.makeText(activity, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callRegenerateAccessTokenAPI(AppCompatActivity activity) {
        try {
            if (Connectivity.isConnected(activity)) {
                RequestRegenerateAccessToken objRequestRegenerateAccessToken = new RequestRegenerateAccessToken();
                objRequestRegenerateAccessToken.setDeviceUUID(Settings.Secure.getString(activity.getContentResolver(),
                        Settings.Secure.ANDROID_ID));

                final Call<ResponseRegisterDevice> requestRegenerateAccessToken = RestClient.getClient().callRegenerateAccessToken(objRequestRegenerateAccessToken);
                requestRegenerateAccessToken.enqueue(new Callback<ResponseRegisterDevice>() {
                    @Override
                    public void onResponse(Call<ResponseRegisterDevice> call, Response<ResponseRegisterDevice> response) {

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Prefs.setAuthKey(response.body().getData().getToken());
                                    ((SplashScreen) activity).takeUserIntoTheApp();
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to register device", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseRegisterDevice> call, Throwable t) {
                        Toast.makeText(activity, "Device registration failed. Please try again.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Toast.makeText(activity, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callRequestOTPApi(Context context, RequestOTP objRequestOTP, boolean navigate) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;

                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Sending OTP..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
//                Map<String, String> map = new HashMap<>();
//                map.put("Authorization", Prefs.getAuthKey());
//                map.put("Content-Type", "application/json");
//                final Call<ResponseRequestOTP> requestOTPApiCall = service.callRequestOTP(map, objRequestOTP);

                final Call<ResponseRequestOTP> requestOTPApiCall = service.callRequestOTP(objRequestOTP);
                requestOTPApiCall.enqueue(new Callback<ResponseRequestOTP>() {
                    @Override
                    public void onResponse(Call<ResponseRequestOTP> call, Response<ResponseRequestOTP> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
//                            Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    if (objRequestOTP.getRequestType().equals(Defaults.REQUEST_TYPE_REGISTRATION))
                                        Prefs.setProfileCompleted(false);

                                    if (navigate) {
                                        Intent inte = new Intent(activity, VerifyOTPScreen.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable("objRequestOTP", objRequestOTP);
                                        inte.putExtras(bundle);
                                        activity.startActivity(inte);
                                        activity.finish();
                                    }
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to send otp", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseRequestOTP> call, Throwable t) {
                        Toast.makeText(activity, "OTP sending failed. Please try again.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callVerifyOTPApi(Context context, RequestOTP objRequestOTP, String otp) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                RequestVerifyOTP requestVerifyOTP = new RequestVerifyOTP();
                requestVerifyOTP.setMobile(objRequestOTP.getMobile());
                requestVerifyOTP.setOtp(otp);
                requestVerifyOTP.setRequestType(objRequestOTP.getRequestType());
                requestVerifyOTP.setReferralCode("");

//                if (requestVerifyOTP.getRequestType().equals(Defaults.REQUEST_TYPE_REGISTRATION)) {
//                    requestVerifyOTP.setReferralCode("get and send referral code from here");
//                } else {
//                    requestVerifyOTP.setReferralCode("");
//                }

                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Verifying OTP..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();

                final Call<ResponseVerifyOTP> verifyOTPApiCall = service.callVerifyOTP(requestVerifyOTP);
                verifyOTPApiCall.enqueue(new Callback<ResponseVerifyOTP>() {
                    @Override
                    public void onResponse(Call<ResponseVerifyOTP> call, Response<ResponseVerifyOTP> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (requestVerifyOTP.getRequestType().equalsIgnoreCase(Defaults.REQUEST_TYPE_FORGOT_PASSWORD)) {
                                        Prefs.setPhoneNumberVerified(true);
                                        Prefs.setMobileNumber(requestVerifyOTP.getMobile());

                                        Intent inte = new Intent(activity, ChangePasswordScreen.class);
                                        activity.startActivity(inte);
                                        Toast.makeText(activity, "Mobile Number Verified Successfully. Please Do Change Password.", Toast.LENGTH_LONG).show();
                                        activity.finish();
                                    } else {
                                        Prefs.setPhoneNumberVerified(true);
                                        Prefs.setMobileNumber(requestVerifyOTP.getMobile());

                                        Toast.makeText(activity, "Mobile Number Verified Successfully. Please Sign In.", Toast.LENGTH_LONG).show();
                                        activity.finish();
                                    }
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to verify otp", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseVerifyOTP> call, Throwable t) {
                        Toast.makeText(activity, "OTP verification failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callLoginApi(Context context, RequestLogin objRequestLogin) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Logging in..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();

                final Call<ResponseProfile> callGetProfile = service.callLogin(objRequestLogin);
                callGetProfile.enqueue(new Callback<ResponseProfile>() {
                    @Override
                    public void onResponse(Call<ResponseProfile> call, Response<ResponseProfile> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
//                                  Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    Prefs.setPhoneNumberVerified(true);
                                    Prefs.setMobileNumber(response.body().getProfileData().getUserProfile().getMobile());
                                    Prefs.setUserId(response.body().getProfileData().getUserProfile().getId());
                                    Prefs.setUserName(response.body().getProfileData().getUserProfile().getFirstName() + " " + response.body().getProfileData().getUserProfile().getLastName());
//                                    if (!Prefs.getMobileNumber().equals(Prefs.getLogoutMobileNumber()))
//                                        Prefs.setProfileCompleted(false);

                                    DBHandler.getInstance(context).insertTBPROFILEDATA(response.body().getProfileData().getUserProfile());
                                    ApiCallerUtility.callGetPgMetaDataApi(activity);

                                    Intent inte = null;
//                                    if (Prefs.isProfileCompleted()) {
//                                        if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
//                                            inte = new Intent(activity, OwnerHomeScreenActivity.class);
//                                        else
//                                            inte = new Intent(activity, HomeScreenBottomNavigationActivity.class);
//                                    } else {
//                                        inte = new Intent(activity, EditProfileActivity.class);
//                                        inte.putExtra("navigate", true);
//                                    }

//                                    COMMENTED DUE TO AVOIDING CALLING HOMESCREEN AGAIN.
//                                    if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
//                                        inte = new Intent(activity, OwnerHomeScreenActivity.class);
//                                    else
//                                        inte = new Intent(activity, HomeScreenBottomNavigationActivity.class);
//
//                                    activity.startActivity(inte);
//                                    activity.finish();


                                    if (objRequestLogin.isNavigateToHomeScreen()) {
                                        if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
                                            inte = new Intent(activity, OwnerHomeScreenActivity.class);
                                        else
                                            inte = new Intent(activity, HomeScreenBottomNavigationActivity.class);
                                        inte.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        inte.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        inte.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        inte.putExtra("EXIT", true);
                                    } else if (response.body().getProfileData() == null || response.body().getProfileData().getUserProfile() == null
                                            || response.body().getProfileData().getUserProfile().getFirstName().trim().isEmpty()) {
                                        Prefs.setBooleanValue(Keys.PREF_IS_RESTART_APP, true);
                                        inte = new Intent(activity, EditProfileActivity.class);
                                        inte.putExtra("navigate", false);
                                    } else {
                                        Prefs.setBooleanValue(Keys.PREF_IS_RESTART_APP, true);
                                    }
                                    if (inte != null)
                                        activity.startActivity(inte);
                                    activity.finish();
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to login", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseProfile> call, Throwable t) {
                        Toast.makeText(activity, "Login failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetProfileDataApi(Context context, RequestGetProfile objRequestGetProfile) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting profile information..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();

                final Call<ResponseProfile> callGetProfile = service.callGetProfile(objRequestGetProfile);
                callGetProfile.enqueue(new Callback<ResponseProfile>() {
                    @Override
                    public void onResponse(Call<ResponseProfile> call, Response<ResponseProfile> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
//                              Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    Prefs.setProfileCompleted(true);
                                    Prefs.setUserId(response.body().getProfileData().getUserProfile().getId());
                                    Prefs.setUserName(response.body().getProfileData().getUserProfile().getFirstName() + " " + response.body().getProfileData().getUserProfile().getLastName());

                                    DBHandler.getInstance(context).insertTBPROFILEDATA(response.body().getProfileData().getUserProfile());
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseProfile", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FILL_PROFILE_DATA, context, mBundle);
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to retrieve profile information", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseProfile> call, Throwable t) {
                        Toast.makeText(activity, "Profile retrieval failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callEditProfileApi(Context context, RequestEditProfile objRequestEditProfile, boolean navigate) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Updating Profile..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();

                final Call<ResponseProfile> editProfileCall = service.callEditProfile(objRequestEditProfile);
                editProfileCall.enqueue(new Callback<ResponseProfile>() {
                    @Override
                    public void onResponse(Call<ResponseProfile> call, Response<ResponseProfile> response) {

                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
//                            Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                    Prefs.setProfileCompleted(true);
                                    Prefs.setUserId(response.body().getProfileData().getUserProfile().getId());
                                    Prefs.setUserName(response.body().getProfileData().getUserProfile().getFirstName() + " " + response.body().getProfileData().getUserProfile().getLastName());

                                    DBHandler.getInstance(context).insertTBPROFILEDATA(response.body().getProfileData().getUserProfile());

                                    if (navigate) {
                                        Intent intent;
//                                        Intent intent = new Intent(activity, HomeScreenBottomNavigationActivity.class);
                                        if (Prefs.getStringValue(Keys.PREF_USER_TYPE).equals(Defaults.USER_TYPE_OWNER))
                                            intent = new Intent(activity, OwnerHomeScreenActivity.class);
                                        else
                                            intent = new Intent(activity, HomeScreenBottomNavigationActivity.class);
                                        activity.startActivity(intent);
                                        activity.finish();
                                    } else {
                                        Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                        activity.finish();
                                    }
                                } else {
                                    showErrorMessageFromServer((AppCompatActivity) context, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to update profile", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseProfile> call, Throwable t) {
                        Toast.makeText(activity, "Profile updation failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callRegisterPushTokenApi(Context context, RequestFCMPushToken objRequestFCMPushToken) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient();

                final Call<ResponseFCMPushToken> responseFCMPushTokenCall = service.callRegisterPushToken(objRequestFCMPushToken);
                responseFCMPushTokenCall.enqueue(new Callback<ResponseFCMPushToken>() {
                    @Override
                    public void onResponse(Call<ResponseFCMPushToken> call, Response<ResponseFCMPushToken> response) {
                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
//                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Prefs.setPushTokenRegistered(true);
                                }
                            } else {
//                            Toast.makeText(context, "Unable to get connections", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseFCMPushToken> call, Throwable t) {
//                        Toast.makeText(context, "Failed to get push token.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
//                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callUploadProfileImageApi(Context context, Uri uri) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient();

                String path = StorageHelper.getMediaPath(context, uri);
                File file = new File(path);

                try {
                    Double imageSize = file.length() / 1024.0 / 1024.0;
                    if (imageSize > Defaults.MAXIMUM_IMAGES_SIZE_TO_UPLOAD) {
                        Bundle mBundle = new Bundle();
                        mBundle.putBoolean("sizeExceeded", true);
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, mBundle);
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(context.getContentResolver().getType(uri)),
                                file
                        );

                // MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);

                // add another part within the multipart request
                RequestBody requestBody =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, Prefs.getUserId());

                // finally, execute the request
                Call<ResponseProfile> call = service.callUploadProfileImage(requestBody, body);
                call.enqueue(new Callback<ResponseProfile>() {
                    @Override
                    public void onResponse(Call<ResponseProfile> call, Response<ResponseProfile> response) {
                        Log.v("Upload", "success");
                        if (response.body().isStatus()) {
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable("ResponseProfile", response.body());
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS, context, mBundle);
                        } else {
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseProfile> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
                Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
        }
    }

    public static void callUPloadProfileImageApiAdditional_FOR_REFERENCE(Context context, Uri uri) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient();

                File file = new File(StorageHelper.getMediaPath(context, uri));

                // Parsing any Media type file
                RequestBody requestFile = RequestBody.create(MediaType.parse("*/*"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);
                RequestBody userId = RequestBody.create(MediaType.parse("text/plain"), Prefs.getUserId());

//                RequestBody requestFile =
//                        RequestBody.create(MediaType.parse("multipart/form-data"), file);

//                MultipartBody.Part body =
//                        MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);

//                RequestBody userId =
//                        RequestBody.create(MediaType.parse("multipart/form-data"), Prefs.getUserId());

                Call<ResponseProfile> call = service.callUploadProfileImage(userId, body);
                call.enqueue(new Callback<ResponseProfile>() {
                    @Override
                    public void onResponse(Call<ResponseProfile> call, Response<ResponseProfile> response) {
                        Log.v("Upload", "success");
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable("ResponseProfile", response.body());
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_SUCCESS, context, mBundle);
                    }

                    @Override
                    public void onFailure(Call<ResponseProfile> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROFILE_PIC_UPLOAD_FAILED, context, null);
        }
    }

    public static void callChangePasswordApi(Context context, RequestChangePassword objRequestChangePassword) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Changing Password..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseVerifyOTP> verifyOTPApiCall = service.callChangePassword(objRequestChangePassword);
                verifyOTPApiCall.enqueue(new Callback<ResponseVerifyOTP>() {
                    @Override
                    public void onResponse(Call<ResponseVerifyOTP> call, Response<ResponseVerifyOTP> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Toast.makeText(activity, "Password Changed Successfully. Please Sign In.", Toast.LENGTH_LONG).show();
//                                    Utility.logoutFromDeviceAndCloseAllPreviousActivities(activity, SignInActivity.class);
                                    Utility.logoutFromDeviceAndCloseAllPreviousActivities(activity, SplashScreen.class);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to change password", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseVerifyOTP> call, Throwable t) {
                        Toast.makeText(activity, "Password change failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetAllAmenitiesApi(Context context) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient();

                final Call<ResponseGetAllAmenities> getAllAmenitiesApiCall = service.callGetAllAmenities();
                getAllAmenitiesApiCall.enqueue(new Callback<ResponseGetAllAmenities>() {
                    @Override
                    public void onResponse(Call<ResponseGetAllAmenities> call, Response<ResponseGetAllAmenities> response) {
                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
//                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
//                                    Toast.makeText(activity, "Password Changed Successfully. Please Sign In.", Toast.LENGTH_LONG).show();
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
//                                Toast.makeText(activity, "Unable to change password", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetAllAmenities> call, Throwable t) {
//                        Toast.makeText(activity, "Password change failed.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
//                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetPgMetaDataApi(Context context) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient();

                final Call<ResponseGetAppMetaData> getPgMetaDataApiCall = service.callGetPgMetaData();
                getPgMetaDataApiCall.enqueue(new Callback<ResponseGetAppMetaData>() {
                    @Override
                    public void onResponse(Call<ResponseGetAppMetaData> call, Response<ResponseGetAppMetaData> response) {
                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
//                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    DBHandler.getInstance(context).fillAppMetaData(response.body());
                                } else {
//                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
//                                Toast.makeText(activity, "Unable to change password", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetAppMetaData> call, Throwable t) {
                        Toast.makeText(context, "Password change failed.", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
//                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callAddPGApi(Context context, RequestAddPG objRequestAddPG) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Adding Pg..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseVerifyOTP> addPgApiCall = service.callAddPG(objRequestAddPG);
                addPgApiCall.enqueue(new Callback<ResponseVerifyOTP>() {
                    @Override
                    public void onResponse(Call<ResponseVerifyOTP> call, Response<ResponseVerifyOTP> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Toast.makeText(activity, "PG Added Successfully.", Toast.LENGTH_LONG).show();
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_OWNERS_PG_LISTING, context, null);
                                    activity.finish();
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to add PG", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseVerifyOTP> call, Throwable t) {
                        Toast.makeText(activity, "PG Adding failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callUploadPropertyImageApi(Context context, Uri uri, String uniqueImageName) {
        try {
            if (Connectivity.isConnected(context)) {
                RestClient.GitApiInterface service = RestClient.getClient();

                String path = StorageHelper.getMediaPath(context, uri);
                File file = new File(path);

                try {
                    Double imageSize = file.length() / 1024.0 / 1024.0;
                    if (imageSize > Defaults.MAXIMUM_IMAGES_SIZE_TO_UPLOAD) {
                        Bundle mBundle = new Bundle();
                        mBundle.putBoolean("sizeExceeded", true);
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_FAILED, context, mBundle);
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(context.getContentResolver().getType(uri)),
                                file
                        );

                // MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part multipartBody =
                        MultipartBody.Part.createFormData("image", file.getName(), requestFile);

                // add another part within the multipart request
                RequestBody requestBody =
                        RequestBody.create(
                                okhttp3.MultipartBody.FORM, uniqueImageName);

                // finally, execute the request
                Call<ResponseUploadPropertyImage> call = service.callUploadPropertyImage(requestBody, multipartBody);
                call.enqueue(new Callback<ResponseUploadPropertyImage>() {
                    @Override
                    public void onResponse(Call<ResponseUploadPropertyImage> call, Response<ResponseUploadPropertyImage> response) {
                        Log.v("Upload", "success");
                        if (response.body().isStatus()) {
                            Bundle mBundle = new Bundle();
                            mBundle.putSerializable("ResponseUploadPropertyImage", response.body());
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_SUCCESS, context, mBundle);
                        } else {
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
//                            Toast.makeText(context, "Step-2 STATUS FALSE " + response.body().getMessage(), Toast.LENGTH_LONG).show();
                            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_FAILED, context, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseUploadPropertyImage> call, Throwable t) {
                        Log.e("Upload error:", t.getMessage());
//                        Toast.makeText(context, "Step-3 ON FAILURE CALLED. MESSAGE: " + t.getMessage() + " CAUSE: " + t.getCause(), Toast.LENGTH_LONG).show();
                        Utility.sendUpdateListBroadCast(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_FAILED, context, null);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
//                Toast.makeText(context, "Step-4 NO INTERNET ", Toast.LENGTH_LONG).show();
                Utility.sendUpdateListBroadCast(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_FAILED, context, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
//            Toast.makeText(context, "Step-5 EXCEPTION CAUGHT. MESSAGE: " + e.getMessage() + " CAUSE: " + e.getCause(), Toast.LENGTH_LONG).show();
            Utility.sendUpdateListBroadCast(Defaults.ACTION_PROPERTY_IMAGE_UPLOAD_FAILED, context, null);
        }
    }

//    public static void callGetPgListingSingleKeyValueApi(Context context, MutableLiveData<List<PropertyListItem>> mPropertyList, MutableLiveData<Boolean> mShowProgressBar, String requestTypeValue) {
//        try {
//            if (Connectivity.isConnected(context)) {
//                mShowProgressBar.setValue(true);
//
//                RequestGetPgListingSingleValue objGetPgListingSingleKeyValueApi = new RequestGetPgListingSingleValue();
//                switch (requestTypeValue) {
//                    case Defaults.REQUEST_TYPE_OWNER_ID:
//                        objGetPgListingSingleKeyValueApi.setRequestType(requestTypeValue);
//                        objGetPgListingSingleKeyValueApi.setValue(Prefs.getUserId());
//                        break;
//                    case Defaults.REQUEST_TYPE_SERVICE_CITY_CODE:
//                        objGetPgListingSingleKeyValueApi.setRequestType(requestTypeValue);
//                        objGetPgListingSingleKeyValueApi.setValue(Prefs.getServiceCityCode());
//                        break;
//                    case Defaults.REQUEST_TYPE_IS_RECOMMENDED:
//                        objGetPgListingSingleKeyValueApi.setRequestType(Defaults.REQUEST_TYPE_SERVICE_CITY_CODE);
//                        objGetPgListingSingleKeyValueApi.setValue(Prefs.getServiceCityCode());
//                        objGetPgListingSingleKeyValueApi.setIsRecommended("1");
//                        break;
//                    case Defaults.REQUEST_TYPE_ROOM_ACCOMODATION:
//                    case Defaults.REQUEST_TYPE_FULLY_FURNISHED:
//                        objGetPgListingSingleKeyValueApi.setRequestType(requestTypeValue);
//                        objGetPgListingSingleKeyValueApi.setValue("1");
//                        break;
//                    default:
//                        break;
//                }
//
//                RestClient.GitApiInterface service = RestClient.getClient();
//                final Call<ResponsePgListing> callGetPgListingSingleKeyValue = service.callGetPgListingSingleKeyValueApi(objGetPgListingSingleKeyValueApi);
//                callGetPgListingSingleKeyValue.enqueue(new Callback<ResponsePgListing>() {
//                    @Override
//                    public void onResponse(Call<ResponsePgListing> call, Response<ResponsePgListing> response) {
//                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
//                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
//                        } else {
//                            if (response != null && response.body() != null) {
//                                if (response.body().isStatus()) {
//                                    mPropertyList.setValue(response.body().getData());
//                                } else {
////                                    showErrorMessageFromServer(context, response.errorBody(), response.body().getMessage());
//                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
//                                }
//                            } else {
//                                Toast.makeText(context, "Unable to retrieve property list", Toast.LENGTH_LONG).show();
//                            }
//                        }
//                        mShowProgressBar.setValue(false);
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResponsePgListing> call, Throwable t) {
//                        Toast.makeText(context, "Property list retrieval failed.", Toast.LENGTH_LONG).show();
//                        mShowProgressBar.setValue(false);
//                    }
//                });
//            } else {
//                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            mShowProgressBar.setValue(false);
//        }
//    }

    public static void callGetMyPgListingApi(Context context, MutableLiveData<List<PropertyListItem>> mPropertyList, MutableLiveData<Boolean> mShowProgressBar, String requestTypeValue) {
        try {
            if (Connectivity.isConnected(context)) {
                mShowProgressBar.setValue(true);

                RequestGetMyPgs objReqestRequestGetMyPgs = new RequestGetMyPgs();
                switch (requestTypeValue) {
                    case Defaults.REQUEST_TYPE_OWNER_ID:
                        objReqestRequestGetMyPgs.setOwnerId(Prefs.getUserId());
                        objReqestRequestGetMyPgs.setIsActive(Defaults.IS_ACTIVE_STATUS_ALL);
                        break;
                    default:
                        break;
                }

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponsePgListing> callGetPgListingSingleKeyValue = service.callGetMyPgListing(objReqestRequestGetMyPgs);
                callGetPgListingSingleKeyValue.enqueue(new Callback<ResponsePgListing>() {
                    @Override
                    public void onResponse(Call<ResponsePgListing> call, Response<ResponsePgListing> response) {
                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    mPropertyList.setValue(response.body().getData());
                                } else {
//                                    showErrorMessageFromServer(context, response.errorBody(), response.body().getMessage());
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(context, "Unable to retrieve property list", Toast.LENGTH_LONG).show();
                            }
                        }
                        mShowProgressBar.setValue(false);
                    }

                    @Override
                    public void onFailure(Call<ResponsePgListing> call, Throwable t) {
                        Toast.makeText(context, "Property list retrieval failed.", Toast.LENGTH_LONG).show();
                        mShowProgressBar.setValue(false);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mShowProgressBar.setValue(false);
        }
    }

    public static void callGetPgListingApi(Context context, MutableLiveData<List<PropertyListItem>> mPropertyList, MutableLiveData<Boolean> mShowProgressBar, RequestGetPgListing objRequestGetPgListing) {
        try {
            if (Connectivity.isConnected(context)) {
                mShowProgressBar.setValue(true);

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponsePgListing> callGetPgListingSingleKeyValue = service.callGetPgListingMasterApi(objRequestGetPgListing);
                callGetPgListingSingleKeyValue.enqueue(new Callback<ResponsePgListing>() {
                    @Override
                    public void onResponse(Call<ResponsePgListing> call, Response<ResponsePgListing> response) {
                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    mPropertyList.setValue(response.body().getData());
                                } else {
//                                    showErrorMessageFromServer(context, response.errorBody(), response.body().getMessage());
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(context, "Unable to retrieve property list", Toast.LENGTH_LONG).show();
                            }
                        }
                        mShowProgressBar.setValue(false);
                    }

                    @Override
                    public void onFailure(Call<ResponsePgListing> call, Throwable t) {
                        Toast.makeText(context, "Property list retrieval failed.", Toast.LENGTH_LONG).show();
                        mShowProgressBar.setValue(false);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mShowProgressBar.setValue(false);
        }
    }

    public static void callAddToFavoriteApi(Context context, RequestAddToFavorites objRequestAddToFavorites) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Adding Favorite..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseVerifyOTP> addPgApiCall = service.callAddToFavorite(objRequestAddToFavorites);
                addPgApiCall.enqueue(new Callback<ResponseVerifyOTP>() {
                    @Override
                    public void onResponse(Call<ResponseVerifyOTP> call, Response<ResponseVerifyOTP> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (objRequestAddToFavorites.getFavoriteStatus().equals(Defaults.FAVORITE_STATUS_SELECTED))
                                        Toast.makeText(activity, "Favorite Added Successfully.", Toast.LENGTH_LONG).show();
                                    else
                                        Toast.makeText(activity, "Favorite Removed Successfully.", Toast.LENGTH_LONG).show();
                                    Bundle mBundle = new Bundle();
                                    response.body().setMessage(objRequestAddToFavorites.getFavoriteStatus());
                                    mBundle.putSerializable("ResponseVerifyOTP", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_FAVORITE_ADDED, context, mBundle);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to add favorite", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseVerifyOTP> call, Throwable t) {
                        Toast.makeText(activity, "Favorite Adding failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callBookPGApi(Context context, RequestBookPg objRequestBookPg) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Processing Payment..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseBookPg> bookPgApiCall = service.callBookPG(objRequestBookPg);
                bookPgApiCall.enqueue(new Callback<ResponseBookPg>() {
                    @Override
                    public void onResponse(Call<ResponseBookPg> call, Response<ResponseBookPg> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseBookPg", response.body());
                                    if (objRequestBookPg.getPaymentMode().equals(Defaults.PAYMENT_MODE_ONLINE)) {
                                        Utility.sendUpdateListBroadCast(Defaults.ACTION_HASH_RECEIVED, context, mBundle);
                                    } else if (objRequestBookPg.getPaymentMode().equals(Defaults.PAYMENT_MODE_OFFLINE)) {
                                        Utility.sendUpdateListBroadCast(Defaults.ACTION_OFFLINE_TRANSACTION_COMPLETE_FROM_SERVER_END, context, mBundle);
                                    }
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to Book PG", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBookPg> call, Throwable t) {
                        Toast.makeText(activity, "PG Booking failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callVerifyChecksumAndFinalizeBookingApi(Context context, RequestVerifyChecksum objRequestVerifyChecksum) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Finalizing Transaction..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseVerifyChecksum> verifyChecksumCall = service.callVerifyChecksumAndFinalizeBooking(objRequestVerifyChecksum);
                verifyChecksumCall.enqueue(new Callback<ResponseVerifyChecksum>() {
                    @Override
                    public void onResponse(Call<ResponseVerifyChecksum> call, Response<ResponseVerifyChecksum> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseVerifyChecksum", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_ONLINE_TRANSACTION_COMPLETE_FROM_SERVER_END, context, mBundle);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to finalize transaction", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseVerifyChecksum> call, Throwable t) {
                        Toast.makeText(activity, "Transaction finalization failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callChangeAvailableStatusApi(Context context, RequestChangeAvailabilityStatus objRequestChangeAvailabilityStatus) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Changing Status..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseChangeAvailabilityStatus> changeAvailabilityStatusApiCall = service.callChangeAvailabilityStatus(objRequestChangeAvailabilityStatus);

                changeAvailabilityStatusApiCall.enqueue(new Callback<ResponseChangeAvailabilityStatus>() {
                    @Override
                    public void onResponse(Call<ResponseChangeAvailabilityStatus> call, Response<ResponseChangeAvailabilityStatus> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Toast.makeText(activity, "Status Changed Added Successfully.", Toast.LENGTH_LONG).show();
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseChangeAvailabilityStatus", response.body());
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_AVAILABILITY_STATUS_CHANGED, context, mBundle);
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_OWNERS_PG_LISTING, context, null);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to change status", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseChangeAvailabilityStatus> call, Throwable t) {
                        Toast.makeText(activity, "Status Changing failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * NOT USED. DEPRECATED.
     *
     * @param context
     * @param objRequestMyBookings
     */
    public static void callGetMyBookingsApi(Context context, RequestMyBookings objRequestMyBookings) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting Your Bookings..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseMyBookings> getMyBookingsApiCall = service.callGetMyBookings(objRequestMyBookings);
                getMyBookingsApiCall.enqueue(new Callback<ResponseMyBookings>() {
                    @Override
                    public void onResponse(Call<ResponseMyBookings> call, Response<ResponseMyBookings> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    ResponseMyBookings objResponseMyBookings = response.body();
                                    try {
                                        for (int i = 0; i < objResponseMyBookings.getData().size(); i++) {
                                            if (objResponseMyBookings.getData().get(i).getPayuJson() != null && !objResponseMyBookings.getData().get(i).getPayuJson().isEmpty()) {
                                                Gson gson = new Gson();
                                                objResponseMyBookings.getData().get(i).setPayuTransactionResponse(gson.fromJson(objResponseMyBookings.getData().get(i).getPayuJson(), PayuTransactionResponse.class));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Bundle mBundle = new Bundle();
                                    mBundle.putSerializable("ResponseMyBookings", objResponseMyBookings);
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_BOOKING_LISTING, context, mBundle);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to get booking list", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseMyBookings> call, Throwable t) {
                        Toast.makeText(activity, "Failed to get booking list", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetMyBookingsApi(Context context, MutableLiveData<List<DataItem>> mBookingDataItems, MutableLiveData<Boolean> mShowProgressBar, RequestMyBookings objRequestMyBookings) {
        try {
            if (Connectivity.isConnected(context)) {
                mShowProgressBar.setValue(true);

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseMyBookings> getMyBookingsApiCall = service.callGetMyBookings(objRequestMyBookings);
                getMyBookingsApiCall.enqueue(new Callback<ResponseMyBookings>() {
                    @Override
                    public void onResponse(Call<ResponseMyBookings> call, Response<ResponseMyBookings> response) {
                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {

                                    ResponseMyBookings objResponseMyBookings = response.body();
                                    try {
                                        for (int i = 0; i < objResponseMyBookings.getData().size(); i++) {
                                            if (objResponseMyBookings.getData().get(i).getPayuJson() != null && !objResponseMyBookings.getData().get(i).getPayuJson().isEmpty()) {
                                                Gson gson = new Gson();
                                                objResponseMyBookings.getData().get(i).setPayuTransactionResponse(gson.fromJson(objResponseMyBookings.getData().get(i).getPayuJson(), PayuTransactionResponse.class));
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mBookingDataItems.setValue(objResponseMyBookings.getData());
                                } else {
//                                    showErrorMessageFromServer(context, response.errorBody(), response.body().getMessage());
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(context, "Unable to retrieve booking list", Toast.LENGTH_LONG).show();
                            }
                        }
                        mShowProgressBar.setValue(false);
                    }

                    @Override
                    public void onFailure(Call<ResponseMyBookings> call, Throwable t) {
                        Toast.makeText(context, "Booking list retrieval failed.", Toast.LENGTH_LONG).show();
                        mShowProgressBar.setValue(false);
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mShowProgressBar.setValue(false);
        }
    }

    public static void callGetAllNotificationsApi(Context context, MutableLiveData<List<UserNotificationsItem>> mNotifications, MutableLiveData<Boolean> mShowProgressBar, RequestGetAllNotification objRequestGetAllNotification) {
        try {
            if (Connectivity.isConnected(context)) {
                mShowProgressBar.setValue(true);

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseGetAllNotifications> getAllNotificationsCall = service.callGetAllNotifications(objRequestGetAllNotification);
                getAllNotificationsCall.enqueue(new Callback<ResponseGetAllNotifications>() {
                    @Override
                    public void onResponse(Call<ResponseGetAllNotifications> call, Response<ResponseGetAllNotifications> response) {

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (response.body().getData() != null && response.body().getData() != null)
                                        mNotifications.setValue(response.body().getData());
                                } else {
//                                    showErrorMessageFromServer(context, response.errorBody(), response.body().getMessage());
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(context, "Unable to retrieve notification list", Toast.LENGTH_LONG).show();
                            }
                        }
                        mShowProgressBar.setValue(false);
                    }

                    @Override
                    public void onFailure(Call<ResponseGetAllNotifications> call, Throwable t) {
                        Toast.makeText(context, "notification list retrieval failed.", Toast.LENGTH_LONG).show();
                        mShowProgressBar.setValue(false);
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mShowProgressBar.setValue(false);
        }
    }

    public static void callDeletePGApi(Context context, RequestDeletePG objRequestDeletePG) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Deleting Pg..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseDeletePG> deletePgApiCall = service.callDeletePg(objRequestDeletePG);
                deletePgApiCall.enqueue(new Callback<ResponseDeletePG>() {
                    @Override
                    public void onResponse(Call<ResponseDeletePG> call, Response<ResponseDeletePG> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    Toast.makeText(activity, "PG Deleted Successfully.", Toast.LENGTH_LONG).show();
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_OWNERS_PG_LISTING, context, null);
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to delete PG", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDeletePG> call, Throwable t) {
                        Toast.makeText(activity, "PG deletion failed.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetAllCitiesApi(Context context) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Getting cities..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseGetCityMaster> getAllCitiesApiCall = service.callGetAllCities();
                getAllCitiesApiCall.enqueue(new Callback<ResponseGetCityMaster>() {
                    @Override
                    public void onResponse(Call<ResponseGetCityMaster> call, Response<ResponseGetCityMaster> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
//                                    Toast.makeText(activity, "PG Deleted Successfully.", Toast.LENGTH_LONG).show();
                                    Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_OWNERS_PG_LISTING, context, null);
                                    activity.finish();
                                } else {
                                    showErrorMessageFromServer(activity, response.errorBody(), response.body().getMessage());
                                }
                            } else {
                                Toast.makeText(activity, "Unable to get cities", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseGetCityMaster> call, Throwable t) {
                        Toast.makeText(activity, "Failed to get cities", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callApplyCouponApi(Context context, RequestVerifyCoupon objRequestVerifyCoupon) {
        try {
            if (Connectivity.isConnected(context)) {
                AppCompatActivity activity = (AppCompatActivity) context;
                AlertDialog progressDialog = Utility.getProgressDialog(activity, "", "Applying Coupon Code..");
                progressDialog.show();

                RestClient.GitApiInterface service = RestClient.getClient();

                final Call<ResponseVerifyCoupon> applyCouponCall = service.callVerifyCoupon(objRequestVerifyCoupon);
                applyCouponCall.enqueue(new Callback<ResponseVerifyCoupon>() {
                    @Override
                    public void onResponse(Call<ResponseVerifyCoupon> call, Response<ResponseVerifyCoupon> response) {
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(activity, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                Bundle mBundle = new Bundle();
                                mBundle.putSerializable("ResponseVerifyCoupon", response.body());
                                Utility.sendUpdateListBroadCast(Defaults.ACTION_COUPON_CODE_APPLIED, context, mBundle);
                            } else {
                                Toast.makeText(activity, "Unable Apply Coupon Code", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseVerifyCoupon> call, Throwable t) {
                        Toast.makeText(activity, "Failed to perform this action.", Toast.LENGTH_LONG).show();
                        if (progressDialog != null && progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void callGetAllCouponsApi(Context context, MutableLiveData<List<CoupansItem>> listCouponsMutableLiveData, MutableLiveData<Boolean> mShowProgressBar, RequestMyBookings objRequestMyBookings) {
        try {
            if (Connectivity.isConnected(context)) {
                mShowProgressBar.setValue(true);

                RestClient.GitApiInterface service = RestClient.getClient();
                final Call<ResponseGetCoupon> getAllCouponsCall = service.callGetCoupons(objRequestMyBookings);
                getAllCouponsCall.enqueue(new Callback<ResponseGetCoupon>() {
                    @Override
                    public void onResponse(Call<ResponseGetCoupon> call, Response<ResponseGetCoupon> response) {
                        if (response != null && response.code() != Defaults.RESPONSE_CODE_OK) {
                            Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show();
                        } else {
                            if (response != null && response.body() != null) {
                                if (response.body().isStatus()) {
                                    if (response.body().getData() != null && response.body().getData() != null) {
                                        int arr[] = {R.drawable.oval_blue, R.drawable.oval_green, R.drawable.oval_orange, R.drawable.oval_red, R.drawable.oval_yellow};
                                        for (int i = 0; i < response.body().getData().getCoupans().size(); i++) {
                                            try {
                                                response.body().getData().getCoupans().get(i).setDrawableResource(arr[(new Random().nextInt(arr.length))]);
                                            } catch (Exception e) {
                                                response.body().getData().getCoupans().get(i).setDrawableResource(R.drawable.oval_orange);
                                            }
                                        }
                                        listCouponsMutableLiveData.setValue(response.body().getData().getCoupans());
                                    }
                                } else {
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(context, "Unable to retrieve coupon list", Toast.LENGTH_LONG).show();
                            }
                        }
                        mShowProgressBar.setValue(false);
                    }

                    @Override
                    public void onFailure(Call<ResponseGetCoupon> call, Throwable t) {
                        Toast.makeText(context, "Coupon list retrieval failed.", Toast.LENGTH_LONG).show();
                        mShowProgressBar.setValue(false);
                    }
                });

            } else {
                Toast.makeText(context, "No Internet.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            mShowProgressBar.setValue(false);
        }
    }

    public static void showErrorMessageFromServer(AppCompatActivity activity, ResponseBody errorBody, String messageFromServer) {
        try {
            AlertDialog textDialog = null;
            if (errorBody != null) {
                Gson gson = new Gson();
                ErrorResponse objErrorResponse = gson.fromJson(errorBody.string(), ErrorResponse.class);
                Errors objErrors = objErrorResponse.getErrors();

                if (objErrors != null && objErrors.getFirstname() != null && objErrors.getFirstname().size() > 0) {
                    String errMsg = "";
                    for (int i = 0; i < objErrors.getFirstname().size(); i++) {
                        errMsg += "\n" + objErrors.getFirstname().get(i) + "\n";
                    }

                    textDialog = Utility.getTextDialog_Material(activity, activity.getResources().getString(R.string.dialog_title_information), errMsg);
                    textDialog.setButton(DialogInterface.BUTTON_POSITIVE, activity.getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    textDialog.show();
                }
            } else {
                if (messageFromServer != null && !messageFromServer.trim().equals("")) {
                    textDialog = Utility.getTextDialog_Material(activity, activity.getResources().getString(R.string.dialog_title_information), messageFromServer);
                    textDialog.setButton(DialogInterface.BUTTON_POSITIVE, activity.getString(R.string.ok_action).toUpperCase(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    textDialog.show();
                }
            }

            if (textDialog != null) {
                if (textDialog.getButton(DialogInterface.BUTTON_POSITIVE) != null) {
                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(activity.getResources().getColor(R.color.app_text_color));
                    textDialog.getButton(DialogInterface.BUTTON_POSITIVE).setBackground(activity.getResources().getDrawable(R.drawable.ripple));
                }
                if (textDialog.getButton(DialogInterface.BUTTON_NEGATIVE) != null) {
                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(activity.getResources().getColor(R.color.app_text_color));
                    textDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setBackground(activity.getResources().getDrawable(R.drawable.ripple));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
