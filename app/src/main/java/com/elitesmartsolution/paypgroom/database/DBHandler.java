package com.elitesmartsolution.paypgroom.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.elitesmartsolution.paypgroom.R;
import com.elitesmartsolution.paypgroom.models.SpinnerItemBO;
import com.elitesmartsolution.paypgroom.models.pgmetadata.AmenitiesItem;
import com.elitesmartsolution.paypgroom.models.pgmetadata.CityMasterItem;
import com.elitesmartsolution.paypgroom.models.pgmetadata.PropertyTypesItem;
import com.elitesmartsolution.paypgroom.models.pgmetadata.ResponseGetAppMetaData;
import com.elitesmartsolution.paypgroom.models.pgmetadata.RoomTypesItem;
import com.elitesmartsolution.paypgroom.models.profile.UserProfile;
import com.elitesmartsolution.paypgroom.util.Utility;
import com.elitesmartsolution.paypgroom.util.preferences.Defaults;
import com.elitesmartsolution.paypgroom.util.preferences.Keys;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class DBHandler extends SQLiteOpenHelper {

    public static final String TBPROFILEDATA = "TBPROFILEDATA";
    public static final String property_types = "property_types";
    public static final String room_types = "room_types";
    public static final String amenities = "amenities";
    public static final String cityMaster = "cityMaster";
    public static final String luxury_type = "luxury_type";
    private static final String DATABASE_NAME = "PayPgRoomDB";
    private static final int DATABASE_VERSION = 1;
    private static DBHandler DBHnadlerInstance = null;
    private Context context;

    private DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        this.getWritableDatabase();
    }

    public static DBHandler getInstance(Context ctx) {
        if (DBHnadlerInstance == null) {
            DBHnadlerInstance = new DBHandler(ctx);
        }
        return DBHnadlerInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if (DATABASE_VERSION == 1) {
//            db.execSQL("CREATE TABLE IF NOT EXISTS TBIMAGEUPLOAD (FILENAME text, FILEPATH text, UPLOADSTATUS text)");
//            ExecuteQuery("CREATE TABLE IF NOT EXISTS TBIMAGEUPLOAD (FILENAME text, FILEPATH text, UPLOADSTATUS text)");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + TBPROFILEDATA + " (USER_ID TEXT, FIRST_NAME TEXT, LAST_NAME TEXT, GENDER TEXT, COUNTRY_CODE TEXT, DOB TEXT, EMAIL TEXT, MOBILE TEXT, EMAIL_VERIFIED_AT TEXT, USER_TYPE TEXT, PROFILE TEXT, PROFIT TEXT, IS_ACTIVE TEXT, ADDRESS_ID TEXT, CREATED_AT TEXT, UPDATED_AT TEXT, DELETED_AT TEXT)");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + property_types + " (id TEXT, name TEXT, is_active TEXT, created_at TEXT, updated_at TEXT, deleted_at TEXT)");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + room_types + " (id TEXT, name TEXT, is_active TEXT, created_at TEXT, updated_at TEXT, deleted_at TEXT)");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + cityMaster + " (id TEXT, service_city_name TEXT, service_city_code TEXT, is_active TEXT, created_at TEXT, updated_at TEXT, deleted_at TEXT)");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + amenities + " (id TEXT, name TEXT, icon TEXT, description TEXT, is_active TEXT, created_at TEXT, updated_at TEXT, deleted_at TEXT)");
            db.execSQL("CREATE TABLE IF NOT EXISTS " + luxury_type + " (id TEXT, name TEXT, is_active TEXT, created_at TEXT, updated_at TEXT, deleted_at TEXT)");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            if (oldVersion < newVersion) {
                if (newVersion == 2) {
//                    alterTableAddColumn(db, "TXNSAMPLE", "AMT");
                } else {
//                    db.execSQL("ALTER TABLE TXN102 ADD COLUMN COL22 TEXT");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void alterTableAddColumn(SQLiteDatabase db, String tableName, String columnName) {
        if (db == null) {
            ExecuteQuery("ALTER TABLE " + tableName + " ADD COLUMN " + columnName + " TEXT DEFAULT ''");
        } else {
            try {
                db.execSQL("ALTER TABLE " + tableName + " ADD COLUMN " + columnName + " TEXT DEFAULT ''");
            } catch (SQLiteException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
            }
        }
    }

    public boolean createDataBase() throws IOException {
//        boolean dbExist = checkDataBase();
        boolean dbExist = ifValuePresentInDB("select chapter_number from CHAPTER_HINDI where chapter_number='1'");

        if (dbExist) {
            //do nothing - database already exist
        } else {
            // By calling this method and empty database will be created into the
            // default system path
            // of your application so we are gonna be able to overwrite that
            // database with our database.
//            this.getReadableDatabase();
            this.getWritableDatabase();
            new DatabaseCopyBackground(context).execute();
        }
        return dbExist;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.P) {
            db.disableWriteAheadLogging();
        }
    }

    /**
     * Check if the database already exist to avoid re-copying the file each
     * time you open the application.
     *
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {

        SQLiteDatabase checkDB = null;

        try {
            String myPath = context.getDatabasePath(DATABASE_NAME) + "";

            checkDB = SQLiteDatabase.openDatabase(myPath, null,
                    SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
            // database does't exist yet.
        }

        if (checkDB != null) {
            return ifValuePresentInDB("select chapter_number from CHAPTER_HINDI where chapter_number='1'");
        }

        return checkDB != null;
    }

    private void copyDataBase(Context context) throws IOException {
        try {
            InputStream fis = context.getAssets().open(DATABASE_NAME);

            String decryptedPath = context.getDatabasePath(DATABASE_NAME) + "";

            FileOutputStream fos = new FileOutputStream(decryptedPath);
            SecretKeySpec sks = new SecretKeySpec(
                    "YOUR_KEY".getBytes(), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec ivParameterSpec = new IvParameterSpec("YOUR_KEY".getBytes());
            cipher.init(Cipher.DECRYPT_MODE, sks, ivParameterSpec);

            CipherInputStream cis = new CipherInputStream(fis, cipher);
            int b;
            byte[] d = new byte[8];
            while ((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fos.flush();
            fos.close();
            cis.close();
        } catch (Exception e) {
            Log.e("Exception Caught:", e.getMessage());
        }
    }

    public boolean ExecuteQuery(String Query) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL(Query);
            return true;
        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return false;
    }

    public String[][] genericSelect(String query, int noCols) {
        return genericSelect(query, noCols, false);
    }


    public String[][] genericSelect(String query, int noCols, boolean closeCursor) {
        String[][] strData = null;
        Cursor cur = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cur = db.rawQuery(query, new String[]{});

            if (cur != null) {
                int noOfRows = cur.getCount();
                int count = 0;
                if (noOfRows > 0) {
                    strData = new String[noOfRows][noCols];
                    while (cur.moveToNext()) {
                        for (int i = 0; i < noCols; i++) {
                            strData[count][i] = cur.getString(i);
                        }
                        count++;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (cur != null && !cur.isClosed())
                cur.close();
        }
        return strData;
    }

    public String[][] genericSelect(String select, String tableName,
                                    String where, String groupBy, String having, int noCols) {
        String[][] strData = null;
        String query = "SELECT " + select + " FROM " + tableName;
        if (!where.equals("")) {
            query = query + " WHERE " + where;
        }
        if (!groupBy.equals("")) {
            query = query + " GROUP BY " + groupBy;
        }
        if (!having.equals("")) {
            query = query + " HAVING " + having;
        }
        Cursor cur = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cur = db.rawQuery(query, new String[]{});
            if (cur != null) {
                int noOfRows = cur.getCount();
                int count = 0;
                if (noOfRows > 0) {
                    strData = new String[noOfRows][noCols];
                    while (cur.moveToNext()) {
                        for (int i = 0; i < noCols; i++) {
                            strData[count][i] = cur.getString(i);
                        }
                        count++;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cur != null && !cur.isClosed())
                cur.close();
        }

        return strData;
    }

    public int getTotalRowCount(String tableName, String whereQuery) {
        String query = "select count(*) from " + tableName + " " + whereQuery;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = null;
        int count = 0;

        try {
            cur = db.rawQuery(query, null);

            if (cur != null) {
                cur.moveToFirst();
                count = cur.getInt(0);
            }
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (cur != null && !cur.isClosed())
                cur.close();
        }
        return count;
    }

    public boolean genricDelete(String Tabel_name) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            return db.delete(Tabel_name, null, null) > 0;
        } catch (SQLiteException ex) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int GenricUpdates(String TabelName, String UpdateCol, String dataString, String KEY_COL, String WhereString) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UpdateCol, dataString);
        return db.update(TabelName, values, KEY_COL + " = ?", new String[]{WhereString});
    }

    public Cursor getCusrsor(String SQl) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            return db.rawQuery(SQl, new String[]{});
        } catch (SQLiteException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deletealltable() {
        Cursor cur = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            cur = db.rawQuery(
                    "SELECT name FROM sqlite_master WHERE type='table'", null);

            if (cur.moveToFirst()) {
                while (!cur.isAfterLast()) {
                    String tablename = cur
                            .getString(cur.getColumnIndexOrThrow("name"));
                    if (!tablename.equals("sqlite_sequence"))
                        db.execSQL("DROP TABLE IF EXISTS " + tablename);

                    cur.moveToNext();
                }
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        } finally {
            if (cur != null && !cur.isClosed())
                cur.close();
        }
    }

    public boolean ifValuePresentInDB(String query) {
        Cursor cursor = null;
        try {
            cursor = getCusrsor(query);
            if (cursor != null) {
                cursor.moveToFirst();

                if (cursor.getCount() > 0) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
        }
        return false;
    }

    public void deleteWhereValue(String tableName, String columnName, String whereValue) {
        String query = "DELETE FROM " + tableName + " where " + columnName + "='" + whereValue + "' ";
        ExecuteQuery(query);
    }

    public ArrayList<String> getDBColumns(String tableName) {
        ArrayList<String> columnList = new ArrayList<>();
        Cursor cursor = null;
        try {
            SQLiteDatabase sqldb = this.getWritableDatabase();
            cursor = sqldb.query(tableName, null, null, null, null, null, null);
            if (cursor != null) {
                String[] columnNames = cursor.getColumnNames();
                Collections.addAll(columnList, columnNames);
            }
        } catch (SQLException e) {
        } catch (Throwable t) {
        } finally {
            if (cursor != null && !cursor.isClosed())
                cursor.close();
        }
        return columnList;
    }

//    public void insertTBIMAGEUPLOAD(TBIMAGEUPLOAD objTbimageupload) {
//        SQLiteDatabase sqldb = null;
//        try {
//            sqldb = this.getWritableDatabase();
//            ContentValues cv = new ContentValues();
//
//            cv.put("FILENAME", objTbimageupload.getFILENAME());
//            cv.put("FILEPATH", objTbimageupload.getFILEPATH());
//            cv.put("UPLOADSTATUS", objTbimageupload.getUPLOADSTATUS());
//
//            sqldb.insert("TBIMAGEUPLOAD", null, cv);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//        }
//    }
//
//    public ArrayList<TBIMAGEUPLOAD> getPendingDownloads() {
//
//        ArrayList<TBIMAGEUPLOAD> tbimageuploadArrayList = new ArrayList<>();
//        Cursor c = null;
//        try {
//            String query = "SELECT * FROM TBIMAGEUPLOAD WHERE UPLOADSTATUS='" + Constants.UPLOAD_PENDING + "' LIMIT 5";
//            SQLiteDatabase db = this.getWritableDatabase();
//            c = db.rawQuery(query, new String[]{});
//
//            if (c != null)
//                while (c.moveToNext()) {
//                    TBIMAGEUPLOAD objTbimageupload = new TBIMAGEUPLOAD();
//
//                    objTbimageupload.setFILENAME(c.getString(c.getColumnIndexOrThrow("FILENAME")));
//                    objTbimageupload.setFILEPATH(c.getString(c.getColumnIndexOrThrow("FILEPATH")));
//                    objTbimageupload.setUPLOADSTATUS(c.getString(c.getColumnIndexOrThrow("UPLOADSTATUS")));
//
//                    tbimageuploadArrayList.add(objTbimageupload);
//                }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null && !c.isClosed())
//                c.close();
//        }
//        return tbimageuploadArrayList;
//    }
//
//    public TBIMAGEUPLOAD getDetailsForFileName(String fileName) {
//        TBIMAGEUPLOAD objTbimageupload = null;
//        Cursor c = null;
//        try {
//            String query = "SELECT * FROM TBIMAGEUPLOAD WHERE FILENAME='" + fileName + "'";
//            SQLiteDatabase db = this.getWritableDatabase();
//            c = db.rawQuery(query, new String[]{});
//
//            if (c != null)
//                while (c.moveToNext()) {
//                    objTbimageupload = new TBIMAGEUPLOAD();
//
//                    objTbimageupload.setFILENAME(c.getString(c.getColumnIndexOrThrow("FILENAME")));
//                    objTbimageupload.setFILEPATH(c.getString(c.getColumnIndexOrThrow("FILEPATH")));
//                    objTbimageupload.setUPLOADSTATUS(c.getString(c.getColumnIndexOrThrow("UPLOADSTATUS")));
//                }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null && !c.isClosed())
//                c.close();
//        }
//        return objTbimageupload;
//    }

    //      Insert using Cupboard
//    public void insertSamples(ArrayList<TXNSAMPLE> txnsample) {
//        try {
//            SQLiteDatabase sqldb = this.getWritableDatabase();
//            cupboard().withDatabase(sqldb).put(txnsample);
//        } catch (SQLException e) {
//        } catch (Throwable t) {
//        }
//    }


//    Insert using content values
//    public void insertTBPLAN(TBPLAN tbPlan) {
//        SQLiteDatabase sqldb = null;
//        try {
//            sqldb = this.getWritableDatabase();
//
//            ContentValues cv = new ContentValues();
//
//            cv.put("BU", tbPlan.getBU());
//            cv.put("PCODE", tbPlan.getPCODE());
//
//            sqldb.insert("TBPLAN", null, cv);
//            sqldb.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (sqldb != null)
//                sqldb.close();
//        }
//    }

//    public ArrayList<Chapter> getAllChapters(Context context, ArrayList<Chapter> mChapterData) {
//        Cursor c = null;
//        try {
//            String query = "SELECT * FROM CHAPTER" + Prefs.getUserLanguageFromSharedPreferences() + " ORDER BY CAST(chapter_number AS INTEGER)";
//            SQLiteDatabase db = this.getWritableDatabase();
//            c = db.rawQuery(query, new String[]{});
//            TypedArray sportsImageResources =
//                    context.getResources().obtainTypedArray(R.array.sports_images);
//            int i = 0;
//            if (c != null)
//                while (c.moveToNext()) {
//                    Chapter objChapter = new Chapter();
//
//                    objChapter.setChapterNumber(c.getInt(c.getColumnIndexOrThrow("chapter_number")));
//                    objChapter.setChapterSummary(c.getString(c.getColumnIndexOrThrow("chapter_summary")));
//                    objChapter.setName(c.getString(c.getColumnIndexOrThrow("name")));
//                    objChapter.setNameMeaning(c.getString(c.getColumnIndexOrThrow("name_meaning")));
//                    objChapter.setNameTranslation(c.getString(c.getColumnIndexOrThrow("name_translation")));
////                    objChapter.setNameTransliterated(c.getString(c.getColumnIndexOrThrow("name_transliterated")));
//                    objChapter.setVersesCount(c.getInt(c.getColumnIndexOrThrow("verses_count")));
//                    objChapter.setImageResource(sportsImageResources.getResourceId(i++, 0));
//
//                    mChapterData.add(objChapter);
//                }
//            sportsImageResources.recycle();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null && !c.isClosed())
//                c.close();
//        }
//        return mChapterData;
//    }
//
//    public ArrayList<Verse> getAllVersesForThisChapter(Context context, ArrayList<Verse> verseArrayList, int chapterNumber) {
//        Cursor c = null;
//        try {
//            String query = "SELECT * FROM VERSES" + Prefs.getUserLanguageFromSharedPreferences() + " WHERE CHAPTER_NUMBER = '" + chapterNumber + "' ORDER BY CAST(verse_number AS INTEGER)";
//            SQLiteDatabase db = this.getWritableDatabase();
//            c = db.rawQuery(query, new String[]{});
//            if (c != null)
//                while (c.moveToNext()) {
//                    Verse objVerse = new Verse();
//
//                    objVerse.setChapterNumber(c.getInt(c.getColumnIndexOrThrow("chapter_number")));
//                    objVerse.setVerseNumber(c.getString(c.getColumnIndexOrThrow("verse_number")));
//                    objVerse.setMeaning(c.getString(c.getColumnIndexOrThrow("meaning")));
//                    objVerse.setText(c.getString(c.getColumnIndexOrThrow("text")));
//                    objVerse.setTransliteration(c.getString(c.getColumnIndexOrThrow("transliteration")));
//                    objVerse.setWordMeanings(c.getString(c.getColumnIndexOrThrow("word_meanings")));
//
//                    verseArrayList.add(objVerse);
//                }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (c != null && !c.isClosed())
//                c.close();
//        }
//        return verseArrayList;
//    }

    public void insertTBPROFILEDATA(UserProfile objUserProfile) {
        SQLiteDatabase sqldb = null;
        try {
            genricDelete("TBPROFILEDATA");

            sqldb = this.getWritableDatabase();
            ContentValues cv = new ContentValues();

            cv.put("USER_ID", objUserProfile.getId());
            cv.put("FIRST_NAME", objUserProfile.getFirstName());
            cv.put("LAST_NAME", objUserProfile.getLastName());
            cv.put("EMAIL_VERIFIED_AT", objUserProfile.getEmailVerifiedAt());
            cv.put("GENDER", objUserProfile.getGender());
            cv.put("COUNTRY_CODE", objUserProfile.getCountryCode());
            cv.put("DOB", objUserProfile.getDob());
            cv.put("EMAIL", objUserProfile.getEmail());
            cv.put("MOBILE", objUserProfile.getMobile());
            cv.put("USER_TYPE", objUserProfile.getUserType());
            Prefs.setStringValue(Keys.PREF_USER_TYPE, objUserProfile.getUserType());
            cv.put("PROFILE", objUserProfile.getProfile());
            cv.put("PROFIT", objUserProfile.getProfit());
            cv.put("IS_ACTIVE", objUserProfile.getIsActive());
            cv.put("ADDRESS_ID", objUserProfile.getAddressId());
            cv.put("CREATED_AT", objUserProfile.getCreatedAt());
            cv.put("UPDATED_AT", objUserProfile.getUpdatedAt());
            cv.put("DELETED_AT", objUserProfile.getDeletedAt());

            sqldb.insert(TBPROFILEDATA, null, cv);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public UserProfile getProfileInfoFromDB() {
        Cursor c = null;
        UserProfile objUserProfile = new UserProfile();

        try {
            String query = "SELECT * FROM " + TBPROFILEDATA + " WHERE MOBILE='" + Prefs.getMobileNumber() + "'";
            SQLiteDatabase db = this.getWritableDatabase();
            c = db.rawQuery(query, new String[]{});

            if (c != null)
                while (c.moveToNext()) {
                    objUserProfile.setId(c.getString(c.getColumnIndexOrThrow("USER_ID")));
                    objUserProfile.setFirstName(c.getString(c.getColumnIndexOrThrow("FIRST_NAME")));
                    objUserProfile.setLastName(c.getString(c.getColumnIndexOrThrow("LAST_NAME")));
                    objUserProfile.setEmailVerifiedAt(c.getString(c.getColumnIndexOrThrow("EMAIL_VERIFIED_AT")));
                    objUserProfile.setGender(c.getString(c.getColumnIndexOrThrow("GENDER")));
                    objUserProfile.setCountryCode(c.getString(c.getColumnIndexOrThrow("COUNTRY_CODE")));
                    objUserProfile.setDob(c.getString(c.getColumnIndexOrThrow("DOB")));
                    objUserProfile.setEmail(c.getString(c.getColumnIndexOrThrow("EMAIL")));
                    objUserProfile.setMobile(c.getString(c.getColumnIndexOrThrow("MOBILE")));
                    objUserProfile.setUserType(c.getString(c.getColumnIndexOrThrow("USER_TYPE")));
                    objUserProfile.setProfile(c.getString(c.getColumnIndexOrThrow("PROFILE")));
                    objUserProfile.setProfit(c.getString(c.getColumnIndexOrThrow("PROFIT")));
                    objUserProfile.setIsActive(c.getString(c.getColumnIndexOrThrow("IS_ACTIVE")));
                    objUserProfile.setAddressId(c.getString(c.getColumnIndexOrThrow("ADDRESS_ID")));
                    objUserProfile.setCreatedAt(c.getString(c.getColumnIndexOrThrow("CREATED_AT")));
                    objUserProfile.setUpdatedAt(c.getString(c.getColumnIndexOrThrow("UPDATED_AT")));
                    objUserProfile.setDeletedAt(c.getString(c.getColumnIndexOrThrow("DELETED_AT")));
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null && !c.isClosed())
                c.close();
        }
        return objUserProfile;
    }

    public void fillAppMetaData(ResponseGetAppMetaData objResponseGetAppMetaData) {
        insertPropertyTypes(objResponseGetAppMetaData.getData().getPropertyTypes());
        insertRoomTypes(objResponseGetAppMetaData.getData().getRoomTypes());
        insertAmenities(objResponseGetAppMetaData.getData().getAmenities());
        insertCityList(objResponseGetAppMetaData.getData().getCityMasterItemList());
        insertLuxuryTypes(objResponseGetAppMetaData.getData().getPropertyTypes());
    }

    public void insertPropertyTypes(List<PropertyTypesItem> propertyTypesItemList) {
        SQLiteDatabase sqldb = null;
        try {
            genricDelete("property_types");
            sqldb = this.getWritableDatabase();

            for (int i = 0; i < propertyTypesItemList.size(); i++) {
                PropertyTypesItem objPropertyTypesItem = propertyTypesItemList.get(i);
                ContentValues cv = new ContentValues();

                cv.put("id", objPropertyTypesItem.getId());
                cv.put("name", objPropertyTypesItem.getName());
                cv.put("is_active", objPropertyTypesItem.getIsActive());
                cv.put("created_at", objPropertyTypesItem.getCreatedAt());
                cv.put("updated_at", objPropertyTypesItem.getUpdatedAt());
                cv.put("deleted_at", objPropertyTypesItem.getDeletedAt());

                sqldb.insert(property_types, null, cv);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public void insertRoomTypes(List<RoomTypesItem> roomTypes) {
        SQLiteDatabase sqldb = null;
        try {
            genricDelete("room_types");
            sqldb = this.getWritableDatabase();

            for (int i = 0; i < roomTypes.size(); i++) {
                RoomTypesItem objRoomTypesItem = roomTypes.get(i);
                ContentValues cv = new ContentValues();

                cv.put("id", objRoomTypesItem.getId());
                cv.put("name", objRoomTypesItem.getName());
                cv.put("is_active", objRoomTypesItem.getIsActive());
                cv.put("created_at", objRoomTypesItem.getCreatedAt());
                cv.put("updated_at", objRoomTypesItem.getUpdatedAt());
                cv.put("deleted_at", objRoomTypesItem.getDeletedAt());

                sqldb.insert(room_types, null, cv);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public void insertAmenities(List<AmenitiesItem> amenitiesItemList) {
        SQLiteDatabase sqldb = null;
        try {
            genricDelete("amenities");
            sqldb = this.getWritableDatabase();

            for (int i = 0; i < amenitiesItemList.size(); i++) {
                AmenitiesItem objAmenitiesItem = amenitiesItemList.get(i);
                ContentValues cv = new ContentValues();

                cv.put("id", objAmenitiesItem.getId());
                cv.put("name", objAmenitiesItem.getName());
                cv.put("icon", objAmenitiesItem.getIcon());
                cv.put("description", objAmenitiesItem.getDescription());
                cv.put("is_active", objAmenitiesItem.getIsActive());
                cv.put("created_at", objAmenitiesItem.getCreatedAt());
                cv.put("updated_at", objAmenitiesItem.getUpdatedAt());
                cv.put("deleted_at", objAmenitiesItem.getDeletedAt());

                sqldb.insert(amenities, null, cv);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public void insertCityList(List<CityMasterItem> cityMasterItemList) {
        SQLiteDatabase sqldb = null;
        try {
            genricDelete("cityMaster");
            sqldb = this.getWritableDatabase();

            for (int i = 0; i < cityMasterItemList.size(); i++) {
                CityMasterItem objCityMasterItem = cityMasterItemList.get(i);
                ContentValues cv = new ContentValues();

                cv.put("id", objCityMasterItem.getId());
                cv.put("service_city_name", objCityMasterItem.getCityName());
                cv.put("service_city_code", objCityMasterItem.getCityCode());
                cv.put("is_active", objCityMasterItem.getIsActive());
                cv.put("created_at", objCityMasterItem.getCreatedAt());
                cv.put("updated_at", objCityMasterItem.getUpdatedAt());
                cv.put("deleted_at", objCityMasterItem.getDeletedAt());

                sqldb.insert(cityMaster, null, cv);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public void insertLuxuryTypes(List<PropertyTypesItem> propertyTypesItemList) {
        SQLiteDatabase sqldb = null;
        try {
            genricDelete("luxury_type");
            sqldb = this.getWritableDatabase();

//            for (int i = 0; i < propertyTypesItemList.size(); i++) {
//                PropertyTypesItem objPropertyTypesItem = propertyTypesItemList.get(i);
//                ContentValues cv = new ContentValues();
//
//                cv.put("id", objPropertyTypesItem.getId());
//                cv.put("name", objPropertyTypesItem.getName());
//                cv.put("is_active", objPropertyTypesItem.getIsActive());
//                cv.put("created_at", objPropertyTypesItem.getCreatedAt());
//                cv.put("updated_at", objPropertyTypesItem.getUpdatedAt());
//                cv.put("deleted_at", objPropertyTypesItem.getDeletedAt());
//
//                sqldb.insert(luxury_type, null, cv);
//            }

            ContentValues cv = new ContentValues();
            cv.put("id", Defaults.FULLY_FURNISHED);
            cv.put("name", context.getResources().getString(R.string.furnished));
            cv.put("is_active", "1");
            cv.put("created_at", "");
            cv.put("updated_at", "");
            cv.put("deleted_at", "");
            sqldb.insert(luxury_type, null, cv);

            cv = new ContentValues();
            cv.put("id", Defaults.NON_FURNISHED);
            cv.put("name", context.getResources().getString(R.string.non_furnished));
            cv.put("is_active", "1");
            cv.put("created_at", "");
            cv.put("updated_at", "");
            cv.put("deleted_at", "");
            sqldb.insert(luxury_type, null, cv);

//            cv = new ContentValues();
//            cv.put("id", Defaults.SEMI_FURNISHED);
//            cv.put("name", context.getResources().getString(R.string.semi_furnished));
//            cv.put("is_active", "1");
//            cv.put("created_at", "");
//            cv.put("updated_at", "");
//            cv.put("deleted_at", "");
//            sqldb.insert(luxury_type, null, cv);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public ArrayList<SpinnerItemBO> getSpinnerItems(Context context, String firstItem, String tableName) {
        Cursor c = null;
        ArrayList<SpinnerItemBO> spinnerItemList = new ArrayList<>();

        try {
            if (!firstItem.isEmpty()) {
                SpinnerItemBO objSpinnerItem = new SpinnerItemBO();
                objSpinnerItem.setId("-1");
                objSpinnerItem.setTitleText(firstItem);
                spinnerItemList.add(objSpinnerItem);
            }

//            String query = "SELECT * FROM " + Prefs.getUserLanguageFromSharedPreferences() + " WHERE CHAPTER_NUMBER = '" + chapterNumber + "' ORDER BY CAST(verse_number AS INTEGER)";
            String query = "SELECT * FROM " + tableName + " WHERE is_active = '1'";
            SQLiteDatabase db = this.getWritableDatabase();
            c = db.rawQuery(query, new String[]{});

            if (c != null)
                while (c.moveToNext()) {
                    SpinnerItemBO objSpinnerItem = new SpinnerItemBO();

                    objSpinnerItem.setId(c.getString(c.getColumnIndexOrThrow("id")));
                    if (tableName.equalsIgnoreCase(cityMaster))
                        objSpinnerItem.setTitleText(c.getString(c.getColumnIndexOrThrow("service_city_name")));
                    else
                        objSpinnerItem.setTitleText(c.getString(c.getColumnIndexOrThrow("name")));

//                    objSpinnerItem.setImageResource(c.getInt(c.getColumnIndexOrThrow("")));

                    spinnerItemList.add(objSpinnerItem);
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null && !c.isClosed())
                c.close();
        }
        return spinnerItemList;
    }

    public ArrayList<AmenitiesItem> getAmenitiesListFromDB(Context context) {
        Cursor c = null;
        ArrayList<AmenitiesItem> amenitiesItemList = new ArrayList<>();

        try {
            String query = "SELECT * FROM " + amenities + " WHERE is_active = '1'";
            SQLiteDatabase db = this.getWritableDatabase();
            c = db.rawQuery(query, new String[]{});

            if (c != null)
                while (c.moveToNext()) {
                    AmenitiesItem objSpinnerItem = new AmenitiesItem();

                    objSpinnerItem.setId(c.getString(c.getColumnIndexOrThrow("id")));
                    objSpinnerItem.setName(c.getString(c.getColumnIndexOrThrow("name")));
                    objSpinnerItem.setIcon(c.getString(c.getColumnIndexOrThrow("icon")));
                    objSpinnerItem.setDescription(c.getString(c.getColumnIndexOrThrow("description")));
                    objSpinnerItem.setIsActive(c.getString(c.getColumnIndexOrThrow("is_active")));
                    objSpinnerItem.setCreatedAt(c.getString(c.getColumnIndexOrThrow("created_at")));
                    objSpinnerItem.setUpdatedAt(c.getString(c.getColumnIndexOrThrow("updated_at")));
                    objSpinnerItem.setDeletedAt(c.getString(c.getColumnIndexOrThrow("deleted_at")));

                    amenitiesItemList.add(objSpinnerItem);
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null && !c.isClosed())
                c.close();
        }
        return amenitiesItemList;
    }

    public ArrayList<AmenitiesItem> getServiceCityListFromDB(Context context) {
        Cursor c = null;
        ArrayList<AmenitiesItem> amenitiesItemList = new ArrayList<>();

        try {
            String query = "SELECT * FROM " + cityMaster + " WHERE is_active = '1'";
            SQLiteDatabase db = this.getWritableDatabase();
            c = db.rawQuery(query, new String[]{});

            if (c != null)
                while (c.moveToNext()) {
                    AmenitiesItem objSpinnerItem = new AmenitiesItem();

                    objSpinnerItem.setId(c.getString(c.getColumnIndexOrThrow("service_city_code")));
                    objSpinnerItem.setName(c.getString(c.getColumnIndexOrThrow("service_city_name")));
                    objSpinnerItem.setIcon(Defaults.REQUEST_TYPE_SERVICE_CITY_CODE);
                    objSpinnerItem.setDescription("");
                    objSpinnerItem.setIsActive(c.getString(c.getColumnIndexOrThrow("is_active")));
                    objSpinnerItem.setCreatedAt(c.getString(c.getColumnIndexOrThrow("created_at")));
                    objSpinnerItem.setUpdatedAt(c.getString(c.getColumnIndexOrThrow("updated_at")));
                    objSpinnerItem.setDeletedAt(c.getString(c.getColumnIndexOrThrow("deleted_at")));

                    amenitiesItemList.add(objSpinnerItem);
                }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null && !c.isClosed())
                c.close();
        }
        return amenitiesItemList;
    }

    class DatabaseCopyBackground extends AsyncTask<Void, Void, Boolean> {
        Context context;

        public DatabaseCopyBackground(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                copyDataBase(context);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            try {
                if (aBoolean) {
//                    createChapterProgressTable();
                }
                Bundle mBundle = new Bundle();
                Utility.sendUpdateListBroadCast(Defaults.ACTION_REFRESH_GROUP_LIST, context, mBundle);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
