package com.elitesmartsolution.paypgroom;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDexApplication;

import com.elitesmartsolution.paypgroom.payumoney.AppEnvironment;
import com.elitesmartsolution.paypgroom.util.ApplicationUtils;
import com.elitesmartsolution.paypgroom.util.preferences.Keys;
import com.elitesmartsolution.paypgroom.util.preferences.Prefs;
import com.google.android.libraries.places.api.Places;
import com.orhanobut.hawk.Hawk;

/**
 * Created by Ravi on 28/04/16.
 */
public class App extends MultiDexApplication {

    private static App mInstance;
    AppEnvironment appEnvironment;

    public static App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        ApplicationUtils.init(this);
        initialiseStorage();
        initGooglePlaces();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        try {
            if (BuildConfig.DEBUG) {
                Prefs.setBooleanValue(Keys.PREF_IS_PRODUCTION_ENVIRONMENT, false);
            } else {
                Prefs.setBooleanValue(Keys.PREF_IS_PRODUCTION_ENVIRONMENT, true);
            }

            if (Prefs.getBooleanValue(Keys.PREF_IS_PRODUCTION_ENVIRONMENT))
                appEnvironment = AppEnvironment.PRODUCTION;
            else
                appEnvironment = AppEnvironment.SANDBOX;

////            TEMPORARY
//            Prefs.setBooleanValue(Keys.PREF_IS_PRODUCTION_ENVIRONMENT, true);
//            appEnvironment = AppEnvironment.PRODUCTION;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initialiseStorage() {
        try {
            Prefs.init(this);
            Hawk.init(this).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initGooglePlaces() {
        try {
            String apiKey = getString(R.string.paypg_google_places_api_key);
            /**
             * Initialize Places. For simplicity, the API key is hard-coded. In a production
             * environment we recommend using a secure mechanism to manage API keys.
             */
            if (!Places.isInitialized()) {
                Places.initialize(getApplicationContext(), apiKey);
            }
//            // Create a new Places client instance.
//            PlacesClient placesClient = Places.createClient(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AppEnvironment getAppEnvironment() {
        return appEnvironment;
    }

    public void setAppEnvironment(AppEnvironment appEnvironment) {
        this.appEnvironment = appEnvironment;
    }
}